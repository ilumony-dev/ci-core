<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        
    </head>

    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Buying of Portfolios</a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row" id="portfolios-content-box">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Buying of Portfolios <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                    </div>
                                    <div class="element-box">
                                        <div class="row">
                                            <c:forEach items="${portfolios}" var="portfolio">
                                                <div class="col-sm-4 margin-t">
                                                    <div class="portfolio-box" style=" background:url(${home}/resources/img/${portfolio.master}) center top no-repeat;background-size: cover">
                                                        <div class="portfolio-name">${portfolio.name}</div>
                                                        <div class="row-actions">
                                                            <%--<a data-scroll="" href="#buying-portfolio-modal-form" class="portfolio-name-action" data-json-object='${portfolio}'><i class="fa fa-plus-circle"  style="margin-right: 10px; color:#fff"></i></a>--%>
                                                            <a href="./mgmt-buying-portfolio-add?id=${portfolio.fundId}"><i class="fa fa-plus-circle"  style="margin-right: 10px; color:#fff"></i></a>
                                                            <a href="./mgmt-show-buying-portfolio?id=${portfolio.fundId}"><i class="fa fa-file"  style="color:#fff; margin-right: 10px"></i></a>
                                                            <a href="./mgmt-show-buying-portfolio-pending?id=${portfolio.fundId}"><i class="fa fa-clock-o"  style="color:#fff"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                        <%--<jsp:include page = "modals/buying-portfolio-modal.jsp" ></jsp:include>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>   
        <jsp:include page = "modals/user-account-summary.jsp" ></jsp:include>
        <jsp:include page = "modals/user-units-summary.jsp" ></jsp:include>
        <jsp:include page = "modals/user-daily-updates.jsp" ></jsp:include>

            <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
            $(document).ready(function () {
                $('.buying-portfolio-modal-submit').click(function () {
                    $('.buying-portfolio-modal-submit').attr('disabled', true);
                    $("#buying-portfolio-modal-form").submit();
                });
                $('.portfolio-name-action').click(function () {
                    var jsonStr = $(this).data('json-object');
//                    $('#portfolios-form').show();
//                    $('#portfolios-content-box').hide();
                    $('#buying-portfolio-modal-portfolio-name').text(jsonStr.name);
                    $('#buying-portfolio-modal-fundId').val(jsonStr.fundId);
                    var uniqueId = document.getElementById("buying-portfolio-modal-uniqueId");
                    var coinGroup = document.getElementById("buying-portfolio-modal-coin-group");
                    while (coinGroup.firstChild) {
                        coinGroup.removeChild(coinGroup.firstChild);
                    }
                    var coins = jsonStr.shares.split(",");
                    for (var i = 0; i < coins.length; i++) {
                        var coin = coins[i];
                        var child = uniqueId.cloneNode(true); // true means clone all childNodes and all event handlers
                        child.id = coin;
                        child.style.display = "block";
//                        var inp = child.getElementsByTagName("input")[0];
//                        inp.removeAttr("disabled");
                        var sel = child.getElementsByTagName("select")[0];
//                        sel.removeAttr("disabled");
                        sel.value = coin;
                        coinGroup.appendChild(child);
                    }
                });
            });
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
    </body>
</html>
