<%-- 
    Document   : sell-funds-to-portfolio
    Created on : Apr 04, 2018, 9:49:26 AM
    Author     : Anurag
--%>


<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="sell-funds-to-portfolio" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sell-funds-to-portfolio-title">
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <p id="sell-funds-to-portfolio-para" style="padding:20px; font-weight:500;"></p>
            <form action="./add-investment-request" method="POST" id="sell-funds-to-portfolio-form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="walletBalance" id="sell-funds-to-portfolio-walletBalance" value=""/>
                                <input type="hidden" name="actionType" id="sell-funds-to-portfolio-actionType" value=""/>
                                <input type="hidden" name="investmentId" id="sell-funds-to-portfolio-investmentId" value=""/>
                                <input type="hidden" name="fundName" id="sell-funds-to-portfolio-fundName" value=""/>
                                <label>Available balance in portfolio: US$ <span class="sell-funds-to-portfolio-walletBalance-text"></span>.
                                    <!--<span>-: Last Days Close. If the user has submitted sell request(s) then subtract that from this value.</span>-->
                                <!--<span id="sell-funds-to-portfolio-selling-percentage"></span>--> 
                                </label>
                                <label><a href="./txnasdfjsdfjldss">Current Sell request for this portfolio</a>.<span id="sell-funds-to-portfolio-count-pending-sell-requests"></span></label>
                                <label for="percentage" id="sell-funds-to-portfolio-lbl-percentage">Percentage amount of this investment you wish to sell:</label>
                                <input class="form-control" type="number" name="percentage" id="sell-funds-to-portfolio-percentage" step="0.01" placeholder="Enter percentage" max="100.00"/>
                                <div class="percentage-icon">
                                    <span><i class="fa fa-percent"></i></span>
                                </div>
<!--                                <ol>
                                    <li>Admin can override the sell request(s) and add/subtract amount.</li>
                                    <li>Also add notes</li>
                                    <li>Add 3 status for sell redemption. Received (user can cancel), Acknowledged (user cannot cancel but admin can), In processing(no one can cancel, in completion(no one can cancel</li>
                                    <li>Each of above status change will auto emails.</li>
                                </ol>-->
                                <label for="amount" id="sell-funds-to-portfolio-lbl-amount" style="display:block; margin:0">Approximate sale value</label>
                                <small id="sell-funds-to-portfolio-lbl-note"></small>
                                <input class="form-control" type="number" name="amount" id="sell-funds-to-portfolio-amount" step="0.01" placeholder="Enter amount in US$ " tabindex="-1"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description" id="sell-funds-to-portfolio-lbl-description">Optional: Reference or reason for sale</label>
                                <textarea class="form-control" type="text" name="description" id="sell-funds-to-portfolio-description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn btn-primary" id="sell-funds-to-portfolio-submit" type="button">Submit</button>
                    </div>
                    <p id="sell-funds-to-portfolio-para2" ><i>On completion of sale all money will go to your Invsta Cash Account. From the cash account you can choose to re-invest into another portfolio, withdraw to your bank account, or continue to hold as cash in your Invsta account. </i></p>
                </div>
            </form>
        </div>
    </div>
</div>