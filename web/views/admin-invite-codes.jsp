<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Invite Codes</span>
                        </li>
                        
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display:block">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invite Codes <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-box-tp">
                                            <div class="controls-above-table">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-sm btn-secondary" href="#" data-target="#addinvitecode" data-toggle="modal">Create Invite Code</a>
                                                    </div>
                                                    <div class="col-sm-6">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="table-responsive">
                                                <table id="invite-codes-table" class="table table-bordered table-lg table-v2 table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Invite Code
                                                            </th>
                                                            <th>
                                                                Purpose/ Expiry Date
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                            <th>
                                                                Allocate Email
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${inviteCodes}" var="code">
                                                            <tr>
                                                                <td class="text-center">
                                                                    ${code.inviteCode}
                                                                </td>
                                                                <td class="text-center">
                                                                    ${code.purpose}/ ${code.expiryTs}
                                                                </td>
                                                                <td class="text-center">
                                                                    ${code.emailId}
                                                                </td>
                                                                <td class="text-center">
                                                                    <form action="./admin-allocate-email" method="post">
                                                                        <input type="hidden" name="id" value="${code.id}"/>
                                                                        <input type="hidden" name="inviteCode" value="${code.inviteCode}"/>
                                                                        <div class="allocte-email">
                                                                            <input class="form-control" placeholder="Enter email" type="text" name="emailId" id="emailId"/>
                                                                            <button class="btn btn-success btn-sm" type="submit">Apply & Send Email</button>
                                                                        </div>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <jsp:include page = "modals/addinvitecode.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/update-investment-shares.jsp" />

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/fire-events.js"></script>
        <script>
        var shareOptions = '';
            <c:forEach items="${shares}" var="share" varStatus="loop">
        shareOptions = shareOptions + '<option value="${share.shareId}">${share.name}</option>';
            </c:forEach>
        $(document).ready(function () {
            var table1 = $('#invite-codes-table').DataTable({
                scrollY: "550px",
//                scrollX: true,
                scrollCollapse: true,
                paging: false,
                fixedColumns: true
            });
            $(table1.table().container()).on('keyup', 'tfoot input', function () {
                table1.column($(this).data('index')).search(this.value).draw();
            });
        });

        function newInviteCode() {
            var url = './rest/cryptolabs/api/randomstring';
            $.ajax({
                url: url,
                type: 'GET',
                async: true,
                dataType: "json",
                success: function (data) {
                    $("#txnTitle").text(data);
                }
            });
        }
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
    </body>
</html>
