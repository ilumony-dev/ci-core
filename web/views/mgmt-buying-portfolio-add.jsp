<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Buying of Portfolio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${portfolio.name}</a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row" id="portfolios-content-box">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Buying of Portfolio <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                    </div>
                                    <div class="element-box">
                                        <div class="row">
                                            <form method="post" action="./mgmt-add-portfolio-buying" id="buying-portfolio-modal-form">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="buying-portfolio-modal-portfolio-name">${portfolio.name}</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="element-box">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">
                                                                Master <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                            </h6>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="created_date">Date</label>
                                                                    <input type="date" class="form-control" id="buying-portfolio-modal-date" name="created_date" value="${buyingPortfolio.created_date}" required="required"/>
                                                                    <input type="hidden" id="buying-portfolio-modal-fundId" name="fund_id"  value="${portfolio.fundId}"/>
                                                                    <input type="hidden" id="buying-portfolio-modal-txn_Req_id" name="txn_Req_id"  value="${buyingPortfolio.txn_Req_id}"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label for="email">Username</label>
                                                                    <select name="user_id" class="form-control" id="buying-portfolio-modal-email">
                                                                        <option value="0">-Select-</option>
                                                                        <c:forEach items="${totalUsers}" var="user">
                                                                            <option value="${user.userId}" 
                                                                                    <c:if test="${user.userId eq buyingPortfolio.user_id}">
                                                                                        selected = "selected"
                                                                                    </c:if>>${user.email}
                                                                            </option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="usd">USD</label>
                                                                    <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-usd" name="usd" value="${buyingPortfolio.usd}" required="required"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="nzd">NZD</label>
                                                                    <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-nzd" name="local"  value="${buyingPortfolio.local}" required="required"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group">
                                                                    <label for="minto">Units</label>
                                                                    <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-minto" name="minto" value="${buyingPortfolio.minto}" required="required"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="buying-portfolio-modal-coin-group">
                                                        <c:forEach items="${managementSharesByPortfolio}" var="share">
                                                            <div class="col-sm-3" id="${share.coin_id}">
                                                                <div class="form-group margin-t">
                                                                    <select name="coin_id" class="form-control">
                                                                        <option value="0">-Select-</option>
                                                                        <c:forEach items="${coins}" var="coin"> 
                                                                            <option value="${coin.uniqueId}" 
                                                                                    <c:if test="${share.coin_id eq coin.uniqueId}">selected="selected"</c:if>>
                                                                                ${coin.name}
                                                                            </option>
                                                                        </c:forEach>
                                                                    </select>
                                                                    <input type="number" placeholder="Please enter amount" step="0.01" class="form-control" name="amount" required="required" value="${share.amount}" style="width:40%; float: left"/>
                                                                    <input type="number" placeholder="Please enter quantity" step="0.00000001" class="form-control" name="quantity" required="required"  value="${share.amount}" style="width:60%; float: right"/>
                                                                </div>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label for="particulars" style="display:block">Notes</label>
                                                                <textarea class="form-control" name="particulars" id="buying-portfolio-modal-particulars" style="width:100%; overflow: hidden">${buyingPortfolio.particulars}</textarea>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <!--<button type="button" class="btn btn-primary" id="hidemodal" data-target="#nextmodal" data-toggle="modal">Show</button>-->
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="button" class="btn btn-primary buying-portfolio-modal-submit">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>   
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
            $(document).ready(function () {
                $('.buying-portfolio-modal-submit').click(function () {
                    $('.buying-portfolio-modal-submit').attr('disabled', true);
                    $("#buying-portfolio-modal-form").submit();
                });
                $('.portfolio-name-action').click(function () {
                    var jsonStr = $(this).data('json-object');
                    $('#buying-portfolio-modal-portfolio-name').text(jsonStr.name);
                    $('#buying-portfolio-modal-fundId').val(jsonStr.fundId);
                    var uniqueId = document.getElementById("buying-portfolio-modal-uniqueId");
                    var coinGroup = document.getElementById("buying-portfolio-modal-coin-group");
                    while (coinGroup.firstChild) {
                        coinGroup.removeChild(coinGroup.firstChild);
                    }
                    var coins = jsonStr.shares.split(",");
                    for (var i = 0; i < coins.length; i++) {
                        var coin = coins[i];
                        var child = uniqueId.cloneNode(true); // true means clone all childNodes and all event handlers
                        child.id = coin;
                        child.style.display = "block";
                        var sel = child.getElementsByTagName("select")[0];
                        sel.value = coin;
                        coinGroup.appendChild(child);
                    }
                });
            });
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
    </body>
</html>
