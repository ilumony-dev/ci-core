var obj20 = new Object();
obj20.lineChart = [{
        color: '#ffcc00',
        name: 'BTC',
        data: [100.0, 99.1, 122.5, 108.2, 142.4, 241.2, 243.9, 272.3, 490.0, 441.1, 677.9, 1099.4, 1368.0, 918.6, 1096.9]
    }, {
        color: '#bb21e3',
        name: 'S&P 500',
        data: [100.0, 101.8, 107.0, 105.5, 106.7, 108.5, 108.2, 110.6, 110.6, 112.5, 115.2, 118.0, 119.4, 126.0, 119.6]
    }, {
        color: '#011c53',
        name: 'ASX',
        data: [100.0, 101.0, 104.2, 104.4, 106.5, 106.8, 110.9, 110.1, 113.3, 112.6, 114.3, 121.4, 116.0, 118.6, 123.0]
    }, {
        color: '#69D2DD',
        name: 'NZX',
        data: [100.0, 105.7, 107.6, 106.9, 104.9, 107.9, 110.8, 115.8, 119.3, 120.3, 121.3, 114.2, 114.2, 114.2, 109.1]
    }];
obj20.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['BTC', 95],
            ['USD', 5]
        ]
    }];
obj20.pdflink = './resources/pdf/bitcoin_factsheet.pdf';
obj20.invested = '$10,000';
obj20.performance = '$144,539';
obj20.gain = '545.4%';
obj20.months3 = '-2.81%';
obj20.months6 = '-9.91%';
obj20.months12 = '-3.29%';
obj20.feeRate = '1.5%';
map['20'] = obj20;
var obj21 = new Object();
obj21.lineChart = [{
        color: '#ffcc00',
        name: 'XRP',
        data: [100.0, 102.7, 84.9, 343.8, 865.5, 5239.6, 4000.4, 2780.4, 3902.0, 3243.3, 3048.4, 4015.4, 37531.4, 15115.1, 14578.3]
    }, {
        color: '#bb21e3',
        name: 'S&P 500',
        data: [100.0, 101.8, 107.0, 105.5, 106.7, 108.5, 108.2, 110.6, 110.6, 112.5, 115.2, 118.0, 119.4, 126.0, 119.6]
    }, {
        color: '#011c53',
        name: 'ASX',
        data: [100.0, 101.0, 104.2, 104.4, 106.5, 106.8, 110.9, 110.1, 113.3, 112.6, 114.3, 121.4, 116.0, 118.6, 123.0]
    }, {
        color: '#69D2DD',
        name: 'NZX',
        data: [100.0, 105.7, 107.6, 106.9, 104.9, 107.9, 110.8, 115.8, 119.3, 120.3, 121.3, 114.2, 114.2, 114.2, 109.1]
    }];
obj21.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['XRP', 95],
            ['USD', 5]
        ]
    }];
obj21.pdflink = './resources/pdf/ripple_factsheet.pdf';
obj21.invested = '$10,000';
obj21.performance = '$144,539';
obj21.gain = '545.4%';
obj21.months3 = '3.36%';
obj21.months6 = '-22.36%';
obj21.months12 = '-39.30%';
obj21.feeRate = '1.5%';
map['21'] = obj21;
var obj22 = new Object();
obj22.lineChart = [{
        color: '#ffcc00',
        name: 'ETH',
        data: [100.0, 131.6, 215.6, 621.6, 952.5, 2711.3, 3206.4, 2775.2, 4808.6, 3730.0, 3555.5, 5670.5, 9289.9, 12606.8, 10686.4]
    }, {
        color: '#bb21e3',
        name: 'S&P 500',
        data: [100.0, 101.8, 107.0, 105.5, 106.7, 108.5, 108.2, 110.6, 110.6, 112.5, 115.2, 118.0, 119.4, 126.0, 119.6]
    }, {
        color: '#011c53',
        name: 'ASX',
        data: [100.0, 101.0, 104.2, 104.4, 106.5, 106.8, 110.9, 110.1, 113.3, 112.6, 114.3, 121.4, 116.0, 118.6, 123.0]
    }, {
        color: '#69D2DD',
        name: 'NZX',
        data: [100.0, 105.7, 107.6, 106.9, 104.9, 107.9, 110.8, 115.8, 119.3, 120.3, 121.3, 114.2, 114.2, 114.2, 109.1]
    }];
obj22.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['ETH', 95],
            ['USD', 5]
        ]
    }];
obj22.pdflink = './resources/pdf/ethereum_factsheet.pdf';
obj22.invested = '$10,000';
obj22.performance = '$144,539';
obj22.gain = '545.4%';
obj22.months3 = '4.13%';
obj22.months6 = '-33.52%';
obj22.months12 = '-43.35%';
obj22.feeRate = '1.5%';
map['22'] = obj22;
var obj23 = new Object();
obj23.lineChart = null;
obj23.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['BTC', 95],
            ['USD', 5]]
    }];
obj23.pdflink = null;
obj23.invested = '$10,000';
obj23.performance = '$144,539';
obj23.gain = '545.4%';
obj23.months3 = '5.23%';
obj23.months6 = '0.09%';
obj23.months12 = '-2.43%';
obj23.feeRate = '2.5%';
map['23'] = obj23;
var obj24 = new Object();
obj24.lineChart = null;
obj24.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['BTC', 95],
            ['USD', 5]]
    }];
obj24.pdflink = null;
obj24.invested = '$10,000';
obj24.performance = '$144,539';
obj24.gain = '545.4%';
obj24.months3 = '7.49%';
obj24.months6 = '1.50% ';
obj24.months12 = '0.11%';
obj24.feeRate = '2.5%';
map['24'] = obj24;
var obj25 = new Object();
obj25.lineChart = null;
obj25.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['BTC', 95],
            ['USD', 5]]
    }];
obj25.pdflink = null;
obj25.invested = '$10,000';
obj25.performance = '$144,539';
obj25.gain = '545.4%';
obj25.months3 = '6.63%';
obj25.months6 = '1.20%';
obj25.months12 = '7.35%';
obj25.feeRate = '3.0%';
map['25'] = obj25;
var obj26 = new Object();
obj26.lineChart = null;
obj26.pieChart = [{
        type: 'pie',
        innerSize: '80%',
        data: [
            ['BTC', 95],
            ['USD', 5]]
    }];
obj26.pdflink = null;
obj26.invested = '$10,000';
obj26.performance = '$144,539';
obj26.gain = '545.4%';
obj26.months3 = '5.06%';
obj26.months6 = '-2.99%';
obj26.months12 = '-0.66%';
obj26.feeRate = '3.0%';
map['26'] = obj26;
