/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.repositories.CommonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class SessionManagementServiceImpl implements SessionManagementService {

    @Autowired
    private SessionRegistry sessionRegistry;
    @Autowired
    private CommonRepository repository;

    @Override
    public SecuredUser getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof SecuredUser) {
                SecuredUser user = ((SecuredUser) principal);
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    @Override
    public SecuredUser getCurrentUser(Object... args) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof SecuredUser) {
                SecuredUser user = ((SecuredUser) principal);
                if (args != null && args.length > 0) {
                    Object obj = args[0];
                    if (obj instanceof Boolean) {
                        List<Verification> verificationList = repository.findVerification(user.getUserId(), null);
                        if (verificationList != null && !verificationList.isEmpty()) {
                            user.setVerifyCount(new Long(String.valueOf(verificationList.size())));
                        } else {
                            user.setVerifyCount(new Long("0"));
                        }
                    }
                }
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public SecuredUser getCurrentUser(GrantedAuthority role) {
        SecuredUser securedUser = getCurrentUser();
        if (securedUser != null && securedUser.getAuthorities().contains(role)) {
            return securedUser;
        }
        return null;
    }

    @Override
    public void logout(CommonRepository repository) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Object principal = auth.getPrincipal();
            if (principal instanceof SecuredUser) {
                SecuredUser user = ((SecuredUser) principal);
                repository.save(user.getUserId(), "LOGGED OUT");
            }
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    @Override
    public List<SecuredUser> getAllOnlineUsers() {
        if (sessionRegistry == null) {
            return null;
        }
        return (List) sessionRegistry.getAllPrincipals();
    }

    @Override
    public List<SessionInformation> getAllSessions(SecuredUser user, boolean bln) {
        if (sessionRegistry == null) {
            return null;
        }
        return (List) sessionRegistry.getAllSessions(user, bln);
    }

    @Override
    public SessionInformation getSessionInformation(String username) {
        if (sessionRegistry == null) {
            return null;
        }
        return sessionRegistry.getSessionInformation(username);
    }

    @Override
    public void refreshLastRequest(String username) {
        sessionRegistry.refreshLastRequest(username);
    }

    @Override
    public void registerNewSession(String username, SecuredUser user) {
        sessionRegistry.registerNewSession(username, user);
    }

    @Override
    public void removeSessionInformation(String username) {
        sessionRegistry.removeSessionInformation(username);
    }

}
