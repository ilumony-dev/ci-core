/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.components.HTTPRequestManager;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.ObjectCastService;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author palo12
 */
@Service
public class POLiManager {

    /**
     * Request for Initiate Transaction Amount, CurrencyCode, MerchantReference,
     * MerchantReferenceFormat, MerchantData, MerchantHomepageURL, SuccessURL,
     * FailureURL, CancellationURL, NotificationURL, SelectedFICode, CompanyCode
     *
     * @param amount
     * @param curreny
     * @param refrenceNo
     * @return
     * @throws java.io.IOException
     * @throws org.json.JSONException
     */
    private HashMap<String, String> headers;

    public POLiManager() {
        String code = "SS64006726:eY9!lGbp8ZM5V";
        byte[] encodedBytes = Base64.encodeBase64(code.getBytes());
        String auth = new String(encodedBytes);
        headers = new HashMap<>();
        headers.put("Authorization", "Basic " + auth);
    }

    public POLiIntiatedTransaction initiateTransaction(String amount, ChargeRequest.Currency curreny, String refrenceNo, String fundId) throws IOException, JSONException {
        String json = "{"
                + "\"Amount\":\"" + amount + "\","
                + "\"CurrencyCode\":\"" + curreny + "\","
                + "\"MerchantReference\":\"" + refrenceNo + "\","
                //                + "\"MerchantData\":\"" + fundId + "\","
                + "\"MerchantHomepageURL\":\"" + domain + "\","
                + "\"SuccessURL\":\"" + domain + "/user-poli-charge-success\","
                + "\"FailureURL\":\"" + domain + "/user-poli-charge-failure\","
                + "\"CancellationURL\":\"" + domain + "/user-poli-charge-cancelled\","
                + "\"NotificationURL\":\"" + domain + "/user-poli-charge-nudge\""
                + "}";
        String url = "https://poliapi.apac.paywithpoli.com/api/v2/Transaction/Initiate";
        String response = requestManager.sendHTTPPostRequest(url, json, headers);
        JSONObject object = objectCastService.parseJson(response);
        POLiIntiatedTransaction transaction = new POLiIntiatedTransaction(object);
        return transaction;
    }

    public POLiTransaction getTransaction(String token) throws IOException, JSONException {
        String url = "https://poliapi.apac.paywithpoli.com/api/v2/Transaction/GetTransaction";
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        String response = requestManager.sendHTTPGetRequest(url, params, headers);
        JSONObject object = objectCastService.parseJson(response);
        POLiTransaction transaction = new POLiTransaction(object);
        return transaction;
    }

    @Autowired
    private HTTPRequestManager requestManager;
    @Autowired
    private ObjectCastService objectCastService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private String domain;

}
