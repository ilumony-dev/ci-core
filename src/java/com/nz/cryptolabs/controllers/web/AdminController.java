/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.SystemConfig;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.bittrex.modal.accountapi.Balance;
import com.nz.cryptolabs.services.BittrexAccountAPIService;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalancesContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.PastOrder;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.FileUtility;
import com.nz.cryptolabs.services.SessionManagementService;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import static com.nz.cryptolabs.constants.Constants.adminModelMapEntry;
import static com.nz.cryptolabs.constants.ResponseConstants.urlMap;
import com.nz.cryptolabs.mail.AdminMailManager;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.DateTimeService;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Controller
public class AdminController implements Constants {

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private FileUtility fileUtility;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private AdminMailManager adminMailManager;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private BittrexAccountAPIService bittrexAccountAPIService;
    @Autowired
    private String projectName;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AdminController.class);

    @RequestMapping(value = {"/admin-dashboard", "/home"}, method = RequestMethod.GET)
    public String adminDashboard(ModelMap model) throws IOException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Admin Dashboard " + projectName);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("portfolios", repository.funds());
            model.addAttribute("totalUnits", databaseService.totalUnitsById(null, null, null));
            model.addAttribute("newWithdrawlRequests", repository.userTransactionRequests(null, "PENDING", null, null));
            model.addAttribute("pendingWalletTxns", repository.pendingWalletTxns(null, null));
            model.addAttribute("pendingInvestments", databaseService.pendingInvestments(null));
            model.addAttribute("pendingTransactions", databaseService.pendingSellTransactions(null));
            List<Map<String, Object>> totals = repository.totals();
            for (Map<String, Object> map : totals) {
                String tbl = (String) map.get("tbl");
                if (tbl.equals("user_master")) {
                    model.addAttribute("totalCustomers", map.get("total"));
                } else if (tbl.equals("investment")) {
                    model.addAttribute("totalInvestments", map.get("total"));
                } else if (tbl.equals("user_shares_transaction")) {
                    model.addAttribute("totalSharesValue", map.get("total"));
                }
            }
            List<ShareFund> dailyUpdates = databaseService.dailyUpdates(null, null, null, false);
            model.addAttribute("dailyUpdates", dailyUpdates);
            model.addAttribute("common", common);
            model.addAttribute("systemConfig", Constants.systemConfig);
            StringBuffer cssVariablesValues = new StringBuffer();
            cssVariablesValues
                    .append("document.documentElement.style.setProperty('--main-bg-color', '" + Constants.systemConfig.getBackgroundColor() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-pf-color', '" + Constants.systemConfig.getProfileColor() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-mnu-color', '" + Constants.systemConfig.getMenuColor() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-btn-color', '" + Constants.systemConfig.getPrimaryButtonColor() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-txt-color', '" + Constants.systemConfig.getTextColor() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-logo', '" + Constants.systemConfig.getLogo() + "');\n")
                    .append("document.documentElement.style.setProperty('--main-bg-img', '" + Constants.systemConfig.getBackgroundImage() + "');");
            model.addAttribute("cssVariablesValues", cssVariablesValues);
            String key = urlMap().get("admin-dashboard");
            model.addAttribute("key", key);
            adminModelMapEntry(key, model);
            return "admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

//    @RequestMapping(value = {"/admin-send-mail-to"}, method = RequestMethod.GET)
//    public String adminSendMailToEmail(ModelMap model, @RequestParam("email") String email) {
//        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
//        if (admin != null) {
//            adminMailManager.sendEmailTemplateAsDemo(email);
//            return "redirect:/home";
//        } else {
//            return "redirect:/login";
//        }
//    }

    @RequestMapping(value = {"/admin-invite-codes"}, method = RequestMethod.GET)
    public String adminInviteCodes(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Invite Codes " + projectName);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("portfolios", repository.funds());
            model.addAttribute("inviteCodes", repository.inviteCodes(true, false, false));
            return "admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-user-accounts"}, method = RequestMethod.GET)
    public String adminUA(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "User Accounts " + projectName);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("totalUsers", repository.users());
            return "admin-show-user-accounts";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-new-users"}, method = RequestMethod.GET)
    public String adminLatestUsers(ModelMap model,
            @RequestParam(name = "daterange", required = false) String daterange) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "User Accounts " + projectName);
            Date firstDate = null;
            Date lastDate = null;
            String[] arr = null;
            if (daterange != null) {
                arr = daterange.split(" - ");
            }
            if (arr != null && arr.length == 2) {
                firstDate = common.parseDate(arr[0], CommonMethods.format3);
                lastDate = common.parseDate(arr[1], CommonMethods.format3);
            } else {
                lastDate = dateTimeService.now();
                Calendar cal = Calendar.getInstance();
                cal.setTime(lastDate);
                cal.add(Calendar.DAY_OF_MONTH, -7);
                firstDate = cal.getTime();
            }
            daterange = common.dateFormat(firstDate, CommonMethods.format3) + " - " + common.dateFormat(lastDate, CommonMethods.format3);
            model.addAttribute("daterange", daterange);
            model.addAttribute("totalUsers", repository.users(firstDate, lastDate));
            return "admin-show-new-users";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-change-profiles"}, method = RequestMethod.GET)
    public String adminChangingProfiles(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "User Changing Profiles " + projectName);
            model.addAttribute("profiles", repository.getChangingProfiles(null));
            return "admin-show-change-profiles";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-portfolios"}, method = RequestMethod.GET)
    public String adminShowPortfolios(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            try {
                model.addAttribute("info", admin);
                model.addAttribute("title", "Portfolios " + projectName);
                model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                List<String> days = new ArrayList<>();
                Calendar now = Calendar.getInstance();
                now.setTime(common.today());//today
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -1);//yesterday
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -6);//before week
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -24);//before month
                days.add(format.format(now.getTime()));
                model.addAttribute("daterange24hr", days.get(1) + "%20-%20" + days.get(0));
                model.addAttribute("daterange7d", days.get(2) + "%20-%20" + days.get(0));
                model.addAttribute("daterange30d", days.get(3) + "%20-%20" + days.get(0));
                List<PortfolioBean> portfoliosReport = databaseService.portfoliosReport(days);
                model.addAttribute("portfoliosReport", portfoliosReport);
                model.addAttribute("currencies", repository.currencies());
                List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
                model.addAttribute("coins", coinList);
                return "admin-show-portfolios";
            } catch (IOException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
                return "forward:/admin-dashboard";
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-shares"}, method = RequestMethod.GET)
    public String adminShowShares(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Shares " + projectName);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("portfolios", repository.funds());
            return "admin-show-shares";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-funds"}, method = RequestMethod.GET)
    public String adminShowFunds(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            try {
                model.addAttribute("info", admin);
                model.addAttribute("title", "Funds " + projectName);
                model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
                model.addAttribute("shares", repository.shares());
                model.addAttribute("portfolios", repository.funds());
                List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
                model.addAttribute("coins", coinList);
                return "admin-show-funds";
            } catch (IOException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
                return "forward:/admin-dashboard";
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-portfolio"}, method = RequestMethod.GET)
    public String adminShowPortfolio(ModelMap model, @RequestParam("id") String id) throws IOException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
//            try {
//                String main2 = poloniexExchangeService.main2();
//                if (main2.contains("<script type=\"text/javascript\" src=\"/cdn-cgi/scripts/cf.challenge.js\"") && main2.contains("</html>")) {
//                    main2 = main2.replaceAll("\"/cdn-cgi/scripts", "\"https://www.poloniex.com/cdn-cgi/scripts");
//                    main2 = main2.replaceAll("\"/cdn-cgi/l", "\"./admin-cdn-cgi/l");
//                    model.addAttribute("htmlCode", main2);
////                    return "emptyfile";
//                    return "redirect:https://www.poloniex.com/public?command=returnTicker";
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            if ("19".equalsIgnoreCase(id)) {
                GetBalancesContainer balancesContainer = bittrexAccountAPIService.getBalances();
                List<Balance> balances = balancesContainer.getBalances();
                List<Balance> totalBalances = new LinkedList<>();
                StringBuilder currency = new StringBuilder();
                for (int i = 0; i < balances.size(); i++) {
                    Balance balance = balances.get(i);
                    if (balance.getBalance() > 0) {
                        currency.append(balance.getCurrency()).append(i == balances.size() - 1 ? "" : ",");
                        totalBalances.add(balance);
                    }
                }
                ObjectNode rateForPair = databaseService.rateForPair(currency.toString(), "BTC", null);
                Double totalBtcValue = 0d;
                for (Balance balance : totalBalances) {
                    JsonNode curr = rateForPair.get(balance.getCurrency());
                    double btcPrice = curr.get("BTC").asDouble();
                    double currValue = balance.getBalance();
                    double btcValue = btcPrice * currValue;
                    balance.setBtcValue(btcValue);
                    totalBtcValue += btcValue;
                }
                model.addAttribute("totalBtcValue", totalBtcValue);
                GetOrderHistoryContainer orderHistoryContainer = bittrexAccountAPIService.getOrderHistory();
                Date today0000 = dateTimeService.today0000();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(today0000);
                calendar.add(Calendar.DAY_OF_MONTH, -1);
//                Date yesterday0000 = calendar.getTime();
                List<PastOrder> pastOrders = orderHistoryContainer.getPastOrders();
                List<PastOrder> todayOrders = new LinkedList<>();
                for (PastOrder order : pastOrders) {
                    Date dateTime = order.getDateTime();
                    if (dateTime.after(today0000)) {
//                    if (dateTime.after(today0000)) {
                        todayOrders.add(order);
                    }
                }
                model.addAttribute("todayOrders", todayOrders);
            }
            ShareFund portfolio = repository.fundById(id, null);
            model.addAttribute("info", admin);
            model.addAttribute("title", portfolio.getName() + projectName);
            model.addAttribute("trade", repository.tradeByFundId(id));
            model.addAttribute("portfolio", portfolio);
//            ShareFund totalUnits = repository.totalUnitsById(id, null);
//            model.addAttribute("totalUnits", totalUnits);
            model.addAttribute("hh", common.hours());
            model.addAttribute("mm", common.minutes());
            return "admin-show-portfolio";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-currencies"}, method = RequestMethod.GET)
    public String adminShowCurrencies(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Currencies " + projectName);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("currencies", repository.currencies());
            model.addAttribute("portfolios", repository.funds());
            return "admin-show-currencies";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-verification-done"}, method = RequestMethod.GET)
    public String adminVerificationDone(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Verification Done " + projectName);
            List<Verification> verificationList = repository.findVerification(null, "DONE");
            model.addAttribute("verificationList", verificationList);
            return "admin-verification-done";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-verification-pending"}, method = RequestMethod.GET)
    public String adminVerificationPending(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", "Verification Pending " + projectName);
            List<Verification> verificationList = repository.findVerification(null, "PENDING");
            model.addAttribute("verificationList", verificationList);
            return "admin-verification-pending";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-cancel-change-profile"}, method = RequestMethod.GET)
    public String adminCancelChangeProfile(ModelMap model,
            @RequestParam("ok") String id) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateCancelChangeProfile(id);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-approved-change-profile"}, method = RequestMethod.GET)
    public String adminApprovedChangeProfile(ModelMap model,
            @RequestParam("ok") String id) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateApprovedChangeProfile(id);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/system-config"}, method = RequestMethod.GET)
    public String systemConfig(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("title", " System Settings " + projectName);
            model.addAttribute("systemConfig", Constants.systemConfig);
            return "system-config";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-cdn-cgi/l/chk_captcha"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String adminCdnCgilChkCaptcha(ModelMap model,
            @RequestParam("id") String id,
            @RequestParam("g-recaptcha-response") String gRecaptchaResponse) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            return "redirect:https://www.poloniex.com/cdn-cgi/l/chk_captcha?id=" + id + "&g-recaptcha-response" + gRecaptchaResponse;
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-portfolio-report"}, method = RequestMethod.GET)
    public String adminPortfolioReport(ModelMap model,
            @RequestParam("id") String id) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            ShareFund portfolio = repository.fundById(id, null);
            model.addAttribute("info", admin);
            model.addAttribute("title", portfolio.getName() + " Holding Report " + projectName);
            model.addAttribute("trade", repository.tradeByFundId(id));
            model.addAttribute("portfolio", portfolio);
            model.addAttribute("hh", common.hours());
            model.addAttribute("mm", common.minutes());
            List<PortfolioBean> report = databaseService.portfolioReportById(id);
            model.addAttribute("report", report);
            List<PortfolioBean> userUnits = repository.userPortfolioUnits(id);
            model.addAttribute("userUnits", userUnits);
            List<PortfolioBean> userShares = repository.userPortfolioShares(id);
            model.addAttribute("userShares", userShares);
            return "admin-portfolio-rep";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-portfolio-report-type"}, method = RequestMethod.GET)
    public String adminPortfolioReportType(ModelMap model,
            @RequestParam(name = "id") String id,
            @RequestParam(name = "reportType", required = false) String reportType,
            @RequestParam(name = "daterange", required = false) String daterange
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            ShareFund portfolio = repository.fundById(id, null);
            String REPORT_TYPE = common.reportTypes().get(reportType);
            if (REPORT_TYPE == null) {
                reportType = "DR";
                REPORT_TYPE = common.reportTypes().get(reportType);
            }
            model.addAttribute("info", admin);
            model.addAttribute("title", portfolio.getName() + " " + REPORT_TYPE + " " + projectName);
            model.addAttribute("trade", repository.tradeByFundId(id));
            model.addAttribute("portfolio", portfolio);
            model.addAttribute("id", portfolio.getFundId());
            Date firstDate = null;
            Date lastDate = null;
            String[] arr = null;
            if (daterange != null) {
                arr = daterange.split(" - ");
            }
            if (arr != null && arr.length == 2) {
                firstDate = common.parseDate(arr[0], CommonMethods.format3);
                lastDate = common.parseDate(arr[1], CommonMethods.format3);
            } else {
                lastDate = dateTimeService.now();
                Calendar cal = Calendar.getInstance();
                cal.setTime(lastDate);
                cal.add(Calendar.DAY_OF_MONTH, -7);
                firstDate = cal.getTime();
            }
            daterange = common.dateFormat(firstDate, CommonMethods.format3) + " - " + common.dateFormat(lastDate, CommonMethods.format3);
            model.addAttribute("daterange", daterange);
            model.addAttribute("reportType", REPORT_TYPE);
            model.addAttribute("reportTypeValue", reportType);
            List<TransactionBean> report = repository.portfolioReportType(reportType, firstDate, lastDate, portfolio.getFundId());
            model.addAttribute("report", report);
            return "admin-portfolio-report-type";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-bitcoin"}, method = RequestMethod.POST)
    public String adminAddBitcoin(ModelMap modelMap,
            @ModelAttribute("bitcoin") ShareFund bitcoin
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            Date date = dateTimeService.now();
            bitcoin.setCreatedDate(common.dateFormat(date));
            repository.addBitcoin(bitcoin);
            return "redirect:/admin-show-portfolio?id=" + bitcoin.getFundId();
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-trade"}, method = RequestMethod.POST)
    public String adminAddTrade(ModelMap modelMap,
            @ModelAttribute("trade") ShareFund trade
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            databaseService.addTrade(trade);
            return "redirect:/admin-show-portfolio?id=" + trade.getFundId();
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-invite-code"}, method = RequestMethod.POST)
    public String adminAddInviteCode(ModelMap modelMap,
            @ModelAttribute("code") InviteCode code
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            code.setCreatedBy(admin.getUserId());
            Date date = dateTimeService.now();
            code.setCreatedTs(common.dateFormat(date));
            Integer year = common.parseInt(code.getTimeFrame());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.YEAR, year);
            date = cal.getTime();
            code.setExpiryTs(common.dateFormat(date));
            repository.save(code);
            if (code.getId() != null && code.getName() != null && code.getEmailId() != null && code.getInviteCode() != null) {
                adminMailManager.allocateEmail(admin, code.getName(), code.getEmailId(), code.getInviteCode());
//                mailManager.allocateEmailADMIN(admin, code.getName(), code.getEmailId(), code.getInviteCode());
            }
            return "redirect:/admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-allocate-email"}, method = RequestMethod.POST)
    public String adminAllocateEmail(ModelMap modelMap,
            @ModelAttribute("code") InviteCode code
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.allocateEmail(code);
            if (code.getId() != null && code.getEmailId() != null && code.getInviteCode() != null) {
                adminMailManager.allocateEmail(admin, code.getName(), code.getEmailId(), code.getInviteCode());
            }
            return "redirect:/admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-share"}, method = RequestMethod.POST)
    public String adminAddShare(ModelMap modelMap,
            @ModelAttribute("share") ShareFund share
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            share.setCreatedBy(admin.getUserId());
            Integer id = common.parseInt(share.getShareId());
            if (id > 0) {
                repository.updateShare(share);
            } else {
                repository.addShare(share);
            }
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-currency"}, method = RequestMethod.POST)
    public String adminAddCurrency(ModelMap modelMap,
            @ModelAttribute("currency") ShareFund currency
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            currency.setCreatedBy(admin.getUserId());
            repository.addCurrency(currency);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-conversion-pairs"}, method = RequestMethod.POST)
    public String adminUpdateConversionPairs(ModelMap modelMap,
            @ModelAttribute("currency") ShareFund currency
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            currency.setCreatedBy(admin.getUserId());
            repository.addCurrency(currency);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-share-price"}, method = RequestMethod.POST)
    public String adminUpdateSharePrice(ModelMap modelMap,
            @ModelAttribute("share") ShareFund share
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            //            Date date = new Date();
            Date date = dateTimeService.now();
            share.setCreatedDate(common.dateFormat(date));
            share.setCreatedBy(admin.getUserId());
            repository.updateSharePrice(share);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-fund"}, method = RequestMethod.POST)
    public String adminAddFund(ModelMap modelMap,
            @ModelAttribute("fund") ShareFund fund,
            final @RequestPart(value = "file", required = false) MultipartFile file
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            fund.setCreatedBy(admin.getUserId());
            Integer id = common.parseInt(fund.getFundId());
            if (id > 0) {
                repository.updateFund(fund);
            } else {
                repository.addFund(fund);
            }
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-fund-price"}, method = RequestMethod.POST)
    public String adminUpdateSharePrice(ModelMap modelMap,
            @ModelAttribute("fund") PortfolioBean fund) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateFundPrice(fund);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-purchase-pending-shares"}, method = RequestMethod.POST)
    public String adminPurchasePendingShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            Date date = dateTimeService.now();
            if (investmentShares.getCreatedDate() == null) {
                investmentShares.setCreatedDate(common.dateFormat(date));
            }
            investmentShares.setCreatedBy(admin.getUserId());
            repository.updatePendingShares(investmentShares);
            TransactionBean tran = repository.purchaseShares(investmentShares);
            ShareFund portfolio = repository.fundById(null, tran.getInvestment_id());
            tran.setFund_id(portfolio.getFundId());
            tran.setCreated_date(investmentShares.getCreatedDate());
            tran.setCreated_ts(common.dateFormat(date));
            repository.addPortfolioBuying(tran);
            UserInfo user = repository.findByUserId(investmentShares.getUserId());
            adminMailManager.sendPurchasedSharesEmail(admin, user.getFullName(), user.getEmail(), tran);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-purchased-shares"}, method = RequestMethod.POST)
    public String adminUpdatePurchasedShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund updatedShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updatePurchasedShares(updatedShares);
            return "redirect:/admin-show-user-accounts";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-sell-shares"}, method = RequestMethod.POST)
    public String adminSellShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            Date date = dateTimeService.now();
            investmentShares.setCreatedDate(common.dateFormat(date));
            investmentShares.setCreatedBy(admin.getUserId());
            if ("SELL_TO_MANAGEMENT".equalsIgnoreCase(investmentShares.getSellOption())) {
                List<InvestmentBean> invs = repository.findManagementInvestments(null, null, null, investmentShares.getInvestmentId());
                InvestmentBean mgmtInv = !invs.isEmpty() ? invs.get(0) : null;
                String userId = investmentShares.getUserId();
                String investmentId = investmentShares.getInvestmentId();
                String mgmtInvId = mgmtInv.getInvestmentId();
                String mgmtUserId = mgmtInv.getUserId();
                if (mgmtInvId != null) {
                    investmentShares.setRefInvId(mgmtInvId);
                    repository.sellShares(investmentShares);
                    investmentShares.setUserId(mgmtUserId);
                    investmentShares.setInvestmentId(mgmtInvId);
                    investmentShares.setRefInvId(investmentId);
                    repository.purchaseShares(investmentShares);
                    UserInfo user = repository.findByUserId(userId);
                    adminMailManager.sendSoldCompletedEmail(admin, user.getFullName(), user.getEmail(), null);
                }
            } else {
                String userId = investmentShares.getUserId();
                repository.sellShares(investmentShares);
                UserInfo user = repository.findByUserId(userId);
                adminMailManager.sendSoldCompletedEmail(admin, user.getFullName(), user.getEmail(), null);
            }
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-purchase-shares"}, method = RequestMethod.POST)
    public String adminPurchaseShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            Date date = dateTimeService.now();
            investmentShares.setCreatedDate(common.dateFormat(date));
            investmentShares.setCreatedBy(admin.getUserId());
            repository.purchaseShares(investmentShares);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-transacation-request"}, method = RequestMethod.POST)
    public String adminChangeAmount(ModelMap modelMap, @ModelAttribute("request") RequestBean request) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateTransactionRequest(request);
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-edit-verification"}, method = RequestMethod.POST)
    public String adminEditVerification(ModelMap modelMap, @ModelAttribute("verification") Verification verification) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateVerification(verification);
            return "redirect:/admin-verification-pending";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-user-status"}, method = RequestMethod.POST)
    public String adminUpdateUserStatus(ModelMap modelMap, @ModelAttribute("userInfo") UserInfo userInfo) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updateUserStatus(userInfo);
            return "redirect:/admin-show-user-accounts";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-amounted"}, method = RequestMethod.POST)
    public String adminUpdateAmounted(ModelMap modelMap,
            @ModelAttribute("ref") String ref
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            return "redirect:/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/system-config"}, method = RequestMethod.POST)
    public String sytemConfig(ModelMap model,
            @ModelAttribute("config") SystemConfig config,
            final @RequestPart(value = "logo1", required = false) MultipartFile logofile,
            final @RequestPart(value = "backgroundImage1", required = false) MultipartFile backgroundImagefile
    ) throws IOException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            if (!logofile.isEmpty()) {
                config.setLogo(logofile.getOriginalFilename());
                fileUtility.storeImage(15, logofile, config.getLogo());
                Constants.systemConfig.setLogo("15/" + config.getLogo());
            }
            if (!backgroundImagefile.isEmpty()) {
                config.setBackgroundImage(backgroundImagefile.getOriginalFilename());
                fileUtility.storeImage(15, backgroundImagefile, config.getBackgroundImage());
                Constants.systemConfig.setBackgroundImage("15/" + config.getBackgroundImage());
            }
            Constants.systemConfig.setBackgroundColor(config.getBackgroundColor());
            Constants.systemConfig.setProfileColor(config.getProfileColor());
            Constants.systemConfig.setMenuColor(config.getMenuColor());
            Constants.systemConfig.setPrimaryButtonColor(config.getPrimaryButtonColor());
            Constants.systemConfig.setTextColor(config.getTextColor());
            return "redirect:/system-config";
        } else {
            return "redirect:/login";
        }
    }

}
