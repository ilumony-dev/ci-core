/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import java.util.Collection;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Administrator
 */
public class SecuredUser extends org.springframework.security.core.userdetails.User implements ToObjectConverter {

    /**
     * @return the verifyLater
     */
    public Boolean getVerifyLater() {
        if (verifyLater == null) {
            verifyLater = false;
        }
        return verifyLater;
    }

    /**
     * @param verifyLater the verifyLater to set
     */
    public void setVerifyLater(Boolean verifyLater) {
        this.verifyLater = verifyLater;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the loginCount
     */
    public Long getLoginCount() {
        return loginCount;
    }

    /**
     * @param loginCount the loginCount to set
     */
    public void setLoginCount(Long loginCount) {
        this.loginCount = loginCount;
    }

    /**
     * @return the verifyCount
     */
    public Long getVerifyCount() {
        return verifyCount;
    }

    /**
     * @param verifyCount the verifyCount to set
     */
    public void setVerifyCount(Long verifyCount) {
        this.verifyCount = verifyCount;
    }

    /**
     * @return the management
     */
    public Boolean getManagement() {
        return management;
    }

    /**
     * @param management the management to set
     */
    public void setManagement(Boolean management) {
        this.management = management;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the createdTs
     */
    public String getCreatedTs() {
        return createdTs;
    }

    /**
     * @param createdTs the createdTs to set
     */
    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    /**
     * @return the createdTime
     */
    public Long getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime the createdTime to set
     */
    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * @return the config
     */
    public Configuration getConfig() {
        return config;
    }

    /**
     * @param config the config to set
     */
    public void setConfig(Configuration config) {
        this.config = config;
    }

    /**
     * @return the verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * @param verified the verified to set
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public SecuredUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the inviteCode
     */
    public String getInviteCode() {
        return inviteCode;
    }

    /**
     * @param inviteCode the inviteCode to set
     */
    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return the user
     */
    public Boolean getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Boolean user) {
        this.user = user;
    }

    /**
     * @return the admin
     */
    public Boolean getAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     */
    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

    private String active;
    private String userId;
    private String fullName;
    private String firstName;
    private String lastName;
    private String email;
    private String dob;
    private String createdTs;
    private Long createdTime;
    private String inviteCode;
    private String mobileNo;
    private String refId;
    private Boolean user;
    private Boolean admin;
    private Boolean management;
    private Boolean verified;
    private static Boolean verifyLater;
    private Configuration config;
    private Long verifyCount;
    private Long loginCount;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SecuredUser.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                if (fieldValue instanceof String) {
                    String checkNull = checkNull(fieldValue);
                    if (checkNull != null) {
//                        checkNull = encrypt();
                        buffer.append(checkNull);
                    } else {
                        buffer.append(checkNull);
                    }
                } else {
                    buffer.append(checkNull(fieldValue));
                }
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
