/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.interceptors;

/**
 *
 * @author ADMIN
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class RequestProcessingTimeInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(RequestProcessingTimeInterceptor.class);
//    private static final int REQUEST_TIME = 10000;
//    private long lastRequestTime;
//    private String lastRequestedUrl;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long startTime = System.currentTimeMillis();
        String requestedUrl = request.getRequestURL().toString();
//        lastRequestTime = startTime;
//        lastRequestedUrl = requestedUrl;
        String log = "Request URL::" + requestedUrl + ":: Start Time=" + startTime;
        logger.debug(log);
        String ipAddress = request.getHeader("Remote_Addr");
        if (ipAddress == null || ipAddress.isEmpty()) {
            ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
            if (ipAddress == null || ipAddress.isEmpty()) {
                ipAddress = request.getRemoteAddr();
            }
        }
        logger.debug("Request URL=" + requestedUrl + ":: IP Address=" + ipAddress);
        request.setAttribute("startTime", startTime);
        //if returned false, we need to make sure 'response' is sent
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String requestedUrl = request.getRequestURL().toString();
        String log = ("Request URL::" + requestedUrl + " Sent to Handler :: Current Time=" + System.currentTimeMillis());
        logger.debug(log);
//we can add attributes in the modelAndView and use that in the view page
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long startTime = (Long) request.getAttribute("startTime");
        long endTime = (Long) System.currentTimeMillis();
        String requestedUrl = request.getRequestURL().toString();
        String log = ("Request URL::" + requestedUrl + ":: End Time=" + endTime);
        logger.debug(log);
        log = ("Request URL::" + requestedUrl + ":: Time Taken=" + (endTime - startTime));
        logger.debug(log);
    }

}
