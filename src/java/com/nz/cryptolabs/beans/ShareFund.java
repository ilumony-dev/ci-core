/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class ShareFund implements ToObjectConverter {

    /**
     * @return the targetPercentage
     */
    public String getTargetPercentage() {
        return targetPercentage;
    }

    /**
     * @param targetPercentage the targetPercentage to set
     */
    public void setTargetPercentage(String targetPercentage) {
        this.targetPercentage = targetPercentage;
    }

    /**
     * @return the percent
     */
    public String getPercent() {
        return percent;
    }

    /**
     * @param percent the percent to set
     */
    public void setPercent(String percent) {
        this.percent = percent;
    }

    /**
     * @return the refInvId
     */
    public String getRefInvId() {
        return refInvId;
    }

    /**
     * @param refInvId the refInvId to set
     */
    public void setRefInvId(String refInvId) {
        this.refInvId = refInvId;
    }

    /**
     * @return the sellOption
     */
    public String getSellOption() {
        return sellOption;
    }

    /**
     * @param sellOption the sellOption to set
     */
    public void setSellOption(String sellOption) {
        this.sellOption = sellOption;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the imageName
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * @param imageName the imageName to set
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * @return the iconName
     */
    public String getIconName() {
        return iconName;
    }

    /**
     * @param iconName the iconName to set
     */
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    /**
     * @return the localInvestedAmount
     */
    public String getLocalInvestedAmount() {
        return localInvestedAmount;
    }

    /**
     * @param localInvestedAmount the localInvestedAmount to set
     */
    public void setLocalInvestedAmount(String localInvestedAmount) {
        this.localInvestedAmount = localInvestedAmount;
    }

    /**
     * @return the localPrice
     */
    public String getLocalPrice() {
        return localPrice;
    }

    /**
     * @param localPrice the localPrice to set
     */
    public void setLocalPrice(String localPrice) {
        this.localPrice = localPrice;
    }

    /**
     * @return the localShareAmount
     */
    public String getLocalShareAmount() {
        return localShareAmount;
    }

    /**
     * @param localShareAmount the localShareAmount to set
     */
    public void setLocalShareAmount(String localShareAmount) {
        this.localShareAmount = localShareAmount;
    }

    /**
     * @return the hh
     */
    public Integer getHh() {
        return hh;
    }

    /**
     * @param hh the hh to set
     */
    public void setHh(Integer hh) {
        this.hh = hh;
    }

    /**
     * @return the mm
     */
    public Integer getMm() {
        return mm;
    }

    /**
     * @param mm the mm to set
     */
    public void setMm(Integer mm) {
        this.mm = mm;
    }

    /**
     * @return the ss
     */
    public Integer getSs() {
        return ss;
    }

    /**
     * @param ss the ss to set
     */
    public void setSs(Integer ss) {
        this.ss = ss;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the usd
     */
    public String getUsd() {
        return usd;
    }

    /**
     * @param usd the usd to set
     */
    public void setUsd(String usd) {
        this.usd = usd;
    }

    /**
     * @return the btc
     */
    public String getBtc() {
        return btc;
    }

    /**
     * @param btc the btc to set
     */
    public void setBtc(String btc) {
        this.btc = btc;
    }

    /**
     * @return the minto
     */
    public String getMinto() {
        return minto;
    }

    /**
     * @param minto the minto to set
     */
    public void setMinto(String minto) {
        this.minto = minto;
    }

    /**
     * @return the fromCurrSym
     */
    public String getFromCurrSym() {
        return fromCurrSym;
    }

    /**
     * @param fromCurrSym the fromCurrSym to set
     */
    public void setFromCurrSym(String fromCurrSym) {
        this.fromCurrSym = fromCurrSym;
    }

    /**
     * @return the fromQty
     */
    public String getFromQty() {
        return fromQty;
    }

    /**
     * @param fromQty the fromQty to set
     */
    public void setFromQty(String fromQty) {
        this.fromQty = fromQty;
    }

    /**
     * @return the toCurrSym
     */
    public String getToCurrSym() {
        return toCurrSym;
    }

    /**
     * @param toCurrSym the toCurrSym to set
     */
    public void setToCurrSym(String toCurrSym) {
        this.toCurrSym = toCurrSym;
    }

    /**
     * @return the toQty
     */
    public String getToQty() {
        return toQty;
    }

    /**
     * @param toQty the toQty to set
     */
    public void setToQty(String toQty) {
        this.toQty = toQty;
    }

    /**
     * @return the currencySymbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * @param currencySymbol the currencySymbol to set
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * @return the currencyName
     */
    public String getCurrencyName() {
        return currencyName;
    }

    /**
     * @param currencyName the currencyName to set
     */
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the purTrades
     */
    public String getPurTrades() {
        return purTrades;
    }

    /**
     * @param purTrades the purTrades to set
     */
    public void setPurTrades(String purTrades) {
        this.purTrades = purTrades;
    }

    /**
     * @return the soldTrades
     */
    public String getSoldTrades() {
        return soldTrades;
    }

    /**
     * @param soldTrades the soldTrades to set
     */
    public void setSoldTrades(String soldTrades) {
        this.soldTrades = soldTrades;
    }

    /**
     * @return the purCoins
     */
    public String getPurCoins() {
        return purCoins;
    }

    /**
     * @param purCoins the purCoins to set
     */
    public void setPurCoins(String purCoins) {
        this.purCoins = purCoins;
    }

    /**
     * @return the soldCoins
     */
    public String getSoldCoins() {
        return soldCoins;
    }

    /**
     * @param soldCoins the soldCoins to set
     */
    public void setSoldCoins(String soldCoins) {
        this.soldCoins = soldCoins;
    }

    /**
     * @return the opBal
     */
    public String getOpBal() {
        return opBal;
    }

    /**
     * @param opBal the opBal to set
     */
    public void setOpBal(String opBal) {
        this.opBal = opBal;
    }

    /**
     * @return the closeBal
     */
    public String getCloseBal() {
        return closeBal;
    }

    /**
     * @param closeBal the closeBal to set
     */
    public void setCloseBal(String closeBal) {
        this.closeBal = closeBal;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the percent1hr
     */
    public double getPercent1hr() {
        return percent1hr;
    }

    /**
     * @param percent1hr the percent1hr to set
     */
    public void setPercent1hr(double percent1hr) {
        this.percent1hr = percent1hr;
    }

    /**
     * @return the percent24hr
     */
    public double getPercent24hr() {
        return percent24hr;
    }

    /**
     * @param percent24hr the percent24hr to set
     */
    public void setPercent24hr(double percent24hr) {
        this.percent24hr = percent24hr;
    }

    /**
     * @return the percent7d
     */
    public double getPercent7d() {
        return percent7d;
    }

    /**
     * @param percent7d the percent7d to set
     */
    public void setPercent7d(double percent7d) {
        this.percent7d = percent7d;
    }

    /**
     * @return the investedAmount
     */
    public String getInvestedAmount() {
        return investedAmount;
    }

    /**
     * @param investedAmount the investedAmount to set
     */
    public void setInvestedAmount(String investedAmount) {
        this.investedAmount = investedAmount;
    }

    /**
     * @return the master
     */
    public String getMaster() {
        return master;
    }

    /**
     * @param master the master to set
     */
    public void setMaster(String master) {
        this.master = master;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the exchangeCode
     */
    public String getExchangeCode() {
        return exchangeCode;
    }

    /**
     * @param exchangeCode the exchangeCode to set
     */
    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    /**
     * @return the custodianId
     */
    public String getCustodianId() {
        return custodianId;
    }

    /**
     * @param custodianId the custodianId to set
     */
    public void setCustodianId(String custodianId) {
        this.custodianId = custodianId;
    }

    /**
     * @return the shareId
     */
    public String getShareId() {
        return shareId;
    }

    /**
     * @param shareId the shareId to set
     */
    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    /**
     * @return the percentage
     */
    public String getPercentage() {
        return percentage;
    }

    /**
     * @param percentage the percentage to set
     */
    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the fundId
     */
    public String getFundId() {
        return fundId;
    }

    /**
     * @param fundId the fundId to set
     */
    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the shareAmount
     */
    public String getShareAmount() {
        return shareAmount;
    }

    /**
     * @param shareAmount the shareAmount to set
     */
    public void setShareAmount(String shareAmount) {
        this.shareAmount = shareAmount;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the investmentId
     */
    public String getInvestmentId() {
        return investmentId;
    }

    /**
     * @param investmentId the investmentId to set
     */
    public void setInvestmentId(String investmentId) {
        this.investmentId = investmentId;
    }

    /**
     * @return the reqId
     */
    public String getReqId() {
        return reqId;
    }

    /**
     * @param reqId the reqId to set
     */
    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    /**
     * @return the shareName
     */
    public String getShareName() {
        return shareName;
    }

    /**
     * @param shareName the shareName to set
     */
    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    /**
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * @param fundName the fundName to set
     */
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    /**
     * @return the shares
     */
    public String getShares() {
        return shares;
    }

    /**
     * @param shares the shares to set
     */
    public void setShares(String shares) {
        this.shares = shares;
    }

    /**
     * @return the coinId
     */
    public String getCoinId() {
        return coinId;
    }

    /**
     * @param coinId the coinId to set
     */
    public void setCoinId(String coinId) {
        this.coinId = coinId;
    }

    private String id;
    private String reqId;
    private String customerName, email;
    private String name;
    private String description;
    private String exchangeCode;
    private String custodianId;
    private String shareId;
    private String fundId;
    private String userId;
    private String investmentId;
    private String refInvId;
    private String coinId;
    private String percentage;
    private String targetPercentage;
    private String price;
    public String shareAmount;
    private String quantity;
    private String createdBy;
    private String createdDate;
    private String shareName;
    private String fundName;
    private String currencyName;
    private String currencySymbol;
    private String shares;
    private String master;
    private String investedAmount;
    private Date date;
    private String opBal;
    private String closeBal;
    private String purTrades;
    private String soldTrades;
    private String purCoins;
    private String soldCoins;
    private String fromCurrSym;
    private String fromQty;
    private String toCurrSym;
    private String toQty;
    private double percent1hr;
    private double percent24hr;
    private double percent7d;
    private String local, usd, btc, minto, percent;
    private Integer hh, mm, ss;
    private String localPrice;
    private String localShareAmount;
    private String localInvestedAmount;
    private String imageName;
    private String iconName;
    private String sellOption;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ShareFund.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public void sameNameUpdatePercentage(List<ShareFund> list2) {
        for (ShareFund share : list2) {
            if (share.getShareName().equals(this.shareName)) {
                this.setPercentage(share.getPercentage());
                break;
            }
        }
    }

}
