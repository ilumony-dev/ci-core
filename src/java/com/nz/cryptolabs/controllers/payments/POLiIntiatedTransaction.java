/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

import com.nz.cryptolabs.beans.ToObjectConverter;
import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author palo12
 */
public class POLiIntiatedTransaction implements ToObjectConverter {

    POLiIntiatedTransaction(JSONObject object) throws JSONException {
        Success = (Boolean) object.get("Success");
        NavigateURL = (String) object.get("NavigateURL");
        ErrorCode = (Integer) object.get("ErrorCode");
        ErrorMessage = object.isNull("ErrorMessage") ? null : (String) object.get("ErrorMessage");
        TransactionRefNo = (String) object.get("TransactionRefNo");
    }

    @Override
    public String toString() {
        return "{"
                + "\"Success\":" + (Success) + ", "
                + "\"NavigateURL\":" + checkNull(NavigateURL) + ", "
                + "\"ErrorCode\":" + (ErrorCode) + ", "
                + "\"ErrorMessage\":" + checkNull(ErrorMessage) + ", "
                + "\"TransactionRefNo\":" + checkNull(TransactionRefNo) + " "
                + "}";
    }

    @Override
    public String toJSONObject() {
        return "{"
                + "Success:" + (Success) + ", "
                + "NavigateURL:" + checkNull(NavigateURL) + ", "
                + "ErrorCode:" + (ErrorCode) + ", "
                + "ErrorMessage:" + checkNull(ErrorMessage) + ", "
                + "TransactionRefNo:" + checkNull(TransactionRefNo) + " "
                + "}";
    }

    /**
     * @return the Success
     */
    public Boolean getSuccess() {
        return Success;
    }

    /**
     * @param Success the Success to set
     */
    public void setSuccess(Boolean Success) {
        this.Success = Success;
    }

    /**
     * @return the NavigateURL
     */
    public String getNavigateURL() {
        return NavigateURL;
    }

    /**
     * @param NavigateURL the NavigateURL to set
     */
    public void setNavigateURL(String NavigateURL) {
        this.NavigateURL = NavigateURL;
    }

    /**
     * @return the ErrorCode
     */
    public Integer getErrorCode() {
        return ErrorCode;
    }

    /**
     * @param ErrorCode the ErrorCode to set
     */
    public void setErrorCode(Integer ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    /**
     * @return the ErrorMessage
     */
    public String getErrorMessage() {
        return ErrorMessage;
    }

    /**
     * @param ErrorMessage the ErrorMessage to set
     */
    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    /**
     * @return the TransactionRefNo
     */
    public String getTransactionRefNo() {
        return TransactionRefNo;
    }

    /**
     * @param TransactionRefNo the TransactionRefNo to set
     */
    public void setTransactionRefNo(String TransactionRefNo) {
        this.TransactionRefNo = TransactionRefNo;
    }

    private Boolean Success;
    private String NavigateURL;
    private Integer ErrorCode;
    private String ErrorMessage;
    private String TransactionRefNo;
}
