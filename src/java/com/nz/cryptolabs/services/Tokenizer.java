/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import org.springframework.stereotype.Service;

/**
 *
 * @author Maninderjit
 */
@Service
public class Tokenizer {

    int ONLY_NUM = 4;// NEVER MODIFY

//    public String tokenNo(String refId, String base, String recordId) {
//        String next = "";
//        for (byte b : base.getBytes()) {
//            next += ((b * ONLY_NUM));
//            next += (char) (b - ONLY_NUM);
//        }
//        return ("M-" + next + base + recordId);
//    }
    
    public String tokenNo(String refId, String base, String recordId) {
// here is ("N" + base) support NWR, NIR, NWT
        return ("INV4N" + base + recordId);
    }

    public String fetchRecordId(String tokenNo, String base) {
        String[] arr = tokenNo.split(base);
        return arr[arr.length - 1];
    }

    public String refNo(int i, String fullName) {
        return ("M-" + String.valueOf(i * ONLY_NUM) + fullName.substring(0, 2).toUpperCase());
    }
}
