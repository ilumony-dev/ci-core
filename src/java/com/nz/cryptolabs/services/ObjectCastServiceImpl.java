/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.nz.cryptolabs.components.CommonMethods;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class ObjectCastServiceImpl implements ObjectCastService {

    @Autowired
    private CommonMethods common;

    @Override
    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException {
        //JSON from String to Object
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonInString, clazz);
    }

    @Override
    public <T> T jSONcast(Class<T> clazz, File jsonInFile) throws IOException {
        //JSON from file to Object
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonInFile, clazz);
    }

    @Override
    public <T> T jSONcast(Class<T> clazz, URL jsonInURL) throws IOException {
        //JSON from URL to Object
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonInURL, clazz);
    }

    @Override
    public <T> T cast(Class<T> clazz, Object object) {
        try {
            T castedObject = null;
            if (object == null) {
                return null;
            }
            if (clazz.isInstance(object)) {
                castedObject = clazz.cast(object);
            } else {
                String val = String.valueOf(object);
                castedObject = newInstance(clazz, val);
            }
            return castedObject;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private <T> T newInstance(Class<T> clazz, String val) {
        if (clazz.isAssignableFrom(BigDecimal.class)) {
            return (T) new BigDecimal(val);
        } else if (clazz.isAssignableFrom(Integer.class)) {
            return (T) new Integer(val);
        } else if (clazz.isAssignableFrom(Date.class)) {
            return (T) common.parseDate(val);
        }
        return (T) val;
    }

    @Override
    public JSONObject parseJson(String jsonString) {
        try {
            return new JSONObject(jsonString);
        } catch (JSONException ex) {
            return null;
        }
    }

}
