<%-- 
    Document   : update-sell-purchased-shares
    Created on : Aug,9 2018, 9:47:52 AM
    Author     : Maninderjit Singh
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="update-sell-purchased-shares" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="post" action="./" id="update-sell-purchased-shares-form">
            <div class="modal-content">
                <div class="modal-header ">
                    <div class="modal-heading">
                        <h5 class="modal-title txnTitle" id="txnTitle" style="display:inline-block">
                            Purchase Shares/ Coins 
                        </h5>
                        <h5 class="modal-title fundName" style="display:inline-block"></h5>
                    </div>
                    <input type="hidden" id="update-sell-purchased-shares-action"/>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Customer Id
                                                    </th>
                                                    <th>
                                                        Customer Name
                                                    </th>
                                                    <th>
                                                        Percentage
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th class="options">
                                                        Options
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select class="form-control" id="update-sell-purchased-shares-fundPrices"></select> 
                                                    </td>
                                                    <td class="refId">
                                                    </td>
                                                    <td class="customerName">
                                                    </td>
                                                    <td class="percentage">
                                                    </td>
                                                    <td class="investmentAmount">
                                                    </td>
                                                    <td class="options">
                                                        <select class="form-control" name="sellOption">
                                                            <option value="SELL_TO_MARKET">Sell to Market</option>
                                                            <option value="SELL_TO_MANAGEMENT">Sell to Management</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Local Currency
                                                    </th>
                                                    <th>
                                                        N.A.V
                                                    </th>
                                                    <th>
                                                        US Dollars
                                                    </th>
                                                    <th>
                                                        Percentage
                                                    </th>
                                                    <th>
                                                        Invsta Units
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control localInvestmentAmount" name="local" id="update-sell-purchased-shares-local"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="nav"  id="update-sell-purchased-shares-nav"/>
                                                        <input type="hidden" name="createdDate"  id="update-sell-purchased-shares-date"/>
                                                        <input type="hidden" name="currency"  id="update-sell-purchased-shares-currency"/>
                                                        <input type="hidden" name="USD_LOCAL"  id="update-sell-purchased-shares-USD_LOCAL"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control shareAmountTotal2" name="usd"  id="update-sell-purchased-shares-usd"  onkeyup="usd2unit2();" readonly/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" class="percentage"/>
                                                        <input type="text" class="form-control percentageTotal2 percentage-text" id="update-sell-purchased-shares-percentage" readonly/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="minto" id="update-sell-purchased-shares-minto"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Share/Coin
                                                    </th>
                                                    <th>
                                                        Percentage
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th>
                                                        Price
                                                    </th>
                                                    <th>
                                                        Quantity 
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="purchasedSharesTable">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary" id="update-sell-purchased-shares-submit" type="button">Proceed</button>
                    </div>
                </div>
            </div>
        </form>    
    </div>
</div>