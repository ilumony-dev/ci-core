<%-- 
    Document   : updateshareprice
    Created on : Oct 15, 2017, 9:50:14 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="updateshareprice" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Update Share Price
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./admin-update-share-price" method="post">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Share Name</label>
                                <!--<input class="form-control" placeholder="Enter Share Name" type="text" name="name" id="name"/>-->
                                <select class="form-control" id="shareId" name="shareId">
                                    <option value="0" selected="true">-Select Share-</option>
                                    <c:forEach items="${shares}" var="share">
                                        <option value="${share.shareId}">${share.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="price">Share Price</label>
                                <input class="form-control" placeholder="Enter Updated Share Price" type="number" step="0.0001" name="price" id="price"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer"> 
                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>