<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <!--<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>-->
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script>
            function mobileMenu() {
                var element = document.getElementById("hamburger-1");
                element.classList.toggle("is-active");
                var x = document.getElementsByClassName("menu-and-user")[0];
                if (x.style.display === "block") {
                    x.style.display = "none";
                } else {
                    x.style.display = "none";
                    x.style.display = "block";
                }
            }
        </script>
        <script>
            !function () {
            var a = window.VL = window.VL || {};
            return a.instances = a.instances || {}, a.invoked ? void(window.console && console.error && console.error("VL snippet loaded twice.")) : (a.invoked = !0, void(a.load = function (b, c, d) {
            var e = {};
            e.publicToken = b, e.config = c || {};
            var f = document.createElement("script");
            f.type = "text/javascript", f.id = "vrlps-js", f.async = !0, f.src = "https://app.viral-loops.com/client/vl/vl.min.js";
            var g = document.getElementsByTagName("script")[0];
            return g.parentNode.insertBefore(f, g), f.onload = function () {
            a.setup(e), a.instances[b] = e, d && "function" == typeof d && d(null, e)
            }, e.identify = e.identify || function (a, b) {
            e.afterLoad = {identify: {userData: a, cb: b}}
            }, e.pendingEvents = [], e.track = e.track || function (a, b) {
            e.pendingEvents.push({event: a, cb: b})
            }, e
            }))
            }();
            var campaign = VL.load("qsUUK4cUR0BNRWEHa8aMbrtbSeg");
            campaign.identify({
            "firstname": "${fullName}",
                    "lastname": "",
                    "email": "${email}",
                    "createdAt": ${createdTime} //Creation date of your user in UNIX timestamp format. Leave empty for new users.
            }, function (err, details) {
            campaign.widgets.load();
            });
        </script>
        <script type="text/javascript">
            window.onload = function () {
            var frame0 = document.getElementsByClassName("vl-frame")[0];
            var frame1 = document.getElementsByClassName("vl-frame")[1];
            frame0.parentNode.removeChild(frame0);
            frame1.parentNode.removeChild(frame1);
            }
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Referral</span>
                        </li>
                        <!--            <li class="breadcrumb-item">
                                      <span>Laptop with retina screen</span>
                                    </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-box">
                                        <div data-vl-widget="sharingWidget"></div>
                                        <div data-vl-widget="rewardStats"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>  
        <jsp:include page="footer.jsp"/>
    </body>
</html>
