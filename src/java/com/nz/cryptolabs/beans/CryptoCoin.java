/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import java.util.Date;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
public class CryptoCoin implements ToObjectConverter {

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the uniqueId
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * @param uniqueId the uniqueId to set
     */
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * @return the usdPrice
     */
    public double getUsdPrice() {
        return usdPrice;
    }

    /**
     * @param usdPrice the usdPrice to set
     */
    public void setUsdPrice(double usdPrice) {
        this.usdPrice = usdPrice;
    }

    /**
     * @return the eurPrice
     */
    public double getEurPrice() {
        return eurPrice;
    }

    /**
     * @param eurPrice the eurPrice to set
     */
    public void setEurPrice(double eurPrice) {
        this.eurPrice = eurPrice;
    }

    /**
     * @return the btcPrice
     */
    public double getBtcPrice() {
        return btcPrice;
    }

    /**
     * @param btcPrice the btcPrice to set
     */
    public void setBtcPrice(double btcPrice) {
        this.btcPrice = btcPrice;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the percent1hr
     */
    public double getPercent1hr() {
        return percent1hr;
    }

    /**
     * @param percent1hr the percent1hr to set
     */
    public void setPercent1hr(double percent1hr) {
        this.percent1hr = percent1hr;
    }

    /**
     * @return the percent24hr
     */
    public double getPercent24hr() {
        return percent24hr;
    }

    /**
     * @param percent24hr the percent24hr to set
     */
    public void setPercent24hr(double percent24hr) {
        this.percent24hr = percent24hr;
    }

    /**
     * @return the percent7d
     */
    public double getPercent7d() {
        return percent7d;
    }

    /**
     * @param percent7d the percent7d to set
     */
    public void setPercent7d(double percent7d) {
        this.percent7d = percent7d;
    }

    private int id;
    private String uniqueId;
    private String name;
    private String symbol;
    private String rank;
    private double usdPrice;
    private double eurPrice;
    private double btcPrice;
    private Date lastUpdated;
    private double percent1hr;
    private double percent24hr;
    private double percent7d;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CryptoCoin.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
