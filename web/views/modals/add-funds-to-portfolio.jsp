<%-- 
    Document   : add-funds-to-portfolio
    Created on : Apr 04, 2018, 9:49:26 AM
    Author     : Anurag
--%>


<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add-funds-to-portfolio" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add-funds-to-portfolio-title">
                    Add to this investment <br>Available Invsta cash balance: 
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <p id="add-funds-to-portfolio-para" style="padding:20px; font-weight:500;">
                To add money to this investment, you must have sufficient cash available in your Invsta account (current balance shown above). To add money to your Invsta account, please go to Cash & Transactions via the main menu
            </p>
            <form action="./via-wallet-add-investment" method="POST" id="add-funds-to-portfolio-form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="fundId" id="add-funds-to-portfolio-fundId"/>
                                <input type="hidden" name="walletBalance" id="add-funds-to-portfolio-walletBalance" value=""/>
                                <input type="hidden" name="investmentId" id="add-funds-to-portfolio-investmentId" value=""/>
                                <input type="hidden" name="fundName" id="add-funds-to-portfolio-fundName" value=""/>
                                <label for="investmentAmount" id="add-funds-to-portfolio-lbl-amount" style="display:block; margin:0">Amount:</label>
                                <small id="add-funds-to-portfolio-lbl-note"></small>
                                <input type="number" id="add-funds-to-portfolio-investmentAmount" name="investmentAmount" class="form-control" step="0.01" placeholder="Enter amount in US$ " tabindex="-1"/>
                                <input type="hidden" id="add-funds-to-portfolio-localInvestmentAmount" name="localInvestmentAmount"/>
                                <input type="hidden" id="add-funds-to-portfolio-localCurrency" name="currency"/>
                                <input type="hidden" name="regularlyAmount" value="0"/>
                                <input type="hidden" name="years" value="10"/>
                                <input type="hidden" name="timeFrame" value="FIXED"/>        
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description" id="add-funds-to-portfolio-lbl-description">Optional: Reference for additional investment</label>
                                <textarea class="form-control" type="text" name="description" id="add-funds-to-portfolio-description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn btn-primary" id="add-funds-to-portfolio-submit" type="button">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>