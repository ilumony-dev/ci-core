/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.controllers.payments.ChargeRequest;
import com.stripe.exception.StripeException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface DatabaseService {

    public List<ShareFund> fundActualShares(String fundId, String investmentAmount);

    public List<ShareFund> investmentActualDetails(String investmentId, String reqId);

    public List<ShareFund> sharesByInvestment(String investmentId, String amountParam, String percentageParam);

    public List<ShareFund> actualSharesByAmount(String investmentId, String userId, String investmentAmount);

    public List<StateCity> countries(Boolean states, Boolean cities);

    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId, String currency);

    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId, String currency);

    public List<InvestmentBean> purchasedInvestments(String userId, String investmentId, String fundId, String currency);

    public List<InvestmentBean> coinsByInvestments(String userId, String investmentId, Boolean total, String fundId);

    public Map<String, Object> saveStripeCharge(ChargeRequest chargeRequest, String userId) throws StripeException;

    public ShareFund findFundPortfolio(String fundId);

    public List<ShareFund> totalCoins(String userId, String investmentId);

    public Map<Date, BigDecimal> latestBalanceData(String type, String userId, String investmentId);

    public List<ShareFund> detailsByInvestment(String userId, String investmentId, String action);

    public List<ShareFund> totalUnitsById(String fundId, String userId, String investmentId);

    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate);

    public ObjectNode rateForPair(String currency0, String currency1, Long timestamp) throws IOException;

    public List<PortfolioBean> portfolioReportById(String portfolioId);

    public List<PortfolioBean> portfoliosReport(List<String> days);

    public BigDecimal currencyConversionRate(String currencyPair, String timestamp) throws IOException;//conversion rate

    public void addTrade(ShareFund trade);

    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals);

    public void saveTodayUpdates(String formattedate);

    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate, Boolean sum, String currency);

    public void saveSellInvestmentRequest(AccountEventCommand request);

    public List<InvestmentBean> pendingInvestments(String userId);

    public List<InvestmentBean> pendingSellTransactions(String userId);
}
