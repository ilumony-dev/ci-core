/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// format the number with comma seprated and round up of last value
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

Number.prototype.roundTo = function (d) {
    var n = this, neg = false;
    if (d === undefined) {d = 0;}
    if (n < 0) {
        neg = true;
        n = n * -1;
    }
    var m = Math.pow(10, d);
    n = parseFloat((n * m).toFixed(16));
    n = (Math.round(n) / m).toFixed(d);
    if (neg) {
        n = (n * -1);
    }
    return n;
};
