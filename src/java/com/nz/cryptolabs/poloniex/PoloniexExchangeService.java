/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.poloniex;

import com.nz.cryptolabs.mail.SendGridMailManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.poloniex.PoloniexExchange;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;
import org.knowm.xchange.service.account.AccountService;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.knowm.xchange.service.trade.TradeService;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author Maninderjit
 */
@Service
public class PoloniexExchangeService {

    static String useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11";
    static String username = "unit";
    static String password = "";
    static String apikey = "68FP8MAS-W5N6X3EC-V0JY8Q5F-SGD9F2UB";
    static String secretkey = "7015ccd198d8aa5413e3640023975992a5f3a1014fe6f0ab794d2172ef7c101113649d36ae07bb9c4aeedd6dd3ac894b31842a2465dc80d43fe7529967686fb3";
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PoloniexExchangeService.class);

    public void main() throws Exception {
        Exchange poloniex = null;
        poloniex = ExchangeFactory.INSTANCE.createExchange(PoloniexExchange.class.getName());
        MarketDataService marketDataService = poloniex.getMarketDataService();
        Ticker ticker = marketDataService.getTicker(CurrencyPair.BTC_USD);
        logger.debug("Ticker of BTC-USD-> " + ticker.toString());
        ExchangeSpecification exSpec = new PoloniexExchange().getDefaultExchangeSpecification();
        exSpec.setUserName(username);
        exSpec.setApiKey(apikey);
        exSpec.setSecretKey(secretkey);
        poloniex = ExchangeFactory.INSTANCE.createExchange(exSpec);
        logger.debug("UserName-->" + username);
        logger.debug("ApiKey-->" + apikey);
        logger.debug("Secret-->" + secretkey);
        AccountService accountService = poloniex.getAccountService();
        AccountInfo accountInfo = accountService.getAccountInfo();
        logger.debug("Account info" + accountInfo.toString());
        TradeService tradeService = poloniex.getTradeService();
        UserTrades tradeHistory = tradeService.getTradeHistory(tradeService.createTradeHistoryParams());
        List<UserTrade> userTrades = tradeHistory.getUserTrades();
//        for (UserTrade userTrade : userTrades) {
//             userTrade.
//        }
    }

    public String main2() throws Exception {
        String accessPublic = accessPublic();
        logger.debug("Public info" + accessPublic);
        return accessPublic;
//        String accessPoloniex = accessPoloniex();
//        logger.debug("Account info" + accessPoloniex);
//        return accessPoloniex;
    }

    public String returnBalances() {
        return "";
    }

    public String returnCompleteBalances() {
        return "";
    }

    public String returnDepositAddresses() {
        return "";
    }

    public String generateNewAddress() {
        return "";
    }

    public String returnDepositsWithdrawals() {
        return "";
    }

    public String returnOpenOrders() {
        return "";
    }

    public String returnTradeHistory() {
        return "[{ \"globalTradeID\": 25129732, \"tradeID\": \"6325758\", \"date\": \"2016-04-05 08:08:40\", \"rate\": \"0.02565498\", \"amount\": \"0.10000000\", \"total\": \"0.00256549\", \"fee\": \"0.00200000\", \"orderNumber\": \"34225313575\", \"type\": \"sell\", \"category\": \"exchange\" }, { \"globalTradeID\": 25129628, \"tradeID\": \"6325741\", \"date\": \"2016-04-05 08:07:55\", \"rate\": \"0.02565499\", \"amount\": \"0.10000000\", \"total\": \"0.00256549\", \"fee\": \"0.00200000\", \"orderNumber\": \"34225195693\", \"type\": \"buy\", \"category\": \"exchange\" }]";
    }

    public String returnOrderTrades() {
        return "";
    }

    public String buy() {
        return "";
    }

    public String sell() {
        return "";
    }

    public String cancelOrder() {
        return "";
    }

    public String moveOrder() {
        return "";
    }

    public String withdraw() {
        return "";
    }

    public String returnFeeInfo() {
        return "";
    }

    public String returnAvailableAccountBalances() {
        return "";
    }

    public String returnTradableBalances() {
        return "";
    }

    public String transferBalance() {
        return "";
    }

    public String returnMarginAccountSummary() {
        return "";
    }

    public static final String accessPublic() throws IOException {
        String connectionString = "https://www.poloniex.com/public?command=returnTicker";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet req = new HttpGet(connectionString);
        req.addHeader("User-Agent", useragent);
        CloseableHttpResponse response = null;
        response = httpClient.execute(req);
        logger.debug("Status Line", response.getStatusLine());
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity);
    }

    public static final String accessPoloniex() throws IOException {
        String nonce = String.valueOf(System.currentTimeMillis());
        String connectionString = "https://www.poloniex.com/tradingApi";
        String queryArgs = "command=returnBalances&nonce=" + nonce;
        String hmac512 = hmac512Digest(queryArgs, secretkey);
        // Produce the output
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.append(queryArgs);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost req = new HttpPost(connectionString);
        req.addHeader("User-Agent", useragent);
        req.addHeader("Key", apikey);
        req.addHeader("Sign", hmac512);
        req.setEntity(new ByteArrayEntity(out.toByteArray()));
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("command", "returnBalances"));
        params.add(new BasicNameValuePair("nonce", nonce));
        CloseableHttpResponse response = null;
        req.setEntity(new UrlEncodedFormEntity(params));
        response = httpClient.execute(req);
        logger.debug("Status Line",response.getStatusLine());
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity);
    }

    public static String hmac512Digest(String queryArgs, String secretkey) {
        try {
            Mac shaMac = Mac.getInstance("HmacSHA512");
            SecretKeySpec keySpec = new SecretKeySpec(secretkey.getBytes(), "HmacSHA512");
            shaMac.init(keySpec);
            final byte[] macData = shaMac.doFinal(queryArgs.getBytes());
            return Hex.encodeHexString(macData); //again with try/catch for InvalidKeyException
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }
}
