package com.nz.cryptolabs.mail;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.FileUtility;
import com.nz.cryptolabs.components.RandomStringGenerator;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DateTimeService;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
@Service
public class SendGridMailManager implements MailManager {

    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private String domain;
    @Autowired
    private String adminEmailId;
    @Autowired
    private FileUtility fileUtility;
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private String mailFolder;

    final private String SENDGRID_APIKEY = "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";
    final private String SUBJECT = "Message from Invsta";
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SendGridMailManager.class);

    @Override
    public void sendConfirmationMailToEmail(String firstName, String to, String refId, String token) {
        String link = domain + "/user-confirm-email?token=" + token + "&rId=" + refId;
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "allocateEmail.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var customerName", firstName);
        htmlMessage = htmlMessage.replace("var link", link);
        sendMail(to, adminEmailId, "Confirmation Email", htmlMessage);
    }

    @Override
    public void sendWelcomeToNewClient(String to, String firstName) {
        String htmlMessage = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "	<head>\n"
                + "		<title>Invsta</title>\n"
                + "		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
                + "		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                + "		<style type=\"text/css\">\n"
                + "			a{\n"
                + "				outline:none;\n"
                + "				color:#7c4dff;\n"
                + "				text-decoration:none;\n"
                + "			}\n"
                + "			a:hover{text-decoration:none !important;}\n"
                + "			a[x-apple-data-detectors]{color:inherit !important; text-decoration:none !important;}\n"
                + "			.active:hover{opacity:0.8;}\n"
                + "			.active{transition:all 0.3s ease;}\n"
                + "			.active-d, .active-l{overflow:hidden;}\n"
                + "			.active-d a, .active-l a{position:relative; display:inline-block;}\n"
                + "			.active-d a:after, .active-l a:after{transition:all 0.3s ease; content:\"\"; position:absolute; left:0; top:0; right:0; bottom:0;}\n"
                + "			.active-d a:hover:after{background:rgba(0,0,0,0.2);}\n"
                + "			.active-l a:hover:after{background:rgba(255,255,255,0.2);}\n"
                + "			a img{border:none;}\n"
                + "			table td{mso-line-height-rule:exactly;}\n"
                + "			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}\n"
                + "			@media only screen and (max-width:500px) {\n"
                + "				/* default style */\n"
                + "				table[class~=\"flexible\"]{width:100% !important;}\n"
                + "				table[class~=\"table-center\"]{float:none !important; margin:0 auto !important; width:auto !important;}\n"
                + "				span[class~=\"db\"]{display:block !important;}\n"
                + "				td[class~=\"img-flex\"] img{width:100% !important; height:auto !important;}\n"
                + "				td[class~=\"aligncenter\"]{text-align:center !important;}\n"
                + "				tr[class~=\"table-holder\"]{display:table !important; width:100% !important;}\n"
                + "				th[class~=\"tcap\"]{display:table-caption !important; width:100% !important;}\n"
                + "				th[class~=\"thead\"]{display:table-header-group !important; width:100% !important;}\n"
                + "				th[class~=\"trow\"]{display:table-row !important; width:100% !important;}\n"
                + "				th[class~=\"tfoot\"]{display:table-footer-group !important; width:100% !important;}\n"
                + "				th[class~=\"flex\"]{display:block !important; width:100% !important;}\n"
                + "				*[class~=\"hide\"]{display:none !important; width:0 !important; height:0 !important; padding:0 !important; font-size:0 !important; line-height:0 !important;} *[class~=\"w-a\"]{width:auto !important;} *[class~=\"h-a\"]{height:auto !important;} *[class~=\"p-0\"]{padding:0 !important;} *[class~=\"p-5\"]{padding:5px !important;} *[class~=\"p-10\"]{padding:10px !important;} *[class~=\"p-15\"]{padding:15px !important;} *[class~=\"p-20\"]{padding:20px !important;} *[class~=\"p-25\"]{padding:25px !important;} *[class~=\"p-30\"]{padding:30px !important;} *[class~=\"plr-0\"]{padding-left:0 !important; padding-right:0 !important;} *[class~=\"plr-5\"]{padding-left:5px !important; padding-right:5px !important;} *[class~=\"plr-10\"]{padding-left:10px !important; padding-right:10px !important;} *[class~=\"plr-15\"]{padding-left:15px !important; padding-right:15px !important;} *[class~=\"plr-20\"]{padding-left:20px !important; padding-right:20px !important;} *[class~=\"plr-25\"]{padding-left:25px !important; padding-right:25px !important;} *[class~=\"plr-30\"]{padding-left:30px !important; padding-right:30px !important;} *[class~=\"pl-0\"]{padding-left:0 !important;} *[class~=\"pl-5\"]{padding-left:5px !important;} *[class~=\"pl-10\"]{padding-left:10px !important;} *[class~=\"pl-15\"]{padding-left:15px !important;} *[class~=\"pl-20\"]{padding-left:20px !important;} *[class~=\"pl-25\"]{padding-left:25px !important;} *[class~=\"pl-30\"]{padding-left:30px !important;} *[class~=\"pr-0\"]{padding-right:0 !important;} *[class~=\"pr-5\"]{padding-right:5px !important;} *[class~=\"pr-10\"]{padding-right:10px !important;} *[class~=\"pr-15\"]{padding-right:15px !important;} *[class~=\"pr-20\"]{padding-right:20px !important;} *[class~=\"pr-25\"]{padding-right:25px !important;} *[class~=\"pr-30\"]{padding-right:30px !important;} *[class~=\"pt-0\"]{padding-top:0 !important;} *[class~=\"pt-1\"]{padding-top:1px !important;} *[class~=\"pt-2\"]{padding-top:2px !important;} *[class~=\"pt-3\"]{padding-top:3px !important;} *[class~=\"pt-4\"]{padding-top:4px !important;} *[class~=\"pt-5\"]{padding-top:5px !important;} *[class~=\"pt-6\"]{padding-top:6px !important;} *[class~=\"pt-7\"]{padding-top:7px !important;} *[class~=\"pt-8\"]{padding-top:8px !important;} *[class~=\"pt-9\"]{padding-top:9px !important;} *[class~=\"pt-10\"]{padding-top:10px !important;} *[class~=\"pt-11\"]{padding-top:11px !important;} *[class~=\"pt-12\"]{padding-top:12px !important;} *[class~=\"pt-13\"]{padding-top:13px !important;} *[class~=\"pt-14\"]{padding-top:14px !important;} *[class~=\"pt-15\"]{padding-top:15px !important;} *[class~=\"pt-16\"]{padding-top:16px !important;} *[class~=\"pt-17\"]{padding-top:17px !important;} *[class~=\"pt-18\"]{padding-top:18px !important;} *[class~=\"pt-19\"]{padding-top:19px !important;} *[class~=\"pt-20\"]{padding-top:20px !important;} *[class~=\"pt-21\"]{padding-top:21px !important;} *[class~=\"pt-22\"]{padding-top:22px !important;} *[class~=\"pt-23\"]{padding-top:23px !important;} *[class~=\"pt-24\"]{padding-top:24px !important;} *[class~=\"pt-25\"]{padding-top:25px !important;} *[class~=\"pt-26\"]{padding-top:26px !important;} *[class~=\"pt-27\"]{padding-top:27px !important;} *[class~=\"pt-28\"]{padding-top:28px !important;} *[class~=\"pt-29\"]{padding-top:29px !important;} *[class~=\"pt-30\"]{padding-top:30px !important;} *[class~=\"pt-31\"]{padding-top:31px !important;} *[class~=\"pt-32\"]{padding-top:32px !important;} *[class~=\"pt-33\"]{padding-top:33px !important;} *[class~=\"pt-34\"]{padding-top:34px !important;} *[class~=\"pt-35\"]{padding-top:35px !important;} *[class~=\"pt-36\"]{padding-top:36px !important;} *[class~=\"pt-37\"]{padding-top:37px !important;} *[class~=\"pt-38\"]{padding-top:38px !important;} *[class~=\"pt-39\"]{padding-top:39px !important;} *[class~=\"pt-40\"]{padding-top:40px !important;} *[class~=\"pt-41\"]{padding-top:41px !important;} *[class~=\"pt-42\"]{padding-top:42px !important;} *[class~=\"pt-43\"]{padding-top:43px !important;} *[class~=\"pt-44\"]{padding-top:44px !important;} *[class~=\"pt-45\"]{padding-top:45px !important;} *[class~=\"pt-46\"]{padding-top:46px !important;} *[class~=\"pt-47\"]{padding-top:47px !important;} *[class~=\"pt-48\"]{padding-top:48px !important;} *[class~=\"pt-49\"]{padding-top:49px !important;} *[class~=\"pt-50\"]{padding-top:50px !important;} *[class~=\"pb-0\"]{padding-bottom:0 !important;} *[class~=\"pb-1\"]{padding-bottom:1px !important;} *[class~=\"pb-2\"]{padding-bottom:2px !important;} *[class~=\"pb-3\"]{padding-bottom:3px !important;} *[class~=\"pb-4\"]{padding-bottom:4px !important;} *[class~=\"pb-5\"]{padding-bottom:5px !important;} *[class~=\"pb-6\"]{padding-bottom:6px !important;} *[class~=\"pb-7\"]{padding-bottom:7px !important;} *[class~=\"pb-8\"]{padding-bottom:8px !important;} *[class~=\"pb-9\"]{padding-bottom:9px !important;} *[class~=\"pb-10\"]{padding-bottom:10px !important;} *[class~=\"pb-11\"]{padding-bottom:11px !important;} *[class~=\"pb-12\"]{padding-bottom:12px !important;} *[class~=\"pb-13\"]{padding-bottom:13px !important;} *[class~=\"pb-14\"]{padding-bottom:14px !important;} *[class~=\"pb-15\"]{padding-bottom:15px !important;} *[class~=\"pb-16\"]{padding-bottom:16px !important;} *[class~=\"pb-17\"]{padding-bottom:17px !important;} *[class~=\"pb-18\"]{padding-bottom:18px !important;} *[class~=\"pb-19\"]{padding-bottom:19px !important;} *[class~=\"pb-20\"]{padding-bottom:20px !important;} *[class~=\"pb-21\"]{padding-bottom:21px !important;} *[class~=\"pb-22\"]{padding-bottom:22px !important;} *[class~=\"pb-23\"]{padding-bottom:23px !important;} *[class~=\"pb-24\"]{padding-bottom:24px !important;} *[class~=\"pb-25\"]{padding-bottom:25px !important;} *[class~=\"pb-26\"]{padding-bottom:26px !important;} *[class~=\"pb-27\"]{padding-bottom:27px !important;} *[class~=\"pb-28\"]{padding-bottom:28px !important;} *[class~=\"pb-29\"]{padding-bottom:29px !important;} *[class~=\"pb-30\"]{padding-bottom:30px !important;} *[class~=\"pb-31\"]{padding-bottom:31px !important;} *[class~=\"pb-32\"]{padding-bottom:32px !important;} *[class~=\"pb-33\"]{padding-bottom:33px !important;} *[class~=\"pb-34\"]{padding-bottom:34px !important;} *[class~=\"pb-35\"]{padding-bottom:35px !important;} *[class~=\"pb-36\"]{padding-bottom:36px !important;} *[class~=\"pb-37\"]{padding-bottom:37px !important;} *[class~=\"pb-38\"]{padding-bottom:38px !important;} *[class~=\"pb-39\"]{padding-bottom:39px !important;} *[class~=\"pb-40\"]{padding-bottom:40px !important;} *[class~=\"pb-41\"]{padding-bottom:41px !important;} *[class~=\"pb-42\"]{padding-bottom:42px !important;} *[class~=\"pb-43\"]{padding-bottom:43px !important;} *[class~=\"pb-44\"]{padding-bottom:44px !important;} *[class~=\"pb-45\"]{padding-bottom:45px !important;} *[class~=\"pb-46\"]{padding-bottom:46px !important;} *[class~=\"pb-47\"]{padding-bottom:47px !important;} *[class~=\"pb-48\"]{padding-bottom:48px !important;} *[class~=\"pb-49\"]{padding-bottom:49px !important;} *[class~=\"pb-50\"]{padding-bottom:50px !important;} *[class~=\"fs-0\"]{font-size:0 !important;} *[class~=\"fs-1\"]{font-size:1px !important;} *[class~=\"fs-2\"]{font-size:2px !important;} *[class~=\"fs-3\"]{font-size:3px !important;} *[class~=\"fs-4\"]{font-size:4px !important;} *[class~=\"fs-5\"]{font-size:5px !important;} *[class~=\"fs-6\"]{font-size:6px !important;} *[class~=\"fs-7\"]{font-size:7px !important;} *[class~=\"fs-8\"]{font-size:8px !important;} *[class~=\"fs-9\"]{font-size:9px !important;} *[class~=\"fs-10\"]{font-size:10px !important;} *[class~=\"fs-11\"]{font-size:11px !important;} *[class~=\"fs-12\"]{font-size:12px !important;} *[class~=\"fs-13\"]{font-size:13px !important;} *[class~=\"fs-14\"]{font-size:14px !important;} *[class~=\"fs-15\"]{font-size:15px !important;} *[class~=\"fs-16\"]{font-size:16px !important;} *[class~=\"fs-17\"]{font-size:17px !important;} *[class~=\"fs-18\"]{font-size:18px !important;} *[class~=\"fs-19\"]{font-size:19px !important;} *[class~=\"fs-20\"]{font-size:20px !important;} *[class~=\"fs-21\"]{font-size:21px !important;} *[class~=\"fs-22\"]{font-size:22px !important;} *[class~=\"fs-23\"]{font-size:23px !important;} *[class~=\"fs-24\"]{font-size:24px !important;} *[class~=\"fs-25\"]{font-size:25px !important;} *[class~=\"fs-26\"]{font-size:26px !important;} *[class~=\"fs-27\"]{font-size:27px !important;} *[class~=\"fs-28\"]{font-size:28px !important;} *[class~=\"fs-29\"]{font-size:29px !important;} *[class~=\"fs-30\"]{font-size:30px !important;} *[class~=\"fs-31\"]{font-size:31px !important;} *[class~=\"fs-32\"]{font-size:32px !important;} *[class~=\"fs-33\"]{font-size:33px !important;} *[class~=\"fs-34\"]{font-size:34px !important;} *[class~=\"fs-35\"]{font-size:35px !important;} *[class~=\"fs-36\"]{font-size:36px !important;} *[class~=\"fs-37\"]{font-size:37px !important;} *[class~=\"fs-38\"]{font-size:38px !important;} *[class~=\"fs-39\"]{font-size:39px !important;} *[class~=\"fs-40\"]{font-size:40px !important;} *[class~=\"fs-41\"]{font-size:41px !important;} *[class~=\"fs-42\"]{font-size:42px !important;} *[class~=\"fs-43\"]{font-size:43px !important;} *[class~=\"fs-44\"]{font-size:44px !important;} *[class~=\"fs-45\"]{font-size:45px !important;} *[class~=\"fs-46\"]{font-size:46px !important;} *[class~=\"fs-47\"]{font-size:47px !important;} *[class~=\"fs-48\"]{font-size:48px !important;} *[class~=\"fs-49\"]{font-size:49px !important;} *[class~=\"fs-50\"]{font-size:50px !important;} *[class~=\"lh-0\"]{line-height:0 !important;} *[class~=\"lh-1\"]{line-height:1px !important;} *[class~=\"lh-2\"]{line-height:2px !important;} *[class~=\"lh-3\"]{line-height:3px !important;} *[class~=\"lh-4\"]{line-height:4px !important;} *[class~=\"lh-5\"]{line-height:5px !important;} *[class~=\"lh-6\"]{line-height:6px !important;} *[class~=\"lh-7\"]{line-height:7px !important;} *[class~=\"lh-8\"]{line-height:8px !important;} *[class~=\"lh-9\"]{line-height:9px !important;} *[class~=\"lh-10\"]{line-height:10px !important;} *[class~=\"lh-11\"]{line-height:11px !important;} *[class~=\"lh-12\"]{line-height:12px !important;} *[class~=\"lh-13\"]{line-height:13px !important;} *[class~=\"lh-14\"]{line-height:14px !important;} *[class~=\"lh-15\"]{line-height:15px !important;} *[class~=\"lh-16\"]{line-height:16px !important;} *[class~=\"lh-17\"]{line-height:17px !important;} *[class~=\"lh-18\"]{line-height:18px !important;} *[class~=\"lh-19\"]{line-height:19px !important;} *[class~=\"lh-20\"]{line-height:20px !important;} *[class~=\"lh-21\"]{line-height:21px !important;} *[class~=\"lh-22\"]{line-height:22px !important;} *[class~=\"lh-23\"]{line-height:23px !important;} *[class~=\"lh-24\"]{line-height:24px !important;} *[class~=\"lh-25\"]{line-height:25px !important;} *[class~=\"lh-26\"]{line-height:26px !important;} *[class~=\"lh-27\"]{line-height:27px !important;} *[class~=\"lh-28\"]{line-height:28px !important;} *[class~=\"lh-29\"]{line-height:29px !important;} *[class~=\"lh-30\"]{line-height:30px !important;} *[class~=\"lh-31\"]{line-height:31px !important;} *[class~=\"lh-32\"]{line-height:32px !important;} *[class~=\"lh-33\"]{line-height:33px !important;} *[class~=\"lh-34\"]{line-height:34px !important;} *[class~=\"lh-35\"]{line-height:35px !important;} *[class~=\"lh-36\"]{line-height:36px !important;} *[class~=\"lh-37\"]{line-height:37px !important;} *[class~=\"lh-38\"]{line-height:38px !important;} *[class~=\"lh-39\"]{line-height:39px !important;} *[class~=\"lh-40\"]{line-height:40px !important;} *[class~=\"lh-41\"]{line-height:41px !important;} *[class~=\"lh-42\"]{line-height:42px !important;} *[class~=\"lh-43\"]{line-height:43px !important;} *[class~=\"lh-44\"]{line-height:44px !important;} *[class~=\"lh-45\"]{line-height:45px !important;} *[class~=\"lh-46\"]{line-height:46px !important;} *[class~=\"lh-47\"]{line-height:47px !important;} *[class~=\"lh-48\"]{line-height:48px !important;} *[class~=\"lh-49\"]{line-height:49px !important;} *[class~=\"lh-50\"]{line-height:50px !important;}\n"
                + "				/* custom style */\n"
                + "				td[class~=\"br-0\"]{border-radius:0 !important;}\n"
                + "				td[class~=\"w-122\"] img{width:122px !important;}\n"
                + "				table[class~=\"w-100\"]{width:100px !important;}\n"
                + "				td[class~=\"plr-18\"]{padding-left:18px !important; padding-right:18px !important;}\n"
                + "			}\n"
                + "		</style>\n"
                + "	</head>\n"
                + "	<body bgcolor=\"#ffffff\" style=\"margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;\">\n"
                + "		<table bgcolor=\"#ffffff\" width=\"100%\" style=\"min-width:320px;margin-top: 18px;\" cellspacing=\"0\" cellpadding=\"0\">\n"
                + "			<!-- fix for gmail -->\n"
                + "			<tbody><tr>\n"
                + "				<td class=\"hide\" style=\"line-height:0;\"><div style=\"white-space:nowrap; font:15px/0 courier;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>\n"
                + "			</tr>\n"
                + "			<tr>\n"
                + "				<td>\n"
                + "					<table class=\"flexible\" width=\"590\" align=\"center\" style=\"margin:0 auto;\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "						<!-- fix for gmail -->\n"
                + "						<tbody><tr>\n"
                + "							<td class=\"hide\">\n"
                + "								<table width=\"590\" style=\"width:590px !important;\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "									<tbody><tr>\n"
                + "										<td style=\"min-width:590px; font-size:0; line-height:0;\">&nbsp;</td>\n"
                + "									</tr>\n"
                + "								</tbody></table>\n"
                + "							</td>\n"
                + "						</tr>\n"
                + "						<!-- header -->\n"
                + "						<tr>\n"
                + "							<td bgcolor=\"#7451d7\" background=\"https://www.invsta.com/img/banner/invsta-banner1.jpg\" style=\"background-image:url(https://www.invsta.com/img/banner/invsta-banner1.jpg); background-position:50% 0;\">\n"
                + "								<!--[if gte mso 9]>\n"
                + "									<v:rect xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"true\" stroke=\"false\" style=\"width:590px; height:357px;\">\n"
                + "										<v:fill type=\"tile\" src=\"http://s15.postimg.org/7gqq9lj4r/bg_header.jpg\" color=\"#7451d7\" />\n"
                + "										<v:textbox inset=\"0,0,0,0\">\n"
                + "											<![endif]-->\n"
                + "													<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "														<tbody><tr>\n"
                + "															<td class=\"hide\" width=\"23\" height=\"200\"></td>\n"
                + "															<td valign=\"bottom\">\n"
                + "																<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "																	<tbody><tr>\n"
                + "																		<td class=\"pt-23 pb-40 w-122\" align=\"center\" style=\"padding:0 0 27px;\">\n"
                + "																			<a target=\"_blank\" href=\"http://link\">\n"
                + "																				<img src=\"https://beta.invsta.com/resources/img/invsta-white-logo.png\" style=\"vertical-align:top; width:155px; font:bold 20px/40px Arial, Helvetica, sans-serif; color:#fff;\" width=\"155\" alt=\"Invsta\">\n"
                + "																			</a>\n"
                + "																		</td>\n"
                + "																	</tr>\n"
                + "																	<tr>\n"
                + "																		<td class=\"plr-15 fs-24 lh-32\" height=\"60\" valign=\"top\" align=\"center\" style=\"padding:0 0 0px; font:24px/27px Arial, Helvetica, sans-serif; color:#fff;     font-weight: 600;\">\n"
                + "																			Thanks for signing<br>\n"
                + "																								</td>\n"
                + "																	</tr>\n"
                + "																	<tr>\n"
                + "																		<td class=\"br-0br-0\" bgcolor=\"#ffffff\" align=\"center\" style=\"padding:22px 15px 16px; border-radius:5px 5px 0 0;\">\n"
                + "																			<img src=\"http://invsta-dev.azurewebsites.net/image.gif\" style=\"vertical-align:top; \" alt=\"\">\n"
                + "																		</td>\n"
                + "																	</tr>\n"
                + "																</tbody></table>\n"
                + "															</td>\n"
                + "															<td class=\"hide\" width=\"23\"></td>\n"
                + "														</tr>\n"
                + "													</tbody></table>\n"
                + "												<!--[if gte mso 9]>\n"
                + "										</v:textbox>\n"
                + "									</v:rect>\n"
                + "								<![endif]-->\n"
                + "							</td>\n"
                + "						</tr>\n"
                + "						<!-- main -->\n"
                + "						<tr>\n"
                + "							<td class=\"plr-0\" bgcolor=\"#f4f4f4\" style=\"padding:0 22px;\">\n"
                + "								<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "									<!-- content -->\n"
                + "									\n"
                + "									\n"
                + "									<!-- pre-footer -->\n"
                + "									<tbody><tr>\n"
                + "										<td class=\"plr-18 br-0\" bgcolor=\"#ffffff\" style=\"padding:19px 35px 16px; border-radius:0 0 5px 5px;\">\n"
                + "											<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "												<tbody><tr>\n"
                + "													<td align=\"left\" style=\"padding:0 0 16px; font:16px/22px Arial, Helvetica, sans-serif; color:#3e3e3e;\">\n"
                + "														Hi " + firstName + ",<br>\n"
                + "														Thanks for signing up with Invsta<br>\n"
                + "														We are happy to have you onboard .<br>\n"
                + "														Click here to learn more about us.<br>\n"
                + "														Thanks <br>\n"
                + "														Invsta\n"
                + "														<a href=\"https://beta.invsta.com/\" class=\"Confirm_mail\" style=\"background: #1d3448;padding: 11px;font-size: 22px;margin-top: 18px;text-transform:  capitalize;color: #fff;display:  block;max-width: 247px;border-radius: 4px;margin:  auto;text-align:  center;\">Learn More</a>\n"
                + "													</td>\n"
                + "												</tr>\n"
                + "																		\n"
                + "								\n"
                + "											</tbody></table>\n"
                + "										</td>\n"
                + "									</tr>\n"
                + "									<!-- footer -->\n"
                + "									<tr>\n"
                + "										<td align=\"center\" style=\"padding:13px 18px 25px; font:12px/16px Arial, Helvetica, sans-serif; color:#6b7378;\">\n"
                + "										<a style=\"color:#6b7378; text-decoration:none;\" href=\"http://link\">Terms and Conditions</a> 	\n"
                + "										</td>\n"
                + "									</tr>\n"
                + "								</tbody></table>\n"
                + "							</td>\n"
                + "						</tr>\n"
                + "					</tbody></table>\n"
                + "				</td>\n"
                + "			</tr>\n"
                + "		</tbody></table>\n"
                + "	\n"
                + "</body>\n"
                + "</html>";

        sendMail(to, adminEmailId, "Welcome to Invsta", htmlMessage);
    }

    @Override
    public void sendResetToUser(String email, String firstName) {
        org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder encoder = new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
        RandomStringGenerator generator = new RandomStringGenerator();
        String token = encoder.encode(email.concat(generator.generate(16)));
        String csrf = encoder.encode(new java.util.Date().toString());
        String link = domain + "/resetpwd?token=" + token + "&csrf=" + csrf;
        ExpiryDate date = new ExpiryDate();
        date.setEmail(email);
        Calendar cal = Calendar.getInstance(); // creates calendar
        Date now = dateTimeService.now();
        cal.setTime(now); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 2); // adds two hour
        date.setExpiryDate(cal.getTime());
        MailConstants.map.put(token, date);
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "resetpwd.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var link", link);
        sendMail(email, adminEmailId, "Reset Password", htmlMessage);
    }

    private String withdrawlNotification(RequestBean bean, String firstName) {
        UserInfo user = repository.findByUserId(bean.getUserId());
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "withdrawlNotificationFromWallet.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var fullName", firstName);
        htmlMessage = htmlMessage.replace("var customerName", user.getFullName());
        htmlMessage = htmlMessage.replace("var customerEmail", user.getEmail());
        htmlMessage = htmlMessage.replace("var customerId", user.getRefId());
        htmlMessage = htmlMessage.replace("var maxAmount", bean.getMaxAmount());
        htmlMessage = htmlMessage.replace("var accountNumber", bean.getAccount());
        htmlMessage = htmlMessage.replace("var accountHolderName", bean.getAccountName());
        htmlMessage = htmlMessage.replace("var bankName", bean.getBankName());
        htmlMessage = htmlMessage.replace("var description", bean.getDescription());
        htmlMessage = htmlMessage.replace("var localCurrency", common.currSymbol().get(bean.getCurrency()));
        htmlMessage = htmlMessage.replace("var localAmount", new BigDecimal(bean.getLocalAmount()).setScale(2, RoundingMode.HALF_UP).toPlainString());
        htmlMessage = htmlMessage.replace("var refId", bean.getRefId());

        return htmlMessage;
    }

    @Override
    public void sendWithdrawlNotificationToUser(String to, String name, RequestBean bean) {
        String htmlMessage = withdrawlNotification(bean, name);
        sendMail(to, adminEmailId, "Notification of Withdrawal", htmlMessage);
    }

    @Override
    public void sendWithdrawlNotificationToAdmin(String to, RequestBean bean) {
        String name = "Admin";
        String htmlMessage = withdrawlNotification(bean, name);
        sendMail(to, adminEmailId, "Notification of Withdrawal", htmlMessage);
    }

    private String addFundsToWalletNotification(RequestBean bean, String firstName) {
        UserInfo user = repository.findByUserId(bean.getUserId());
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "addFundsToWalletNotification.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var fullName", firstName);
        htmlMessage = htmlMessage.replace("var customerName", user.getFullName());
        htmlMessage = htmlMessage.replace("var customerEmail", user.getEmail());
        htmlMessage = htmlMessage.replace("var customerId", user.getRefId());
        htmlMessage = htmlMessage.replace("var localCurrency", common.currSymbol().get(bean.getCurrency()));
        htmlMessage = htmlMessage.replace("var localAmount", bean.getLocalAmount());
        htmlMessage = htmlMessage.replace("var amount", bean.getAmount());
        htmlMessage = htmlMessage.replace("var description", bean.getDescription());
        htmlMessage = htmlMessage.replace("var amount", bean.getAmount());
        htmlMessage = htmlMessage.replace("var txnId", bean.getTxnId());
        HashMap<String, String> accountDetails = common.accountDetails(bean.getCurrency());
        htmlMessage = htmlMessage.replace("var bankName", accountDetails.get("bankName"));
        htmlMessage = htmlMessage.replace("var bankAddress", accountDetails.get("bankAddress"));
        htmlMessage = htmlMessage.replace("var bankPhone", accountDetails.get("bankPhone"));
        htmlMessage = htmlMessage.replace("var swiftCode", accountDetails.get("swiftCode"));
        htmlMessage = htmlMessage.replace("var accName", accountDetails.get("accName"));
        htmlMessage = htmlMessage.replace("var accAddress", accountDetails.get("accAddress"));
        htmlMessage = htmlMessage.replace("var accNumber", accountDetails.get("accNumber"));
        return htmlMessage;
    }

    @Override
    public void sendAddFundsNotificationToUser(String to, String name, RequestBean bean) {
        String htmlMessage = addFundsToWalletNotification(bean, name);
        sendMail(to, adminEmailId, "Crediting your Invsta account", htmlMessage);
    }

    @Override
    public void sendAddFundsNotificationToAdmin(String to, RequestBean bean) {
        String name = "Admin";
        String htmlMessage = addFundsToWalletNotification(bean, name);
        sendMail(to, adminEmailId, "Crediting your Invsta account", htmlMessage);
    }

    private String sellFundsNotification(AccountEventCommand bean, String firstName) {
        UserInfo user = repository.findByUserId(bean.getUserId());
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "sellFundsNotification.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var fullName", firstName);
        htmlMessage = htmlMessage.replace("var customerName", user.getFullName());
        htmlMessage = htmlMessage.replace("var customerEmail", user.getEmail());
        htmlMessage = htmlMessage.replace("var customerId", user.getRefId());
        htmlMessage = htmlMessage.replace("var amount", new BigDecimal(bean.getAmount()).setScale(2, RoundingMode.HALF_UP).toString());
        htmlMessage = htmlMessage.replace("var percentange", bean.getPercentage());
        htmlMessage = htmlMessage.replace("var portfolio", bean.getFundName());
        return htmlMessage;
    }

    @Override
    public void sendSellFundsNotificationToUser(String to, String name, AccountEventCommand investmentReq) {
        String htmlMessage = sellFundsNotification(investmentReq, name);
        sendMail(to, adminEmailId, "Sell request", htmlMessage);
    }

    @Override
    public void sendSellFundsNotificationToAdmin(String to, AccountEventCommand investmentReq) {
        String name = "Admin";
        String htmlMessage = sellFundsNotification(investmentReq, name);
        sendMail(to, adminEmailId, "Sell request", htmlMessage);
    }

    private String addFundsToPortfolioNotification(InvestmentBean investmentReq, String firstName) {
        UserInfo user = repository.findByUserId(investmentReq.getUserId());
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "addFundsToPortfolioNotification.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var fullName", firstName);
        htmlMessage = htmlMessage.replace("var customerName", user.getFullName());
        htmlMessage = htmlMessage.replace("var customerEmail", user.getEmail());
        htmlMessage = htmlMessage.replace("var customerId", user.getRefId());
        htmlMessage = htmlMessage.replace("var amount", new BigDecimal(investmentReq.getInvestmentAmount() != null ? investmentReq.getInvestmentAmount() : "0.00").setScale(2, RoundingMode.HALF_UP).toPlainString());
        if (investmentReq.getFundActualShares() != null && !investmentReq.getFundActualShares().isEmpty()) {
            ShareFund share = investmentReq.getFundActualShares().get(0);
            htmlMessage = htmlMessage.replace("var portfolio", share.getFundName());
        }
        return htmlMessage;
    }

    @Override
    public void sendAddFundsNotificationToUser(String to, String name, InvestmentBean investmentReq) {
        String htmlMessage = addFundsToPortfolioNotification(investmentReq, name);
        sendMail(to, adminEmailId, "Add Fund Investment request", htmlMessage);
    }

    @Override
    public void sendAddFundsNotificationToAdmin(String to, InvestmentBean investmentReq) {
        String name = "Admin";
        String htmlMessage = addFundsToPortfolioNotification(investmentReq, name);
        sendMail(to, adminEmailId, "Add Fund Investment request", htmlMessage);
    }

    @Override
    public void sendMail(String toEmail, String fromEmail, String subject, String htmlMessage) {
        try {
            Email from = new Email(fromEmail);
            Email to = new Email(toEmail);
            Content content = new Content("text/html", htmlMessage);
            Mail mail = new Mail(from, subject, to, content);
            SendGrid sg = new SendGrid(SENDGRID_APIKEY);
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            logger.debug("Status Code", response.getStatusCode());
            logger.debug(response.getBody());
            logger.debug("Headers Map", response.getHeaders());
        } catch (IOException ex) {
            logger.error("", ex);
        }
    }

}
