/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Service;

/**
 *
 * @author Maninderjit Singh
 */
@Service
public class DateTimeServiceImpl implements DateTimeService {

    private static ZoneId TOZONEID = ZoneId.of("Pacific/Auckland");

    public static Date current() {
        ZonedDateTime zdt = Instant.now().atZone(TOZONEID);
        LocalDateTime ldt = zdt.toLocalDateTime();
        Date now = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        return now;
    }

    @Override
    public void setZoneId(String zoneId) {
        TOZONEID = ZoneId.of(zoneId);
    }

    @Override
    public Date today0000() {
        Calendar calendar = calendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        return date;
    }

    @Override
    public Date today1200() {
        Calendar calendar = calendar();
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();
        return date;
    }

    @Override
    public Date now() {
        return current();
    }

    @Override
    public long timeDate() {
        return now().getTime();
    }

    @Override
    public Calendar calendar() {
        Date now = current();
        Calendar instance = Calendar.getInstance();
        instance.setTime(now);
        return instance;
    }

}
