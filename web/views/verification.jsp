<!DOCTYPE html>
<html>
    <head>
        <title>Admin Dashboard HTML Template</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">

        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="./resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="./resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="./resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="./resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="./resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="./resources/icon_fonts_assets/themefy/themify-icons.css" rel="stylesheet">
        <link href="./resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w wider">
                <div class="logo-w">
                    <a href="index.html"><img alt="" src="resources/img/invsta-03(200px).png"></a>
                </div>

                <div class="element-wrapper">
                    <div class="element-box">
                        <form action="./verification" method="post">
                            <div class="steps-w">
                                <div class="step-triggers">
                                    <a class="step-trigger active" href="#stepContent1"><i  class="os-icon os-icon-arrow-right"></i></a>
                                    <a class="step-trigger" href="#stepContent2"><i  class="os-icon os-icon-arrow-right"></i></a>
                                    <a class="step-trigger" href="#stepContent3"><i  class="os-icon os-icon-arrow-right"></i></a>
                                    <!--<a class="step-trigger" href="#stepContent4"><i  class="os-icon os-icon-arrow-right"></i></a>-->
                                </div>
                                <div class="step-contents">
                                    <div class="step-content active" id="stepContent1">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="auth-header" style="font-size: 1.2rem; padding-left: 0">
                                                    To help prevent money laundering, we are legally required to confirm your identification. Please complete the next two steps to allow us to confirm that you are who you say you are. Thanks <i class="fa fa-smile-o"></i> 
                                                </h4>
                                                <div class="form-group">
                                                    <label for=""> Please enter your home or residential address</label>
                                                    <input class="form-control" placeholder="Enter Address" id="address" required="required" type="text" name="address">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-buttons-w text-right">
                                            <a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>
                                        </div>
                                    </div>
                                    <div class="step-content" id="stepContent2">
                                        <h4 class="auth-header" style="padding-left: 0">
                                            Verification - Second Step 
                                        </h4>
                                        <label for="">We'll need your ID details - Please select either your Passport or Drivers License, and make sure you have got these details with you now</label>
                                        <br>
                                        <div class="row">


                                            <div class="col-sm-6">
                                                <!-- Button trigger modal -->
                                                <button type="button" class="dropzone" id="my-awesome-dropzone" data-toggle="modal" data-target="#drivingLicense">
                                                    Upload Driving License
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="drivingLicense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="drivingLicense">Driving License</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!--                                                                <div class="row">
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">First Name:</label>
                                                                                                                                            <input type="text" name="dl_first_name" class="dl-text form-control" placeholder="First Name">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">Middle Name:</label>
                                                                                                                                            <input type="text" name="dl_middle_name" class="form-control" placeholder="Middle Name">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">last Name:</label>
                                                                                                                                            <input type="text" name="dl_last_name" class="form-control" placeholder="Last Name">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>-->
                                                                <!--                                                                <div class="row">
                                                                                                                                    <div class="col-sm-12">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">DOB:</label>
                                                                                                                                            <input type="text" name="dl_dob" class="dl-text form-control" placeholder="DOB">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>-->
                                                                <!--                                                                <div class="row">
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">Street Name:</label>
                                                                                                                                            <input type="text" name="dl_street_name" class="dl-text form-control" placeholder="Street Name">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">Street Number:</label>
                                                                                                                                            <input type="text" name="dl_street_number" class="dl-text form-control" placeholder="Street Number">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-sm-4">
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label class="label">Street:</label>
                                                                                                                                            <input type="text" name="dl_street" class="dl-text form-control" placeholder="Street">
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>-->
                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label class="label">License Number:</label>
                                                                            <input  type="text" name="dl_license_number" class="dl-text form-control" placeholder="License Number">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label class="label">License Version:</label>
                                                                            <input  type="text" name="dl_license_version" class="dl-text form-control" placeholder="License Version">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="label">Expiry Date:</label>
                                                                            <input type="text" name="dl_dob" class="dl-text form-control" placeholder="Expiry Date">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <!-- Button trigger modal -->
                                                <button type="button" class="dropzone" id="my-awesome-dropzone" data-toggle="modal" data-target="#passport" style="width:100%;">
                                                    Passport
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade" id="passport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="passport">Passport</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="label">Passport number:</label>
                                                                            <input type="text" name="pp_passport_number" class="form-control" placeholder="Passport Number">
                                                                            <!--<small>Make sure your name entered above exactly matches the name shown on your passport, including middle name(s).</small>-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="label">Passport expiry date:</label>
                                                                            <input type="text" name="pp_passport_expiry_date" class="form-control" placeholder="Passport Expiry Date">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                                            <div class="col-sm-4">
                                                                                             Button trigger modal 
                                                                                            <button type="button" class="dropzone" id="my-awesome-dropzone" data-toggle="modal" data-target="#postalAdress" style="height: 107px">
                                                                                                Postal Address
                                                                                            </button>
                                                                                             Modal 
                                                                                            <div class="modal fade" id="postalAdress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                                                <div class="modal-dialog" role="document">
                                                                                                    <div class="modal-content">
                                                                                                        <div class="modal-header">
                                                                                                            <h5 class="modal-title" id="postalAdress">Postal Adress</h5>
                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                                                <span aria-hidden="true">&times;</span>
                                                                                                            </button>
                                                                                                        </div>
                                                                                                        <div class="modal-body">
                                                                                                            <div class="row">
                                                                                                                <div class="col-sm-6">
                                                                                                                    <div class="form-check">
                                                                                                                        <label class="form-check-label">
                                                                                                                            <input class="form-check-input" id="postal-add" type="radio" name="radio" value="option1">Same as residential Address?
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-sm-6">
                                                                                                                    <div class="form-check">
                                                                                                                        <label class="form-check-label">
                                                                                                                            <input class="form-check-input" id="postal-ad" type="radio" name="radio" value="option2">Is your postal Address is not same?
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="row" id="postal-ad1">
                                                                                                                <div class="col-sm-6">
                                                                                                                    <div class="form-group">
                                                                                                                        <label class="label">
                                                                                                                            Postal Address:
                                                                                                                        </label>
                                                                                                                        <input type="text" name="pa_postal_office" class="pa-text form-control" placeholder="Postal Office">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="col-sm-6">
                                                                                                                    <div class="form-group">
                                                                                                                        <label class="label">
                                                                                                                            Postal Code:
                                                                                                                        </label>
                                                                                                                        <input type="text" name="pa_postal_code" class="pa-text form-control" placeholder="Postal Code">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="modal-footer">
                                                                                                                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>-->
                                        </div>
                                        <div class="row" style="margin-top:20px">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-6 text-right">
                                                <a class="btn btn-warning step-trigger-btn" href="#stepContent1"> Back</a>
                                                <a class="btn btn-primary step-trigger-btn" href="#stepContent3"> Continue</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="step-content" id="stepContent3">            
                                        <div class="row">
                                            <h4 class="auth-header" style="padding-left: 0">
                                                Verification Complete 
                                            </h4>
                                            <div class="col-sm-12 text-center">
                                                <img src="resources/img/tick.gif" style="width:20% ; margin: 20px;"/>
                                                <h6 class="text-center"> 
                                                    Thanks for providing those details, we know it's a bit of a pain. We'll confirm your details now, and let you know if there is anything further that we need from you!
                                                </h6>
                                            </div>             
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-buttons-w text-center">
                                                    <button class="btn btn-primary" type="submit">Submit</button>
                                                    <!--<a class="btn btn-primary" href="./user-dashboard">Cool</a>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/moment/moment.js"></script>
        <script src="./resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="./resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="./resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="./resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="./resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="./resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="./resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="./resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="./resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="./resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="./resources/js/main.js?version=3.5.1"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-XXXXXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>

        <script>
            var placeSearch, autocomplete;
            var componentForm = {
            locality: 'long_name',
                    sublocality_level_1: 'long_name'
            };
            function initAutocomplete() {

            autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['address'], componentRestrictions: {country: 'nz'}});
            autocomplete.addListener('place_changed', fillInAddress);
            }

            function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            }
            }
            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.

        </script>                                       
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
        <script>
            $("#postal-ad1").hide();
            $("#postal-ad").click(function () {
            $("#postal-ad1").show();
            });
            $("#postal-add").click(function () {
            $("#postal-ad1").hide();
            });
        </script>

    </body>
</html>
