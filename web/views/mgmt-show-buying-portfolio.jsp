<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">-->
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${name}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${daterange}</a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            ${name} <img src="./resources/img/info-icon.png" style="object-fit: contain">
                                            <div class="date-range-picker">
                                                <form class="form-inline">
                                                    <div class="form-group">
                                                        <select class="form-control" name="portfolio" id="portfolio">
                                                            <c:forEach items="${portfolios}" var="pf">
                                                                <option value="${pf.fundId}"<c:if test="${pf.fundId eq id}"> selected="selected" </c:if> >${pf.name}
                                                                    </option>
                                                            </c:forEach>
                                                        </select>
                                                        <input type="text" class="form-control" name="daterange" placeholder="Pick Date Range" value="${daterange}"/>
                                                        <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                    </div>
                                                </form>
                                            </div>
                                        </h6>                             
                                        <div class="element-box-tp">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                DATE
                                                            </th>
                                                            <th>
                                                                CUSTOMER ID /NAME /EMAIL
                                                            </th>
                                                            <th>
                                                                USD
                                                            </th>
                                                            <th>
                                                                UNITS
                                                            </th>
                                                            <th>
                                                                NOTES
                                                            </th>
                                                            <th>
                                                                ACTIONS
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${report}" var="tran">
                                                            <tr>
                                                                <td>
                                                                    ${tran.created_date} 
                                                                </td>
                                                                <td>
                                                                    ${tran.ref_id} /
                                                                    ${tran.full_name} /
                                                                    ${tran.email_id}
                                                                </td>
                                                                <td>
                                                                    ${tran.usd}
                                                                </td>
                                                                <td>
                                                                    ${tran.units}
                                                                </td>
                                                                <td>
                                                                    ${tran.particulars}
                                                                </td>
                                                                <td>
                                                                    <button onclick="toggleTable('${tran.id}')" class="btn btn-success btn-details" id="btn-details-${tran.id}">Show Details
                                                                        <table class="toggle-table" id="details-table-${tran.id}" style="width:100%; margin-top:10px; display: none">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        Share /Coin
                                                                                    </th>
                                                                                    <th>
                                                                                        Amount
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <c:forEach items="${tran.details}" var="txn">
                                                                                    <tr>
                                                                                        <td>
                                                                                            ${txn.coin_id}
                                                                                        </td>
                                                                                        <td>
                                                                                            ${txn.amount}
                                                                                        </td>
                                                                                    </tr>
                                                                                </c:forEach>
                                                                            </tbody>
                                                                        </table>
                                                                    </button>
                                                                    <a class="btn btn-primary" href="./mgmt-buying-portfolio-add?id=${id}&ok=${tran.id}">Modify</a>
                                                                    <a class="btn btn-primary" href="./mgmt-show-buying-portfolio-history?id=${id}&ok=${tran.txn_Req_id}">History</a>
                                                                    <!--<button class="btn btn-primary btn-history" id="btn-history-${tran.id}">History</button>-->
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>
                                                                DATE
                                                            </th>
                                                            <th>
                                                                CUSTOMER ID /NAME /EMAIL
                                                            </th>
                                                            <th>
                                                                USD
                                                            </th>
                                                            <th>
                                                                UNITS
                                                            </th>
                                                            <th>
                                                                NOTES
                                                            </th>
                                                            <th>
                                                                Actions
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>                

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <!--<script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>-->
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <!--<script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>-->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
                                                                        $(document).ready(function () {
                                                                            $('.toggle-table').hide();
                                                                            $('input[name="daterange"]').daterangepicker({
                                                                                opens: 'left',
                                                                                locale: {
                                                                                    format: 'YYYY-MM-DD'
                                                                                }
                                                                            }, function (start, end, label) {
                                                                                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                                                                            });
                                                                            $('.applyBtn').click(function () {
                                                                                var portfolio = $('#portfolio').val();
                                                                                var daterange = $('.drp-selected').text();
                                                                                location.href = '${home}/mgmt-show-buying-portfolio?id=' + portfolio + '&daterange=' + daterange;
                                                                            });
                                                                            var table1 = $('#portfolio-report-type-table').DataTable({
                                                                                scrollY: "450px",
                                                                                scrollCollapse: true,
                                                                                paging: false,
                                                                                fixedColumns: true,
                                                                                drawCallback: function () {
                                                                                    var api = this.api();
                                                                                }
                                                                            });
                                                                            $(table1.table().container()).on('keyup', 'tfoot input', function () {
                                                                                table1.column($(this).data('index')).search(this.value).draw();
                                                                            });
                                                                            toggleTable = function (idx) {
                                                                                $('#details-table-' + idx).toggle();
                                                                            };
                                                                        });
        </script>
    </body>
</html>
