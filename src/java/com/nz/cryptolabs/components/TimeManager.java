/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import static com.nz.cryptolabs.constants.Constants.CoinPriceMapClear;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.DateTimeServiceImpl;
import java.util.Calendar;
import java.util.Date;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class TimeManager {

    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonMethods common;
    private TodayUpdates todayUpdates = null;
    private CleaningMap cleaningMap = null;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TimeManager.class);

    public TimeManager() {
        todayUpdates = new TodayUpdates();
        todayUpdates.start();
        cleaningMap = new CleaningMap();
        cleaningMap.start();
    }

    class TodayUpdates extends Thread {

        private Date targetDate = null;

        public TodayUpdates() {
            Date now = DateTimeServiceImpl.current();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.set(Calendar.HOUR, 11);
            cal.set(Calendar.MINUTE, 55);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.AM_PM, Calendar.PM);
            targetDate = cal.getTime();
        }

        @Override
        public void run() {
            while (true) {
                Date now = DateTimeServiceImpl.current();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                now = cal.getTime();
                logger.debug("Right Now:-" + now + ", Target DateTime:-" + targetDate);
                System.err.println("Right Now:-" + now + ", Target DateTime:-" + targetDate);
                try {
                    if (now.after(targetDate)) {
                        String formattedate = common.dateFormat(targetDate, CommonMethods.format3);
                        databaseService.saveTodayUpdates(formattedate);
                        cal.add(Calendar.DAY_OF_MONTH, 1);
                        targetDate = cal.getTime();
                    }
                    Thread.sleep(CommonMethods.ONE_MINUTE);
                } catch (InterruptedException ex) {
                    logger.debug("Right Now:-" + now, ex);
                }
            }
        }
    }

    class CleaningMap extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    CoinPriceMapClear();
                    Thread.sleep(CommonMethods.ONE_MINUTE * 15);
                } catch (InterruptedException ex) {
                    logger.debug("", ex);
                }
            }
        }
    }

}
