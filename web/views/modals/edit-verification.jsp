<%-- 
    Document   : edit-investment-shares
    Created on : 18 Dec, 2017, 10:54:21 AM
    Author     : palo12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="edit-verification" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title">
                    Edit Verification
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <form method="post" action="./admin-edit-verification" id="edit-verification-form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="os-tabs-w">
                                        <div class="os-tabs-controls">
                                            <ul class="nav nav-tabs smaller">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#tab_overview">Personal</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_latest_week">Passport </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_latest_15_days">Driving License</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_latest_30_days">Tax Details</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_overview">
                                                <div class="in_form_container form_text">
                                                    <div class="form-style-6">
                                                        <div class="where_reside">
                                                            <h4>Please enter user's personal details.</h4>
                                                        </div>
                                                        <input type="hidden" class="form-control" id="edit-verification-id" name="id" >
                                                        <input type="hidden" class="form-control" id="edit-verification-user_id" name="user_id" >

                                                        <div class="form-group">
                                                            <label>
                                                                Full Name:
                                                            </label>
                                                            <input type="text" class="form-control" id="edit-verification-full_name" name="full_name" placeholder="Your Name" value="" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label>
                                                                Date of Birth
                                                            </label>                                                            
                                                            <input type="text" class="form-control" id="edit-verification-dob" name="dob" placeholder="Date of Birth" data-format="YYYY/MM/DD" data-lang="en" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>
                                                                Home Address
                                                            </label>
                                                            <textarea class="form-control" name="address" id="edit-verification-address" placeholder="Address"  style="padding-bottom:0; resize: none" autocomplete="off"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane " id="tab_latest_week" style="">
                                                <div class="in_form_container form_text">
                                                    <div class="where_reside">
                                                        <h4>Please enter user's passport details</h4>
                                                    </div>
                                                    <div class="form-style-3">
                                                        <div class="form-style-3_form">
                                                            <label for="field1"><span>Passport number</span>
                                                                <input type="text" class="input-field form-control" name="pp_passport_number"  id="edit-verification-pp_passport_number" value="">
                                                            </label>
                                                            <label for="field2"><span>Country of Issue</span>
                                                                <input type="text" class="input-field form-control" name="pp_issue_country"  id="edit-verification-pp_issue_country" value="">
                                                            </label>
                                                            <label for="field3"><span>Expiry Date</span>
                                                                <input type="text" class="form-control" id="edit-verification-pp_passport_expiry_date" name="pp_passport_expiry_date" placeholder="Expiry Date" value="" data-years="2018-2099" data-format="YYYY/MM/DD" data-lang="en" >
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane " id="tab_latest_15_days" style="">
                                                <div class="in_form_container form_text">
                                                    <div class="where_reside">
                                                        <h4>Please enter user's driving license details below:</h4>
                                                    </div>
                                                    <div class="form-style-3">
                                                        <div class="form-style-3_form">
                                                            <label for="dl_issue_country"><span>Country of issue: </span>
                                                                <input type="text" class="input-field form-control" name="dl_issue_country" id="edit-verification-dl_issue_country" value="">
                                                            </label>
                                                            <label for="dl_license_number"><span>License Number</span>
                                                                <input type="text" class="input-field form-control" name="dl_license_number" id="edit-verification-dl_license_number" value="">
                                                            </label>
                                                            <label for="dl_license_version"><span>Version Number</span>
                                                                <input type="text" class="input-field form-control" name="dl_license_version" id="edit-verification-dl_license_version" value="">
                                                            </label>
                                                            <label for="dl_expiry_date"><span>Expiry Date</span>
                                                                <input type="text" class="form-control" id="edit-verification-dl_expiry_date" name="dl_expiry_date" placeholder="Expiry Date" value="" data-years="2018-2099" data-format="YYYY/MM/DD" data-lang="en" >
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane " id="tab_latest_30_days" style="">
                                                <div class="in_form_container in_form2 form_text">
                                                    <div class="where_reside">
                                                        <h4>Please enter user's tax details below:</h4>
                                                    </div>
                                                    <div class="form-style-3">
                                                        <div class="form-style-3_form">
                                                            <label for="countries" style="display: block"><span>User resident of countries:</span>
                                                                <select name="countries" id="edit-verification-countries" class="select-field" multiple>
                                                                </select>
                                                            </label>
                                                            <label for="tax_resi_country" style="display: block"><span>User tax resident of countries: </span>
                                                                <select name="tax_resi_country" id="edit-verification-tax_resi_country" class="select-field" multiple>
                                                                </select>
                                                            </label>
                                                            <label for="tax_id"><span>Tax number (please list your tax numbers for each country)</span>
                                                                <input type="text" name="tax_id" id="edit-verification-tax_id" class="input-field form-control" value="">
                                                            </label>
                                                            <label for="src_of_fund"><span>What is the source of money you are investing through Invsta?</span>
                                                                <select name="src_of_fund" id="edit-verification-src_of_fund" class="select-field form-control" value="">
                                                                    <option value="Appointment">Salary</option>
                                                                    <option value="Interview">Savings</option>
                                                                    <option value="Regarding a post">Inheritance</option>
                                                                    <option value="Regarding a post">Sale of an asset</option>
                                                                    <option id="other" value="Other">Other</option>
                                                                </select>
                                                            </label>
                                                            <div id="text" class="animated zoomIn" style="display: none;">
                                                                <label for="other"><span>Other </span><input type="text" class="input-field" name="other" id="edit-verification-other" value=""></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="form-group" style="margin-right: auto">
                            <textarea class="form-control" cols="80" name="notes" id="edit-verification-notes" placeholder="Custom Notes"  style="resize: none" autocomplete="off"></textarea>
                        </div>
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary" type="submit">Proceed</button>
                    </div>
                </form>    
            </div>
        </div>
    </div>
</div>