/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import com.stripe.model.Charge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo12
 */
public class StripeCharge {

    /**
     * @return the stripe_created_ts
     */
    public String getStripe_created_ts() {
        return stripe_created_ts;
    }

    /**
     * @param stripe_created_ts the stripe_created_ts to set
     */
    public void setStripe_created_ts(String stripe_created_ts) {
        this.stripe_created_ts = stripe_created_ts;
    }

    /**
     * @return the txn_id
     */
    public String getTxn_id() {
        return txn_id;
    }

    /**
     * @param txn_id the txn_id to set
     */
    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }

    public StripeCharge() {

    }

    public StripeCharge(Charge chg) {
        this.txn_id = (chg.getId());
        this.bal_txn_id = (chg.getBalanceTransaction());
//        this.created_ts = common.dateFormat(new Date(chg.getCreated()));
        this.amount = String.valueOf(chg.getAmount());
        this.amount_refunded = String.valueOf(chg.getAmountRefunded());
        this.currency = (chg.getCurrency());
        this.status = (chg.getStatus());
        this.fail_code = (chg.getFailureCode());
        this.fail_message = (chg.getFailureMessage());
        this.description = (chg.getDescription());
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the bal_txn_id
     */
    public String getBal_txn_id() {
        return bal_txn_id;
    }

    /**
     * @param bal_txn_id the bal_txn_id to set
     */
    public void setBal_txn_id(String bal_txn_id) {
        this.bal_txn_id = bal_txn_id;
    }

    /**
     * @return the fund_id
     */
    public String getFund_id() {
        return fund_id;
    }

    /**
     * @param fund_id the fund_id to set
     */
    public void setFund_id(String fund_id) {
        this.fund_id = fund_id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the amount_refunded
     */
    public String getAmount_refunded() {
        return amount_refunded;
    }

    /**
     * @param amount_refunded the amount_refunded to set
     */
    public void setAmount_refunded(String amount_refunded) {
        this.amount_refunded = amount_refunded;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the fail_code
     */
    public String getFail_code() {
        return fail_code;
    }

    /**
     * @param fail_code the fail_code to set
     */
    public void setFail_code(String fail_code) {
        this.fail_code = fail_code;
    }

    /**
     * @return the fail_message
     */
    public String getFail_message() {
        return fail_message;
    }

    /**
     * @param fail_message the fail_message to set
     */
    public void setFail_message(String fail_message) {
        this.fail_message = fail_message;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    private String id;
    private String txn_id;
    private String user_id;
    private String bal_txn_id;
    private String fund_id;
    private String stripe_created_ts;
    private String created_ts;
    private String amount;
    private String amount_refunded;
    private String currency;
    private String status;
    private String fail_code;
    private String fail_message;
    private String description;
    private static final Logger logger = LoggerFactory.getLogger(StripeCharge.class);

}
