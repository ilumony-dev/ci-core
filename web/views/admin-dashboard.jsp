<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible"/>
        <meta content="template language" name="keywords"/>
        <meta content="Tamerlan Soziev" name="author"/>
        <meta content="Admin dashboard html template" name="description"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="${home}/resources/favicon.png" rel="shortcut icon"/>
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet"/>
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link href="${home}/resources/css/sweetalert2.css" rel="stylesheet"/>

    </head>
    <%--    <script type="text/javascript">
            ${cssVariablesValues}
        </script>--%>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Summary Page</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display:block">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                        </div>
                                        <h6 class="element-header">
                                            Admin Dashboard <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Customers
                                                        </div>
                                                        <div class="value">
                                                            ${totalCustomers}
                                                        </div>
                                                        <div class="trending trending-up">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Investments
                                                        </div>
                                                        <div class="value">
                                                            ${totalInvestments}
                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Current Balance
                                                        </div>
                                                        <div class="value currentBalance">
                                                            ${totalSharesValue}
                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Total Portfolios <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-box-tp">
                                            <div class="controls-above-table">
                                                <div class="row">
                                                    <c:forEach items="${portfolios}" var="portfolio">
                                                        <div class="col-md-3 col-sm-4 col-12">
                                                            <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/${portfolio.master}) center center no-repeat; background-size: cover;" >
                                                                <h5 class="color3">${portfolio.name}</h5>
                                                            </div>
                                                            <div class="parent-row-action">
                                                                <div class="row-actions">                                                                    
                                                                    <a href="#${portfolio.fundId}" onclick='onPortfolioById(${portfolio.toJSONObject()});' data-target="#addfund" data-toggle="modal"  style="color:white"><i class="os-icon os-icon-pencil-2" style="font-size:20px; margin-right: 10px"></i></a>
                                                                    <a href="./admin-portfolio-report-type?id=${portfolio.fundId}" style="color:white"><i class="fa fa-file"  style="font-size:20px; margin-right: 10px"></i></a>
                                                                    <a href="./admin-portfolio-report?id=${portfolio.fundId}" style="color:white"><i class="fa fa-line-chart"  style="font-size:20px"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header" style="display:block">
                                            User Investment Requests
                                            <img src="./resources/img/info-icon.png" style="vertical-align:text-top; object-fit: contain">
                                            <div class="pdf-btn">
                                                <button class="btn btn-primary"  onclick="generatePdf('pdf-contents0', 'Money Investment In Portfolio Allocation', false)"  value="">Generate PDF</button>
                                            </div>
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive" id="pdf-contents0">
                                                <table id="inv-requests-table" class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Transaction Id
                                                            </th>
                                                            <th>
                                                                DateTime
                                                            </th>
                                                            <th>
                                                                Customer Id /Name
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                            <th>
                                                                Portfolio
                                                            </th>
                                                            <th>
                                                                Shares
                                                            </th>
                                                            <th class="text-right">
                                                                Amount
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${pendingInvestments}" var="investment">
                                                            <tr>
                                                                <td>
                                                                    ${investment.tranId}
                                                                </td>
                                                                <td>
                                                                    ${investment.createdDate}
                                                                </td>
                                                                <td>
                                                                    ${investment.refId} /
                                                                    ${investment.customerName}
                                                                </td>
                                                                <td>
                                                                    ${investment.email}
                                                                </td>
                                                                <td>
                                                                    ${investment.fundName}
                                                                </td>
                                                                <td>
                                                                    ${investment.shares}
                                                                </td>
                                                                <td class="text-right">
                                                                    ${common.formatMoney(investment.investmentAmount, 2, 'USD')}
                                                                </td>
                                                                <td>
                                                                    <div style="display:flex">
                                                                        <a class='btn btn-secondary btn-sm' href='#' onclick='pendingShares2(${investment.toJSONObject()});' data-target='#update-investment-shares' data-toggle='modal'>
                                                                            <!--<i class="os-icon os-icon-wallet-loaded"></i>-->
                                                                            <span>Purchase</span>
                                                                        </a>

                                                                        <a href="./home" class="btn btn-danger btn-sm btn-tran-request-cancel" data-json-obj='{"ok":"${investment.reqId}", "bs": "NIR", "act": "${investment.action}"}' type="button">Cancel</a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header" style="display:block">
                                            User Sell Requests <img src="./resources/img/info-icon.png" style="vertical-align:text-top;object-fit: contain">
                                            <div class="pdf-btn">
                                                <button class="btn btn-primary" onclick="generatePdf('pdf-contents1', 'Sell Funds Transaction Requests', false)" value="">Generate PDF</button>
                                            </div>
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive" id="pdf-contents1">
                                                <table id="sell-requests-table" class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Transaction Id
                                                            </th>
                                                            <th>
                                                                DateTime
                                                            </th>
                                                            <th>
                                                                Customer Id /Name
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                            <th>
                                                                Portfolio
                                                            </th>
                                                            <th class="text-right">
                                                                Approx. Amount
                                                            </th>
                                                            <th class="text-right">
                                                                Percentage
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${pendingTransactions}" var="investment">
                                                            <tr>
                                                                <td>
                                                                    ${investment.tranId}
                                                                </td>
                                                                <td>
                                                                    ${investment.createdDate}
                                                                </td>
                                                                <td>
                                                                    ${investment.refId} /
                                                                    ${investment.customerName}
                                                                </td>
                                                                <td>
                                                                    ${investment.email}
                                                                </td>
                                                                <td>
                                                                    ${investment.fundName}
                                                                </td>
                                                                <td class="text-right">
                                                                    ${common.formatMoney(investment.investmentAmount, 2, 'USD')}
                                                                </td>
                                                                <td class="text-right">
                                                                    ${common.formatMoney(investment.percentage, 2)}
                                                                </td>
                                                                <td>
                                                                    <div style="display:flex">
                                                                        <c:choose>
                                                                            <c:when test="${investment.action eq 'DEPOSITE'}">
                                                                                <a class="btn btn-secondary btn-sm btn-add-funds" href="#" data-json-object='${investment.toString()}' data-target="#via-wallet" data-toggle="modal">
                                                                                    <!--<i class="os-icon os-icon-wallet-loaded"></i>-->
                                                                                    <span>ADD FUNDS</span>
                                                                                </a>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <a class="btn btn-secondary btn-sm btn-sell-funds" href="#" data-json-object='${investment.toString()}' data-target="#update-sell-purchased-shares" data-toggle="modal">
                                                                                    <!--<i class="os-icon os-icon-wallet-loaded"></i>-->
                                                                                    <span>SELL</span>
                                                                                </a>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                        <a href="./home" class="btn btn-danger btn-sm btn-tran-request-cancel" data-json-obj='{"ok":"${investment.reqId}", "bs": "NIR", "act": "${investment.action}"}' type="button">Cancel</a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header" style="display:block">
                                            New Money In - Bank to Cash Wallet <img src="./resources/img/info-icon.png" style="vertical-align:text-top; object-fit: contain">
                                            <div class="pdf-btn">
                                                <button class="btn btn-primary" onclick="generatePdf('pdf-contents2', 'New Money In - Bank to Cash Wallet', false)" value="">Generate PDF</button>
                                            </div>
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive"  id="pdf-contents2">
                                                <table id="tran-requests-table" class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Transaction Id
                                                            </th>
                                                            <th>
                                                                DateTime
                                                            </th>
                                                            <th>
                                                                Customer Id/ Name/ Email
                                                            </th>
                                                            <th>
                                                                Description
                                                            </th>
                                                            <th>
                                                                A/c Details
                                                            </th>
                                                            <th class="text-right">
                                                                Amount
                                                            </th>
                                                            <th class="text-right">
                                                                Local Amount
                                                            </th>
                                                            <th>
                                                                Master
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${newWithdrawlRequests}" var="req">
                                                            <tr>
                                                                <td>
                                                                    ${req.txnId}
                                                                </td>
                                                                <td>
                                                                    ${req.createdDate}
                                                                </td>
                                                                <td>
                                                                    ${req.refId} /
                                                                    ${req.customerName}/
                                                                    ${req.email}
                                                                </td>
                                                                <td>
                                                                    ${req.description}
                                                                </td>
                                                                <td>
                                                                    <p>${req.bankName}</p>
                                                                    <p>${req.accountName}</p>
                                                                    <p>${req.account}</p>
                                                                </td>

                                                                <td class="text-right">
                                                                    ${common.formatMoney(req.amount, 2, 'USD')}
                                                                </td>
                                                                <td class="text-right">
                                                                    ${common.formatMoney(req.localAmount, 2, req.currency)}
                                                                </td>
                                                                <td>
                                                                    <c:choose>
                                                                        <c:when test="${req.master eq 'WITHDRAWL'}">
                                                                            WITHDRAWAL
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            ${req.master}
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </td>
                                                                <td>
                                                                    <div style="display:flex">
                                                                        <a class="btn-request-cancel"  href="./welcome" data-json-object='${req.toString()}'><i class="fa fa-close" style="font-size:25px"></i></a>
                                                                        <a class="btn-request-done" href="./welcome" data-json-object='${req.toString()}'><i class="fa fa-check" style="font-size:25px"></i></a>
                                                                        <a class="btn-request-edit" href="#" data-json-object='${req.toString()}' data-target="#edit-transaction-request-modal" data-toggle="modal"><i class="fa fa-edit" style="font-size:25px"></i></a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header" style="display:block">
                                            Admin approval of change in units <img src="./resources/img/info-icon.png" style="vertical-align:text-top; object-fit: contain">
                                            <div class="pdf-btn">
                                                <button class="btn btn-primary" onclick="generatePdf('pdf-contents3', 'New Wallet Transactions', false)" value="">Generate PDF</button>
                                            </div>
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive"  id="pdf-contents3">
                                                <table id="wallet-txns-table" class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Transaction Id
                                                            </th>
                                                            <th>
                                                                DateTime
                                                            </th>
                                                            <th>
                                                                Customer Id /Name
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                            <th>
                                                                Particulars 
                                                            </th>
                                                            <th>
                                                                Portfolio 
                                                            </th>
                                                            <th>
                                                                Payment Method 
                                                            </th>
                                                            <th class="text-right">
                                                                Amount
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${pendingWalletTxns}" var="txn">
                                                            <tr>
                                                                <td>
                                                                    ${txn.txn_id}
                                                                </td>
                                                                <td>
                                                                    ${txn.created_ts}
                                                                </td>
                                                                <td>
                                                                    ${txn.ref_id} /
                                                                    ${txn.full_name}
                                                                </td>
                                                                <td>
                                                                    ${txn.email_id}
                                                                </td>
                                                                <td>
                                                                    ${txn.particulars}
                                                                </td>
                                                                <td>
                                                                    ${txn.fund_name}
                                                                </td>
                                                                <td>
                                                                    ${txn.brand}
                                                                </td>
                                                                <td class="text-right">
                                                                    ${common.formatMoney(txn.amount, 2, 'USD')}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-secondary btn-sm btn-approved" href="./welcome" data-json-object='${txn.toString()}'>
                                                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                                                        <span>Approved</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invsta Unit Summary <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-box-tp">

                                            <div class="table-responsive">
                                                <table id="unit-summary-table" class="table table-bordered">
                                                    <thead id="unit-summary-head">
                                                        <tr>
                                                            <th>Customer Id / Name</th>
                                                            <th>Email</th>
                                                            <th>Portfolio</th>
                                                            <th>Invsta Units</th>
                                                            <th>N.A.V</th>
                                                            <th >US$</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="unit-summary-body">
                                                        <c:forEach items="${totalUnits}" var="unit" varStatus="loop">
                                                            <tr>
                                                                <td>
                                                                    <a href="./user-dashboard?un=${unit.userId}" title="User Dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>
                                                                        ${unit.reqId} / ${unit.customerName}
                                                                    </a>
                                                                </td>
                                                                <td>${unit.email}</td>
                                                                <td>
                                                                    <a href="./summary?un=${unit.userId}&ok=${unit.investmentId}" title="Investment Dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i>
                                                                        ${unit.name}
                                                                    </a>
                                                                </td>
                                                                <td>${unit.minto}</td>
                                                                <td>$ ${unit.price}</td>
                                                                <td>$ ${unit.usd}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                    <tfoot id="unit-summary-foot">
                                                        <tr class="totals">
                                                            <th>Customer Id / Name</th>
                                                            <th>Email</th>
                                                            <th>Portfolio</th>
                                                            <th id="unit-summary-foot-minto">Invsta Units</th>
                                                            <th>N.A.V</th>
                                                            <th id="unit-summary-foot-usd">US$</th>
                                                        </tr>
                                                        <tr class="titles">
                                                            <th>Customer Id / Name</th>
                                                            <th>Email</th>
                                                            <th>Investment</th>
                                                            <th>Invsta Units</th>
                                                            <th>N.A.V</th>
                                                            <th>US$</th>
                                                        </tr>                                                        
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="controls-below-table">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        Coin Summary <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                                    </h6>
                                                                    <div class="element-box-tp">
                                                                        <div class="controls-above-table">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="coin-summary-table" class="table table-bordered">
                                                    <thead id="coin-summary-head">
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr>
                                                                    <th style="min-width:150px;">Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                    </thead>
                                                    <tbody id="coin-summary-body">
                                                    </tbody>
                                                    <tfoot id="coin-summary-foot">
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr class="titles">
                                                                    <th>Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr class="totals">
                                                                    <th>Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/update-sell-purchased-shares.jsp" />
        <jsp:include page = "modals/update-investment-shares.jsp" />
        <jsp:include page = "modals/unit-shifted-2-mgmt-acc.jsp" />
        <jsp:include page = "modals/via_wallet.jsp"/>
        <jsp:include page = "modals/edit-transaction-request.jsp"/>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <!--<script src="${home}/resources/js/cryptolabs/common-main.js"></script>-->
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="${home}/resources/js/cryptolabs/admin-charts.js"></script>
        <script src="${home}/resources/js/cryptolabs/generate-pdf.js"></script>
        <script src="${home}/resources/js/sweetalert2.js"></script>
        <script src="./resources/js/switch-color.js"></script>
        <script>
                                                    var totalUnits = null, investments = null, refIds = null, funds = null, coins = null, dailyUpdates = null;
                                                    $(document).ready(function () {
                                                        $.get("./rest/cryptolabs/modelmap/admin/${key}", function (data, status) {
                                                            var jO = JSON.parse(data);
                                                            totalUnits = jO.totalUnits;
                                                            investments = jO.investments;
                                                            refIds = jO.refIds;
                                                            funds = jO.funds;
                                                            coins = jO.coins;
                                                            dailyUpdates = jO.dailyUpdates;
                                                        });
                                                        $("#updateinvestmentshares-fundPrices").on('change', function () {
                                                            $("#updateinvestmentshares-nav").val(this.value);
                                                            var text = this.options[this.selectedIndex].text;
                                                            $("#updateinvestmentshares-date").val(text);
                                                            usd2unit();
                                                        });
                                                        $("#updateinvestmentshares-submit").bind('click', function () {
                                                            $("#updateinvestmentshares-submit").unbind('click');
                                                            var action = $("#updateinvestmentshares-action").val();
                                                            if (action === 'ADMIN_PURCHASE_PENDING_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for validating the purchase of this portfolio."
                                                                });
                                                            } else if (action === 'ADMIN_PURCHASE_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for update investment shares."
                                                                });
                                                            } else if (action === 'ADMIN_SELL_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for update investment shares."
                                                                });
                                                            }
                                                            $("#updateinvestmentshares-form").submit();
                                                        });
                                                        $("#update-sell-purchased-shares-submit").bind('click', function () {
                                                            $("#update-sell-purchased-shares-submit").unbind('click');
                                                            var action = $("#update-sell-purchased-shares-action").val();
                                                            if (action === 'ADMIN_PURCHASE_PENDING_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for update investment shares."
                                                                });
                                                            } else if (action === 'ADMIN_PURCHASE_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for update investment shares."
                                                                });
                                                            } else if (action === 'ADMIN_SELL_SHARES') {
                                                                swal({
                                                                    title: "Success!",
                                                                    text: "Thanks for update investment shares."
                                                                });
                                                            }
                                                            $("#update-sell-purchased-shares-form").submit();
                                                        });
                                                        $('.btn-add-funds').click(function () {
                                                            var jsonStr = $(this).data('json-object');
                                                            $("#via-wallet-reqId").val(jsonStr.reqId);
                                                            $("#via-wallet-userId").val(jsonStr.userId);
                                                            $("#via-wallet-title").text(jsonStr.customerName + " wants to invest in " + jsonStr.fundName);
                                                            $("#via-wallet-fundId").val(jsonStr.fundId);
                                                            $("#via-wallet-investmentAmount").val(jsonStr.investmentAmount);
                                                            $("#via-wallet-localInvestmentAmount").val(jsonStr.localInvestmentAmount);
                                                            $("#via-wallet-currency").val(jsonStr.currency);
                                                            $("#via-wallet-submit").text("Approve");
                                                        });
                                                        $('.btn-sell-funds').click(function () {
                                                            var jsonStr = $(this).data('json-object');
                                                            purchasedShares2(jsonStr);
                                                        });
                                                        $('.btn-approved').click(function () {
                                                            var jsonObj = $(this).data('json-object');
                                                            var url = './rest/cryptolabs/api/met13u4i23lkjh?id=' + jsonObj.id + '&email=' + jsonObj.email_id + '&name=' + jsonObj.full_name;
                                                            $.ajax({
                                                                url: url,
                                                                type: 'GET',
                                                                async: true,
                                                                dataType: "json",
                                                                success: function (data) {
                                                                }
                                                            });
                                                        });
                                                        $('.btn-request-done').click(function () {
                                                            var jsonObj = $(this).data('json-object');
                                                            var url = './rest/cryptolabs/api/met089234845034';
                                                            $.post(url, {ok: jsonObj.id, un: jsonObj.userId});
                                                        });
                                                        $('.btn-request-cancel').click(function () {
                                                            var jsonObj = $(this).data('json-object');
                                                            var url = './rest/cryptolabs/api/met091234850423';
                                                            $.post(url, {ok: jsonObj.id, un: jsonObj.userId});
                                                        });
                                                        $('.btn-tran-request-cancel').click(function () {
                                                            var obj = $(this).data('json-obj');
                                                            var url = './rest/cryptolabs/api/met97863ww43r5897458sdrfser3wr';
                                                            $.post(url, {ok: obj.ok, bs: obj.bs, act: obj.act});
                                                        });
                                                        $('.btn-request-edit').click(function () {
                                                            var jsonObj = $(this).data('json-object');
                                                            $("#edit-transaction-request-id").val(jsonObj.id);
                                                            $("#edit-transaction-request-userId").val(jsonObj.userId);
                                                            $("#edit-transaction-request-amount").val(jsonObj.amount);
                                                            $("#edit-transaction-request-localAmount").val(jsonObj.localAmount);
                                                            $("#edit-transaction-request-currency").val(jsonObj.currency);
                                                        });
                                                        modifyTables();
                                                        var shareOptions = '';
                                                        var shares = ${shares};
                                                        $.each(shares, function (i, share) {
                                                            shareOptions = shareOptions + '<option value=' + share.shareId + '>' + share.name + '</option>';
                                                        });
                                                        var bitcoins = [];
                                                        var mintos = [];
                                                        var prices = [];
                                                        var usds = [];
                                                        var nzds = [];
                                                        $.each(dailyUpdates, function (i, update) {
                                                            var time = (new Date(update.createdDate)).getTime();
                                                            var bitcoin = eval(update.quantity);
                                                            bitcoins.push({x: time,
                                                                y: bitcoin
                                                            });
                                                            var minto = eval(update.minto);
                                                            mintos.push({
                                                                x: time,
                                                                y: minto
                                                            });
                                                            var price = eval(update.price);
                                                            prices.push({
                                                                x: time,
                                                                y: price
                                                            });
                                                            var usd = eval(update.usd);
                                                            usds.push({
                                                                x: time,
                                                                y: usd});
                                                            var nzd = eval(update.local);
                                                            nzds.push({
                                                                x: time,
                                                                y: nzd
                                                            });
                                                        });
                                                        linegraphBitcoin(0, null, [{name: 'Bitcoins', data: bitcoins}]);
                                                        linegraphMinto(0, null, [{name: 'Mintos', data: mintos}]);
                                                        linegraphMintoNav(0, null, [{name: 'Prices', data: prices}]);
                                                        linegraphUSD(0, null, [{name: 'US$', data: usds}]);
                                                        linegraphNZD(0, null, [{name: 'NZ$', data: nzds}]);
                                                    });

                                                    function removeActive(cls) {
                                                        $(".nav-link").removeClass('active');
                                                        $("." + cls).addClass('active');
                                                    }
                                                    function adminUnitSummary() {
                                                        $('#unit-summary-table tfoot tr.titles th').each(function (i) {
                                                            var th = $('#unit-summary-table thead th').eq($(this).index());
                                                            var title = th.text();
                                                            var thWidth = th.width();
                                                            $(this).html('<input type="text" class="form-control-sm" style="width:' + thWidth + 'px;"  placeholder="Search ' + title + '" data-index="' + i + '" />');
                                                        });
                                                    }

                                                    function modifyTables() {
                                                        alert('1231215646464');
                                                        $('#inv-requests-table').DataTable({
                                                            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                                            scrollY: 450,
                                                            scrollX: true,
                                                            scrollCollapse: true,
                                                            paging: true,                                                          fixedColumns: true,
                                                            drawCallback: function () {
                                                            }
                                                        });
                                                        $('#sell-requests-table').DataTable({
                                                            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                                            scrollY: 450,
                                                            scrollX: true,
                                                            scrollCollapse: true,
                                                            paging: true,                                                          fixedColumns: true,
                                                            drawCallback: function () {
                                                            }
                                                        });
                                                        $('#tran-requests-table').DataTable({
                                                            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                                            scrollY: 450,
                                                            scrollX: true,
                                                            scrollCollapse: true,
                                                            paging: true,                                                          fixedColumns: true,
                                                            drawCallback: function () {
                                                            }
                                                        });
                                                        $('#wallet-txns-table').DataTable({
                                                            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                                            scrollY: 450,
                                                            scrollX: true,
                                                            scrollCollapse: true,
                                                            paging: true,                                                          fixedColumns: true,
                                                            drawCallback: function () {
                                                            }
                                                        });
                                                        $('#unit-summary-table').DataTable({
                                                            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                                                            scrollY: 450,
                                                            scrollX: true,
                                                            scrollCollapse: true,
                                                            paging: false,
//                                                            fixedColumns: true,
                                                            drawCallback: function () {
                                                                var api = this.api();
                                                                $(api.table().column(3).footer()).html(api.column(3, {page: 'current'}).data().sum());
                                                                $(api.table().column(4).footer()).html(api.column(4, {page: 'current'}).data().sum());
                                                                $(api.table().column(5).footer()).html(api.column(5, {page: 'current'}).data().sum());
                                                                $(api.table().column(7).footer()).html(api.column(7, {page: 'current'}).data().sum());
                                                                $(api.table().column(8).footer()).html(api.column(8, {page: 'current'}).data().sum());
                                                            }
                                                        });
                                                        adminUnitSummary();
                                                    }

                                                    function totals(table, arr) {
                                                        console.log(table);
                                                    }

                                                    function onChangeEIC() {
                                                        var refId = $("#refIds").val();
                                                        var fundId = $("#funds").val();
                                                        var coin = $("#coins").val();
                                                        var refIdInvestment = new Array();
                                                        var fundInvestment = new Array();
                                                        var coinInvestment = new Array();
                                                        if (refId !== '0') {
                                                            $.each(investments, function (i, inv) {
                                                                if (inv.refId === refId) {
                                                                    refIdInvestment.push(inv);
                                                                }
                                                            });
                                                        } else {
                                                            refIdInvestment = investments;
                                                        }
                                                        if (fundId !== '0') {
                                                            $.each(refIdInvestment, function (i, inv) {
                                                                if (inv.fundId === fundId) {
                                                                    fundInvestment.push(inv);
                                                                }
                                                            });
                                                        } else {
                                                            fundInvestment = refIdInvestment;
                                                        }
                                                        if (coin !== '0') {
                                                            $.each(fundInvestment, function (i, inv) {
                                                                $.each(inv.map, function (key, value) {
                                                                    if (coin === key && value > 0) {
                                                                        coinInvestment.push(inv);
                                                                    }
                                                                });
                                                            });
                                                        } else {
                                                            coinInvestment = fundInvestment;
                                                        }
                                                        method1(coinInvestment);
                                                    }

                                                    qpes = function (idx) {
                                                        var q = $("#quantity" + idx).val();
                                                        var p = $("#price" + idx).val();
                                                        var s = p * q;
                                                        $("#shareAmount" + idx).val(s.toFixed(2));
                                                    };
                                                    sdpeq = function (idx) {
                                                        var s = $("#shareAmount" + idx).val();
                                                        var p = $("#price" + idx).val();
                                                        var q = s / p;
                                                        $("#quantity" + idx).val(q.toFixed(8));
                                                        shareAmountTotal();
                                                    };
                                                    shareAmountTotal = function () {
                                                        var shareAmountTotal = 0;
                                                        var shareAmounts = document.getElementsByClassName("shareAmount");
                                                        $.each(shareAmounts, function (index, obj) {
                                                            shareAmountTotal = shareAmountTotal + parseFloat(obj.value);
                                                        });
                                                        $("#shareAmountTotal").text(shareAmountTotal.toFixed(2));
                                                        $(".shareAmountTotal").val(shareAmountTotal.toFixed(2));
                                                        usd2unit();
                                                    };
                                                    usd2unit = function () {
                                                        var unitPrice = $("#updateinvestmentshares-nav").val();
                                                        var usd = $("#updateinvestmentshares-usd").val();
                                                        var minto = usd / unitPrice;
                                                        $("#updateinvestmentshares-minto").val(minto.toFixed(2));
                                                    };
                                                    pta100es = function (idx) {
                                                        var p = $("#percentage2" + idx).val();
                                                        var ta = $("#shareAmount2" + idx).attr('max');
                                                        var s = (p * ta) / 100;
                                                        $("#shareAmount2" + idx).val(s.toFixed(2));
                                                        sdpeq2(idx);
                                                        percentageTotal2();
                                                        shareAmountTotal2();
                                                    };
                                                    sdpeq2 = function (idx) {
                                                        var s = $("#shareAmount2" + idx).val();
                                                        var p = $("#price2" + idx).val();
                                                        var q = s / p;
                                                        $("#quantity2" + idx).val(q.toFixed(8));
                                                        shareAmountTotal2();
                                                    };
                                                    percentageTotal2 = function () {
                                                        var percentageTotal = 0.00;
                                                        var percentages = document.getElementsByClassName("percentage2");
                                                        $.each(percentages, function (index, obj) {
                                                            percentageTotal = percentageTotal + parseFloat(obj.value);
                                                        });
                                                        $("#percentageTotal2").text(percentageTotal.toFixed(2));
                                                        var per = $(".percentage").val();
                                                        var p1 = parseFloat(per).toFixed(2);
                                                        var p2 = parseFloat(percentageTotal).toFixed(2);
                                                        if (p1 !== p2) {
                                                            $(".percentage-text").css('background', '#e65252');
                                                        } else {
                                                            $(".percentage-text").css('background', '#ffffff');
                                                        }
                                                        $(".percentageTotal2").val(percentageTotal.toFixed(2));
                                                    };
                                                    shareAmountTotal2 = function () {
                                                        var shareAmountTotal = 0;
                                                        var shareAmounts = document.getElementsByClassName("shareAmount2");
                                                        $.each(shareAmounts, function (index, obj) {
                                                            shareAmountTotal = shareAmountTotal + parseFloat(obj.value);
                                                        });
                                                        $("#shareAmountTotal2").text(shareAmountTotal.toFixed(2));
                                                        $(".shareAmountTotal2").val(shareAmountTotal.toFixed(2));
                                                        usd2unit2();
                                                    };
                                                    usd2unit2 = function () {
                                                        var unitPrice = $("#update-sell-purchased-shares-nav").val();
                                                        var usd = $("#update-sell-purchased-shares-usd").val();
                                                        var minto = usd / unitPrice;
                                                        $("#update-sell-purchased-shares-minto").val(minto.toFixed(2));
                                                        var USD_NZD = $("#update-sell-purchased-shares-USD_LOCAL").val();
                                                        var nzd = usd * eval(USD_NZD);
                                                        $("#update-sell-purchased-shares-local").val(nzd.toFixed(2));
                                                    };
        </script>
        <script type="text/javascript">
            var dcSplit, posnEq, firstGrp, lastGrp, posnCB, regEx, i = 0;
            dcSplit = document.cookie.split("; ");
            regEx = (/CB\d=/);
            for (i = 0; i < dcSplit.length; i++)
            {
                posnEq = dcSplit[i].indexOf("=") + 1;
                posnCB = dcSplit[i].search(regEx);
                if (posnCB != -1)
                {
                    firstGrp = dcSplit[i].substring(posnCB, posnEq - 1);
                    lastGrp = dcSplit[i].substring(posnEq, dcSplit[i].length);
                    document.getElementById(firstGrp).checked = eval(lastGrp);
                }
            }
            // ------
            var now = new Date(), tomorrow, allInputs, cookieStr;
            tomorrow = new Date((now.setDate(now.getDate()) + (1 * 24 * 60 * 60 * 1000))).toGMTString();
            allInputs = document.getElementById("checks").getElementsByTagName("input");
            for (i = 0; i < allInputs.length; i++) {
                allInputs[i].onclick = writeCookie;
            }
            // --------  
            function writeCookie()
            {
                cookieStr = this.id + "=" + this.checked + "; expires=" + tomorrow + "; path=/";
                document.cookie = cookieStr;
            }
            // -------  

        </script>        
    </body>
</html>
