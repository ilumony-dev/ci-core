/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import com.nz.cryptolabs.controllers.payments.ChargeRequest;
import com.nz.cryptolabs.services.DateTimeServiceImpl;
import com.nz.cryptolabs.services.ObjectCastService;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class CommonMethods {

    private final Map<String, ChargeRequest.Currency> payCurrency = new HashMap<>();
    private final Map<String, String> modelIds = new HashMap<String, String>();
    private final Map<String, String> userCurrency = new HashMap<String, String>();
    private final Map<String, String> currSymbol = new HashMap<String, String>();
    private final Map<String, String> reportTypes = new HashMap<String, String>();
    private static final long ONE_SECOND = 1000;
    public static final long ONE_MINUTE = 60 * ONE_SECOND;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    private static final String DATE_FORMAT1 = "yyyy/MM/dd HH:mm:ss";
    private static final String DATE_FORMAT2 = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT3 = "yyyy-MM-dd";
    private static final String DATE_FORMAT_dd_MM = "dd/MM";
    public static final SimpleDateFormat format1 = new SimpleDateFormat(DATE_FORMAT1);
    public static final SimpleDateFormat format2 = new SimpleDateFormat(DATE_FORMAT2);
    public static final SimpleDateFormat format3 = new SimpleDateFormat(DATE_FORMAT3);
    public static final SimpleDateFormat format_dd_MM = new SimpleDateFormat(DATE_FORMAT_dd_MM);

    public int getRandomNumber() {
        Random random = new Random();
        int number = 100000 + random.nextInt(900000);
        return number;
    }

    public Date parseDate(String date) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            return format1.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date parseDate(String date, String format) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public Date parseDate(String date, DateFormat dateFormat) {
        if (date == null) {
//            return new Date();
            return today();
        }
        try {
            return dateFormat.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }

    public String ts2date(String timestamp) {
        Date date = parseDate(timestamp, format2);
        return format3.format(date);
    }

    public BigDecimal parseBigDecimal(String bigDecimal) {
        if (bigDecimal == null) {
            bigDecimal = "0";
        }
        return new BigDecimal(bigDecimal);
    }

    public Integer parseInt(String numeric) {
        if (numeric == null) {
            numeric = "0";
        }
        return isNumeric(numeric) ? Integer.parseInt(numeric) : 0;
    }

    public boolean isNumeric(String maybeNumeric) {
        maybeNumeric = maybeNumeric.replaceAll(",", "");
        return maybeNumeric != null && maybeNumeric.matches("[0-9]+");
    }

    public Date today() {
        return DateTimeServiceImpl.current();
    }

    public String dateFormat(java.util.Date date) {
        return format1.format(date);
    }

    public String dateFormat(java.util.Date date, DateFormat format) {
        return format.format(date);
    }

    public String dateFormat(java.util.Date date, String DATE_FORMAT) {
        DateFormat format = new SimpleDateFormat(DATE_FORMAT);
        return format.format(date);
    }

    public String checkNull(String string, String defaultString) {
        return string != null || string != null && string.equalsIgnoreCase("null") ? string : defaultString;
    }

    public boolean checkToken(String token) {
        return token != null;
    }

    public String timeDiff(Date d1, Date d2) {
        //in milliseconds
        long diff = Math.abs(d1.getTime() - d2.getTime());

        long diffSeconds = diff / 1000;
        if (diffSeconds < 60) {
            return "Just Now";
        }
        long diffMinutes = diff / (60 * 1000);
        if (diffMinutes < 60) {
            return diffMinutes + " minutes ago";
        }
        long diffHours = diff / (60 * 60 * 1000);
        if (diffHours < 24) {
            return diffHours + " hours ago";
        }
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return diffDays + " days ago";
    }

    public BigDecimal round(BigDecimal bd, int places) {
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd;
    }

    public BigDecimal round(String value, int places) {
        if (value == null || value != null && value.trim().isEmpty()) {
            value = "0.00";
        }
        return round(new BigDecimal(value), places);
    }

    public String round2String(String value, int places) {
        double d = round(value, places).doubleValue();
        return String.valueOf(d);
    }

    public String dayDateOfMonth(java.util.Date dt) {
        String day = day(dt.getDay());
        String date = date(dt.getDate());
        String month = month(dt.getMonth() + 1);
        return date + "/" + month;
    }

    public String dateOfMonth(java.util.Date dt) {
        return format_dd_MM.format(dt);
    }

    private static String day(int day) {
        String dy = null;
        if (1 == day) {
            dy = "Monday";
        } else if (2 == day) {
            dy = "Tuesday";
        } else if (3 == day) {
            dy = "Wednesday";
        } else if (4 == day) {
            dy = "Thursday";
        } else if (5 == day) {
            dy = "Friday";
        } else if (6 == day) {
            dy = "Saturday";
        } else if (7 == day) {
            dy = "Sunday";
        }
        return dy;
    }

    private static String date(int date) {
        String dt = null;
        if (1 == date) {
            dt = "1st";
        } else if (2 == date) {
            dt = "2nd";
        } else if (3 == date) {
            dt = "3rd";
        } else if (4 <= date) {
            dt = date + "th";
        }
        return dt;
    }

    public static String month(int month) {
        String mth = null;
        if (1 == month) {
            mth = "January";
        } else if (2 == month) {
            mth = "February";
        } else if (3 == month) {
            mth = "March";
        } else if (4 == month) {
            mth = "April";
        } else if (5 == month) {
            mth = "May";
        } else if (6 == month) {
            mth = "June";
        } else if (7 == month) {
            mth = "July";
        } else if (8 == month) {
            mth = "August";
        } else if (9 == month) {
            mth = "September";
        } else if (10 == month) {
            mth = "October";
        } else if (11 == month) {
            mth = "November";
        } else if (12 == month) {
            mth = "December";
        }
        return mth;
    }

    public List<String> hours() {
        List<String> hours = new ArrayList<>();
        for (int i = 0; i <= 23; i++) {
            String hour = "";
            if (i < 10) {
                hour = "0";
            }
            hour += i;
            hours.add(hour);
        }
        return hours;
    }

    public List<String> minutes() {
        List<String> minutes = new ArrayList<>();
        for (int i = 0; i <= 59; i++) {
            String minute = "";
            if (i < 10) {
                minute = "0";
            }
            minute += i;
            minutes.add(minute);
        }
        return minutes;
    }

    {
        currSymbol.put("USD", "US$");
        currSymbol.put("NZD", "NZ$");
        currSymbol.put("SGD", "SGD$");
        currSymbol.put("AUD", "AU$");
        currSymbol.put("GBP", "GB£");
        currSymbol.put("HKD", "HK$");
        currSymbol.put("INR", "INR");
    }

    {
        reportTypes.put("BR", "Burn Report");
        reportTypes.put("MR", "Management Account Transfer Report");
        reportTypes.put("DR", "Deposits Report");
        reportTypes.put("WR", "Withdraw Report");
    }

    {
        payCurrency.put("USD", ChargeRequest.Currency.USD);
        payCurrency.put("NZD", ChargeRequest.Currency.NZD);
        payCurrency.put("SGD", ChargeRequest.Currency.SGD);
        payCurrency.put("AUD", ChargeRequest.Currency.AUD);
        payCurrency.put("GBP", ChargeRequest.Currency.GBP);
        payCurrency.put("HKD", ChargeRequest.Currency.HKD);
        payCurrency.put("INR", ChargeRequest.Currency.INR);
    }

    public final Map<String, String> userCurrency() {
        return userCurrency;
    }

    public final Map<String, String> currSymbol() {
        return currSymbol;
    }

    public final Map<String, String> modelIds() {
        return modelIds;
    }

    public final Map<String, String> reportTypes() {
        return reportTypes;
    }

    public final Map<String, ChargeRequest.Currency> payCurrency() {
        return payCurrency;
    }

    public final HashMap<String, String> accountDetails(String currency) {
        HashMap<String, String> currencyAcc = new HashMap<>();
        if (null != currency) {
            switch (currency) {
                case "USD":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "Level 1, 187-193 Broadway, Newmarket, Auckland, New Zealand");
                    currencyAcc.put("bankPhone", "0800 269 249");
                    currencyAcc.put("swiftCode", "ANZBNZ22");
                    currencyAcc.put("accName", "Ilumony USD Client Funds A/C");
                    currencyAcc.put("accAddress", "151 Reeves Road, Pakuranga Heights, Auckland 2010, New Zealand");
                    currencyAcc.put("accNumber", "253901USD00001");
                    break;
                case "NZD":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "");
                    currencyAcc.put("bankPhone", "");
                    currencyAcc.put("swiftCode", "");
                    currencyAcc.put("accName", "Ilumony Client Funds");
                    currencyAcc.put("accAddress", "");
                    currencyAcc.put("accNumber", "06-0193-0808849-00");
                    break;
                case "SGD":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "Level 1, 187-193 Broadway, Newmarket, Auckland, New Zealand");
                    currencyAcc.put("bankPhone", "0800 269 249");
                    currencyAcc.put("swiftCode", "ANZBNZ22");
                    currencyAcc.put("accName", "Ilumony USD Client Funds A/C");
                    currencyAcc.put("accAddress", "151 Reeves Road, Pakuranga Heights, Auckland 2010, New Zealand");
                    currencyAcc.put("accNumber", "253901SGD00001");
                    break;
                case "AUD":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "Level 1, 187-193 Broadway, Newmarket, Auckland, New Zealand");
                    currencyAcc.put("bankPhone", "0800 269 249");
                    currencyAcc.put("swiftCode", "ANZBNZ22");
                    currencyAcc.put("accName", "Ilumony USD Client Funds A/C");
                    currencyAcc.put("accAddress", "151 Reeves Road, Pakuranga Heights, Auckland 2010, New Zealand");
                    currencyAcc.put("accNumber", "253901AUD00001");
                    break;
                case "GBP":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "Level 1, 187-193 Broadway, Newmarket, Auckland, New Zealand");
                    currencyAcc.put("bankPhone", "0800 269 249");
                    currencyAcc.put("swiftCode", "ANZBNZ22");
                    currencyAcc.put("accName", "Ilumony USD Client Funds A/C");
                    currencyAcc.put("accAddress", "151 Reeves Road, Pakuranga Heights, Auckland 2010, New Zealand");
                    currencyAcc.put("accNumber", "253901GBP00001");
                    break;
                case "HKD":
                    currencyAcc.put("bankName", "ANZ Bank");
                    currencyAcc.put("bankAddress", "Level 1, 187-193 Broadway, Newmarket, Auckland, New Zealand");
                    currencyAcc.put("bankPhone", "0800 269 249");
                    currencyAcc.put("swiftCode", "ANZBNZ22");
                    currencyAcc.put("accName", "Ilumony USD Client Funds A/C");
                    currencyAcc.put("accAddress", "151 Reeves Road, Pakuranga Heights, Auckland 2010, New Zealand");
                    currencyAcc.put("accNumber", "253901HKD00001");
                    break;
                default:
                    break;
            }
        }
        return currencyAcc;
    }

    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException {
        return objectCastService.jSONcast(clazz, jsonInString);
    }

    public List<String> getAllDatesOfLatestWeek() {
        Calendar now = Calendar.getInstance();
        now.setTime(today());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<String> days = new ArrayList<>();
        now.add(Calendar.DAY_OF_MONTH, -7);
        for (int i = 0; i < 8; i++) {
            days.add(format.format(now.getTime()));
            now.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    public List<String> getAllDates(String fDate, String tDate) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = parseDate(fDate, format);
        Date toDate = parseDate(tDate, format);
        cal.setTime(fromDate);
        List<String> days = new ArrayList<>();
        while (cal.getTime().before(toDate) || cal.getTime().equals(toDate)) {
            days.add(format.format(cal.getTime().getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return days;
    }

    public String formatMoney(String amount, Integer length) {
        if (amount != null && !amount.isEmpty()) {
            BigDecimal money = new BigDecimal((String) amount);
            money = money.setScale(length, RoundingMode.HALF_UP);
            String mny0 = NumberFormat.getCurrencyInstance().format(money);
            String mny1 = mny0.replaceFirst("\\$", "");
            String mny2 = mny1.replaceFirst("Rs.", "");
            return mny2;
        }
        return amount;
    }

    public String formatMoney(Object amount, Integer length, String currency) {
        String money = formatMoney((String) amount, length);
        if (currency != null) {
            return currSymbol().get(currency) + " " + money;
        }
        return money;
    }
    @Autowired
    private ObjectCastService objectCastService;
}
