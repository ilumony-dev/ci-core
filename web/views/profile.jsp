<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Profile</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">   
                            <div class="row market">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Your Personal Information
                                        </h6>
                                        <div class="element-box">
                                            <form method="post" id="submit-complete" action="./profile">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>FIRST NAME</label>
                                                            <input class="form-control edit-input" placeholder="First Name" name="firstName" id="firstName" type="text" value="${userInfo.firstName}" disabled="disabled">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>LAST NAME</label>
                                                            <input class="form-control edit-input" placeholder="LAST NAME" name="lastName" id="lastName" type="text" value="${userInfo.lastName}" disabled="disabled">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>PHONE NUMBER  </label>
                                                            <input class="form-control edit-input" placeholder="PHONE NUMBER" name="mobileNo" id="mobileNo" type="text" value="${userInfo.mobileNo}" disabled="disabled">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label> EMAIL ADDRESS</label>
                                                            <input class="form-control" placeholder="EMAIL ADDRESS" name="email" type="text" value="${userInfo.email}" readonly="readonly"  disabled="disabled">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>DATE OF BIRTH</label>
                                                            <div class="date-input">
                                                                <input class="single-daterange form-control" id="dob" name="dob" type="text" value="${userInfo.dob}" readonly="readonly"  disabled="disabled">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- <div class="col-sm-6">
                                                                                                                <div class="form-group">
                                                                                                                    <label >GENDER</label>    </br>           
                                                                                                                    <div class="form-check col-sm-6">
                                                                                                                        <label class="form-check-label">
                                                                                                                            <input checked="checked" class="form-check-input" type="radio" name="gender" value="MALE">Male
                                                                                                                        </label>
                                                                                                                    </div>  
                                                                                                                    <div class="form-check col-sm-6" style="display:inline">
                                                                                                                        <label class="form-check-label">
                                                                                                                            <input class="form-check-input" type="radio" name="gender" value="FEMALE">Female
                                                                                                                        </label>
                                                                                                                    </div>  
                                                                                                                </div>
                                                    </div>--%>
                                                </div>
                                                <%--                                                <div class="row">
                                                                                                    <div class="col-sm-6">
                                                                                                        <div class="form-group">
                                                                                                            <label> COUNTRY OF CITIZENSHIP</label>
                                                                                                            <select class="form-control" id="countries" name="country" value="${userInfo.country}" onchange="populateStates(this.value);">
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-sm-6">
                                                                                                        <div class="form-group">
                                                                                                            <label>STATE  </label>
                                                                                                            <select class="form-control" id="states" name="state" value="${userInfo.state}">
                                                                                                                <option>
                                                                                                                    Select State
                                                                                                                </option>
                                                                                                                <option>
                                                                                                                    Auckland
                                                                                                                </option>
                                                                                                                <option>
                                                                                                                    Wellington
                                                                                                                </option>
                                                                                                                <option>
                                                                                                                    Christchurch
                                                                                                                </option>
                                                                                                                <option>
                                                                                                                    Hamilton
                                                                                                                </option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>--%>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label> HOME ADDRESS</label>
                                                            <input class="form-control edit-input" placeholder="HOME ADDRESS" type="text" name="address"  id="address" value="${userInfo.address}"  readonly="readonly"  disabled="disabled">
                                                        </div>
                                                    </div>
                                                    <%--                                                    <div class="col-sm-3">
                                                                                                            <div class="form-group">
                                                                                                                <label> ZIPCODE </label>
                                                                                                                <input class="form-control" placeholder="ZIPCODE" type="text" name="zipcode" value="${userInfo.zipcode}">
                                                                                                            </div>
                                                                                                        </div>--%>
                                                </div>
                                                <div class="form-buttons-w">
                                                    <a href="javascript:void()" class="btn btn-warning btn-edit">Edit</a>
                                                    <button id="edit-profile" class="btn btn-primary" type="button"> Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>            
                            </div>
<!--                            <div class="element-wrapper">
                                <h6 class="element-header">
                                    Account Progress
                                </h6>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="os-progress-bar primary">
                                            <div class="bar-labels">
                                                <div class="bar-label-left">
                                                    <span>Progress</span><span class="positive">+${progress}%</span>
                                                </div>
                                                <div class="bar-label-right">
                                                    <span class="info">100%</span>
                                                </div>
                                            </div>
                                            <div class="bar-level-1" style="width: 100%">
                                                <div class="bar-level-2" style="width:100%">
                                                    <div class="bar-level-3" style="width:${progress}%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="element-box-tp">
                                    <div class="activity-boxes-w">
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/verification.png"> 
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty verification.pp_passport_number}">
                                                            <img alt="" src="./resources/img/pp.png"  style="width:30%">
                                                        </c:if>
                                                        <c:if test="${not empty verification.pa_number}">
                                                            <img alt="" src="./resources/img/pa.png"  style="width:30%"> 
                                                        </c:if>
                                                        <c:if test="${not empty verification.address}">
                                                            <img alt="" src="./resources/img/pa.png"  style="width:30%"> 
                                                        </c:if>
                                                        <c:if test="${not empty verification.dl_license_number}">
                                                            <img alt="" src="./resources/img/dl.png"  style="width:30%"> 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">   User Verification</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/fundselection.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userFunds}">
                                                            <img alt="" src="./resources/img/portfolio13.jpg"  style="width:30%">+${userInfoFunds.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">  Fund Selection</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/fundselection.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userInvestments}">
                                                            <img alt="" src="./resources/img/portfolio14.jpg"  style="width:30%">+${userInfoInvestments.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title"> Fund Allocation</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time active">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/makingmoney.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userInvestments}">
                                                            <img alt="" src="./resources/img/ilumony-bg5.png"  style="width:30%">+${totalcoins.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">  Making Money</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                        <jsp:include page="user-right-sidebar.jsp"/>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
        <script type="text/javascript">
            var countries = new Array();
            $(document).ready(function () {
            $(".btn-edit").click(function () {
            $(".edit-input").removeAttr("disabled");
            $(".edit-input").removeAttr("readonly");
            });
            $("#edit-profile").click(function(){
                    swal({
                    title: "Proceed",
                    text: "Thanks for updating your profile details. We'll review this new information shortly, and once this has been verified you'll be able to see your updated details here.",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: true
            }, function(){
                document.getElementById("submit-complete").submit(); //form submission
//            window.location.href = "./welcome";
            });
                });
            $.ajax({
            type: "GET",
                    dataType: "json",
                    url: "${home}/rest/cryptolabs/api/countries",
                    success: function (data) {
                    countries = data;
                    console.log("response:" + data);
                    var val = '<option>Select Country</option>';
                    $.each(countries, function (index, obj) {
                    if (obj.code === '${userInfo.country}') {
                    val = val + '<option selected value=' + obj.code + '>' + obj.name + '</option>';
                    } else {
                    val = val + '<option value=' + obj.code + '>' + obj.name + '</option>';
                    }
                    });
                    $("#countries").html(val);
                    populateStates('${userInfo.country}');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    console.log(' Error in processing! ' + textStatus);
                    }
            });
            });
            function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
            if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
            return;
            }
            }
            }
            function populateStates(code) {
            $("#states").html('');
            var val = '<option>Select State</option>';
            $.each(countries, function (index, c) {
            if (c.code === code) {
            $.each(c.details, function (index, s) {
            val = val + '<option value=' + s.detId + '>' + s.detName + '</option>';
            });
            }
            });
            $("#states").html(val);
            }
        </script>
        <script>
            var placeSearch, autocomplete;
            var componentForm = {
            locality: 'long_name',
                    sublocality_level_1: 'long_name'
            };
            function initAutocomplete() {

            autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('address')),
            {types: ['address'], componentRestrictions: {country: 'nz'}});
            autocomplete.addListener('place_changed', fillInAddress);
            }

            function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            }
            }
            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
        async defer></script>
    </body>
</html>
