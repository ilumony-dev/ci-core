<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="User dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/sweetalert2.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Portfolios</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${investment.fundName}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">${investment.fundName}</h6>
                                        <p>Hey ${info.firstName}, welcome to Invsta! Here's an overview of your investment. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-box">
                                        <div class="os-tabs-w">
                                            <div class="os-tabs-controls">
                                                <ul class="nav nav-tabs smaller">
                                                    <!--                                                    <li class="nav-item">
                                                                                                            <a class="nav-link" data-toggle="tab" href="#tab-balance">Balance</a>
                                                                                                        </li>-->
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab-live">Live</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-latestweek">Daily Chart</a>
                                                    </li>
                                                </ul>
                                                <ul class="nav nav-pills smaller hidden-sm-down">
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane" id="tab-balance">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Crypto Current Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">

                                                    </div>
                                                    <div class="el-chart-w" id="lineChart">
                                                    </div>
                                                </div>
                                                <div class="tab-pane active" id="tab-live">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Live Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">
                                                    </div>
                                                    <div class="el-chart-w" id="linegraph-live"></div>
                                                </div>
                                                <div class="tab-pane" id="tab-latestweek">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Daily Chart
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">
                                                    </div>
                                                    <div class="el-chart-w" id="lg-latestweek"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='element-box <c:if test="${info.active eq 'P'}">pending-box</c:if>' style="display: flex; justify-content: space-between">
                                            <a href="#" data-target="#add-funds-to-portfolio" id="add-funds-to-portfolio-action" data-toggle="modal" class="color3 btn btn-success btn-hover" style="width:40%">Add Funds</a>
                                            <a href="#" data-target="#sell-funds-to-portfolio" id="sell-funds-to-portfolio-action" data-toggle="modal" class="color3 btn btn-danger btn-hover" style="width:40%">Sell Funds</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="elementboxcontent" class="element-box">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Portfolio Description</h6>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                ${portfolio.description}
                                            </div>
                                        </div>
                                    </div>
                                    <div id="elementboxcontent" class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Performance Chart</h6>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4">
                                                <div class="el-chart-w" id="hr24Chart"></div>
                                                <div id="legend-3"></div>
                                                <center>1 Day</center>
                                            </div>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="el-chart-w" id="hr247Chart"></div>
                                                <div id="legend-4"></div>
                                                <center>1 Week</center>
                                            </div>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="el-chart-w" id="hr1Chart"></div>
                                                <div id="legend-2"></div>
                                                <center>1 Month</center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div id="elementboxcontent" class="element-box">
                                                <div class="element-wrapper">
                                                    <h6 class="element-header" >Your Investment Details</h6>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-12">
                                                        <h6 style="font-size: 14px;text-align: center;">Total Invested</h6>
                                                        <div class="col-md-12 color3 invest6 btn-hover">
                                                            <center> ${common.formatMoney(investment.investmentAmount, 2, currency)}</center>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-12">
                                                        <h6 style="font-size: 14px;text-align: center;">Current Balance</h6>
                                                        <div class="col-md-12 invest1 color3 btn-hover">
                                                            <input type="hidden" id="actualInvested"/>
                                                            <input type="hidden" id="actualCurrentAmount" value="${investment.actualCurrentAmount}"/>
                                                            <center><span id="currentBalance">${investment.unitsAmount}</span></center>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-12">
                                                        <h6 style="font-size: 14px;text-align: center;">Performance</h6>
                                                        <div class="col-md-12 invest1 color3 btn-hover">
                                                            <center id="performance">0.00%</center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Target Investment</h6>
                                        </div>
                                        <div class="el-chart-w" id="firstday-chart" data-highcharts-chart="0"></div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="display: none;">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Current Investment</h6>
                                        </div>
                                        <div class="el-chart-w" id="today-chart"></div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Top 5 Investments</h6>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="" class="a">Share/Coin</th>
                                                        <th class="b">Target %age</th>
                                                        <th class="b">Current %age</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${top5Shares}" var="share">
                                                        <tr>
                                                            <td style="text-transform: uppercase"><img src="./resources/img/coin-images/${share.shareName}.png" style="width:25px;height: 25px; margin-right: 5px">${share.shareName}</td>
                                                            <td style="">${share.targetPercentage} %</td>
                                                            <td style="">${share.percentage} %</td>                                                
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="display-type"></div>
        </div>
        <%@include file="../views/modals/add-funds-to-portfolio.jsp" %>
        <%@include file="../views/modals/sell-funds-to-portfolio.jsp" %>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/investmentJs/SliderJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/MyPlanJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/CalculateMyBalanceJs.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="${home}/resources/js/cryptolabs/user-main.js"></script>
        <script src="${home}/resources/js/sweetalert2.js"></script>
        <script>
        var endUser = null, latestWeek = null, investment = null, sumInv = null, countOfSellRequests = 0, portfolioReports = null, walletBalance = null;
        var localCurrency = '${localCurrency}', currSymbol = '${currSymbol}';
        </script>
        <script src="${home}/resources/js/cryptolabs/user-charts.js"></script>
        <script>
        $(document).ready(function () {
        $.get("./rest/cryptolabs/modelmap/${key}", function (data, status) {
        var jO = JSON.parse(data);
        endUser = jO.info;
        latestWeek = jO.latestWeek;
        investment = jO.investment;
        portfolioReports = jO.portfolioReports;
        walletBalance = jO.walletBalance;
        sumOfSellingPercentage = jO.sumOfSellingPercentage;
        countOfSellRequests = jO.countOfSellRequests;
        var shares = investment.fundActualShares;
        firstdayChart(shares);
        todayChart(shares);
        var percent1hr = 0;
        var percent24hr = 0;
        var percent7d = 0;
        var amt = eval(investment.unitsAmount);
        percent24hr = eval(portfolioReports[1].performance);
        hr24hChart(percent24hr, 100);
        percent7d = eval(portfolioReports[2].performance);
        hr7dChart(percent7d, 100);
        percent1hr = eval(portfolioReports[3].performance);
        hr30dChart(percent1hr, 100);
        var values = new Array();
        values.push(percent7d);
        values.push(percent24hr);
        values.push(percent1hr);
        values.push(amt);
        var labels = new Array();
        labels.push('Before 7 days');
        labels.push('Before 1 day');
        labels.push('Before 1 hour');
        labels.push('Current Amount');
        lineChart('lineChart', values, labels);
        linegraphlive(amt, true);
        setInterval(function () {
        amt = eval(investment.unitsAmount) + Math.random();
        var val = eval(investment.investmentAmount);
        var inc = amt - val;
        var per = inc * 100 / val;
        $('#performance').text(per.toFixed(2) + '%');
        $('#currentBalance').text(currSymbol + amt.toFixed(2));
        $("#actualInvested").val(amt.toFixed(2));
        }, 1000);
        var vals = new Array();
        $.each(latestWeek, function (idx, day) {
        vals.push({
        x: new Date(day.current_date),
                y: eval(day.investment_amount)
        });
        });
        vals.push({
        x: new Date(),
                y: amt
        });
        lglatestweek(vals, 'lg-latestweek');
        endOfReady();
        });
        $("#add-funds-to-portfolio-action").click(function () {
        $("#add-funds-to-portfolio-title").html('Add to this investment <br>Available Invsta cash balance: ' + walletBalance + '');
        $("#add-funds-to-portfolio-investmentId").val(investment.investmentId);
        $("#add-funds-to-portfolio-walletBalance").val(walletBalance);
        $("#add-funds-to-portfolio-fundName").val(investment.fundName);
        $("#add-funds-to-portfolio-localCurrency").val(localCurrency);
        $("#add-funds-to-portfolio-fundId").val(investment.fundId);
        $("#add-funds-to-portfolio-investmentAmount").attr('max', walletBalance);
        $("#add-funds-to-portfolio-investmentAmount").attr('value', 0);
        $("#add-funds-to-portfolio-submit").attr('disabled', true);
        });
        $("#add-funds-to-portfolio-investmentAmount").keyup(function () {
        $("#add-funds-to-portfolio-submit").attr('disabled', true);
        var amount = eval(this.value);
        var wallet = eval($("#add-funds-to-portfolio-walletBalance").val());
        if (amount > 0.00 && amount <= wallet) {
        $("#add-funds-to-portfolio-submit").attr('disabled', false);
        }
        });
        $("#add-funds-to-portfolio-submit").bind('click', function () {
        swal({
        title: "Success!",
                text: "Thanks for purchasing this fund. We will invest your money in your chosen portfolio."
        });
        $("#add-funds-to-portfolio-form").submit();
        $("#add-funds-to-portfolio-submit").unbind('click');
        });
        $("#sell-funds-to-portfolio-action").click(function () {
        $("#sell-funds-to-portfolio-title").text('Sell funds from this portfolio');
        $("#sell-funds-to-portfolio-para").text('To withdraw all or part of your money from this portfolio, please enter the below details.');
        $("#sell-funds-to-portfolio-para2").html('<i>On completion of sale all money will go to your Invsta Cash Account. From the cash account you can choose to re-invest into another portfolio, withdraw to your bank account, or continue to hold as cash in your Invsta account. </i>');
        $("#sell-funds-to-portfolio-actionType").val('WITHDRAWL');
        $("#sell-funds-to-portfolio-investmentId").val(investment.investmentId);
//        var maxAmount = $("#actualInvested").val();
        var maxAmount = $("#actualCurrentAmount").val();
        $("#sell-funds-to-portfolio-amount").attr('max', maxAmount);
        $("#sell-funds-to-portfolio-amount").attr('readonly', true);
        $("#sell-funds-to-portfolio-fundName").val(investment.fundName);
        $("#sell-funds-to-portfolio-lbl-percentage").show();
        $("#sell-funds-to-portfolio-percentage").show();
        $("#sell-funds-to-portfolio-lbl-amount").text('Approximate sale value:');
        $("#sell-funds-to-portfolio-lbl-percentage").text('Percentage amount of this investment you wish to sell:');
        $("#sell-funds-to-portfolio-lbl-description").text('Optional: Reference or reason for sale');
        $("#sell-funds-to-portfolio-lbl-note").text('Note: this may change as sell orders are placed at the end of day.');
        $("#sell-funds-to-portfolio-amount").attr('value', 0);
        $("#sell-funds-to-portfolio-percentage").attr('max', 100.00 - eval(sumOfSellingPercentage));
        $("#sell-funds-to-portfolio-percentage").attr('value', 0);
        $("#sell-funds-to-portfolio-selling-percentage").text(sumOfSellingPercentage + '%');
        $("#sell-funds-to-portfolio-count-pending-sell-requests").text(countOfSellRequests);
        $("#sell-funds-to-portfolio-submit").attr('disabled', true);
        $(".percentage-icon").show();
        $(".sell-funds-to-portfolio-walletBalance-text").text(maxAmount);
        });
        $("#sell-funds-to-portfolio-submit").bind('click', function () {
        swal({
        title: "Success!",
                text: "Thanks for selling your portfolio. We will transfer your fund in your wallet within 5 to 6 working days."
        });
        $("#sell-funds-to-portfolio-form").submit();
        $("#sell-funds-to-portfolio-submit").unbind('click');
        });
       $("#sell-funds-to-portfolio-percentage").on('keyup mousedown change',function () {
            $("#sell-funds-to-portfolio-submit").attr('disabled', true);
            var maxPercentange = parseFloat($("#sell-funds-to-portfolio-percentage").attr('max'));
            var percentage = parseFloat(this.value);
            //        var maxAmount = eval($("#actualInvested").val());
            var maxAmount = parseFloat($("#actualCurrentAmount").val());
            var val = percentage / 100 * maxAmount;
            $("#sell-funds-to-portfolio-amount").attr('value', val.toFixed(2));
            if (percentage > 0.00 && percentage <= maxPercentange) {
                $("#sell-funds-to-portfolio-submit").attr('disabled', false);
            }
        });
        });
        </script>
    </body>
</html>
