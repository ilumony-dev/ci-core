<%-- 
    Document   : user-right-sidebar
    Created on : 31 Oct, 2017, 12:50:23 PM
    Author     : palo12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--<div class="content-panel">
    <div class="content-panel-close">
        <i class="os-icon os-icon-close"></i>
    </div>
    <div class="element-wrapper">
        <h6 class="element-header">
            Quick Links
        </h6>
        <div class="element-box-tp">
            <div class="el-buttons-list full-width">
                <a class="btn btn-white btn-sm" href="#" data-target="" data-toggle="modal"><i class="os-icon os-icon-delivery-box-2"></i><span>Working on the Best Next thing</span></a>
            </div>
        </div>
    </div>
</div>-->
<jsp:include page = "modals/addtransaction.jsp"></jsp:include>
<script>
        window.intercomSettings = {
            app_id: "q28x66d9",
            name: '${endUser.fullName}', // Full name
            email: '${endUser.email}', // Email address
            created_at: ${endUser.createdTime} // Signup date as a Unix timestamp
        };
</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/q28x66d9';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()
</script>
