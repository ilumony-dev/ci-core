<%-- 
    Document   : admin-right-sidebar
    Created on : Oct 23, 2017, 12:38:19 PM
    Author     : Administrator
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--<div class="content-panel">
    <div class="content-panel-close">
        <i class="os-icon os-icon-close"></i>
    </div>
    <div class="element-wrapper">
        <h6 class="element-header">
            Quick Links
        </h6>
        <div class="element-box-tp">
            <div class="el-buttons-list full-width">
                <a class="btn btn-white btn-sm" href="./admin-invite-codes" ><i class="os-icon os-icon-delivery-box-2"></i><span>Invite Codes</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-currencies" ><i class="os-icon os-icon-delivery-box-2"></i><span>Currencies</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-shares" ><i class="os-icon os-icon-delivery-box-2"></i><span>Shares</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-funds" ><i class="os-icon os-icon-delivery-box-2"></i><span>Funds</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-portfolios" ><i class="os-icon os-icon-delivery-box-2"></i><span>Portfolios</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-user-accounts" ><i class="os-icon os-icon-delivery-box-2"></i><span>User Accounts</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-new-users" ><i class="os-icon os-icon-delivery-box-2"></i><span>New User View</span></a>
                <a class="btn btn-white btn-sm" href="./admin-show-portfolio-buying" ><i class="os-icon os-icon-delivery-box-2"></i><span>Portfolio Buying</span></a>
            </div>

        </div>
    </div>
</div>-->
<jsp:include page = "modals/addshare.jsp" ></jsp:include>
<jsp:include page = "modals/update-share-price.jsp" ></jsp:include>
<jsp:include page = "modals/addfund.jsp" ></jsp:include>
    <script>
        window.intercomSettings = {
            app_id: "q28x66d9",
            name: '${info.fullName}', // Full name
            email: '${info.username}', // Email address
            created_at: ${info.createdTime} // Signup date as a Unix timestamp
        };
</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/q28x66d9';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()
</script>
