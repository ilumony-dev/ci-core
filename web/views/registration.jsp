<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="./resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="./resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="./resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="./resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="./resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link rel="stylesheet" href="./resources/css/awesomplete.css" />
        <link rel="stylesheet" href="./resources/css/intl/intlTelInput.css">
        <link rel="stylesheet" href="./resources/css/intl/demo.css">
        <link href="./resources/css/ion.calender.css" rel="stylesheet"/>
        <link href="./resources/css/checkbox.css" rel="stylesheet"/>

        <script src="./resources/js/awesomplete.js"></script>
        <script src="./resources/js/jquery.min.js"></script>
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>-->
        <!--<script src="./resources/js/jquery_3.2.1.min.js"></script>-->
        <script src="./resources/js/jquery.easing.min.js"></script>
        <script src="./resources/js/v2-jquery.min.js"></script>
        <script src="./resources/js/jquery.powertip.js"></script>
        <script src="./resources/js/validation.js"></script> 
        <script src="./resources/js/jquery-ui.js"></script>
        <script src="./resources/js/index.js"></script>
        <!--<script src="./resources/js/validation.js"></script>-->

        <script>
            !function () {
                var analytics = window.analytics = window.analytics || [];
                if (!analytics.initialize)
                    if (analytics.invoked)
                        window.console && console.error && console.error("Segment snippet included twice.");
                    else {
                        analytics.invoked = !0;
                        analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"];
                        analytics.factory = function (t) {
                            return function () {
                                var e = Array.prototype.slice.call(arguments);
                                e.unshift(t);
                                analytics.push(e);
                                return analytics
                            }
                        };
                        for (var t = 0; t < analytics.methods.length; t++) {
                            var e = analytics.methods[t];
                            analytics[e] = analytics.factory(e)
                        }
                        analytics.load = function (t, e) {
                            var n = document.createElement("script");
                            n.type = "text/javascript";
                            n.async = !0;
                            n.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                            var o = document.getElementsByTagName("script")[0];
                            o.parentNode.insertBefore(n, o);
                            analytics._loadOptions = e
                        };
                        analytics.SNIPPET_VERSION = "4.1.0";
                        analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
                        analytics.page();
                    }
            }();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {hjid: 846291, hjsv: 6};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
                app_id: "q28x66d9"
            };
        </script>

        <script>
            (function () {
                var w = window;
                varic = w.Intercom;
                if (typeof ic === "function") {
                    ic('reattach_activator');
                    ic('update', intercomSettings);
                } else {
                    var d = document;
                    var i = function () {
                        i.c(arguments)
                    };
                    i.q = [];
                    i.c = function (args) {
                        i.q.push(args)
                    };
                    w.Intercom = i;
                    function l() {
                        var s = d.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = 'https://widget.intercom.io/widget/q28x66d9';
                        var x = d.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s, x);
                    }
                    if (w.attachEvent) {
                        w.attachEvent('onload', l);
                    } else {
                        w.addEventListener('load', l, false);
                    }
                }
            })()
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w wider1">
                <div class="logo-w">
                    <a href="./login"><img alt="" src="./resources/img/invsta-03(200px).png" style="width:40%"></a>
                </div>



                <div class="steps-w" style="padding:2em">
                    <form:form method="POST" id="msform" name="msRegform" action="./register" class="form-signup" modelAttribute="user" autocomplete="off" >
                        <!--                                                                <div class="step-triggers">

                                                                                            <a class="step-trigger active" href="#stepContent1">Step 1</a>
                                                                                            <a class="step-trigger" href="#stepContent2">Step 2</a>
                                                                                            <a class="step-trigger" href="#stepContent3">Step 3</a>
                                                                                            <a class="step-trigger" href="#stepContent4">Step 4</a>
                                                                                            <a class="step-trigger" href="#stepContent5">Step 5</a>

                                                                                        </div>-->
                        <!--<a href="./register" style="color:#011c53; display: block; text-align: center; margin-bottom: 20px; font-weight: 500">REGISTER HERE</a>-->
                        <ul id="progressbar">
                            <li class="active">Step</li>
                            <li>Step</li>
                            <li>Step</li>
                            <li>Step</li>
                            <li>Step</li>
                        </ul>

                        <!--<div class="step-contents">-->

                        <!--<div class="step-content active" id="stepContent1">-->
                        <!--                                        <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="fullName"> Full Name</label>
                                                                            <input type="text" class="form-control" id="fullName" name="fullName" required="required" placeholder="Enter full name"  >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="dob"> Date of Birth</label>
                                                                            <input type="text" class="single-daterange form-control" id="dob" name="dob" required="required" placeholder="Enter date of birth">
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                        <fieldset id="firstfs">
                            <div class="element-wrapper">

                                <h5 class="element-header">
                                    You will need an invite code to create a new account. Don't have one? Send us an email at ${adminEmailId} asking to join our beta test group.
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email"> Email address</label>
                                        <input type="email" class="form-control email" id="email" name="email" required="required" placeholder="Enter email" onblur="isEmailAlreadyExist();">
                                        <!--<span style="display:none; " id="validationId" ></span>-->
                                        <span style="display:block; " id="validationId" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password">Create your password</label>
                                        <input type="password" class="form-control" id="password-field" name="password" required="required" placeholder="Enter password">
                                        <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye-slash" title="Show password"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w row" id="div-form-button">
                                <!--<a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>-->
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <!--<input type="button" name="verify" id="firstfs-verify" class="btn btn-primary" value="Verify">-->
                                    <input type="button" name="next" id="firstfs-continue" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>

                        </fieldset>
                        <!--</div>-->
                        <!--<div class="step-content" id="stepContent2">-->
                        <fieldset id="secondfs">
                            <div class="element-wrapper">
                                <h5 class="element-header">
                                    Please enter your details: 
                                </h5>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fullName"> Full Name</label>
                                        <input type="text" class="form-control" style="text-transform:capitalize;" id="fullName" name="fullName" required="required" placeholder="Enter full name" onkeyup="myFunction()" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dob"> Date of Birth</label>
                                        <input type="text" name="dob" id="dob" placeholder="Date of Birth" class="form-control" data-format="YYYY/MM/DD" data-lang="en" required/>
                                    </div>
                                </div>
                            </div>
                            <!--                                        <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="email"> Email address</label>
                                                                                <input type="email" class="form-control" id="email" name="email" required="required" placeholder="Enter email">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="password">Password</label>
                                                                                <input type="password" class="form-control" id="password-field" name="password" required="required" placeholder="Enter password">
                                                                                <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="previous" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <input type="button" name="next" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>
                            <!--</div>-->
                        </fieldset>

                        <!--<div class="step-content" id="stepContent3">-->
<!--                        <fieldset  id="thirdfs">
                            <div class="element-wrapper">
                                <h5 class="element-header">
                                    Please enter the invite code you have received via email. 
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="inviteCode" style="margin-right:10px"> Invite Code:</label>
                                            <input type="text" class="form-control" id="inviteCode" name="inviteCode" required="required" placeholder="Enter invite code"  onblur="isInviteCodeUsed();" onkeyup="clearValidationId3();" style="margin-right:10px">
                                            <input type="button" class="btn btn-success" value="Submit">
                                            <span style="display:block; " id="validationId3" style="margin-left:10px" ></span>
                                        </div>
                                        <P STYLE="color:#999">Please contact us at ${adminEmailId} if you have not received your invite code.</P>
                                    </div>
                                </div>

                            </div>
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="previous" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <input type="button" id="thirdfs-continue" name="next" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>
                            </div>
                        </fieldset>-->
                        <!--<div class="step-content" id="stepContent4">-->
                        <fieldset  id="fourthfs">
                            <div class="element-wrapper">
                                <h5 class="element-header">
                                    Please enter your mobile phone number, and select to have this verified via Text message or Phone call.  
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryCode"> Country Code</label>
                                        <input type="text" class="form-control"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="mobileNo"> Mobile Number</label>
                                        <input type="text" class="form-control error" id="mobileNo" name="mobileNo" required="required" placeholder="Enter mobile number">
                                    </div>
                                </div>
                                <div class="col-sm-3" style="margin-top:10px">
                                    <input type="radio" class="senderType" name="senderType" value="sms" checked="true">
                                    <!--<i class="fa fa-envelope" style="font-size:20px;margin-left: 10px"></i>-->
                                    <img src="./resources/img/message-icon.png" style="width: 20px; margin-left: 5px;">
                                </div>
                                <div class="col-sm-3" style="margin-top:10px">
                                    <input type="radio" class="senderType" name="senderType" value="call"><i class="fa fa-phone" style="font-size:20px;margin-left: 10px"></i>
                                </div>
                                <div class="col-sm-6">
                                    <!-- Button trigger modal -->
                                    <!--                                                <a href="#" onclick="otpGeneration(this);" class="btn btn-primary" data-target="#otp" data-toggle="modal">
                                                                                        Verify
                                                                                    </a>-->
                                    <!-- Modal -->
                                    <div class="modal fade" id="otp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Please check your phone messages for your code</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-inline">
                                                                <div class="form-group" style="display:inline-block">
                                                                    <label for="onetimepassword" style="margin-right:10px"> Enter your 4 digit code:  </label>
                                                                    <input type="text" name="onetimepassword" id="onetimepassword" class="form-control" placeholder="Enter your 4 digit code">
                                                                </div>
                                                                <a href="#" onclick="verifyOTP(this);" class="btn btn-primary">
                                                                    Submit
                                                                </a>
                                                                <small style="display:block"> If you have not received your 4 digit code within one minute, you can request another by  <a href="#" onclick="otpGeneration(this);" >clicking HERE.</a>.</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3" style="text-align: right">
                                                            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Submit</button>-->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="button" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right" id="verify-otp-btn">
                                    <a href="#" onclick="otpGeneration(this);" class="btn btn-primary" data-target="#otp" data-toggle="modal">
                                        Verify
                                    </a>
                                </div>
                                <div class="col-sm-6 text-right" id="continue-4to5-btn">
                                    <input type="button" class="next btn btn-primary" name="next" value="Continue">
                                </div>
                            </div>
                            <!--</div>-->
                        </fieldset>
                        <!--<div class="step-content" id="stepContent5">-->
                        <fieldset  id="fifthfs">
                            <div class="element-wrapper">
                                <h5 class="element-header">
                                    Please read and accept the below Terms and Conditions to continue. 
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="text-center">WEBSITE TERMS OF USE</h4>
                                    <div style="overflow:auto; height: 200px;">
                                        <ol>
                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                <ol>
                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions. Capitalised words are defined in clause 22.</li>
                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition, tolerance to risk and trading experience.
                                                    </li>
                                                </ol>
                                            </li>
                                            <li> <b>SERVICES PROVIDED</b>
                                                <ol>
                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services") in relation to digital currencies (such as, but not limited to, Bitcoin and Ethereum) selected by us from time to time (?Digital Currency?):
                                                        <ol>
                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                            <li>Chat and user support services.</li>
                                                        </ol>
                                                    </li>
                                                    <li>We do not deal in digital currencies or tokens which are or may be Financial Products under New Zealand law on the
                                                        Platform. For this reason, we are not required to hold any licence or authorization in relation to portfolio management of
                                                        the Digital Currency we do support on the Platform. If we become aware that any Digital Currency we have supported on
                                                        the Platform is or may be (in our sole discretion) a Financial Product, you authorize and instruct us to take immediate steps
                                                        to divest any such holdings at the price applying at the time of disposal.</li>
                                                </ol>
                                            </li>
                                            <li> <b>USER PROFILE</b>
                                                <ol>
                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                        <ol>
                                                            <li>be 18 years of age or older;</li>
                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                        </ol>
                                                    </li>
                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                </ol>
                                            <li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                <ol>
                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                    <li>For lump sum investments, you may give instructionstoexecuteaTransactionviaour Platform usingyour User Profile.</li>
                                                    <li>For regular monthly investments, you may select a monthly contribution amount when you create your User Profile.
                                                        This amount may be amended from time to time via our Platform.</li>
                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy
                                                        for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the
                                                        maximum number of units (including fractions of units, if available) of the relevant investments in accordance with
                                                        your selected Portfolio. Any remaining money after a Transaction has been executed will be held in your Wallet. You
                                                        acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.
                                                        You appoint us, and we accept the appointment, to act as your agent for the execution of any Transaction, in
                                                        accordance with these Terms and Conditions.</li>
                                                    <li>We will use reasonable endeavors to execute Transactions within 24 hours of receiving allocated funds into your
                                                        Wallet (subject to these Terms and Conditions, including clause 4.10). The actual price of a particular investment is
                                                        set by the issuer, provider or the market (as applicable) at the time the order is executed by us. We may set a
                                                        minimum transaction amount at any time. We may choose not to process any orders below the minimum transaction
                                                        amount at our discretion. </li>
                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                    <li>After a Transaction has been executed, we reserve the right to void any Transaction which we consider contains any
                                                        manifest error or is contrary to any law or these Terms and Conditions. In the absence of our fraud or willful default,
                                                        we will not be liable to you for any loss, cost, claim, demand or expense following any such action on our part or
                                                        otherwise in relation to the manifest error or Transaction.  </li>
                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                    <li>Any Transaction via the Platform is deemed to take place in New Zealand and you are deemed to take possession
                                                        of your Portfolio in New Zealand
                                                    </li>
                                                </ol>
                                            </li>
                                            <li><b>PORTFOLIO REBALANCING</b>
                                                <ol>
                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                </ol>
                                            </li>
                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                <ol>
                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                    <li>We will notify you of any changes to the Target Asset Allocation promptly following these changes taking effect.
                                                        Notification of any such changes to the Target Asset Allocation are made via the Platform by way of an update to
                                                        the Target Asset Allocation details provided for each Portfolio. </li>
                                                </ol>
                                            </li>

                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                <ol>
                                                    <li><b>Digital Currency Transactions:</b> Invsta processes supported Digital Currency according to the instructions received
                                                        from its users and we do not guarantee the identity of any party to a Transaction. It is your responsibility to verify all
                                                        transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a
                                                        Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the
                                                        Transaction by the Digital Currency network. A Transaction is not complete while it is in a pending state. Funds
                                                        associated with Transactions that are in a pending state will be designated accordingly, and will not be included in
                                                        your Invsta Account balance or be available to conduct Transactions. We may charge network fees (miner fees) to
                                                        process a Transaction on your behalf. We will calculate the network fee in our sole discretion by reference to the 
                                                        cost to us. </li>
                                                    <li><b>Digital Currency Storage & Transmission Delays:</b> Invsta securely stores all Digital Currency private keys in our control
                                                        in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information
                                                        from offline storage in order to facilitate a Transaction in accordance with your instructions, which may delay the
                                                        initiation or crediting of a Transaction for 48 hours or more. You accept that a Digital Currency Transaction facilitated
                                                        by Invsta may be delayed.</li>
                                                    <li><b>Operation of Digital Currency Protocols.</b> Invsta does not own or control the underlying software protocols which
                                                        govern the operation of Digital Currencies supported on our platform. By using the Invsta platform, you acknowledge
                                                        and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no
                                                        guarantee of their functionality, security, or availability; and (ii) that the underlying protocols may be subject to
                                                        sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function,
                                                        and/or essential nature of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that
                                                        Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its
                                                        sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely.
                                                        You acknowledge and agree that Invsta assumes absolutely no responsibility in respect of an unsupported branch
                                                        of a forked Digital Currency and/or protocol.</li>
                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                    <li>You agree that Digital Curreny investments and any other assets held in your Wallet <b>("Client Property")</b> may be
                                                        pooled with the investments and other assets of other clients and therefore your holdings may not be individually
                                                        identifiable within the Wallet. </li>
                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>("Client Money")</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                    <li>Money received from you or a third party for your benefit, which includes your Client Money held pending
                                                        investment, payment to you as the proceeds and income from selling investments, may attract interest from the
                                                        bank at which it is deposited. You consent to such interest being deducted from that bank account and being
                                                        retained by us as part of the fees for using the Services.
                                                    </li>
                                                    <li>We will keep detailed records of all your Client Property and Client Money in your Wallet at all times. </li>
                                                    <li>Client Property and Client Money received or purchased by us on your behalf will be held on bare trust as your
                                                        nominee in the name or to the order of Invsta or any other approved third party custodian to our order. </li>
                                                    <li>You confirm that you are the beneficial owner of all of the Client Property and Client Money in your account, or you
                                                        are the sole trustee on behalf of the beneficial owner, and that they are and will remain free from any encumbrance.</li>
                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?.
                                                        Withdrawals will be processed on a best endeavors basis as outlined in clause 4.6. Withdrawals of Client Money will
                                                        be processed and paid into a bank account nominated by you in the same name of your User Profile upon
                                                        settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn
                                                        from your Wallet. </li>
                                                    <li>It is up to you to understand whether and to what extent, any taxes apply to any Transactions through the Services
                                                        or the Platform, or in relation to your Portfolio. We accept no responsibility for, nor make any representation in respect
                                                        of, your tax liability.
                                                    </li>
                                                </ol>
                                            </li>

                                            <li><b>OUR WEBSITE POLICY</b>
                                                <ol>
                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                </ol>
                                            </li>

                                            <li><b>RISK WARNINGS</b>
                                                <ol>
                                                    <li>You acknowledge and agree that:
                                                        <ol>
                                                            <li>investing in Digital Currency is unlike investing in traditional currencies, goods or commodities: it involves
                                                                significant and exceptional risks and the losses can be substantial. You should carefully consider and assess
                                                                whether trading or holding of digital currency is suitable for you depending upon your financial condition,
                                                                tolerance to risk and trading experience and consider whether you should take independent financial
                                                                advice; </li>
                                                            <li>we have not advised you to, or recommended that you should, use the Platform and/or Services, or trade
                                                                and/or hold Digital Currency;</li>
                                                            <li>unlike other traditional forms of currency, Digital Currency is decentralised and is not backed by a central
                                                                bank, government or legal entities. As such, the value of Digital Currency can be extremely volatile and may
                                                                swing depending upon the market, confidence of investors, competing currencies, regulatory
                                                                announcements or changes, technical problems or any other factors. We give no warranties or
                                                                representations as to the future value of any Digital Currency and accept no liability for any change in value
                                                                of your Portfolio; and
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li>You acknowledge that we do not issue or deal in Financial Products or provide any financial advice and no offer or
                                                        other disclosure document has been, or will be, prepared in relation to the Services, the Platform and/or any of the
                                                        Digital Currencies, under the Financial Markets Conduct Act 2013, the Financial Advisers Act 2008 or any other similar
                                                        legislation in any jurisdiction.</li>
                                                </ol>
                                            </li>

                                            <li><b>DISPUTE RESOLUTION</b>
                                                <ol>
                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:</br>
                                                        <b>Financial Services Complaints Limited (FSCL)</b></br>
                                                        PO Box 5967, Lambton Quay</br>
                                                        Wellington, 6145</br> 
                                                        Email: info@fscl.org.nz</br>
                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                    </li>
                                                </ol>
                                            </li>

                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                <ol>
                                                    <li>You authorize us to:
                                                        <ol>
                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                            <li>Record all telephone conversations with you;</li>
                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                        <ol>
                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                        <ol>
                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                        <ol>
                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                        </ol>
                                                    </li>
                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                </ol>
                                            </li>
                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                <ol>
                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                </ol>
                                            </li>
                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                <ol>
                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                    <li>You accept that there may be circumstances where we are required to suspend or disable your User Profile to meet
                                                        our anti-money laundering obligations.</li>
                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                </ol>
                                            </li>

                                            <li><b>LIMITATION OF LIABILITY</b>
                                                <ol>
                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                    <li>You acknowledge that:
                                                        <ol>
                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                <ol>
                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest in Crypto Currency Portfolios.</li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                    <li>The provisions of this clause 14 will extend to all our employees, agents and contractors, and to all corporate entities
                                                        in which we may have an interest and to all entities which may distribute our publications.</li>
                                                    <li>Despite anything else in these Terms and Conditions, if we are found to be liable for any loss, cost, damage or
                                                        expense arising out of or in connection with your use of the Platform or the Services or these Terms and Conditions,
                                                        our maximum aggregate liability to you will be limited to two times the total amount of fees and charges that you
                                                        have paid to us in the previous twelve months in accordance with clause 15 below.</li>
                                                </ol>
                                            </li>

                                            <li><b>FEES AND CHARGES FOR SERVICES</b>
                                                <ol>
                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges ("Fees"):
                                                        <ol>
                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                            <table class="table-bordered">
                                                                <tr>
                                                                    <td>Portfolio Management Fee</td>
                                                                    <td>Description</td>
                                                                    <td>Other Expenses</td>
                                                                    <td>Performance Fee</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Up to 3% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                    <td>Based on total investment value, charged monthly</td>
                                                                    <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                    <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                </tr>
                                                            </table>
                                                        </ol>
                                                    </li>
                                                    <li>You must also pay for any other fees and charges for any add on services you obtain via the Platform or as specified
                                                        in these Terms and Conditions.</li>
                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                </ol>
                                            </li>

                                            <li><b>TERMINATION OF YOUR USER PROFILE</b>
                                                <ol>
                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to) where:
                                                        <ol>
                                                            <li>you are insolvent or in liquidation or bankruptcy; or</li>
                                                            <li>you have not paid Fees or other amounts due under these Terms and Conditions by the due date</li>
                                                            <li>you gain or attempt to gain unauthorised access to the Platform or another member?s User Profile or Wallet;</li>
                                                            <li>we consider any conduct by you (whether or not that conduct is related to the Platform or the Services) puts
                                                                the Platform, the Services or other users at risk; </li>
                                                            <li>you use or attempt to use the Platform in order to perform illegal or criminal activities;</li>
                                                            <li>your use of the Platform is subject to any pending investigation, litigation or government proceeding;
                                                            </li>
                                                            <li>you fail to pay or fraudulently pay for any transactions;</li>
                                                            <li>you breach these Terms and Conditions and, where capable of remedy, fail to remedy such breach within 30
                                                                days? written notice from us specifying the breach and requiring it to be remedied;</li>
                                                            <li>your conduct may, in our reasonable opinion, bring us into disrepute or adversely affect our reputation or
                                                                image; and/or</li>
                                                            <li>we receive a valid request from a law enforcement or government agency to do so.</li>
                                                        </ol>
                                                    </li>
                                                    <li>We may also terminate these Terms and cease to provide the Services and the Platform if we undergo an insolvency
                                                        event, meaning that where that party becomes unable to pay its debts as they fall due, or a statutory demand is
                                                        served, a liquidator, receiver or manager (or any similar person) is appointed, or any insolvency procedure under
                                                        the Companies Act 1993 is instituted or occurs. </li>
                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the
                                                        proceeds of sale (less any applicable Fees) to a bank account nominated by you. </li>
                                                </ol>
                                            </li>

                                            <li><b>ASSIGNMENT</b>
                                                <ol>
                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                </ol>
                                            </li>

                                            <li><b>INDEMNITY</b>
                                                <ol>
                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                        <ol>
                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                            <li>which they may incur or suffer as a result of:
                                                                <ol>
                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                </ol>
                                            </li>

                                            <li><b>AMENDMENTS</b>
                                                <ol>
                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                        <ol>
                                                            <li>Notice on our website; or</li>
                                                            <li>Direct communication with you via email,</li>
                                                        </ol>
                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                    </li>
                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                </ol>
                                            </li>

                                            <li><b>NOTICES</b>
                                                <ol>
                                                    <li>Any notice or other communication ("Notice") given for the purposes of these Terms and Conditions:
                                                        <ol>
                                                            <li>Must be in writing; and</li>
                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                        </ol>
                                                    </li>
                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                        <ol>
                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                        </ol>
                                                    </li>
                                                </ol>                
                                            </li>

                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                <ol>
                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                </ol>
                                            </li>

                                            <li><b>DEFINITIONS</b></br>
                                                <b>"Account"</b> means the Client Property and Client Money we hold for you and represents an entry in your name on the
                                                general ledger of ownership of Digital Currency and money maintained and held by</br> 

                                                <!-- <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br> -->

                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.</br>

                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.</br>
                                                <b>"Client Money"</b> has the meaning given to it in clause 7.6.</br>
                                                <b>"Client Property"</b> has the meaning given to it in clause 7.5.</br>
                                                <b>"Digital Currency"</b> means the supported tokens or cryptocurrencies offered on the Platform.</br>

                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.</br> 

                                                <!-- <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br> -->

                                                <!-- <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br>  -->
                                                <!-- <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br>  -->
                                                <b>"Minor"</b> means a person under the age of 18.</br>
                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website </br>

                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. </br>
                                                <b>"Portfolio Manager"</b> means Invsta and associated people responsible for managing your Portfolio.</br>

                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services </br>

                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.</br>

                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.</br>

                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.</br>

                                            </li>

                                            <li><b>GENERAL INTERPRETATION</b>
                                                <ol>
                                                    <li>In these Terms and Conditions:
                                                        <ol>
                                                            <li>Unless the context otherwise requires, references to:
                                                                <ol>
                                                                    <li>"we", "us", Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                </ol>
                                                            </li>
                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                        </ol>
                                                    </li>
                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                </ol>
                                            </li>

                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w">
                                <div class="row">
<!--                                    <div class="col-sm-6"><a class="btn btn-primary step-trigger-btn" href="#stepContent4">Back</a></div>-->
                                    <div class="col-sm-6 text-right">
                                        <!--<input type="button" name="verify" id="fifthfs-verify" class="btn btn-primary" value="Verify">-->
                                        <a href="javascript:submitForm()"  id="fifthfs-submit" onclick="termsAccepted(this);" class="btn btn-primary">
                                            I Accept
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-sm-12" style="text-align: center">
                                    <a href="./resources/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                       text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>
                                </div>
                            </div>
                            <!--</div>-->
                            <!--</div>-->
                        </fieldset>

                    </form:form>
                </div>
            </div>
        </div>



        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/moment/moment.js"></script>
        <script src="./resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="./resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="./resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="./resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="./resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="./resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="./resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="./resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="./resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="./resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="./resources/js/main.js?version=3.5.1"></script>
        <script src="https://app.kickbox.com/authenticate/1/release.js"></script>
        <script src="./resources/js/cryptolabs/user-main.js"></script>
        <script src="./resources/js/intlTelInput.js"></script>
        <!--<script src="./resources/js/ion.calendar.js"></script>-->
        <!--we need adultDOB for works ion.calendar.min.js-->
        <script src="./resources/js/ion.calendar.min.js"></script>
        <script src="./resources/js/moment-with-locales.min.js"></script>
        <!-- -----------Sweet alert --------->
        <script src="./resources/js/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="./resources/css/sweetalert2.min.css">
        <script>
                                            var adultDOB = '${adultDOB}';
                                            $("#countryCode").intlTelInput();
                                            $(document).ready(function () {
                                                $("#continue-4to5-btn").hide();
                                                $(".next").click(function () {
                                                    var valid = $("#msform").valid();
                                                    if (valid) {
                                                        MoveNext1(this);
                                                    }
                                                });
                                                $(".prev").click(function () {
                                                    MovePrevious1(this);
                                                });
                                                $("#fifthfs-verify").click(function () {
                                                    var email = $('#email').val();
                                                    fingerprint(email);
                                                });
                                                $("#dob").ionDatePicker();
                                            });
                                            // First, fetch a fingerprint from Kickbox
                                            fingerprint = function (e) {
                                                Kickbox.fingerprint({
                                                    app: "${kickboxAppCode}",
                                                    email: e, // Email address to authenticate
                                                    onSuccess: function (fp) {
                                                        $.ajax({
                                                            url: "./rest/cryptolabs/api/verifyEmail",
                                                            type: "POST",
                                                            async: false,
                                                            data: {email: e, fingerprint: fp},
                                                            success: function (success) {
//                                                                alert('success-->' + success);
                                                                if (success === true) {
                                                                    $('#fifthfs-submit').show();
                                                                }
                                                            },
                                                            error: function (e) {
                                                                //alert('error' + e);
                                                            }
                                                        });
                                                    },
                                                    onError: function (err) {
                                                        // Handle error
                                                    }
                                                });
                                            };

                                            isEmailAlreadyExist = function () {
                                                var email = $('#email').val();
                                                $('#fifthfs-submit').show();
                                                var url = './rest/cryptolabs/api/isEmailAlreadyExist?e=' + email;
                                                $.ajax({
                                                    url: url,
                                                    type: "GET",
                                                    async: false,
                                                    success: function (response) {
                                                        $("#validationId").css('display', 'inline');
                                                        if (response === 'Email is Valid.') {
                                                            $("#validationId").css('color', 'green').html('');
                                                            $("#validationId").focus();
                                                            $('#firstfs-continue').removeAttr("disabled");
                                                            return true;
                                                        } else {
                                                            $("#validationId").css('color', 'red').html(response);
                                                            $("#validationId").focus();
                                                            return false;
                                                        }
                                                    },
                                                    error: function (e) {
                                                        //handle error
                                                    }
                                                });
                                            }

                                            clearValidationId3 = function () {
                                                $("#validationId3").html('');
                                            };
                                            isInviteCodeUsed = function () {
                                                var inviteCode = $('#inviteCode').val();
                                                $('#thirdfs-continue').attr("disabled", "disabled");
                                                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                                                $.ajax({
                                                    url: url,
                                                    type: "GET",
                                                    async: false,
                                                    success: function (response) {
                                                        $("#validationId3").css('display', 'inline');
                                                        if (response === 'true') {
                                                            $("#validationId3").css({'color': 'green', 'margin-left': '10px'}).html('Invite Code is valid.');
                                                            $("#validationId3").focus();
                                                            $('#thirdfs-continue').removeAttr("disabled");
                                                            return true;
                                                        } else {
                                                            $("#validationId3").css({'color': 'red', 'margin-left': '10px'}).html('Invite Code is used or invalid.');
                                                            $("#validationId3").focus();
                                                            return false;
                                                        }
                                                    },
                                                    error: function (e) {
                                                        //handle error
                                                    }
                                                });
                                            }

                                            MoveNext1 = function (nextVal) {
                                                if (animating) {
                                                    return false;
                                                }
                                                animating = true;
                                                current_fs = $(nextVal).parent().parent().parent();
                                                next_fs = current_fs.next();
                                                //activate next step on progressbar using the index of next_fs
                                                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                                                //show the next fieldset
                                                next_fs.show();
                                                current_fs.hide();
                                                animating = false;
                                                //hide the current fieldset with style
                                                current_fs.animate({opacity: 0}, {
                                                    step: function (now, max) {
                                                        //as the opacity of current_fs reduces to 0 - stored in "now"
                                                        //1. scale current_fs down to 80%
                                                        scale = 1 - (1 - now) * 0.2;
                                                        //2. bring next_fs from the right(50%)
                                                        left = (now * 50) + "%";
                                                        //3. increase opacity of next_fs to 1 as it moves in
                                                        opacity = 1 - now;
                                                        current_fs.css({
                                                            'transform': 'scale(' + scale + ')',
                                                            'position': 'absolute'
                                                        });
                                                        next_fs.css({'left': left, 'opacity': opacity});
                                                    },
                                                    duration: 1500,
                                                    complete: function () {
                                                        current_fs.hide();
                                                        animating = false;
                                                    },
                                                    //this comes from the custom easing plugin
                                                    easing: 'easeInOutBack'
                                                });
                                            };
                                            MovePrevious1 = function (nextVal) {
                                                if (animating) {
                                                    return false;
                                                }
                                                animating = true;
                                                current_fs = $(nextVal).parent().parent().parent();
                                                previous_fs = current_fs.prev();
                                                //de-activate current step on progressbar
                                                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                                                //show the previous fieldset
                                                previous_fs.show();
                                                current_fs.hide();
                                                animating = false;
                                                //hide the current fieldset with style
                                                current_fs.animate({opacity: 0}, {
                                                    step: function (now, mx) {
                                                        //as the opacity of current_fs reduces to 0 - stored in "now"
                                                        //1. scale previous_fs from 80% to 100%
                                                        scale = 0.8 + (1 - now) * 0.2;
                                                        //2. take current_fs to the right(50%) - from 0%
                                                        left = ((1 - now) * 50) + "%";
                                                        //3. increase opacity of previous_fs to 1 as it moves in
                                                        opacity = 1 - now;
                                                        current_fs.css({'left': left});
                                                        previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                                                    },
                                                    duration: 1500,
                                                    complete: function () {
                                                        current_fs.hide();
                                                        animating = false;
                                                    },
                                                    //this comes from the custom easing plugin
                                                    easing: 'easeInOutBack'
                                                });
                                            };
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-XXXXXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <script src="./resources/js/jquery.validate.min.js"></script>
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script>
            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        </script>
        <script>

        </script>
        <script>
            function submitForm() {
                function termsAccepted(link) {
                    link.onclick = function (event) {
                        event.preventDefault();
                    }
                }
                document.msRegform.submit();
            }

            var EmailDomainSuggester = {

                domains: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],
                bindTo: $("#email"),
                init: function () {
                    this.addElements();
                    this.bindEvents();
                },
                addElements: function () {
                    // Create empty datalist
                    this.datalist = $("<datalist />", {
                        id: 'email-options'
                    }).insertAfter(this.bindTo);
                    // Corelate to input
                    this.bindTo.attr("list", "email-options");
                },
                bindEvents: function () {
                    this.bindTo.on("keyup", this.testValue);
                },
                testValue: function (event) {
                    var el = $(this),
                            value = el.val();
                    // email has @
                    // remove != -1 to open earlier
                    if (value.indexOf("@") != -1) {
                        value = value.split("@")[0];
                        EmailDomainSuggester.addDatalist(value);
                    } else {
                        // empty list
                        EmailDomainSuggester.datalist.empty();
                    }
                },
                addDatalist: function (value) {
                    var i, newOptionsString = "";
                    for (i = 0; i < this.domains.length; i++) {
                        newOptionsString +=
                                "<option value='" +
                                value +
                                "@" +
                                this.domains[i] +
                                "'>";
                    }

                    // add new ones
                    this.datalist.html(newOptionsString);
                }
            }
            EmailDomainSuggester.init();
        </script>
    </script>

    <script>
        function myFunction() {
            var name = $('input[name="fname"]').val();
            var first_name = name.split(' ')[0];
            var last_name = name.substring(first_name.length).trim();
            $("#Person_FirstName").val(first_name);
            $("#Person_LastName").val(last_name);
            //console.log(first_name)
            //console.log(last_name)
        }
    </script>
</body>
</html>
