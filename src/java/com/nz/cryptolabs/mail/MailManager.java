/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.mail;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.RequestBean;

/**
 *
 * @author Administrator
 */
public interface MailManager {

    public void sendConfirmationMailToEmail(String firstName,String to, String refId, String token);

    public void sendWelcomeToNewClient(String email, String firstName);

    public void sendResetToUser(String email, String firstName);

    public void sendWithdrawlNotificationToUser(String to, String firstName, RequestBean bean);

    public void sendWithdrawlNotificationToAdmin(String to, RequestBean bean);

    public void sendMail(String toEmail, String fromEmail, String subject, String htmlMessage);

    public void sendAddFundsNotificationToUser(String to, String firstName, RequestBean bean);

    public void sendAddFundsNotificationToAdmin(String to, RequestBean bean);
    
    public void sendSellFundsNotificationToUser(String to, String firstName, AccountEventCommand investmentReq);

    public void sendSellFundsNotificationToAdmin(String to, AccountEventCommand investmentReq);

    public void sendAddFundsNotificationToUser(String email, String fullName, InvestmentBean investmentReq);

    public void sendAddFundsNotificationToAdmin(String to, InvestmentBean investmentReq);
}
