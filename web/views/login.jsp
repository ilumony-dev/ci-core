<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="./resources/favicon.png" rel="shortcut icon">
        <link href="./resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">

        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">

        <link rel="stylesheet" href="./resources/css/awesomplete.css" />
        <script src="./resources/js/awesomplete.js"></script>  

        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/validation.js"></script>        
        <script src="./resources/js/v2-jquery.min.js"></script>
        <script src="./resources/js/jquery.powertip.js"></script>      
        <script src="./resources/js/jquery-ui.js"></script>
        <script src="./resources/js/index.js"></script>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>


    </head>
    <body class="auth-wrapper">
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w">
                <div class="logo-w">
                    <a href="./login"><img class= "main-logo" style="width:40%"></a>
                </div>
                <h4 class="auth-header">
                    Login 
                </h4>
               
                <form id="msform" action="<c:url value='/j_spring_security_check'/>" method="post" class="form-login">
                    <fieldset>
                         <h6 class="${classname}">
                    ${message}
                </h6>
                        <div class="form-group">
                            <label for="username">Email</label>
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                            <input type="email" class="form-control" id="email" name="username" data-error="Your email is invalid" required="required" placeholder="Enter your email" >
                            
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required="required" placeholder="Enter your password" >
                            <span toggle="#password" class="fa fa-fw field-icon toggle-password fa-eye-slash" title="Show password"></span>
                            <div class="pre-icon os-icon os-icon-fingerprint"></div>
                        </div>
                        <div class="buttons-w">
                            <button class="btn btn-primary">Log me in</button>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox">Remember Me
                                </label>
                            </div>           
                        </div>
                    </fieldset>
                    <div class="buttons-w">
                        <p> Don't have account <a  href='./register'>Sign Up</a></p>
                        <p><a  href='./forgetpwd'> Forget Password ?</a></p>
                    </div>
                </form>
            </div>
        </div>
        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>

        <script src="./resources/js/jquery.validate.min.js"></script>

        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script>
                var arr = document.getElementsByClassName('main-logo');
                for (var i = 0; i < arr.length; i++){
                    var ele = arr[i];
                    ele.src = "./resources/img/invsta-03(200px).png";
//                    ele.src = "./resources/img/invsta-03(200px)-crossinvsta.png";
                }
            
            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            var EmailDomainSuggester = {

                domains: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],

                bindTo: $("#email"),

                init: function () {
                    this.addElements();
                    this.bindEvents();
                },

                addElements: function () {
                    // Create empty datalist
                    this.datalist = $("<datalist />", {
                        id: 'email-options'
                    }).insertAfter(this.bindTo);
                    // Corelate to input
                    this.bindTo.attr("list", "email-options");
                },

                bindEvents: function () {
                    this.bindTo.on("keyup", this.testValue);
                },

                testValue: function (event) {
                    var el = $(this),
                            value = el.val();

                    // email has @
                    // remove != -1 to open earlier
                    if (value.indexOf("@") != -1) {
                        value = value.split("@")[0];
                        EmailDomainSuggester.addDatalist(value);
                    } else {
                        // empty list
                        EmailDomainSuggester.datalist.empty();
                    }
                },

                addDatalist: function (value) {
                    var i, newOptionsString = "";
                    for (i = 0; i < this.domains.length; i++) {
                        newOptionsString +=
                                "<option value='" +
                                value +
                                "@" +
                                this.domains[i] +
                                "'>";
                    }

                    // add new ones
                    this.datalist.html(newOptionsString);
                }
            }
            EmailDomainSuggester.init();
        </script>
        <!--        <script>new Awesomplete('input[type="email"]', {
                        list: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],
                        data: function (text, input) {
                            return input.slice(0, input.indexOf("@")) + "@" + text;
                        },
                        filter: Awesomplete.FILTER_STARTSWITH
                    });</script> -->
    </body>
</html>

