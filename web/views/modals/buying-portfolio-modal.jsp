<%-- 
    Document   : newjsp
    Created on : 3 Jul, 2018, 10:39:36 AM
    Author     : palo12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!--modals start-->

<form method="post" action="./mgmt-add-portfolio-buying" id="buying-portfolio-modal-form">
    <div class="modal-header">
        <h5 class="modal-title" id="buying-portfolio-modal-portfolio-name"></h5>
    </div>
    <div class="modal-body">
        <div class="element-box">
            <div class="element-wrapper">
                <h6 class="element-header">
                    Master <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                </h6>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="created_date">Date</label>
                        <input type="date" value="${today}" class="form-control" id="buying-portfolio-modal-date" name="created_date" required="required"/>
                        <input type="hidden" value="0" id="buying-portfolio-modal-fundId" name="fund_id"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="email">Username</label>
                        <select name="email" class="form-control" id="buying-portfolio-modal-email">
                            <option value="0">-Select-</option>
                            <c:forEach items="${totalUsers}" var="user"><option value="${user.userId}">${user.email}</option></c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="usd">USD</label>
                            <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-usd" name="usd"  value="0.00" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="nzd">NZD</label>
                            <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-nzd" name="nzd"  value="0.00" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="minto">Units</label>
                            <input type="number" step="0.01" class="form-control" id="buying-portfolio-modal-minto" name="minto"  value="0.00" required="required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3" id="buying-portfolio-modal-uniqueId" style="display:none;">
                <div class="form-group margin-t">
                    <select name="coin_id" class="form-control">
                        <option value="0">-Select-</option>
                    <c:forEach items="${coins}" var="coin"> <option value="${coin.uniqueId}">${coin.name}</option></c:forEach>
                </select>
                <input type="number" placeholder="Please enter amount" step="0.01" class="form-control" name="amount" required="required" value="0.00" style="width:40%; float: left"/>
                <input type="number" placeholder="Please enter quantity" step="0.00000001" class="form-control" name="quantity" required="required" value="0.00" style="width:60%; float: right"/>
            </div>
        </div>
        <div class="row" id="buying-portfolio-modal-coin-group">
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="particulars" style="display:block">Notes</label>
                    <textarea class="form-control" name="particulars" id="buying-portfolio-modal-particulars" style="width:100%; overflow: hidden"></textarea>
                </div> 
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <!--<button type="button" class="btn btn-primary" id="hidemodal" data-target="#nextmodal" data-toggle="modal">Show</button>-->
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary buying-portfolio-modal-submit">Submit</button>
    </div>
</form>