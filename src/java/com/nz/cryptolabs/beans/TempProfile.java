/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maninderjit Singh
 */
public class TempProfile {

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }

    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the new_first_name
     */
    public String getNew_first_name() {
        return new_first_name;
    }

    /**
     * @param new_first_name the new_first_name to set
     */
    public void setNew_first_name(String new_first_name) {
        this.new_first_name = new_first_name;
    }

    /**
     * @return the new_last_name
     */
    public String getNew_last_name() {
        return new_last_name;
    }

    /**
     * @param new_last_name the new_last_name to set
     */
    public void setNew_last_name(String new_last_name) {
        this.new_last_name = new_last_name;
    }

    /**
     * @return the new_full_name
     */
    public String getNew_full_name() {
        return new_full_name;
    }

    /**
     * @param new_full_name the new_full_name to set
     */
    public void setNew_full_name(String new_full_name) {
        this.new_full_name = new_full_name;
    }

    /**
     * @return the new_mobile_no
     */
    public String getNew_mobile_no() {
        return new_mobile_no;
    }

    /**
     * @param new_mobile_no the new_mobile_no to set
     */
    public void setNew_mobile_no(String new_mobile_no) {
        this.new_mobile_no = new_mobile_no;
    }

    /**
     * @return the new_gender
     */
    public String getNew_gender() {
        return new_gender;
    }

    /**
     * @param new_gender the new_gender to set
     */
    public void setNew_gender(String new_gender) {
        this.new_gender = new_gender;
    }

    /**
     * @return the new_address
     */
    public String getNew_address() {
        return new_address;
    }

    /**
     * @param new_address the new_address to set
     */
    public void setNew_address(String new_address) {
        this.new_address = new_address;
    }

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * @return the full_name
     */
    public String getFull_name() {
        return full_name;
    }

    /**
     * @param full_name the full_name to set
     */
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    /**
     * @return the mobile_no
     */
    public String getMobile_no() {
        return mobile_no;
    }

    /**
     * @param mobile_no the mobile_no to set
     */
    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }
    private String email;
    private String ref_id;
    private String user_id;
    private String id;
    private String created_ts;
    private String new_first_name;
    private String new_last_name;
    private String new_full_name;
    private String new_mobile_no;
    private String new_gender;
    private String new_address;
    private String first_name;
    private String last_name;
    private String full_name;
    private String mobile_no;
    private String gender;
    private String address;
    private static final Logger logger = LoggerFactory.getLogger(TempProfile.class);

}
