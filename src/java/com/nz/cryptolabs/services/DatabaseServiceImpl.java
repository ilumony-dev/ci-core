/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.coinPriceMap;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.controllers.payments.ChargeRequest;
import com.nz.cryptolabs.controllers.payments.StripeService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class DatabaseServiceImpl implements DatabaseService {

    @Override
    public List<ShareFund> fundActualShares(String fundId, String investmentAmount) {
        List<ShareFund> shares = repository.sharesByFund(fundId);
        List<ShareFund> actualShareList = new ArrayList<>();
        BigDecimal amount = new BigDecimal(investmentAmount);
        for (ShareFund share : shares) {
            BigDecimal percentage = new BigDecimal(share.getPercentage() != null ? share.getPercentage() : "0.00");
            BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(amount);
            share.setShareAmount(shareAmount.toPlainString());
            BigDecimal quantity = shareAmount.divide(new BigDecimal(share.getPrice()), Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            share.setQuantity(quantity.toPlainString());
            actualShareList.add(share);
        }
        List<ShareFund> coins = repository.coinsByFund(fundId);
        for (ShareFund coin : coins) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal percentage = new BigDecimal(coin.getPercentage() != null ? coin.getPercentage() : "0.00");
                BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(amount);
                coin.setShareAmount(shareAmount.toPlainString());
                coin.setPrice(String.valueOf(cryptoCoin.getUsdPrice()));
                BigDecimal quantity = shareAmount.divide(new BigDecimal(coin.getPrice()), Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                coin.setQuantity(quantity.toPlainString());
                actualShareList.add(coin);
            }

        }
        return actualShareList;
    }

    @Override
    public List<ShareFund> investmentActualDetails(String investmentId, String reqId) {
        List<ShareFund> shares = repository.pendingSharesByInvestment(investmentId, reqId);
        BigDecimal totalShareAmount = BigDecimal.ZERO;
        List<ShareFund> investmentActualDetails = new ArrayList<>();
        if (!shares.isEmpty()) {
            for (ShareFund share : shares) {
                if (share.getCoinId() != null) {
                    ShareFund coin = share;
                    try {
                        CryptoCoin cryptoCoin = coinPriceMap.get(coin.getCoinId());
                        if (cryptoCoin == null) {
                            cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
                            coinPriceMap.put(coin.getCoinId(), cryptoCoin);
                        }
                        BigDecimal shareAmount = new BigDecimal(coin.getShareAmount());
                        BigDecimal price = new BigDecimal(cryptoCoin.getUsdPrice()).setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                        BigDecimal quantity = shareAmount.divide(price, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                        BigDecimal investmentAmount = price.multiply(quantity);
                        coin.setPrice(price.toPlainString());
                        coin.setQuantity(quantity.toPlainString());
                        coin.setShareAmount(investmentAmount.toPlainString());
                        totalShareAmount = totalShareAmount.add(shareAmount);
                    } catch (IOException ex) {
                        Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    investmentActualDetails.add(coin);
                } else if (share.getShareId() != null) {
                    BigDecimal shareAmount = new BigDecimal(share.getShareAmount());
                    totalShareAmount = totalShareAmount.add(shareAmount);
                    investmentActualDetails.add(share);
                }
            }
        }
        ListIterator<ShareFund> it = investmentActualDetails.listIterator();
        while (it.hasNext()) {
            ShareFund share = it.next();
            BigDecimal shareAmount = new BigDecimal(share.getShareAmount());
            BigDecimal percentage = totalShareAmount.multiply(shareAmount.divide(Constants.HUNDRED, Constants.PERCENTANGE_DECIMAL_PLACES, RoundingMode.HALF_UP));
            share.setPercentage(percentage.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            share.setShareAmount(shareAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
        }
        return investmentActualDetails;
    }

    @Override
    public List<ShareFund> sharesByInvestment(String investmentId, String amountParam, String percentageParam) {
        List<ShareFund> shares = repository.sharesByInvestment(investmentId);
        if (!shares.isEmpty()) {
            BigDecimal totalShareAmount = BigDecimal.ZERO;
            for (ShareFund share : shares) {
                if (share.getCoinId() != null && !share.getCoinId().isEmpty()) {
                    ShareFund coin = share;
                    try {
                        CryptoCoin cryptoCoin = coinPriceMap.get(coin.getCoinId());
                        if (cryptoCoin == null) {
                            cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
                            coinPriceMap.put(coin.getCoinId(), cryptoCoin);
                        }
                        BigDecimal quantity = new BigDecimal(coin.getQuantity());
                        BigDecimal price = new BigDecimal(cryptoCoin.getUsdPrice());
                        BigDecimal shareAmount = price.multiply(quantity);
                        coin.setPrice(price.toPlainString());
                        coin.setQuantity(quantity.toPlainString());
                        coin.setShareAmount(shareAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
                        totalShareAmount = totalShareAmount.add(shareAmount);
                    } catch (IOException ex) {
                        Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (share.getShareId() != null && !share.getShareId().isEmpty()) {
                    String usdPrice = null;
                    if (usdPrice == null) {
                        ShareFund ele = repository.shareById(share.getShareId());
                        usdPrice = ele.getPrice();
                    }
                    BigDecimal quantity = new BigDecimal(share.getQuantity());
                    BigDecimal price = new BigDecimal(usdPrice);
                    BigDecimal shareAmount = price.multiply(quantity);
                    share.setPrice(price.toPlainString());
                    share.setQuantity(quantity.toPlainString());
                    share.setShareAmount(shareAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
                    totalShareAmount = totalShareAmount.add(shareAmount);
                }
            }
            BigDecimal totalPercentage = new BigDecimal(percentageParam != null && !percentageParam.isEmpty() ? percentageParam : "100");
            if (amountParam != null && !amountParam.isEmpty()) {
                BigDecimal amount = new BigDecimal(amountParam);
                for (ShareFund share : shares) {
                    if (share.getName() != null && !share.getName().isEmpty()) {
                        BigDecimal shareAmount = new BigDecimal(share.getShareAmount());
                        BigDecimal price = new BigDecimal(share.getPrice());
                        BigDecimal percentage = shareAmount.divide(totalShareAmount, Constants.PERCENTANGE_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(totalPercentage);
                        shareAmount = amount.multiply(percentage).divide(totalPercentage, Constants.PERCENTANGE_DECIMAL_PLACES, RoundingMode.HALF_UP);
                        BigDecimal quantity = shareAmount.divide(price, Constants.PERCENTANGE_DECIMAL_PLACES, RoundingMode.HALF_UP);
                        share.setPercentage(percentage.setScale(2, RoundingMode.HALF_UP).toPlainString());
                        share.setShareAmount(shareAmount.toPlainString());
                        share.setQuantity(quantity.toPlainString());
                        share.setCloseBal(totalShareAmount.toPlainString());
                    }
                }
            } else {
                for (ShareFund share : shares) {
                    if (share.getName() != null && !share.getName().isEmpty()) {
                        BigDecimal shareAmount = new BigDecimal(share.getShareAmount());
                        BigDecimal percentage = shareAmount.divide(totalShareAmount, Constants.PERCENTANGE_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(totalPercentage);
                        share.setPercentage(percentage.setScale(2, RoundingMode.HALF_UP).toPlainString());
                        share.setCloseBal(totalShareAmount.toPlainString());
                    }
                }
            }
        }
        return shares;
    }

    @Override
    public List<ShareFund> actualSharesByAmount(String investmentId, String userId, String investmentAmount) {
        List<ShareFund> shares = repository.actualSharePercentages(investmentId);
        List<ShareFund> actualShareList = new ArrayList<>();
        BigDecimal amount = new BigDecimal(investmentAmount);
        for (ShareFund share : shares) {
            BigDecimal percentage = new BigDecimal(share.getPercentage() != null ? share.getPercentage() : "0.00");
            BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(amount);
            share.setShareAmount(shareAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            BigDecimal quantity = shareAmount.divide(new BigDecimal(share.getPrice()), Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            share.setQuantity(quantity.toPlainString());
            actualShareList.add(share);
        }
        List<ShareFund> coins = repository.actualCoinPercentages(investmentId);
        if (!coins.isEmpty()) {
            for (ShareFund coin : coins) {
                coin.setName(coin.getCoinId());
                BigDecimal percentage = new BigDecimal(coin.getPercentage() != null ? coin.getPercentage() : "0.00");
                BigDecimal coinAmount = percentage.divide(Constants.HUNDRED, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(amount);
                coin.setShareAmount(coinAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
                try {
                    CryptoCoin cryptoCoin = coinPriceMap.get(coin.getCoinId());
                    if (cryptoCoin == null) {
                        cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
                        coinPriceMap.put(coin.getCoinId(), cryptoCoin);
                    }
                    coin.setPrice(String.valueOf(cryptoCoin.getUsdPrice()));
                } catch (IOException ex) {
                    Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                BigDecimal quantity = coinAmount.divide(new BigDecimal(coin.getPrice()), Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                coin.setQuantity(quantity.toPlainString());
                actualShareList.add(coin);
            }
        }
        return actualShareList;
    }

    @Override
    public List<StateCity> countries(Boolean states, Boolean cities) {
        List<StateCity> countries = repository.countries(states, cities);
        HashMap<String, StateCity> map = new HashMap<>();
        for (StateCity curr : countries) {
            StateCity country = map.get(curr.getCode());
            if (country != null) {
                List<StateCity> details = country.getDetails();
                if (details == null) {
                    details = new ArrayList<>();
                }
                details.add(curr);
                country.setDetails(details);
            } else {
                map.put(curr.getCode(), curr);
            }
        }
        List<StateCity> list = new LinkedList<>();
        Set<Map.Entry<String, StateCity>> set = map.entrySet();
        for (Map.Entry<String, StateCity> entry : set) {
            list.add(entry.getValue());
        }
        return list;
    }

    private List<InvestmentBean> investmentList(List<InvestmentBean> investments, Boolean coins, String currency) throws IOException {

        HashMap<String, List<InvestmentBean>> map = new HashMap<>();
        investments.forEach((investment) -> {
            List<InvestmentBean> list = map.get(investment.getInvestmentId());
            if (list == null) {
                list = new LinkedList<>();
            }
            list.add(investment);
            map.put(investment.getInvestmentId(), list);
        });

        List<InvestmentBean> investmentList = new LinkedList<>();
        Set<Map.Entry<String, List<InvestmentBean>>> set = map.entrySet();
        for (Map.Entry<String, List<InvestmentBean>> entry : set) {
            List<InvestmentBean> list = entry.getValue();
            InvestmentBean inv = list.get(0);
            List<ShareFund> shareList = new LinkedList<>();
            BigDecimal currAmount = BigDecimal.ZERO;
            BigDecimal investedAmount = BigDecimal.ZERO;
            BigDecimal unitsAmount = new BigDecimal(inv.getUnits()).multiply(new BigDecimal(inv.getFundNav()));
            if (unitsAmount.compareTo(BigDecimal.ZERO) > 0) {
                for (InvestmentBean fundShare : list) {
                    ShareFund share = new ShareFund();
                    share.setName(fundShare.getShareName());
                    share.setShareName(fundShare.getShareName());
                    share.setShareId(fundShare.getShareId());
                    share.setCoinId(fundShare.getCoinId());
                    share.setUserId(fundShare.getUserId());
                    share.setFundId(fundShare.getFundId());
                    share.setInvestmentId(fundShare.getInvestmentId());
                    share.setTargetPercentage(fundShare.getTargetPercentage());
                    share.setQuantity(fundShare.getQuantity());
                    share.setPrice(fundShare.getPrice());
                    BigDecimal value = new BigDecimal(fundShare.getValue());
                    if (currency != null && !"USD".equalsIgnoreCase(currency)) {
                        BigDecimal localPrice = currencyConversionRate("USD_" + currency, fundShare.getCreatedDate());
                        value = value.multiply(localPrice);
                    }
                    value = value.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                    share.setInvestedAmount(value.toPlainString());
                    share.setShareAmount(fundShare.getValue());
                    share.setCreatedDate(fundShare.getCreatedDate());
                    if (fundShare.getCoinId() != null && fundShare.getAction() == null) {
                        CryptoCoin cryptoCoin = null;
                        try {
                            cryptoCoin = coinPriceMap.get(share.getCoinId());
                            if (cryptoCoin == null) {
                                cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(share.getCoinId());
                                coinPriceMap.put(share.getCoinId(), cryptoCoin);
                            }
                            share.setPercent1hr(cryptoCoin.getPercent1hr());
                            share.setPercent24hr(cryptoCoin.getPercent24hr());
                            share.setPercent7d(cryptoCoin.getPercent7d());
                            share.setPrice(String.valueOf(cryptoCoin.getUsdPrice()));
//                        BigDecimal amount = new BigDecimal(share.getQuantity()).multiply(new BigDecimal(share.getPrice()));
//                        amount = amount.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
//                        share.setShareAmount(amount.toString());
                        } catch (IOException ex) {
                            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    BigDecimal shareAmount = new BigDecimal(share.getShareAmount());
                    if (share.getCreatedDate() != null) {
                        if (currency != null && !"USD".equalsIgnoreCase(currency)) {
                            BigDecimal localPrice = currencyConversionRate("USD_" + currency, null);
                            shareAmount = shareAmount.multiply(localPrice);
                            shareAmount = shareAmount.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                        }
                    }
                    share.setShareAmount(shareAmount.toPlainString());
                    BigDecimal percentange = shareAmount.divide(unitsAmount, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
                    share.setPercentage(percentange.toPlainString());
                    currAmount = currAmount.add(shareAmount);
                    investedAmount = investedAmount.add(new BigDecimal(share.getInvestedAmount()));
                    share.setCloseBal(unitsAmount.toPlainString());
                    shareList.add(share);
                }
                currAmount = currAmount.setScale(2, BigDecimal.ROUND_CEILING);
                investedAmount = investedAmount.setScale(2, BigDecimal.ROUND_DOWN);
                unitsAmount = unitsAmount.setScale(2, BigDecimal.ROUND_DOWN);
                inv.setValue(currAmount.toPlainString());
                inv.setInvestedAmount(investedAmount.toPlainString());
                inv.setUnitsAmount(unitsAmount.toPlainString());
                inv.setActualCurrentAmount(unitsAmount.toPlainString());
                inv.setFundActualShares(shareList);
                investmentList.add(inv);
            }
        }
        for (InvestmentBean inv : investmentList) {
            if (currency != null && !"USD".equalsIgnoreCase(currency)) {
//                if (currency.equalsIgnoreCase(inv.getCurrency())) {
//                    inv.setInvestmentAmount(inv.getLocalInvestmentAmount());
//                } else {
                BigDecimal localPrice = currencyConversionRate("USD_" + currency, inv.getCreatedDate());

                BigDecimal investmentAmount = new BigDecimal(inv.getInvestmentAmount());
                investmentAmount = investmentAmount.multiply(localPrice);
                investmentAmount = investmentAmount.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                inv.setInvestmentAmount(investmentAmount.toPlainString());

                BigDecimal unitsAmount = new BigDecimal(inv.getUnitsAmount());
                unitsAmount = unitsAmount.multiply(localPrice);
                unitsAmount = unitsAmount.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                inv.setUnitsAmount(unitsAmount.toPlainString());
//                }
            }
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId, String currency) {
        return repository.purchasedFundInvestments(userId, investmentId, fundId);
    }

    @Override
    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId, String currency) {
        return repository.purchasedPortfolioInvestments(userId, investmentId, fundId);
    }

    @Override
    public List<InvestmentBean> purchasedInvestments(String userId, String investmentId, String fundId, String currency) {
        List<InvestmentBean> investments = purchasedPortfolioInvestments(userId, investmentId, fundId, currency);
        List<InvestmentBean> investmentList = null;
        try {
            investmentList = investmentList(investments, true, currency);
        } catch (IOException ex) {
            System.out.println(ex);
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> coinsByInvestments(String userId, String investmentId, Boolean total, String fundId) {
        String currency = "USD";
        currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : "USD";
        List<InvestmentBean> investments = purchasedPortfolioInvestments(userId, investmentId, fundId, currency);
        Set<String> set = new HashSet<>();
        for (ListIterator<InvestmentBean> it = investments.listIterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            List<ShareFund> shares = inv.getFundActualShares();
            for (ListIterator<ShareFund> it1 = shares.listIterator(); it1.hasNext();) {
                ShareFund coin = it1.next();
                set.add(coin.getCoinId());
            }
        }
        for (ListIterator<InvestmentBean> it = investments.listIterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            HashMap<String, String> coinQuantityMap = new HashMap<>();
            for (Iterator<String> i = set.iterator(); i.hasNext();) {
                coinQuantityMap.put(i.next(), "0.000");
            }
            List<ShareFund> shares = inv.getFundActualShares();
            for (ListIterator<ShareFund> it1 = shares.listIterator(); it1.hasNext();) {
                ShareFund coin = it1.next();
                if (coinQuantityMap.containsKey(coin.getCoinId())) {
                    BigDecimal quantity = new BigDecimal(coinQuantityMap.get(coin.getCoinId()));
                    quantity = quantity.add(new BigDecimal(coin.getQuantity()));
                    coinQuantityMap.put(coin.getCoinId(), quantity.toPlainString());
                }
            }
            inv.setMap(coinQuantityMap);
        }
        if (total) {
            HashMap<String, String> coinQuantityMap = new HashMap<>();
            for (Iterator<String> i = set.iterator(); i.hasNext();) {
                coinQuantityMap.put(i.next(), "0.000");
            }
            for (Iterator<InvestmentBean> it = investments.iterator(); it.hasNext();) {
                InvestmentBean inv = it.next();
                Map<String, String> map = inv.getMap();
                Set<String> coins = coinQuantityMap.keySet();
                for (Iterator<String> it1 = coins.iterator(); it1.hasNext();) {
                    String coin = it1.next();
                    BigDecimal coinAmount = new BigDecimal(map.get(coin));
                    if (coinAmount.compareTo(BigDecimal.ZERO) == 1) {
                        BigDecimal coinQuantityAmount = new BigDecimal(coinQuantityMap.get(coin));
                        coinQuantityMap.put(coin, coinQuantityAmount.add(coinAmount).toPlainString());
                    }
                }
            }
            InvestmentBean totalBean = new InvestmentBean();
            totalBean.setFundName("Total");
            totalBean.setAction("background: #f0ad4e; color: #fff;");

            totalBean.setMap(coinQuantityMap);
            investments.add(totalBean);
        }
        return investments;
    }

    @Override
    public List<ShareFund> totalUnitsById(String fundId, String userId, String investmentId) {
        List<ShareFund> totalUnitsById = repository.totalUnitsById(fundId, userId, investmentId);
        for (ShareFund fp : totalUnitsById) {
            BigDecimal nav = new BigDecimal(fp.getPrice()).setScale(16, RoundingMode.HALF_UP);
            BigDecimal units = new BigDecimal(fp.getMinto()).setScale(16, RoundingMode.HALF_UP);
            fp.setUsd(nav.multiply(units).setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
        }
        return totalUnitsById;
    }

    @Override
    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate) {
        List<ShareFund> userUnitSummary = repository.userUnitSummary(userId, fromDate, toDate);
        for (ShareFund fp : userUnitSummary) {
            BigDecimal usd = new BigDecimal(fp.getUsd());
            BigDecimal local = new BigDecimal(fp.getLocal() != null ? fp.getLocal() : "0.00");
            fp.setUsd(usd.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            fp.setLocal(local.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
        }
        return userUnitSummary;
    }

    @Override
    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals) {
        List<ShareFund> dailyUpdates = repository.dailyUpdates(fundId, userId, investmentId, individuals);
        Map<String, ObjectNode> map = new HashMap<>();
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        for (ShareFund fp : dailyUpdates) {
            String usdPrice = "0";
            String nzdPrice = "0";
            try {
                ObjectNode rateForPair = map.get(fp.getCreatedDate());
                if (rateForPair == null) {
                    Date dateTime = common.parseDate(fp.getCreatedDate(), dateFormat);
                    rateForPair = rateForPair("BTC", "USD,NZD", dateTime.getTime());
                    map.put(fp.getCreatedDate(), rateForPair);
                }
                if (rateForPair != null) {
                    JsonNode curr = rateForPair.get("BTC");
                    usdPrice = curr.get("USD").asText();
                    nzdPrice = curr.get("NZD").asText();
                }
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            BigDecimal usd = new BigDecimal(usdPrice);
            BigDecimal nzd = new BigDecimal(nzdPrice);
            BigDecimal quantity = new BigDecimal(fp.getQuantity()).setScale(16, RoundingMode.HALF_UP);
            fp.setUsd(usd.multiply(quantity).setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            fp.setLocal(nzd.multiply(quantity).setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
        }
        return dailyUpdates;
    }

    @Override
    public Map<String, Object> saveStripeCharge(ChargeRequest chargeRequest, String userId) throws StripeException {
        Map<String, Object> map = new HashMap<>();
        Charge charge = stripeService.charge(chargeRequest);
        map.put("charge", charge);
        StripeCharge stripeCharge = new StripeCharge(charge);
        stripeCharge.setStripe_created_ts(common.dateFormat(new Date(charge.getCreated())));
        Date now = dateTimeService.now();
        stripeCharge.setCreated_ts(common.dateFormat(now));
        stripeCharge.setUser_id(userId);
        stripeCharge.setFund_id(chargeRequest.getFundId());
        int id = repository.saveStripeCharge(stripeCharge);
        stripeCharge.setId(String.valueOf(id));
        map.put("stripeCharge", stripeCharge);
        TransactionBean walletTxn = new TransactionBean(stripeCharge);
        double amountInCents = Double.parseDouble(stripeCharge.getAmount());
        double amountInDollars = amountInCents / 100;
        walletTxn.setLocal_amount(String.valueOf(amountInDollars));
        String timestamp = common.dateFormat(now);
        if (stripeCharge.getCurrency() != null
                && !stripeCharge.getCurrency().isEmpty()
                && !stripeCharge.getCurrency().equalsIgnoreCase("USD")) {
            BigDecimal price = currencyConversionRate(stripeCharge.getCurrency() + "_USD", timestamp);
            amountInDollars = amountInDollars * price.doubleValue();
        }
        walletTxn.setCreated_ts(timestamp);
        walletTxn.setAmount(String.valueOf(amountInDollars));
        walletTxn.setActive("Y");
        walletTxn.setInc_dec("Inc");
        walletTxn.setBrand("STRIPE");
        int tid = repository.saveWalletTransaction(walletTxn);
        walletTxn.setId(String.valueOf(tid));
        map.put("walletTxn", walletTxn);
        return map;
    }

    @Override
    public ShareFund findFundPortfolio(String fundId
    ) {
        ShareFund fund = repository.fundById(fundId, null);
//        if (fund == null) {
//            fund = repository.portfolioById(fundId);
//        }
        return fund;
    }

    @Override
    public List<ShareFund> totalCoins(String userId, String investmentId
    ) {
        List<ShareFund> coins = repository.totalCoins(userId, investmentId);
        BigDecimal totalMarketPrice = BigDecimal.ZERO;
        for (ShareFund coin : coins) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal mktPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(cryptoCoin.getUsdPrice()));
                mktPrice = mktPrice.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                totalMarketPrice = totalMarketPrice.add(mktPrice);
                coin.setPrice(mktPrice.toString());
            }
        }
        for (ShareFund coin : coins) {
            coin.setShareName(coin.getCoinId().toUpperCase());
            BigDecimal percentage = new BigDecimal(coin.getPrice()).multiply(Constants.HUNDRED).divide(totalMarketPrice, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            percentage = percentage.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            coin.setPercentage(percentage.toString());
        }
        return coins;
    }

    @Override
    public Map<Date, BigDecimal> latestBalanceData(String type, String userId,
            String investmentId
    ) {
        List<ShareFund> latestBalanceData = repository.latestBalanceData(type, userId, investmentId);
        Map<String, CryptoCoin> coinMap = new LinkedHashMap<>();
        Map<Date, List<ShareFund>> map = new LinkedHashMap<>();
        for (ShareFund coin : latestBalanceData) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMap.get(coin.getCoinId());
                if (cryptoCoin == null) {
                    cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
                    coinMap.put(coin.getCoinId(), cryptoCoin);
                }
            } catch (Exception ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal mktPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(cryptoCoin.getUsdPrice()));
                mktPrice = mktPrice.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                coin.setShareAmount(mktPrice.toString());
                BigDecimal invPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(coin.getPrice()));
                invPrice = invPrice.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                coin.setPrice(invPrice.toString());
            }
            Date date = common.parseDate(coin.getCreatedDate(), common.format2);
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            List<ShareFund> list = map.get(date);
            if (list == null) {
                list = new LinkedList();
            }
            list.add(coin);
            map.put(date, list);
        }
        Map<Date, BigDecimal> map1 = new LinkedHashMap<>();
        Set<Map.Entry<Date, List<ShareFund>>> set = map.entrySet();
        BigDecimal amount = BigDecimal.ZERO;
        for (Iterator<Map.Entry<Date, List<ShareFund>>> it = set.iterator(); it.hasNext();) {
            Map.Entry<Date, List<ShareFund>> entry = it.next();
            Date key = entry.getKey();
            List<ShareFund> list = entry.getValue();
            BigDecimal value = BigDecimal.ZERO;
            for (ShareFund coin : list) {
                value = value.add(new BigDecimal(coin.getShareAmount()));
                amount = amount.add(new BigDecimal(coin.getPrice()));
            }
            map1.put(key, value);
        }
        return map1;
    }

    @Override
    public List<ShareFund> detailsByInvestment(String userId, String investmentId,
            String action
    ) {
        List<ShareFund> shares = repository.detailsByInvestment(userId, investmentId, action);
        for (ShareFund share : shares) {
            if (share.getShareId() == null) {
                share.setShareName(share.getCoinId());
            }
        }
        return shares;
    }

    @Override
    public void addTrade(ShareFund trade) {
        Date date = common.parseDate(trade.getCreatedDate(), "yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, trade.getHh());
        cal.set(Calendar.MINUTE, trade.getMm());
        cal.set(Calendar.SECOND, trade.getSs());
        String dateFormat = common.dateFormat(cal.getTime());
        List<ShareFund> totalUnitsById = repository.totalUnitsById(trade.getFundId(), null, null);
        BigDecimal totalMinto = BigDecimal.ZERO;
        BigDecimal totalBtc = new BigDecimal(trade.getCloseBal());
        for (ShareFund unit : totalUnitsById) {
            totalMinto = totalMinto.add(new BigDecimal(unit.getMinto()));
        }
//      currentBtc =  individualMinto / totalMinto * totalBtc;
        for (ShareFund unit : totalUnitsById) {
            BigDecimal individualMinto = new BigDecimal(unit.getMinto());
            BigDecimal currentBtc = individualMinto.divide(totalMinto, 16, RoundingMode.HALF_UP).multiply(totalBtc);
            unit.setBtc(currentBtc.toPlainString());
            unit.setCreatedDate(dateFormat);
        }
        trade.setCreatedDate(dateFormat);
        repository.addTrade(trade, totalUnitsById);
    }

    @Override
    public void saveTodayUpdates(String formattedate) {
        List<UserFundsCommand> userTodayStatus = repository.getUserTodayStatus(null, null, null, formattedate, formattedate, Boolean.TRUE);
        if (userTodayStatus.isEmpty()) {
            List<InvestmentBean> purchasedInvestments = repository.purchasedInvestmentsAndCash(null, null, null);
            for (InvestmentBean investmentBean : purchasedInvestments) {
                investmentBean.setCreatedDate(formattedate);
            }
            repository.saveUserTodayStatus(purchasedInvestments);
        }
    }

    @Override
    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate, Boolean sum, String currency) {
        List<UserFundsCommand> latestWeek = repository.getUserTodayStatus(fundId, userId, investmentId, fDate, tDate, sum);
        for (UserFundsCommand inv : latestWeek) {
            BigDecimal investment_amount = new BigDecimal(inv.getInvestment_amount());
            if (currency != null && !"USD".equalsIgnoreCase(currency)) {
                BigDecimal localPrice = currencyConversionRate("USD_" + currency, inv.getCreated_ts());
                investment_amount = investment_amount.multiply(localPrice);
            }
            investment_amount = investment_amount.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            inv.setInvestment_amount(investment_amount.toPlainString());
        }
        return latestWeek;
    }

    @Override
    public void saveSellInvestmentRequest(AccountEventCommand request) {
        request.setCurrency("NZD");
        BigDecimal localPrice = currencyConversionRate("USD_" + request.getCurrency(), request.getCreateDate());
        BigDecimal amount = new BigDecimal(request.getAmount());
        amount = amount.multiply(localPrice);
        request.setLocal_amount(amount.toPlainString());
        repository.saveSellInvestmentRequest(request);
    }

    @Override
    public List<InvestmentBean> pendingInvestments(String userId) {
        List<InvestmentBean> pendingInvestments = repository.pendingInvestmentRequests(userId, null, null);
        for (InvestmentBean inv : pendingInvestments) {
            BigDecimal investmentAmount = new BigDecimal(inv.getInvestmentAmount());
            inv.setInvestmentAmount(investmentAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            if (inv.getCurrency() != null && !inv.getCurrency().isEmpty()) {
                String currencyPair = "USD_" + inv.getCurrency();
                BigDecimal localPrice = currencyConversionRate(currencyPair, inv.getCreatedDate());
                investmentAmount = investmentAmount.multiply(localPrice);
                inv.setLocalInvestmentAmount(investmentAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            }
        }
        return pendingInvestments;
    }

    @Override
    public List<InvestmentBean> pendingSellTransactions(String userId) {
        List<InvestmentBean> pendingSellTransactions = repository.pendingSellTransactions(userId, null, null);
        for (InvestmentBean tran : pendingSellTransactions) {
            BigDecimal investmentAmount = new BigDecimal(tran.getInvestmentAmount());
            tran.setInvestmentAmount(investmentAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            if (tran.getCurrency() != null && !tran.getCurrency().isEmpty()) {
                String currencyPair = "USD_" + tran.getCurrency();
                BigDecimal localPrice = currencyConversionRate(currencyPair, tran.getCreatedDate());
                investmentAmount = investmentAmount.multiply(localPrice);
                tran.setLocalInvestmentAmount(investmentAmount.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP).toPlainString());
            }
        }
        return pendingSellTransactions;
    }

    @Override
    public ObjectNode rateForPair(String currency0, String currency1,
            Long timestamp) throws IOException {
        if (timestamp != null && timestamp > 0) {
            ObjectNode rateForPair = coinMarketCapAPIService.getRateForPair(currency0, currency1, timestamp);
            return rateForPair;
        } else {
            ObjectNode rateForPair = coinMarketCapAPIService.getRateForPair(currency0, currency1);
            return rateForPair;
        }
    }

    @Override
    public List<PortfolioBean> portfolioReportById(String portfolioId) {
        return repository.portfolioReportById(common.getAllDatesOfLatestWeek(), portfolioId);
    }

    @Override
    public List<PortfolioBean> portfoliosReport(List<String> days) {
        List<PortfolioBean> portfoliosReport = repository.portfoliosReport();
        for (PortfolioBean portfolio : portfoliosReport) {
            BigDecimal total_units = new BigDecimal(portfolio.getTotal_units());
            BigDecimal nav = new BigDecimal(portfolio.getNav());
            BigDecimal invested_usd = new BigDecimal(portfolio.getInvested_usd());
            BigDecimal total_usd = new BigDecimal(portfolio.getTotal_usd());
            portfolio.setTotal_units(total_units.setScale(2, RoundingMode.HALF_UP).toPlainString());
            portfolio.setNav(nav.setScale(2, RoundingMode.HALF_UP).toPlainString());
            portfolio.setInvested_usd(invested_usd.setScale(2, RoundingMode.HALF_UP).toPlainString());
            portfolio.setTotal_usd(total_usd.setScale(2, RoundingMode.HALF_UP).toPlainString());
            BigDecimal todayNav = null;
            List<PortfolioBean> portfolioReports = repository.portfolioReportById(days, portfolio.getFund_id());
            for (int i = 0; i < portfolioReports.size(); i++) {
                PortfolioBean bean = portfolioReports.get(i);
                if (i == 0) {
                    todayNav = new BigDecimal(bean.getNav());
                } else {
                    BigDecimal otherNav = new BigDecimal(bean.getNav());
                    BigDecimal difference = todayNav.subtract(otherNav);
                    BigDecimal performance = difference.divide(otherNav, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(new BigDecimal("100"));
                    switch (i) {
                        case 1:
                            portfolio.setPercent24hr(performance.doubleValue());
                            break;
                        case 2:
                            portfolio.setPercent7d(performance.doubleValue());
                            break;
                        case 3:
                            portfolio.setPercent30d(performance.doubleValue());
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return portfoliosReport;
    }

    @Override
    public BigDecimal currencyConversionRate(String currencyPair, String ts) {
        try {
            String date = common.ts2date(ts);
            boolean colExist = false;
            if ("usd_nzd".equalsIgnoreCase(currencyPair)
                    || "usd_gbp".equalsIgnoreCase(currencyPair)
                    || "usd_inr".equalsIgnoreCase(currencyPair)
                    || "usd_aud".equalsIgnoreCase(currencyPair)) {
                colExist = true;
            }
            if (colExist) {
                ShareFund conversionRate = repository.currencyConversionRate(currencyPair, date);
                String price = null;
                if (conversionRate != null && conversionRate.getId() != null) {
                    price = conversionRate.getPrice();
                    if (price == null) {
                        ObjectNode response = coinMarketCapAPIService.currencyConversionRate(currencyPair, date);
                        JsonNode rateNode = response.get(currencyPair);
                        price = rateNode.asText();
                        repository.updateCurrencyConversionRate(currencyPair, conversionRate.getId(), price);
                    }
                    if (price != null) {
                        return new BigDecimal(price);
                    }
                } else {
                    ObjectNode response = coinMarketCapAPIService.currencyConversionRate(currencyPair, date);
                    JsonNode rateNode = response.get(currencyPair);
                    price = rateNode.asText();
                    if (price != null) {
                        repository.saveCurrencyConversionRate(currencyPair, date, price);
                        return new BigDecimal(price);
                    }
                }
            } else {
                ObjectNode response = coinMarketCapAPIService.currencyConversionRate(currencyPair, date);
                JsonNode rateNode = response.get(currencyPair);
                String price = rateNode.asText();
                if (price != null) {
                    return new BigDecimal(price);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Autowired
    private StripeService stripeService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private BittrexAccountAPIService bittrexAccountAPIService;
}
