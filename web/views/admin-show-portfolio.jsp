<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Summary Page</span>
                        </li>
                        <!--                        <li class="breadcrumb-item">
                                                    <span>Laptop with retina screen</span>
                                                </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="element-wrapper">
                                <h6 class="element-header">${portfolio.name} Total Units: ${totalUnits.quantity} Closing Balance: ${totalUnits.closeBal} Unit Price: ${totalUnits.price}</h6> <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div class="os-tabs-w">
                                            <div class="os-tabs-controls">
                                                <ul class="nav nav-tabs smaller">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab_overview">Daily Crypto Balance Snapshot
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul class="nav nav-pills smaller hidden-sm-down">
                                                    <li class="nav-item">
                                                        <a class="nav-link todayActive" data-toggle="tab" href="#" onclick="balChart('today')">Today</a>
                                                    </li>
                                                    <!--                                                        <li class="nav-item">
                                                                                                                <a class="nav-link weekActive" data-toggle="tab" href="#" onclick="balChart('week')">Last Week</a>
                                                                                                            </li>
                                                                                                            <li class="nav-item">
                                                                                                                <a class="nav-link monthActive" data-toggle="tab" href="#" onclick="balChart('month')">Last Month</a>
                                                                                                            </li>-->
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab_overview">
                                                    <div class="el-tablo">
                                                        <div class="label">
                                                            Current Balance of all Investments  <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                        </div>
                                                        <div class="value currentBalance">USD $14,747.06</div>
                                                    </div>
                                                    <div class="el-chart-w" id="linegraph" data-highcharts-chart="7">
                                                        <div id="highcharts-gtbtf60-14" style="position: relative; overflow: hidden; width: 633px; height: 400px; text-align: left; line-height: normal; z-index: 0; left: 0px; top: 0.166687px;" dir="ltr" class="highcharts-container ">
                                                            <svg version="1.1" class="highcharts-root" style="font-family:Lucida Grande, Lucida Sans Unicode, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="633" height="400" viewBox="0 0 633 400">
                                                            <desc>Created with Highcharts 6.0.4</desc>
                                                            <defs><clipPath id="highcharts-gtbtf60-15"><rect x="0" y="0" width="512" height="349" fill="none"></rect></clipPath></defs>
                                                            <rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="633" height="400" rx="0" ry="0"></rect>
                                                            <rect fill="none" class="highcharts-plot-background" x="111" y="10" width="512" height="349"></rect>
                                                            <g class="highcharts-pane-group"></g><g class="highcharts-plot-lines-0"><path fill="none" stroke="#808080" stroke-width="1" visibility="hidden"></path></g><g class="highcharts-grid highcharts-xaxis-grid "><path fill="none" opacity="1" class="highcharts-grid-line" d="M 272.90642881528265 10 L 272.90642881528265 359"></path><path fill="none" opacity="1" class="highcharts-grid-line" d="M 444.68635970269713 10 L 444.68635970269713 359"></path><path fill="none" opacity="1" class="highcharts-grid-line" d="M 615.6863597026971 10 L 615.6863597026971 359"></path></g><g class="highcharts-grid highcharts-yaxis-grid "><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 111 185.5 L 623 185.5" opacity="1"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 272.5 L 623 272.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 97.5 L 623 97.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 315.5 L 623 315.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 228.5 L 623 228.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 141.5 L 623 141.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 54.5 L 623 54.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 359.5 L 623 359.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 111 9.5 L 623 9.5"></path></g><rect fill="none" class="highcharts-plot-border" x="111" y="10" width="512" height="349"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 272.90642881528265 359 L 272.90642881528265 369" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 444.68635970269713 359 L 444.68635970269713 369" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 615.736335389488 359 L 615.736335389488 369" opacity="1"></path><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 111 359.5 L 623 359.5"></path></g><g class="highcharts-axis highcharts-yaxis "><text x="26.38333511352539" text-anchor="middle" transform="translate(0,0) rotate(270 26.38333511352539 184.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="184.5"><tspan>Value</tspan></text><path fill="none" class="highcharts-axis-line" d="M 111 10 L 111 359"></path></g><g class="highcharts-series-group"><g class="highcharts-series highcharts-series-0 highcharts-line-series highcharts-color-0 " transform="translate(111,10) scale(1 1)" clip-path="url(#highcharts-gtbtf60-15)"><path fill="none" d="M 5.01960665454801 210.77684031050188 L 18.05021339349574 266.98500000011427 L 35.274337589638336 319.3349999999873 L 52.39573341085323 174.5 L 70.93820508523007 186.7149999999492 L 88.28217905212124 85.50500000027932 L 105.64327441483536 139.60000000019045 L 123.02149117336747 244.29999999993652 L 140.26273676532784 33.155000000088876 L 157.43549677401086 57.5849999999873 L 174.79659213672102 57.5849999999873 L 191.91798795793414 120.40500000008888 L 209.31332611228922 34.900000000126965 L 226.5032075167947 62.81999999978416 L 243.77869590039504 211.14499999984764 L 261.0028200965404 155.30499999989843 L 278.3981582508955 181.47999999983494 L 295.79349640525055 176.2450000000381 L 312.91489222646624 75.03500000005079 L 330.19038061007154 289.670000000292 L 347.4316262020344 130.875 L 364.587264814895 109.93500000017775 L 381.9141173859627 122.15000000012697 L 399.1211201862857 305.375 L 416.2938801949637 162.2850000000508 L 433.4323974119993 150.07000000010157 L 450.7934927747169 284.43500000017775 L 468.171709533247 85.50499999996191 L 485.4471979168548 111.68000000021584 L 502.61995792553034 111.68000000021584 L 506.98039334545126 124.81500486792281" class="highcharts-graph" stroke="#7cb5ec" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path><path fill="none" d="M -4.9803934682777 266.98500000011427 L 5.0196065317223 266.98500000011427 L 22.244170834283 319.3349999999873 L 39.366004137027 174.5 L 57.9089496039 186.7149999999492 L 75.25336673958 85.50500000027932 L 92.614905708562 139.60000000019045 L 109.99356651085 244.29999999993652 L 127.23525264671 33.155000000088876 L 144.40845144936 57.5849999999873 L 161.76999041835 57.5849999999873 L 178.89182372109 120.40500000008888 L 196.28760635668 34.900000000126965 L 213.47792699263 62.81999999978416 L 230.7538567951 211.14499999984764 L 247.97842109766 155.30499999989843 L 265.37420373325 181.47999999983494 L 282.76998636884 176.2450000000381 L 299.89181967159 75.03500000005079 L 317.16774947405 289.670000000292 L 334.40943560992 130.875 L 351.56551257927 109.93500000017775 L 368.89280788164 122.15000000012697 L 386.1002503509 305.375 L 403.27344915356 162.2850000000508 L 420.4124042896 150.07000000010157 L 437.77394325859 284.43500000017775 L 455.15260406087 85.50499999996191 L 472.42853386334 111.68000000021584 L 489.60173266599 111.68000000021584 L 506.98039346828 164.03000000008888 L 516.98039346828 164.03000000008888" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path></g><g class="highcharts-markers highcharts-series-0 highcharts-line-series highcharts-color-0  highcharts-tracker" transform="translate(111,10) scale(1 1)"><path fill="#7cb5ec" d="M 22 148.32500000006348 A 0 0 0 1 1 22 148.32500000006348 Z" class="highcharts-halo highcharts-color-0" fill-opacity="0.25"></path><path fill="#7cb5ec" d="M 21.780797213903966 266.98500000011427 A 4 4 0 1 1 21.780795213904135 266.9810000007809 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 38.8727360371428 319.3349999999873 A 4 4 0 1 1 38.87273403714297 319.33100000065394 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 55.826852033878154 174.5 A 4 4 0 1 1 55.82685003387832 174.49600000066667 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 74.58137274175334 186.7149999999492 A 4 4 0 1 1 74.5813707417535 186.71100000061588 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 91.82685203387815 85.50500000027932 A 4 4 0 1 1 91.82685003387832 85.501000000946 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 109.62995580403356 139.60000000019045 A 4 4 0 1 1 109.62995380403372 139.59600000085712 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 126.62995580403356 244.29999999993652 A 4 4 0 1 1 126.62995380403372 244.2960000006032 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 143.8727360371428 33.155000000088876 A 4 4 0 1 1 143.87273403714298 33.15100000075554 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 160.91844741227604 57.5849999999873 A 4 4 0 1 1 160.9184454122762 57.581000000653965 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 178.67835608358638 57.5849999999873 A 4 4 0 1 1 178.67835408358656 57.581000000653965 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 195.67835608358638 120.40500000008888 A 4 4 0 1 1 195.67835408358656 120.40100000075554 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 212.91844741227604 34.900000000126965 A 4 4 0 1 1 212.9184454122762 34.89600000079363 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 229.96398435467103 62.81999999978416 A 4 4 0 1 1 229.9639823546712 62.81600000045082 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 247.72657166965166 211.14499999984764 A 4 4 0 1 1 247.72656966965184 211.1410000005143 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 264.72657166965166 155.30499999989843 A 4 4 0 1 1 264.72656966965184 155.3010000005651 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 281.963984354671 181.47999999983494 A 4 4 0 1 1 281.9639823546712 181.4760000005016 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 299.7746006587606 176.2450000000381 A 4 4 0 1 1 299.7745986587608 176.24100000070476 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 316.7746006587606 75.03500000005079 A 4 4 0 1 1 316.7745986587608 75.03100000071746 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 334.00934506660724 289.670000000292 A 4 4 0 1 1 334.0093430666074 289.66600000095866 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 351.00934506660724 130.875 A 4 4 0 1 1 351.0093430666074 130.87100000066667 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 368.05452775732147 109.93500000017775 A 4 4 0 1 1 368.05452575732164 109.93100000084442 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 385.822441154811 122.15000000012697 A 4 4 0 1 1 385.82243915481115 122.14600000079363 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 403.05452775732147 305.375 A 4 4 0 1 1 403.05452575732164 305.37100000066664 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 420.05452775732147 162.2850000000508 A 4 4 0 1 1 420.05452575732164 162.28100000071746 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 437.05452775732147 150.07000000010157 A 4 4 0 1 1 437.05452575732164 150.06600000076824 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 454.822441154811 284.43500000017775 A 4 4 0 1 1 454.82243915481115 284.4310000008444 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 472.05452775732147 85.50499999996191 A 4 4 0 1 1 472.05452575732164 85.50100000062858 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 489.05452775732147 111.68000000021584 A 4 4 0 1 1 489.05452575732164 111.67600000088251 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 506.0995306430784 111.68000000021584 A 4 4 0 1 1 506.0995286430786 111.67600000088251 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 510 164 A 4 4 0 1 1 509.9999980000002 163.99600000066667 Z" class="highcharts-point highcharts-color-0"></path></g></g><text x="317" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text><text x="317" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text><g class="highcharts-axis-labels highcharts-xaxis-labels "><text x="273.7127154994467" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="378" opacity="1"><tspan>15:41:50</tspan></text><text x="444.92677017349064" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="378" opacity="1"><tspan>15:42:00</tspan></text><text x="598.6666660308838" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="378" opacity="1"><tspan>15:42:10</tspan></text></g><g class="highcharts-axis-labels highcharts-yaxis-labels "><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="187" opacity="1"><tspan>14 747</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="274" opacity="1"><tspan>14 746.5</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="99" opacity="1"><tspan>14 747.5</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="317" opacity="1"><tspan>14 746.25</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="230" opacity="1"><tspan>14 746.75</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="143" opacity="1"><tspan>14 747.25</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="56" opacity="1"><tspan>14 747.75</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="361" opacity="1"><tspan>14 746</tspan></text><text x="96" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="12" opacity="1"><tspan>14 748</tspan></text></g><g class="highcharts-label highcharts-tooltip highcharts-color-0" style="cursor:default;pointer-events:none;white-space:nowrap;" transform="translate(59,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 63.5 C 147.5 66.5 147.5 66.5 144.5 66.5 L 79.5 66.5 73.5 72.5 67.5 66.5 3.5 66.5 C 0.5 66.5 0.5 66.5 0.5 63.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 63.5 C 147.5 66.5 147.5 66.5 144.5 66.5 L 79.5 66.5 73.5 72.5 67.5 66.5 3.5 66.5 C 0.5 66.5 0.5 66.5 0.5 63.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 63.5 C 147.5 66.5 147.5 66.5 144.5 66.5 L 79.5 66.5 73.5 72.5 67.5 66.5 3.5 66.5 C 0.5 66.5 0.5 66.5 0.5 63.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 63.5 C 147.5 66.5 147.5 66.5 144.5 66.5 L 79.5 66.5 73.5 72.5 67.5 66.5 3.5 66.5 C 0.5 66.5 0.5 66.5 0.5 63.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path><text x="8" style="font-size:12px;color:#333333;fill:#333333;" y="20"><tspan style="font-weight:bold">Live data</tspan><tspan x="8" dy="15">2017-12-23 15:41:01</tspan><tspan x="8" dy="15">14 747.15</tspan></text></g></svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab_sales"></div>
                                                <div class="tab-pane" id="tab_conversion"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6 style="font-size: 14px;text-align: center;">Initial Amount</h6>
                                                    <div class="col-md-12 color3 invest6 btn-hover">
                                                        <center>$ ${trade.opBal}</center>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h6 style="font-size: 14px;text-align: center;">Current Balance</h6>
                                                    <div class="col-md-12 color3 invest1 btn-hover">
                                                        <center>$ ${trade.closeBal}</center>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 20px">
                                                    <h6 style="font-size: 14px;text-align: center;">Profit/Loss</h6>
                                                    <div class="col-md-12 color3 invest3 btn-hover">
                                                        <center>$ ${trade.closeBal - trade.opBal}</center>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="margin-top: 20px">
                                                    <h6 style="font-size: 14px;text-align: center;">% Profit/Loss</h6>
                                                    <div class="col-md-12 color3 invest5 btn-hover">
                                                        <center>${(trade.closeBal - trade.opBal) / trade.opBal * 100}%</center>
                                                    </div>
                                                </div>
                                            </div>
                                            <c:choose>
                                                <c:when test="${portfolio.fundId eq 19}">
                                                    <div class="row" ng-app="btcApp19" ng-init="quantity = 0;price = 0" ng-controller="btcCtrl19">
                                                        <form method="POST" action="./admin-add-bitcoin">
                                                            <div class="steps-w">
                                                                <div class="step-contents">
                                                                    <div class="step-content active" id="stepContent1">
                                                                        <div class="row">
                                                                            <input type="hidden" name="fundId" value="${portfolio.fundId}">
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label for="quantity">Total Bitcoin Quantity</label>
                                                                                    <input type="text" class="form-control" id="quantity" name="quantity" ng-model="quantity" required="required" placeholder="Enter bitcoin quantity"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label for="price">USD price Per Bitcoin</label>
                                                                                    <input type="text" class="form-control" id="price" name="price" ng-model="price" required="required" placeholder="Enter USD price per bitcoin"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label for="shareAmount">Bitcoin Value in USD</label>
                                                                                    <input type="text" class="form-control" id="shareAmount" name="shareAmount" ng-value="shareAmount()"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 offset-6">
                                                                                <div class="form-buttons-w">
                                                                                    <input class="btn btn-primary" type="submit" value="Submit"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="row">
                                                        <form method="POST" id="trade" name="trade" action="./admin-add-trade">
                                                            <div class="steps-w">
                                                                <div class="step-contents">
                                                                    <div class="step-content active" id="stepContent1">
                                                                        <div class="row">
                                                                            <input type="hidden" name="fundId" value="${portfolio.fundId}">
                                                                            <div class="col-sm-8">
                                                                                <div class="form-group">
                                                                                    <label for="createdDate">Created Date</label>
                                                                                    <input type="date" class="form-control" id="createdDate" name="createdDate" required="required" placeholder="Enter created date"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <label for="price">Minto Price</label>
                                                                                    <input type="text" class="form-control" id="price" name="price" required="required" placeholder="Enter unit price"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <label for="time">Timestamp</label>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-4">
                                                                                            <select name="hh" class="form-control">
                                                                                                <c:forEach items="${hh}" var="hour">
                                                                                                    <option>${hour}</option>
                                                                                                </c:forEach>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <select name="mm" class="form-control">
                                                                                                <c:forEach items="${mm}" var="hour">
                                                                                                    <option>${hour}</option>
                                                                                                </c:forEach>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <select name="ss"  class="form-control">
                                                                                                <c:forEach items="${mm}" var="hour">
                                                                                                    <option>${hour}</option>
                                                                                                </c:forEach>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="opBal">Opening Bitcoins</label>
                                                                                    <input type="text" class="form-control" id="opBal" name="opBal" required="required" placeholder="Enter opening bitcoins."/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="closeBal">Closing Bitcoins</label>
                                                                                    <input type="text" class="form-control" id="closeBal" name="closeBal" required="required" placeholder="Enter closing bitcoins."/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="purTrades">Purchased Trades</label>
                                                                                    <input type="text" class="form-control" id="purTrades" name="purTrades" required="required" placeholder="Enter purchased trades."/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="soldTrades">Sold Trades</label>
                                                                                    <input type="text" class="form-control" id="soldTrades" name="soldTrades" required="required" placeholder="Enter sold trades.">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="purCoins">Purchased Coins</label>
                                                                                    <input type="text" class="form-control" id="purCoins" name="purCoins" required="required" placeholder="Enter purchased coins.">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <div class="form-group">
                                                                                    <label for="soldCoins">Sold Coins</label>
                                                                                    <input type="text" class="form-control" id="soldCoins" name="soldCoins" required="required" placeholder="Enter sold coins.">                                                                        
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6"></div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-buttons-w pull-right">
                                                                                    <input class="btn btn-primary" type="submit" value="Submit"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>                   
                                                </c:otherwise>
                                            </c:choose>

                                            <div class="row" style="margin-top: 20px">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-loosing-trade"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-winning-trade"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-today-trade"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-profit-loss"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-btc/usd-trade"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-topcoin-byvolume-trade"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper"></div>
                                        <div id="container-topcoin-trade"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--------------------START - Sidebar------------------>
                        <%--<jsp:include page = "admin-right-sidebar.jsp" />--%>
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/update-investment-shares.jsp" />

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>


        <script>
                                                                                        function qpes(idx) {
                                                                                            var q = $("#quantity" + idx).val();
                                                                                            var p = $("#price" + idx).val();
                                                                                            var s = p * q;
                                                                                            $("#shareAmount" + idx).val(s);
                                                                                        }
                                                                                        function sdpeq(idx) {
                                                                                            var s = $("#shareAmount" + idx).val();
                                                                                            var p = $("#price" + idx).val();
                                                                                            var q = s / p;
                                                                                            $("#quantity" + idx).val(q.toFixed(4));
                                                                                        }
                                                                                        var app = angular.module("btcApp19", []);
                                                                                        app.controller("btcCtrl19", function ($scope) {
//                                                                                        $scope.formatNumber = function (i) {
//                                                                                            if (i > 0) {
//                                                                                                return i.toFixed(8);
//                                                                                            } else {
//                                                                                                return i;
//                                                                                            }
//                                                                                        }
                                                                                            $scope.shareAmount = function () {
                                                                                                return eval($scope.price * $scope.quantity).toFixed(8);
                                                                                            };
                                                                                        });
        </script>

        <script>
            $(document).ready(function () {
                adminCoinSummary();
                var shareOptions = '';
            <c:forEach items="${shares}" var="share" varStatus="loop">
                shareOptions = shareOptions + '<option value="${share.shareId}">${share.name}</option>';
            </c:forEach>
            });
        </script>
        <script>

            var gaugeOptions = {
                chart: {
                    type: 'solidgauge'
                },
                title: null,
                pane: {
                    center: ['50%', '50%'],
                    size: '100%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                        innerRadius: '80%',
                        outerRadius: '100%',
                        shape: 'circle'
                    }
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                // the value axis
                yAxis: {
//                    stops: [
//                        [0.1, '#55BF3B'], // green
//                        [0.5, '#DDDF0D'], // yellow
//                        [0.9, '#DF5353'] // red
//                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickAmount: 2,
                    title: {
                        y: -70
                    },
                    labels: {
                        y: 16
                    }
                },
                plotOptions: {
                    solidgauge: {
                        innerRadius: '80%',
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            };

            var loosingTrades = Highcharts.chart('container-loosing-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 10,
                    title: {
                        text: 'No. of Loosing Trades'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
//                        name: 'No. of Loosing Trades',
                        data: [5],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">Loosing Trades</span></div>'
                        },
                        tooltip: {
                            valueSuffix: 'Loosing Trades'
                        }
                    }]
            }));

            var winningTrades = Highcharts.chart('container-winning-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 10,
                    title: {
                        text: 'No. of Winning Trades'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
//                        name: 'No. of Loosing Trades',
                        data: [5],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">Winning Trades</span></div>'
                        },
                        tooltip: {
                            valueSuffix: 'Winning Trades'
                        }
                    }]
            }));

            var todayTrades = Highcharts.chart('container-today-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 10,
                    title: {
                        text: 'No. of Today Trades'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
//                        name: 'No. of Loosing Trades',
                        data: [${todayOrders}],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">Todays Trades</span></div>'
                        },
                        tooltip: {
                            valueSuffix: 'Loosing Trades'
                        }
                    }]
            }));

            // The RPM gauge
            var chartRpm2 = Highcharts.chart('container-profit-loss', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 5,
                    title: {
                        text: 'Profit/Loss'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                        name: 'RPM',
                        data: [1],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">* 1000 / min</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

            }));

            // The RPM gauge
            var chartRpm3 = Highcharts.chart('container-btc/usd-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 5,
                    title: {
                        text: 'BTC/USD Trades'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                        name: 'RPM',
                        data: [1],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">* 1000 / min</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

            }));

            // The RPM gauge
            var chartRpm4 = Highcharts.chart('container-topcoin-byvolume-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 5,
                    title: {
                        text: 'Top Coin By Volume'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                        name: 'RPM',
                        data: [1],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">* 1000 / min</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

            }));

            // The RPM gauge
            var chartRpm5 = Highcharts.chart('container-topcoin-trade', Highcharts.merge(gaugeOptions, {
                yAxis: {
                    showFirstLabel: false,
                    showLastLabel: false,
                    min: 0,
                    max: 5,
                    title: {
                        text: 'Top Coin Trades'
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                        name: 'RPM',
                        data: [1],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.1f}</span><br/>' +
                                    '<span style="font-size:12px;color:silver">* 1000 / min</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

            }));


            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                },
                credits: {
                    enabled: false
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total fruit consumption'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: 0,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                        name: 'John',
                        data: [5, 3, 4, 7, 2]
                    }, {
                        name: 'Jane',
                        data: [2, 2, 3, 2, 1]
                    }, {
                        name: 'Joe',
                        data: [3, 4, 4, 2, 5]
                    }]
            });

            // Bring life to the dials
            setInterval(function () {
                // Speed
                var point,
                        newVal,
                        inc;

                if (chartSpeed) {
                    point = chartSpeed.series[0].points[0];
                    inc = Math.round((Math.random() - 0.5) * 100);
                    newVal = point.y + inc;

                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y - inc;
                    }

                    point.update(newVal);
                }

                // RPM
                if (chartRpm) {
                    point = chartRpm.series[0].points[0];
                    inc = Math.random() - 0.5;
                    newVal = point.y + inc;

                    if (newVal < 0 || newVal > 5) {
                        newVal = point.y - inc;
                    }

                    point.update(newVal);
                }
            }, 2000);



        </script>
    </body>
</html>
