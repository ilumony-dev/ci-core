/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 *
 * @author palo12
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(Exception.class)
    public ModelAndView exception(Exception e) {
        ModelAndView mav = new ModelAndView("exception");
        mav.addObject("name", e.getClass().getSimpleName());
        mav.addObject("message", e.getMessage());
        mav.addObject("localizedMessage", e.getLocalizedMessage());
        return mav;
    }
    
    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView exception(final NoHandlerFoundException e) {
        ModelAndView mav = new ModelAndView("exception");
        mav.addObject("name", e.getClass().getSimpleName());
        mav.addObject("message", e.getMessage());
        mav.addObject("localizedMessage", e.getLocalizedMessage());
        return mav;
    }
//    
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity exception(final Exception e) {
//        return error(e, HttpStatus.NOT_FOUND, e.getMessage());
//    }
//
//    @ExceptionHandler(NullPointerException.class)
//    public ResponseEntity exception(final NullPointerException e) {
//        return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());
//    }
//
//    @ExceptionHandler(IllegalArgumentException.class)
//    public ResponseEntity exception(final IllegalArgumentException e) {
//        return error(e, HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
//    }
//
//    @ExceptionHandler(NotFoundException.class)
//    public ResponseEntity exception(final NotFoundException e) {
//        return error(e, HttpStatus.NOT_FOUND, e.getLocalizedMessage());
//    }
//
//    @ExceptionHandler(InternalServerException.class)
//    public ResponseEntity exception(final InternalServerException e) {
//        return error(e, HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
//    }
//
//    @ExceptionHandler(InternalServerErrorException.class)
//    public ResponseEntity exception(final InternalServerErrorException e) {
//        return error(e, HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
//    }
//
//    private ResponseEntity error(final Exception exception, final HttpStatus httpStatus, final String logRef) {
//        final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
//        return new ResponseEntity<>(new String(logRef + "<\n>" + message), httpStatus);
//    }
}
