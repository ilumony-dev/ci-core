/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.modelmap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nz.cryptolabs.beans.SecuredUser;
import static com.nz.cryptolabs.constants.Constants.userMap;
import com.nz.cryptolabs.services.SessionManagementService;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping(value = "/rest/cryptolabs/modelmap")
public class UserModelMapController {

    @RequestMapping(value = {"/{key}"}, method = RequestMethod.GET)
    @ResponseBody
    public String map(
            @PathVariable("key") String key,
            @RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "id", required = false) String userId,
            @RequestParam(name = "ok", required = false) String investmentId,
            ModelMap modelMap
    ) throws JsonProcessingException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        HashMap<String, Object> userMap = null;
        if (user == null) {
            return "{}";
        } else if (user.getAdmin() && userId != null && investmentId != null) {
            userMap = userMap(key + "?id=" + userId + "&ok=" + investmentId);
        } else if (user.getAdmin() && userId != null) {
            userMap = userMap(key + "?id=" + userId);
        } else if (user.getUser() && userId != null && investmentId != null) {
            userMap = userMap(key + "?id=" + user.getUserId() + "&ok=" + investmentId);
        } else if (user.getUser() && userId != null) {
            userMap = userMap(key + "?id=" + user.getUserId());
        }
        if (userMap == null) {
            return "{}";
        }
        String gsonString = new ObjectMapper().writeValueAsString(userMap);
        System.out.println(gsonString);
        return gsonString;
    }

    @Autowired
    private SessionManagementService sessionManagementService;

}
