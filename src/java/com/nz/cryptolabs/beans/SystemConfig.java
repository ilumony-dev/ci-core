/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Maninderjit Singh
 */
public class SystemConfig implements ToObjectConverter {

    /**
     * @return the adminEmailId
     */
    public String getAdminEmailId() {
        return adminEmailId;
    }

    /**
     * @param adminEmailId the adminEmailId to set
     */
    public void setAdminEmailId(String adminEmailId) {
        this.adminEmailId = adminEmailId;
    }

    /**
     * @return the supportEmailId
     */
    public String getSupportEmailId() {
        return supportEmailId;
    }

    /**
     * @param supportEmailId the supportEmailId to set
     */
    public void setSupportEmailId(String supportEmailId) {
        this.supportEmailId = supportEmailId;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the localCurrency
     */
    public String getLocalCurrency() {
        return localCurrency;
    }

    /**
     * @param localCurrency the localCurrency to set
     */
    public void setLocalCurrency(String localCurrency) {
        this.localCurrency = localCurrency;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the backgroundImage
     */
    public String getBackgroundImage() {
        return backgroundImage;
    }

    /**
     * @param backgroundImage the backgroundImage to set
     */
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * @param textColor the textColor to set
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    /**
     * @return the primaryButtonColor
     */
    public String getPrimaryButtonColor() {
        return primaryButtonColor;
    }

    /**
     * @param primaryButtonColor the primaryButtonColor to set
     */
    public void setPrimaryButtonColor(String primaryButtonColor) {
        this.primaryButtonColor = primaryButtonColor;
    }

    /**
     * @return the menuColor
     */
    public String getMenuColor() {
        return menuColor;
    }

    /**
     * @param menuColor the menuColor to set
     */
    public void setMenuColor(String menuColor) {
        this.menuColor = menuColor;
    }

    /**
     * @return the profileColor
     */
    public String getProfileColor() {
        return profileColor;
    }

    /**
     * @param profileColor the profileColor to set
     */
    public void setProfileColor(String profileColor) {
        this.profileColor = profileColor;
    }

    /**
     * @return the backgroundColor
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public SystemConfig() {
    }
    
    public SystemConfig(String backgroundImage, String logo, String textColor, String primaryButtonColor,
            String menuColor, String profileColor, String backgroundColor) {
        this.backgroundImage = backgroundImage;
        this.logo = logo;
        this.textColor = textColor;
        this.primaryButtonColor = primaryButtonColor;
        this.menuColor = menuColor;
        this.profileColor = profileColor;
        this.backgroundColor= backgroundColor;

    }
    private String backgroundImage, logo, textColor, primaryButtonColor, menuColor, profileColor, backgroundColor;
    private String adminEmailId, supportEmailId, currency, localCurrency, name;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SystemConfig.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
