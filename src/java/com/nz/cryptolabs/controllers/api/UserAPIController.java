/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.api;

import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.KickboxResponse;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.ToObjectConverter;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.FileUtility;
import com.nz.cryptolabs.components.HTTPRequestManager;
import com.nz.cryptolabs.components.TimeManager;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import com.nz.cryptolabs.otp.OTPManager;
import com.nz.cryptolabs.otp.OTPResponsePOJO;
import com.nz.cryptolabs.otp.OTPVerifyResponsePOJO;
import com.nz.cryptolabs.controllers.payments.ChargeRequest;
import com.nz.cryptolabs.controllers.payments.POLiIntiatedTransaction;
import com.nz.cryptolabs.controllers.payments.POLiManager;
import com.nz.cryptolabs.controllers.web.UserController;
import com.nz.cryptolabs.mail.MailManager;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.SessionManagementService;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping(value = "/rest/cryptolabs/api")
public class UserAPIController {

    @RequestMapping(value = {"/verifyEmail"}, method = RequestMethod.POST)
    @ResponseBody
    public Boolean verifyEmail(@RequestParam("email") String email,
            @RequestParam("fingerprint") String fingerprint) throws IOException, JSONException {
        String url = "https://api.kickbox.com/v2/authenticate/" + Constants.kickboxAppCode;
        HashMap<String, String> headers = new HashMap<>();
        headers.put("apikey", Constants.kickboxApiKey);
        headers.put("fingerprint", fingerprint);
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("apikey", Constants.kickboxApiKey);
        jsonBody.put("fingerprint", fingerprint);
        String body = jsonBody.toString();
        String res = httpRequestManager.sendHTTPPostRequest(url, body, headers);
        KickboxResponse response = common.jSONcast(KickboxResponse.class, res);
        url = "https://api.kickbox.com/v2/authenticate/" + Constants.kickboxAppCode + "/" + response.getId();
        res = httpRequestManager.sendHTTPGetRequest(url, headers);
        KickboxResponse response1 = common.jSONcast(KickboxResponse.class, res);
        return response1.getSuccess();
    }

    @RequestMapping(value = {"/isEmailAlreadyExist"}, method = RequestMethod.GET)
    @ResponseBody
    public String isEmailAlreadyExist(@RequestParam("e") String emailId) {
        logger.debug("isEmailAlreadyExist --- " + emailId);
        if (emailId != null) {
            String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(emailId.trim());
            if (matcher.matches() == false) {
                return "Email is Invalid.";
            }
        }
        if (repository.findByUsername(emailId.trim()) != null) {
            return "You have already registered in the platform. Please reset your password or contact us at info@invsta.com to restore access. Thanks";
        } else {
            boolean success = true;
//            boolean success = false;
//            try {
//                HashMap<String, String> params = new HashMap<>();
//                params.put("apikey", Constants.kickboxApiKey);
//                params.put("email", emailId);
//                String url = "https://api.kickbox.com/v2/verify";
//                String json = httpRequestManager.sendHTTPGetRequest(url, params);
//                ObjectMapper mapper = new ObjectMapper();
//                JsonNode readTree = mapper.readTree(json);
//                if (readTree instanceof ObjectNode) {
//                    ObjectNode objNode = (ObjectNode) readTree;
//                    success = objNode.get("success").asBoolean();
//                }
//            } catch (IOException ex) {
//                success = false;
//            }
            if (success) {
                return "Email is Valid.";
            }
            return "Email is Invalid.";
        }
    }

    @RequestMapping(value = {"/isInviteCode"}, method = RequestMethod.GET)
    @ResponseBody
    public String isInviteCode(@RequestParam("ic") String inviteCode) {
        logger.debug("isInviteCode --- " + inviteCode);
        if (inviteCode != null) {
            List<InviteCode> codes = repository.checkInviteCode(inviteCode, "REGISTRATION");
            if (codes != null & !codes.isEmpty()) {
                return "true";
            }
        }
        return "false";
    }

    @RequestMapping(value = {"/oTPGeneration", "/sdjklfdfklffksdfksdk"}, method = RequestMethod.GET)
    @ResponseBody
    public OTPResponsePOJO OTPGeneration(
            @RequestParam("mNo") String mobileNo,
            @RequestParam("cCo") String countryCode,
            @RequestParam("sTy") String senderType) throws IOException {
        if (mobileNo != null && countryCode != null && senderType != null) {
            OTPResponsePOJO OTPGeneration = oTPManager.OTPGeneration(senderType, countryCode, mobileNo);
            return OTPGeneration;
        }
        return null;
    }

    @RequestMapping(value = {"/verifyOTP", "/jdfslkfsdjlksdjlkjdflkds"}, method = RequestMethod.GET)
    @ResponseBody
    public OTPVerifyResponsePOJO verifyOTP(
            @RequestParam("mNo") String mobileNo,
            @RequestParam("otp") String otp) throws IOException {
        if (mobileNo != null && otp != null) {
            OTPVerifyResponsePOJO verifyOTP = oTPManager.verifyOTP(mobileNo, otp);
            return verifyOTP;
        }
        return null;
    }

    @RequestMapping(value = {"/checkInviteCode"}, method = RequestMethod.GET)
    @ResponseBody
    public String checkInviteCode(@RequestParam("c") String code,
            @RequestParam("t") String type) {
        List<InviteCode> list = repository.checkInviteCode(code, type);
        if (list != null && !list.isEmpty()) {
            return "Valid Invite Code";
        }
        return "Invalid Invite Code";
    }

    @RequestMapping(value = {"/marketcaps"}, method = RequestMethod.GET)
    @ResponseBody
    public String getAllMarketcap(@RequestParam(name = "api_key", required = false) String apiKey) {
        return coinMarketCapAPIService.getAllCoinMarketCap(10);
    }

    @RequestMapping(value = {"/countries"}, method = RequestMethod.GET)
    @ResponseBody
    public String countries(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "states", required = false) String states,
            @RequestParam(name = "cities", required = false) String cities) {
        List<StateCity> countries = databaseService.countries(Boolean.TRUE, Boolean.TRUE);
        return toString(countries);
    }

    @RequestMapping(value = {"/balanceChart"}, method = RequestMethod.GET)
    @ResponseBody
    public String balanceChart(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("type") String type,
            @RequestParam(name = "invId", required = false) String investmentId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null && user.getAdmin() || user == null) {
            return "{}";
        }
        Map<Date, BigDecimal> map = databaseService.latestBalanceData(type, user.getUserId(), investmentId);
        String values = new String();
        String labels = new String();
        BigDecimal totalBalance = BigDecimal.ZERO;
        Set<Map.Entry<Date, BigDecimal>> set = map.entrySet();
        int i = 0;
        for (Map.Entry<Date, BigDecimal> next : set) {
            Date date = next.getKey();
            String formattedDate = common.dateOfMonth(date);
            totalBalance = totalBalance.add(next.getValue());
            values = values + ((i > 0 && i != map.size()) ? ", " : "") + totalBalance.doubleValue();
            labels = labels + ((i > 0 && i != map.size()) ? ", " : "") + ("\"" + formattedDate + "\"");
            i += 1;
        }
        return "{"
                //                + "\"totalBalance\":" + totalBalance.doubleValue() + " ,\n"
                + "\"labels\":[" + labels + "] ,\n"
                + "\"values\":[" + values + "]\n"
                + "}";
    }

    @RequestMapping(value = {"/registeredShareAmountsByFund", "/aksadjfljdslfjds999"}, method = RequestMethod.GET)
    @ResponseBody
    public String registeredShareAmountsByFund(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "iAmt", required = false) String investmentAmount,
            @RequestParam("fId") String fundId) {
        if (investmentAmount != null && !investmentAmount.isEmpty()) {
            try {
                BigDecimal bD = new BigDecimal(investmentAmount);
                if (bD.compareTo(bD.ZERO) == 1) {
                    investmentAmount = bD.toPlainString();
                }
            } catch (Exception ex) {
                investmentAmount = "0.00";
            }
        } else {
            investmentAmount = "0.00";
        }
        List<ShareFund> shares = null;
        BigDecimal bD = new BigDecimal(investmentAmount);
        if (bD.compareTo(bD.ZERO) == 1) {
            shares = databaseService.fundActualShares(fundId, bD.toPlainString());
        } else {
            shares = repository.sharesByFund(fundId);
        }
        return toString(shares);
    }

    @RequestMapping(value = {"/ooishauiureousdofudsoufo", "/shareById"}, method = RequestMethod.GET)
    @ResponseBody
    public String shareById(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("id") String shareId) {
        ShareFund share = repository.shareById(shareId);
        return share.toString();
    }

    @RequestMapping(value = {"/akljhgtfdghjkdsfadsf", "/registeredShares"}, method = RequestMethod.GET)
    @ResponseBody
    public String registeredSharesByFund(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("fId") String fundId) {
        List<ShareFund> shares = repository.sharesByFund(fundId);
        return toString(shares);
    }

    @RequestMapping(value = {"/ksdajklfjkdsjfldsjf", "/registeredCoins"}, method = RequestMethod.GET)
    @ResponseBody
    public String registeredCoinsByFund(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("fId") String fundId) {
        List<ShareFund> shares = repository.coinsByFund(fundId);
        return toString(shares);
    }
    Map<String, String> referenceNos = new HashMap<>();

    @RequestMapping(value = {"/asd0lkjald0jfdsjsad0insight", "/initiateTransaction"}, method = RequestMethod.GET)
    @ResponseBody
    public String initiateTransaction(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("amt") String amount,
            @RequestParam(name = "fId", required = false) String fundId,
            @RequestParam("curr") String currency) throws IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user == null) {
            return "{}";
        }
        String nzdAmount = amount;
        if (!"NZD".equalsIgnoreCase(currency)) {
            BigDecimal amountbd = new BigDecimal(amount);
            BigDecimal localPrice = databaseService.currencyConversionRate(currency + "_NZD", null);
            amountbd = amountbd.multiply(localPrice);
            amountbd = amountbd.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            nzdAmount = amountbd.toPlainString();
        }
        String usdAmount = amount;
        if (!"USD".equalsIgnoreCase(currency)) {
            BigDecimal amountbd = new BigDecimal(amount);
            BigDecimal localPrice = databaseService.currencyConversionRate(currency + "_USD", null);
            amountbd = amountbd.multiply(localPrice);
            amountbd = amountbd.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
            usdAmount = amountbd.toPlainString();
        }
        fundId = fundId != null ? fundId : "0";
        String referenceNo = user.getRefId().concat(String.valueOf(System.currentTimeMillis())).concat(fundId).concat(user.getUserId()).replaceAll("-", "");
        POLiIntiatedTransaction txn = pOLiManager.initiateTransaction(nzdAmount, ChargeRequest.Currency.NZD, referenceNo, fundId);
        if (txn != null) {
            String txnNavURL = java.net.URLDecoder.decode(txn.getNavigateURL(), "UTF-8");
            String token = txnNavURL.substring(txnNavURL.lastIndexOf("=") + 1);
            txn.setNavigateURL(txn.getNavigateURL().concat("&ref=").concat(referenceNo));
            UserReference ref = new UserReference();
            ref.setFundId(fundId);
            ref.setUserId(user.getUserId());
            ref.setPrice(nzdAmount);
            ref.setReferenceNo(referenceNo);
            ref.setPoliToken(token);
            repository.addReferenceNo(ref);
            return "{"
                    + "\"transaction\":" + (txn.toString()) + ", "
                    + "\"localAmount\":" + (nzdAmount) + ", "
                    + "\"usdAmount\":" + (usdAmount) + " "
                    + "}";
        }

        return null;
    }

    @RequestMapping(value = {"/sdkjfhkdsjlfsder", "/purchasedInvestments"}, method = RequestMethod.GET)
    @ResponseBody
    public String purchasedInvestments(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "uId") String userId,
            @RequestParam(value = "iId", required = false) String investmentId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user == null) {
            return "{}";
        }
        String currency = "USD";
        currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : "USD";
        List<InvestmentBean> userInvestmentsAndCash = repository.purchasedInvestmentsAndCash(userId, null, null);
        if (!userInvestmentsAndCash.isEmpty()) {
            for (InvestmentBean inv : userInvestmentsAndCash) {
                BigDecimal invAmount = new BigDecimal(inv.getInvestmentAmount());
                if (!currency.equalsIgnoreCase("USD")) {
                    try {
                        BigDecimal localPrice = databaseService.currencyConversionRate("USD_" + currency, null);
                        invAmount = invAmount.multiply(localPrice);
                        inv.setInvestmentAmount(invAmount.toPlainString());
                    } catch (IOException ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return toString(userInvestmentsAndCash);
    }

    @RequestMapping(value = {"/fjxzdjfhxckhfkldshffhfkhds", "/pendingShares"}, method = RequestMethod.GET)
    @ResponseBody
    public String pendingSharesByInvestmentRequest(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "rId", required = false) String reqId,
            @RequestParam(value = "mId", required = false) String investmentId,
            @RequestParam(value = "uId", required = false) String userId,
            @RequestParam(value = "fId", required = false) String fundId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user == null) {
            return "{}";
        }
        List<ShareFund> details = databaseService.investmentActualDetails(investmentId, reqId);
        List<ShareFund> fundPrices = null;
        BigDecimal unitPrice = null, USD_NZD = null;
        try {
            fundPrices = repository.fundPrices(fundId, investmentId);
            if (fundPrices != null && !fundPrices.isEmpty()) {
                fundPrices.forEach(new Consumer<ShareFund>() {
                    @Override
                    public void accept(ShareFund fundPrice) {
                        Date createdDate = common.parseDate(fundPrice.getCreatedDate(), CommonMethods.format2);
                        fundPrice.setCreatedDate(common.dateFormat(createdDate, CommonMethods.format3));
                    }
                });
                ShareFund price = fundPrices.get(fundPrices.size() - 1);
                unitPrice = new BigDecimal(price.getPrice());
                USD_NZD = databaseService.currencyConversionRate("USD_NZD", price.getCreatedDate());
            }
        } catch (Exception ex) {
            unitPrice = new BigDecimal("1");
        }
        return "{"
                + "\"details\":" + toString(details) + " ,\n"
                + "\"fundPrices\":" + toString(fundPrices) + ", \n"
                + "\"USD_NZD\":" + ToObjectConverter.checkNull(USD_NZD) + ", \n"
                + "\"unitPrice\":" + unitPrice.toPlainString() + "\n"
                + "}";
    }

    @RequestMapping(value = {"/jsdlfjsldjfwer324rrrre", "/purchasedShares"}, method = RequestMethod.GET)
    @ResponseBody
    public String purchasedSharesByInvestment(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "rId", required = false) String reqId,
            @RequestParam(value = "mId", required = false) String investmentId,
            @RequestParam(value = "uId", required = false) String userId,
            @RequestParam(value = "amt", required = false) String amount,
            @RequestParam(value = "per", required = false) String percentage) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user == null) {
            return "{}";
        }
        InvestmentBean inv = databaseService.purchasedInvestments(userId, investmentId, null, null).get(0);
        List<ShareFund> details = inv.getFundActualShares();
        BigDecimal percentageBD = new BigDecimal(percentage);
        BigDecimal BD = percentageBD.divide(Constants.HUNDRED, 2, RoundingMode.HALF_UP);
        for (ShareFund share : details) {
            BigDecimal sharePercentage = new BigDecimal(share.getPercentage());
            BigDecimal newSharePercentage = BD.multiply(sharePercentage).setScale(2, RoundingMode.HALF_UP);
            share.setPercentage(newSharePercentage.toPlainString());
        }
        List<ShareFund> fundPrices = null;
        BigDecimal unitPrice = null, USD_NZD = null;
        try {
            fundPrices = repository.fundPrices(null, investmentId);
            if (fundPrices != null && !fundPrices.isEmpty()) {
                fundPrices.forEach(new Consumer<ShareFund>() {
                    @Override
                    public void accept(ShareFund fundPrice) {
                        Date createdDate = common.parseDate(fundPrice.getCreatedDate(), CommonMethods.format2);
                        fundPrice.setCreatedDate(common.dateFormat(createdDate, CommonMethods.format3));
                    }
                });
                ShareFund fundPrice = fundPrices.get(fundPrices.size() - 1);
                unitPrice = new BigDecimal(fundPrice.getPrice());
                Date createdDate = common.parseDate(fundPrice.getCreatedDate(), CommonMethods.format3);
                String pdate = common.dateFormat(createdDate, CommonMethods.format2);
                USD_NZD = databaseService.currencyConversionRate("USD_NZD", pdate);
            }
        } catch (Exception ex) {
            unitPrice = new BigDecimal("1");
        }
        return "{"
                + "\"details\":" + toString(details) + " ,\n"
                + "\"fundPrices\":" + toString(fundPrices) + ", \n"
                + "\"USD_NZD\":" + USD_NZD + ", \n"
                + "\"unitPrice\":" + unitPrice.toPlainString() + "\n"
                + "}";
    }

    @RequestMapping(value = {"/getShares"}, method = RequestMethod.GET)
    @ResponseBody
    public String getSharesByAmount(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("ok") String investmentId,
            @RequestParam(value = "un", required = false) String userId,
            @RequestParam(value = "amt") String amount
    ) {
        List<ShareFund> details = databaseService.actualSharesByAmount(investmentId, userId, amount);
        List<ShareFund> fundPrices = null;
        BigDecimal unitPrice = null, USD_NZD = null;
        try {
            fundPrices = repository.fundPrices(userId, investmentId);
            if (fundPrices != null && !fundPrices.isEmpty()) {
                fundPrices.forEach(new Consumer<ShareFund>() {
                    @Override
                    public void accept(ShareFund fundPrice) {
                        Date createdDate = common.parseDate(fundPrice.getCreatedDate(), CommonMethods.format2);
                        fundPrice.setCreatedDate(common.dateFormat(createdDate, CommonMethods.format3));
                    }
                });
                ShareFund price = fundPrices.get(fundPrices.size() - 1);
                unitPrice = new BigDecimal(price.getPrice());
                USD_NZD = databaseService.currencyConversionRate("USD_NZD", price.getCreatedDate());
            }
        } catch (Exception ex) {
            unitPrice = new BigDecimal("1");
        }
        return "{"
                + "\"details\":" + toString(details) + " ,\n"
                + "\"fundPrices\":" + toString(fundPrices) + ", \n"
                + "\"USD_NZD\":" + ToObjectConverter.checkNull(USD_NZD) + ", \n"
                + "\"unitPrice\":" + unitPrice.toPlainString() + "\n"
                + "}";
    }

    @RequestMapping(value = {"/coinsByInvestments", "/qwergnuyhftiyhkgyi"}, method = RequestMethod.GET)
    @ResponseBody
    public String coinsByInvestments(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username
    ) {
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<InvestmentBean> invs = databaseService.coinsByInvestments(userInfo.getUserId(), null, true, null);
            return toString(invs);
        }
        return null;
    }

    @RequestMapping(value = {"/purchasedShares4Edit", "/sdadfjlsdweroweiuxmcbncmxcmc"}, method = RequestMethod.GET)
    @ResponseBody
    public String purchasedShares4Edit(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "uId") String userId,
            @RequestParam(value = "mId") String investmentId,
            @RequestParam(value = "action", required = false) String action
    ) {
        List<ShareFund> shares = databaseService.detailsByInvestment(userId, investmentId, action);
        return toString(shares);
    }

    @RequestMapping(value = {"/tran-req-cancel", "/met324ijlksdlkjfsdjfjdlfd120"}, method = RequestMethod.POST)
    public void withdrawlRequestCancel(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "ok") String tokenNo,
            @RequestParam(name = "bs") String base,
            @RequestParam(name = "act", required = false) String action
    ) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null && tokenNo != null) {
            String status = "CANCEL";
            switch (base) {
                case "NWR":
                    RequestBean txn = repository.userTransactionRequests(user.getUserId(), "PENDING", null, tokenNo).get(0);
                    repository.transactionRequestStatus(txn.getId(), status, txn.getMaster());
                    break;
                case "NIR":
                    if ("WITHDRAWL".equalsIgnoreCase(action)) {
                        InvestmentBean sellTxn = repository.pendingSellTransactions(user.getUserId(), null, tokenNo).get(0);
                        repository.investmentRequestStatus(sellTxn.getReqId(), status);
                    } else if ("DEPOSITE".equalsIgnoreCase(action)) {
                        InvestmentBean invTxn = repository.pendingInvestmentRequests(user.getUserId(), null, tokenNo).get(0);
                        repository.investmentRequestStatus(invTxn.getReqId(), status);
                        repository.deactivePendingSharesStatus(invTxn.getUserId(), invTxn.getInvestmentId(), "PENDING");
                    }
                    break;
            }

        }
    }

    @RequestMapping(value = {"/ok-transfer"}, method = RequestMethod.POST)
    public void okTransfer(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un") String userId,
            @RequestParam(value = "ok") String recordId
    ) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null && user.getUserId().equalsIgnoreCase(userId)) {
            repository.transactionRequestStatus(recordId, "PENDING", "ADDFUNDS");
            List<RequestBean> list = repository.userTransactionRequests(user.getUserId(), "PENDING", recordId, null);
            if (!list.isEmpty()) {
                RequestBean request = list.get(0);
                if (user.getUser()) {
                    mailManager.sendAddFundsNotificationToUser(user.getEmail(), user.getFullName(), request);
                }
                mailManager.sendAddFundsNotificationToAdmin(adminEmailId, request);
            }
        }
    }

    @RequestMapping(value = {"/cancel-transfer"}, method = RequestMethod.POST)
    public void cancelTransfer(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un") String userId,
            @RequestParam(value = "ok") String recordId
    ) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null && user.getUserId().equalsIgnoreCase(userId)) {
            repository.transactionRequestStatus(recordId, "CANCEL", "ADDFUNDS");
        }
    }

    public String toString(List list) {
        if (list == null) {
            return null;
        }
        String objects = "";
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);
            objects = objects + ((i > 0 && i != list.size()) ? "," : "") + obj.toString();
        }
        return "[" + objects + "]";

    }

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private OTPManager oTPManager;
    @Autowired
    private POLiManager pOLiManager;
    @Autowired
    private HTTPRequestManager httpRequestManager;
    @Autowired
    private CommonMethods common;
    @Autowired
    private MailManager mailManager;
    @Autowired
    private String adminEmailId;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserAPIController.class);

}
