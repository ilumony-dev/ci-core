/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

/**
 *
 * @author palo12
 */
//@Data
public class ChargeRequest {

    /**
     * @return the localInvestmentAmount
     */
    public String getLocalInvestmentAmount() {
        return localInvestmentAmount;
    }

    /**
     * @param localInvestmentAmount the localInvestmentAmount to set
     */
    public void setLocalInvestmentAmount(String localInvestmentAmount) {
        this.localInvestmentAmount = localInvestmentAmount;
    }

    /**
     * @return the investmentAmount
     */
    public String getInvestmentAmount() {
        return investmentAmount;
    }

    /**
     * @param investmentAmount the investmentAmount to set
     */
    public void setInvestmentAmount(String investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    /**
     * @return the fundId
     */
    public String getFundId() {
        return fundId;
    }

    /**
     * @param fundId the fundId to set
     */
    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the stripeEmail
     */
    public String getStripeEmail() {
        return stripeEmail;
    }

    /**
     * @param stripeEmail the stripeEmail to set
     */
    public void setStripeEmail(String stripeEmail) {
        this.stripeEmail = stripeEmail;
    }

    /**
     * @return the stripeToken
     */
    public String getStripeToken() {
        return stripeToken;
    }

    /**
     * @param stripeToken the stripeToken to set
     */
    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    public enum Currency {
        USD, AED, AFN, ALL, AMD, ANG, AOA, ARS, AUD, AWG, AZN, BAM, BBD, BDT, BGN,
        BIF, BMD, BND, BOB, BRL, BSD, BWP, BZD, CAD, CDF, CHF, CLP, CNY, COP, CRC,
        CVE, CZK, DJF, DKK, DOP, DZD, EGP, ETB, EUR, FJD, FKP, GBP, GEL, GIP, GMD,
        GNF, GTQ, GYD, HKD, HNL, HRK, HTG, HUF, IDR, ILS, INR, ISK, JMD, JPY, KES,
        KGS, KHR, KMF, KRW, KYD, KZT, LAK, LBP, LKR, LRD, LSL, MAD, MDL, MGA, MKD,
        MMK, MNT, MOP, MRO, MUR, MVR, MWK, MXN, MYR, MZN, NAD, NGN, NIO, NOK, NPR,
        NZD, PAB, PEN, PGK, PHP, PKR, PLN, PYG, QAR, RON, RSD, RUB, RWF, SAR, SBD,
        SCR, SEK, SGD, SHP, SLL, SOS, SRD, STD, SVC, SZL, THB, TJS, TOP, TRY, TTD,
        TWD, TZS, UAH, UGX, UYU, UZS, VND, VUV, WST, XAF, XCD, XOF, XPF, YER, ZAR,
        ZMW, EEK, LVL, VEF;
    }

    private String description;
    private int amount;
    private Currency currency;
    private String stripeEmail;
    private String stripeToken;
    private String fundId;
    private String localInvestmentAmount;
    private String investmentAmount;
}
