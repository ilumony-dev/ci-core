<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Verification</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Done</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <c:forEach items="${verificationList}" var="ver">
                                    <div class="container_contact">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p><span class="heading_ver">Created Date:</span>${ver.created_ts}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p><span class="heading_ver">Approved Date:</span>${ver.approved_ts}</p>
                                            </div>
                                            <div class="col-md-12">
                                                <h5>Personal Detail</h5>
                                            </div>
                                            <div class="col-md-4">
                                                <p><span class="heading_ver">Name:</span>${ver.full_name}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <p><span class="heading_ver">DOB:</span>${ver.dob}</p>
                                            </div>
                                            <div class="col-md-5">
                                                <p><span class="heading_ver">Address:</span>${ver.address}</p>
                                            </div>
                                        </div>
                                        <c:if test="${!ver.pp_passport_number.isEmpty()}">
                                            <div class="row">
                                                <c:choose>
                                                    <c:when test="${ver.is_nz == 'Y'}">
                                                        <div class="col-md-12">
                                                            <h5>Passport - New Zealand</h5>
                                                        </div>                                                    
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="col-md-12">
                                                            <h5>Passport - Rest of World</h5>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">Country:</span>${ver.pp_issue_country}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">Passport:</span>${ver.pp_passport_number}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">Expiry Date:</span>${ver.pp_passport_expiry_date}</p>
                                                </div>                                                
                                            </div>        
                                        </c:if>

                                        <c:if test="${!ver.dl_license_number.isEmpty()}">
                                            <div class="row">
                                                <c:choose>
                                                    <c:when test="${ver.is_nz == 'Y'}">
                                                        <div class="col-md-12">
                                                            <h5>Driving License - New Zealand</h5>
                                                        </div>                                                    
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div class="col-md-12">
                                                            <h5>Driving License - Rest of World</h5>                                                    </div>
                                                        </c:otherwise>
                                                    </c:choose>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">Country:</span>${ver.pp_issue_country}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">License No:</span>${ver.dl_license_number}(${ver.dl_license_version})</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p> <span class="heading_ver">Expiry Date:</span>${ver.dl_expiry_date}</p>
                                                </div>                                                
                                            </div>                                                    
                                        </c:if>
                                    </div>
                                </c:forEach>
                                <!--                                <div class="container_contact">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5>Personal Detail</h5>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <p><span class="heading_ver">Name:</span>Vikash Kumar</p>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <p><span class="heading_ver">DOB:</span>12/1/2015</p>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p><span class="heading_ver">Address:</span></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5>New Zealand Passport</h5>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p> <span class="heading_ver">Passport:</span>12212121</p>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p> <span class="heading_ver">Expiry Date:</span>121211212</p>
                                                                        </div>                                                
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5>New Zealand Driving License</h5>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">License No:</span>12212121</p>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">Version No:</span>121211212</p>
                                                                        </div>                                                
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">Expiry Date:</span>121211212</p>
                                                                        </div>                                                
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5>Passport - Rest of World</h5>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p><span class="heading_ver">Passport:</span>12212121</p>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p><span class="heading_ver">Expiry Date:</span>121211212</p>
                                                                        </div>                                                
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h5>Driving License - Rest of World</h5>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">License No:</span>12212121</p>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">Version No:</span>121211212</p>
                                                                        </div>                                                
                                                                        <div class="col-md-4">
                                                                            <p> <span class="heading_ver">Expiry Date:</span>121211212</p>
                                                                        </div>                                                
                                                                    </div>
                                                                    <fieldset>
                                                                        <div class="veri_btn">
                                                                            <button name="submit" type="submit" >Approved</button>
                                                                            <button name="submit" type="submit" class="cancel_btn">Cancel</button>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>-->
                            </div>
                        </div>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/addcurrency.jsp" />
        <jsp:include page = "modals/update-conversion-pairs.jsp" />

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
        var shareOptions = '';
            <c:forEach items="${shares}" var="share" varStatus="loop">
        shareOptions = shareOptions + '<option value="${share.shareId}">${share.name}</option>';
            </c:forEach>
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
    </body>
</html>
