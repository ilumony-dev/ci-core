<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Template" name="author">
        <meta content="User dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${endUser.fullName} Profile</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${currency}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>

                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">${title}</h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>                      
                                                            <th style="width:110px">
                                                                Date 
                                                            </th>
                                                            <th  style="width:110px" class="text-right">
                                                                Total Users
                                                            </th>
                                                            <th class="text-right">
                                                                Total Invested Value
                                                            </th>
                                                            <th class="text-right">
                                                                ${portfolio.name} Units
                                                            </th>
                                                            <th class="text-right">
                                                                Total Investment Value
                                                            </th>
                                                            <th class="text-right">
                                                                Net Asset Value
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>                      
                                                            <td class="date">
                                                                Date
                                                            </td>
                                                            <td class="user_count text-right">
                                                                Total Users                         
                                                            </td>
                                                            <td class="invested_usd  text-right" >
                                                                Total Invested US$
                                                            </td>
                                                            <td class="total_units  text-right" >
                                                                Total Units
                                                            </td>
                                                            <td class="text-right" >
                                                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:white;">
                                                                    <span class="total_usd" style="display:inline-block; margin-right:10px"></span><i class="os-icon os-icon-pencil-2" style="font-size:20px"></i>
                                                                </a>
                                                            </td>
                                                            <td class="pull-right" >
                                                                <a href="#" data-toggle="modal" data-target="#myModal" style="color:white;">
                                                                    <span class="nav" style="display:inline-block; margin-right: 10px"></span><i class="os-icon os-icon-pencil-2" style="font-size:20px;"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!--<div class="modal fade" id="myModal">-->
                                            <div class="modal fade" id="myModal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">${portfolio.name}</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <div class="modal-body">
                                                            <form method="post" action="./admin-update-fund-price">
                                                                <div class="row">
                                                                    <input type="hidden" name="fund_id" value="${portfolio.fundId}"/>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Date</label>
                                                                        <input type="date" class="form-control date" name="date">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Users</label>
                                                                        <input type="text" class="form-control text-right user_count" name="user_count" readonly="true">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Units</label>
                                                                        <input type="text" class="form-control text-right total_units" id="total_units" name="total_units" readonly="true">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label class="label">Total Value</label>
                                                                        <input type="text" class="form-control text-right total_usd" id="total_usd" name="total_usd">
                                                                    </div>
                                                                    <div  class="col-md-6">
                                                                        <label class="label">N.A.V.</label>
                                                                        <input type="text" class="form-control text-right nav" id="nav" name="nav">
                                                                    </div>
                                                                    <div class="col-md-6 pull-right" style="margin-top: 26px">
                                                                        <input type="submit" class="form-control btn btn-primary" value="Submit"/>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!--<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="linechart1"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart2"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="linechart3"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart4"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="piechart1"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="piechart2"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="linechart5"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart6"></div>
                                </div>
                            </div>


                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <%--<jsp:include page = "user-right-sidebar.jsp" />--%>
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>

                </div>
            </div>
            <div class="display-type"></div>
        </div>


        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script>
        $(document).ready(function () {
            $("#total_usd").keyup(function () {
                var units = $("#total_units").val();
                var usd = $("#total_usd").val();
                $("#nav").val(usd / units);
            });
            $("#nav").keyup(function () {
                var units = $("#total_units").val();
                var nav = $("#nav").val();
                $("#total_usd").val(units * nav);
            });

        });
        </script>
        <script>
            var report = ${report};
            var dates = new Array();
            var user_counts = new Array();
            var total_units = new Array();
            var total_usds = new Array();
            var navs = new Array();
            $.each(report, function (i, bean) {
                dates.push(bean.date);
                $(".date").text(bean.date);
                $(".date").val(bean.date);

                var user_count = eval(bean.user_count);
                user_counts.push(user_count);
//                alert(user_count);
                $(".user_count").text(user_count);
                $(".user_count").val(user_count);

                var total_invsta_units = eval(bean.total_units);
                total_units.push(total_invsta_units);
//                alert(total_invsta_units);
                $(".total_units").text(total_invsta_units);
                $(".total_units").val(total_invsta_units);

                var nav = eval(bean.nav);
                navs.push(nav);
//                alert(nav);
                $(".nav").text(nav);
                $(".nav").val(nav);

                var total_usd = eval(total_invsta_units * nav);
                total_usds.push(total_usd);
//                alert(total_usd);
                $(".total_usd").text(total_usd);
                $(".total_usd").val(total_usd);
                $(".invested_usd").text(bean.invested_usd);
            });

            var linecharts = [
                {title: 'Total Users',
                    dates: dates,
                    series: [{
                            name: 'Total Users',
                            data: user_counts
                        }]
                },
                {title: 'Total Units',
                    dates: dates,
                    series: [{
                            name: 'Total Units',
                            data: total_units
                        }]
                },
                {title: 'Net Asset Value', dates: dates,
                    series: [{
                            name: 'Unit NAV value Daily',
                            data: navs
                        }]
                },
                {title: 'Portfolio Investment Value',
                    dates: dates,
                    series: [{
                            name: 'Daily NAV * Units',
                            data: total_usds
                        }]
                },
                {title: 'Coming soon:- Daily NAV Unit Value * 10000',
                    dates: dates,
                    series: [{
                            name: 'Daily NAV Unit Value * 10000',
                            data: [43934]
                        }]
                },
                {title: 'Coming soon:- Performance Metrics of Portfolio',
                    dates: dates,
                    series: [{
                            name: 'Total Portfolio Holding',
                            data: [43934]
                        }]
                }];

            for (var i = 0; i < linecharts.length; i++) {
                var obj = linecharts[i];
                lineChart(i + 1, obj);
            }
            function lineChart(i, obj) {
                Highcharts.chart('linechart' + i, {
                    title: {
                        text: obj.title
                    },
                    subtitle: {
                        text: obj.title
                    },
                    xAxis: {
                        categories: obj.dates,
                        labels: {
                            style: {
                                color: '#1D3153'
                            }
                        },
                        tickmarkPlacement: 'on',
                        title: {
                            enabled: false
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    yAxis: {
                        title: {
                            enabled: false
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            }
                        }
                    },
                    series: obj.series,
                    responsive: {
                        rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                    }
                });
            }

            var units = ${userUnits};
            var shares = ${userShares};
            var indi_units = new Array();
            var share_amounts = new Array();
            $.each(units, function (idx, unit) {
                indi_units.push({
                    name: unit.full_name,
                    y: eval(unit.total_units)
                });
            });
            $.each(shares, function (idx, shr) {
                share_amounts.push({
                    name: shr.share_id,
                    y: eval(shr.total_usd)
                });
            });
            var piecharts = [
                {title: 'Portfolio Holding By User',
                    data: indi_units},
                {title: 'Total fund holding breakdown',
                    data: share_amounts}];

            for (var j = 0; j < piecharts.length; j++) {
                var obj = piecharts[j];
                pieChart(j + 1, obj);
            }
            function pieChart(j, obj) {
                Highcharts.chart('piechart' + j, {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: obj.title,
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 40
                    },
                    tooltip: {
                        pointFormat: '%age: <b>{point.percentage:.2f}%</b><br>'
                                + 'value: <b>{point.y:.2f}</b>'
//                    pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b><br>'
//                            + '<b>{point.y:.2f}</b>'
                    },
                    credits: {
                        enabled: false
                    },
                    exporting: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            startAngle: -90,
                            endAngle: 270,
                            center: ['50%', '50%']
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: obj.title,
                            innerSize: '80%',
                            data: obj.data
                        }]
                });


            }
        </script>
        <script>
            function mobileMenu() {
                var x = document.getElementsByClassName("menu-and-user")[0];
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
        </script>
    </body>
</html>