<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>New User Accounts - Invsta</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <!--<link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">-->
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>New User Accounts</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header" style="display: block">
                                            New Users <img src="./resources/img/info-icon.png" style="object-fit: contain">
                                            <div class="pdf-btn">
                                                <button class="btn btn-primary" onclick="generatePdf('pdf-contents', 'New Users', false)" value="">Generate PDF</button>
                                            </div>
                                            <div class="date-range-picker">
                                                <form class="form-inline">
                                                    <div class="form-group">
                                                        <label style="margin-right:5px">Pick Date Range:</label>
                                                        <input type="text" class="form-control" name="daterange" value="${daterange}"/>
                                                        <!--                                                        <img src="./resources/img/info-icon.png" style="vertical-align:text-top">-->
                                                    </div>
                                                </form>
                                            </div>
                                        </h6>                                        
                                        <div class="element-box-tp">
                                            <div class="table-responsive" id="pdf-contents">
                                                <table id="unit-summary-table" class="table table-bordered">
                                                    <thead id="unit-summary-head">
                                                        <tr>
                                                            <th>
                                                                CUSTOMER ID/
                                                                NAME
                                                            </th>
                                                            <th>
                                                                D.O.B
                                                            </th>
                                                            <th>
                                                                Username
                                                            </th>
                                                            <th>
                                                                Mobile No.
                                                            </th>
                                                            <th>
                                                                Last Login
                                                            </th>
                                                            <th>
                                                                Created Time
                                                            </th>
                                                            <th>
                                                                Actions
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="unit-summary-body">
                                                        <c:forEach items="${totalUsers}" var="user">
                                                            <tr>
                                                                <td class="text-left">
                                                                    ${user.refId} /<br>
                                                                    ${user.fullName}
                                                                </td>
                                                                <td class="text-left">
                                                                    ${user.dob}
                                                                </td>
                                                                <td class="text-left">
                                                                    ${user.email} 
                                                                </td>
                                                                <td class="text-left">
                                                                    ${user.mobileNo}
                                                                </td>
                                                                <td class="text-left">
                                                                    ${user.dateTime}
                                                                </td>
                                                                <td class="text-left">
                                                                    ${user.createdTs}
                                                                </td>
                                                                <td  class="text-left row-actions">
                                                                    <div style="display: flex">
                                                                    <a href="./user-dashboard?un=${user.userId}" title="User Dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i></a>
                                                                    <a href="#"  data-target="#user-account-summary" data-toggle="modal" title="User Financial Ledger" onclick="onSummaryClick('${user.email}')"><i class="os-icon os-icon-newspaper"></i></a>
                                                                    <a href="#"  data-target="#user-units-summary" data-toggle="modal" title="Minto Units Summary" onclick="onUnitClick('${user.email}')"><i class="os-icon os-icon-grid-squares-22"></i></a>
                                                                    <a href="#"  data-target="#user-daily-updates" data-toggle="modal" title="Individual User Charts" onclick="onUpdateClick('${user.email}')"><i class="os-icon os-icon-ui-51"></i></a>
                                                                    <!--<a href="#"><i class="os-icon os-icon-pencil-2"></i></a>-->
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                    <tfoot id="unit-summary-foot">
                                                        <tr class="titles">
                                                            <th>
                                                                CUSTOMER ID/
                                                                NAME
                                                            </th>
                                                            <th>
                                                                D.O.B
                                                            </th>
                                                            <th>
                                                                Username
                                                            </th>
                                                            <th>
                                                                Mobile No.
                                                            </th>
                                                            <th>
                                                                Last Login
                                                            </th>
                                                            <th>
                                                                Created Time
                                                            </th>
                                                            <th>
                                                                Actions
                                                            </th>
                                                        </tr> 

                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="controls-below-table" id="demo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>                
        <%--<jsp:include page = "modals/update-investment-shares.jsp" />--%>
        <jsp:include page = "modals/user-account-summary.jsp" ></jsp:include>
        <jsp:include page = "modals/user-units-summary.jsp" ></jsp:include>
        <jsp:include page = "modals/user-daily-updates.jsp" ></jsp:include>

            <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <!--<script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>-->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
<!--        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>-->
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/fire-events.js"></script>
        <script src="${home}/resources/js/cryptolabs/generate-pdf.js"></script>
        <script>
                                                                        $(document).ready(function () {
                                                                            $('input[name="daterange"]').daterangepicker({
                                                                                opens: 'left',
                                                                                locale: {
                                                                                    format: 'YYYY-MM-DD'
                                                                                }
                                                                            }, function (start, end, label) {
                                                                                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                                                                            });
                                                                            $('.applyBtn').click(function () {
                                                                                var daterange = $('.drp-selected').text();
                                                                                location.href = '${home}/admin-show-new-users?daterange=' + daterange;
                                                                            });
//                                                                            var table1 = $('#unit-summary-table').DataTable({
//                                                                                scrollY: "450px",
////                                                                                scrollX: true,
//                                                                                scrollCollapse: true,
//                                                                                paging: false,
//                                                                                fixedColumns: true,
//                                                                                drawCallback: function () {
//                                                                                    var api = this.api();
//                                                                                }
//                                                                            });
//                                                                            $(table1.table().container()).on('keyup', 'tfoot input', function () {
//                                                                                table1.column($(this).data('index')).search(this.value).draw();
//                                                                            });
                                                                        });
        </script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="${home}/resources/js/cryptolabs/admin-charts.js"></script>
    </body>
</html>
