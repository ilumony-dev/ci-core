/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.modelmap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nz.cryptolabs.beans.SecuredUser;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import static com.nz.cryptolabs.constants.Constants.adminMap;
import static com.nz.cryptolabs.constants.ResponseConstants.urlMap;
import com.nz.cryptolabs.services.SessionManagementService;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping(value = "/rest/cryptolabs/modelmap/admin")
public class AdminModelMapController {

    @RequestMapping(value = {"/{key}"}, method = RequestMethod.GET)
    @ResponseBody
    public String map(
            @PathVariable("key") String key,
            @RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "id", required = false) String userId,
            @RequestParam(name = "ok", required = false) String investmentId,
            @RequestParam(name = "dr", required = false) String daterange,
            @RequestParam(name = "p", required = false) String portfolioId,
            @RequestParam(name = "rt", required = false) String reportType,
            ModelMap modelMap
    ) throws JsonProcessingException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        HashMap<String, Object> adminMap = null;
        if (admin == null) {
            return "{}";
        } else if (key.equals(urlMap().get("admin-dashboard"))) {
            adminMap = adminMap(key);
        }
        if (adminMap == null) {
            return "{}";
        }
        String gsonString = new ObjectMapper().writeValueAsString(adminMap);
        System.out.println(gsonString);
        return gsonString;
    }

    @Autowired
    private SessionManagementService sessionManagementService;

}
