<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <!--<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>-->
        <script>
            function mobileMenu() {
                var element = document.getElementById("hamburger-1");
                element.classList.toggle("is-active");
                var x = document.getElementsByClassName("menu-and-user")[0];
                if (x.style.display === "block") {
                    x.style.display = "none";
                } else {
                    x.style.display = "none";
                    x.style.display = "block";
                }
            }
        </script>
        <script>
            !function () {
                var analytics = window.analytics = window.analytics || [];
                if (!analytics.initialize)
                    if (analytics.invoked)
                        window.console && console.error && console.error("Segment snippet included twice.");
                    else {
                        analytics.invoked = !0;
                        analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"];
                        analytics.factory = function (t) {
                            return function () {
                                var e = Array.prototype.slice.call(arguments);
                                e.unshift(t);
                                analytics.push(e);
                                return analytics
                            }
                        };
                        for (var t = 0; t < analytics.methods.length; t++) {
                            var e = analytics.methods[t];
                            analytics[e] = analytics.factory(e)
                        }
                        analytics.load = function (t, e) {
                            var n = document.createElement("script");
                            n.type = "text/javascript";
                            n.async = !0;
                            n.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                            var o = document.getElementsByTagName("script")[0];
                            o.parentNode.insertBefore(n, o);
                            analytics._loadOptions = e
                        };
                        analytics.SNIPPET_VERSION = "4.1.0";
                        analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
                        analytics.page();
                    }
            }();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {hjid: 846291, hjsv: 6};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
                app_id: "q28x66d9"
            };
        </script>

        <script>
            (function () {
                var w = window;
                varic = w.Intercom;
                if (typeof ic === "function") {
                    ic('reattach_activator');
                    ic('update', intercomSettings);
                } else {
                    var d = document;
                    var i = function () {
                        i.c(arguments)
                    };
                    i.q = [];
                    i.c = function (args) {
                        i.q.push(args)
                    };
                    w.Intercom = i;
                    function l() {
                        var s = d.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = 'https://widget.intercom.io/widget/q28x66d9';
                        var x = d.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s, x);
                    }
                    if (w.attachEvent) {
                        w.attachEvent('onload', l);
                    } else {
                        w.addEventListener('load', l, false);
                    }
                }
            })()
        </script>

        <script>
            function changeCurrency() {
                var curr = $("#currency").val();
                $("#localCurrency").val(curr);
                $("#showCurrency").val(curr);
            }
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>System Settings</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="element-box">
                                <form action="./system-config" method="post" class="form-login"  enctype="multipart/form-data">
<%--                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">System Settings</h6>
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="adminEmailId">Please Enter Admin Email Id</label>
                                                <input type="text" name="adminEmailId" id="adminEmailId" value="">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="supportEmailId">Please Enter Support Email Id</label>
                                                <input type="text" name="supportEmailId" id="supportEmailId" value="">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="name">Please Enter Company name</label>
                                                <input type="text" name="name" id="name" value="">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="localCurrency">Please enter local currency</label>
                                                <select class="form-control form-width" name="localCurrency" id="localCurrency" value="" style="width:30%;float:left">
                                                    <option value="USD">United States Dollar (US$)</option>
                                                    <option value="NZD">New Zealand Dollar (NZ$)</option>
                                                    <option value="SGD">Singapore Dollar ($)</option>
                                                    <option value="AUD">Australian Dollar (AU$)</option>
                                                    <option value="GBP">Great Britain Pound (GB�)</option>
                                                    <option value="HKD">Hong Kong Dollar (HK$)</option>
                                                    <option value="INR">Indian National Rupee (INR)</option>
                                                </select>
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="currency">Please enter currency</label>
                                                <select class="form-control form-width" name="currency" id="currency" value="" style="width:30%;float:left">
                                                    <option value="USD">United States Dollar (US$)</option>
                                                    <option value="NZD">New Zealand Dollar (NZ$)</option>
                                                    <option value="SGD">Singapore Dollar ($)</option>
                                                    <option value="AUD">Australian Dollar (AU$)</option>
                                                    <option value="GBP">Great Britain Pound (GB�)</option>
                                                    <option value="HKD">Hong Kong Dollar (HK$)</option>
                                                    <option value="INR">Indian National Rupee (INR)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Image Settings</h6>
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please upload logo</label>
                                                <input type="file" name="logo1" id="logo" value="">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose a background image</label>
                                                <input type="file" name="backgroundImage1" id="backgroundImage" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="element-wrapper">
                                                <h6 class="element-header">Color Settings</h6>
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose background color.</label>
                                                <input type="color" name="backgroundColor" id="backgroundColor" value="${systemConfig.backgroundColor}">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose profile side bar color.</label>
                                                <input type="color" name="profileColor" id="profileColor" value="${systemConfig.profileColor}">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose menu side bar color.</label>
                                                <input type="color" name="menuColor" id="menuColor" value="${systemConfig.menuColor}">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose primary button color.</label>
                                                <input type="color" name="primaryButtonColor" id="primaryButtonColor" value="${systemConfig.primaryButtonColor}">
                                            </div>
                                            <div class="form-group form-width" style="width:40%">
                                                <label for="logo">Please choose text color.</label>
                                                <input type="color" name="textColor" id="textColor" value="${systemConfig.textColor}">
                                            </div>
                                        </div>
                                        <div class="buttons-w">
                                            <button class="btn btn-primary" onclick="return Validate()">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
    </body>
    <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js"></script>
    <script type="text/javascript">
                                                var message = '${message}';
                                                if (message.length > 4) {
                                                    swal(message);
                                                }
                                                function Validate() {
                                                    var password = document.getElementById("newPassword").value;
                                                    var confirmPassword = document.getElementById("rePassword").value;
                                                    if (password !== confirmPassword) {
                                                        swal("Passwords do not match.");
                                                        return false;
                                                    }
                                                    return true;
                                                }
    </script>
</html>
