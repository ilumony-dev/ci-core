/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo12
 */
public class PortfolioBean implements ToObjectConverter {

    /**
     * @return the percent24hr
     */
    public Double getPercent24hr() {
        return percent24hr;
    }

    /**
     * @param percent24hr the percent24hr to set
     */
    public void setPercent24hr(Double percent24hr) {
        this.percent24hr = percent24hr;
    }

    /**
     * @return the percent7d
     */
    public Double getPercent7d() {
        return percent7d;
    }

    /**
     * @param percent7d the percent7d to set
     */
    public void setPercent7d(Double percent7d) {
        this.percent7d = percent7d;
    }

    /**
     * @return the percent30d
     */
    public Double getPercent30d() {
        return percent30d;
    }

    /**
     * @param percent30d the percent30d to set
     */
    public void setPercent30d(Double percent30d) {
        this.percent30d = percent30d;
    }

    /**
     * @return the invested_usd
     */
    public String getInvested_usd() {
        return invested_usd;
    }

    /**
     * @param invested_usd the invested_usd to set
     */
    public void setInvested_usd(String invested_usd) {
        this.invested_usd = invested_usd;
    }

    /**
     * @return the fund_name
     */
    public String getFund_name() {
        return fund_name;
    }

    /**
     * @param fund_name the fund_name to set
     */
    public void setFund_name(String fund_name) {
        this.fund_name = fund_name;
    }

    /**
     * @return the performance
     */
    public String getPerformance() {
        return performance;
    }

    /**
     * @param performance the performance to set
     */
    public void setPerformance(String performance) {
        this.performance = performance;
    }

    /**
     * @return the share_id
     */
    public String getShare_id() {
        return share_id;
    }

    /**
     * @param share_id the share_id to set
     */
    public void setShare_id(String share_id) {
        this.share_id = share_id;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the full_name
     */
    public String getFull_name() {
        return full_name;
    }

    /**
     * @param full_name the full_name to set
     */
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the user_count
     */
    public String getUser_count() {
        return user_count;
    }

    /**
     * @param user_count the user_count to set
     */
    public void setUser_count(String user_count) {
        this.user_count = user_count;
    }

    /**
     * @return the fund_id
     */
    public String getFund_id() {
        return fund_id;
    }

    /**
     * @param fund_id the fund_id to set
     */
    public void setFund_id(String fund_id) {
        this.fund_id = fund_id;
    }

    /**
     * @return the total_units
     */
    public String getTotal_units() {
        return total_units;
    }

    /**
     * @param total_units the total_units to set
     */
    public void setTotal_units(String total_units) {
        this.total_units = total_units;
    }

    /**
     * @return the total_usd
     */
    public String getTotal_usd() {
        return total_usd;
    }

    /**
     * @param total_usd the total_usd to set
     */
    public void setTotal_usd(String total_usd) {
        this.total_usd = total_usd;
    }

    /**
     * @return the nav
     */
    public String getNav() {
        return nav;
    }

    /**
     * @param nav the nav to set
     */
    public void setNav(String nav) {
        this.nav = nav;
    }

    private String date;
    private String user_count;
    private String fund_id;
    private String fund_name;
    private String share_id;
    private String total_units;
    private String total_usd;
    private String invested_usd;
    private String performance;
    private String nav;
    private String user_id;
    private String full_name;
    private Double percent24hr;
    private Double percent7d;
    private Double percent30d;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PortfolioBean.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
