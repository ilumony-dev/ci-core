/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.EmailVerificationBean;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.SecurityService;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.RandomStringGenerator;
import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.mail.ExpiryDate;
import com.nz.cryptolabs.mail.MailConstants;
import com.nz.cryptolabs.mail.MailManager;
import com.nz.cryptolabs.mail.ResetPasswordForm;
import com.nz.cryptolabs.services.DateTimeService;
import com.nz.cryptolabs.services.SessionManagementService;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Administrator
 */
@Controller(value = "/")
public class RegisterController {

    private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);

    private User getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof User) {
                return ((User) principal);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("adminEmailId", adminEmailId);
        model.addAttribute("title", "Registration " + projectName);
        model.addAttribute("kickboxApiKey", Constants.kickboxApiKey);
        model.addAttribute("kickboxAppCode", Constants.kickboxAppCode);
        Calendar cal = Calendar.getInstance();
        cal.setTime(common.today());//today
        cal.add(Calendar.YEAR, -18);
        Date adultDOB = cal.getTime();
        model.addAttribute("adultDOB", common.dateFormat(adultDOB, common.format3));
        return "registration";
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public String registerPost(Model model, @ModelAttribute("user") UserInfo user) {
        if (repository.findByUsername(user.getEmail().trim()) != null) {
            return "forward:/forgetpwd";
        }
        user.setRole("ROLE_USER");
        String[] split = user.getFullName().split(" ");
        String firstName = split[0];
        user.setFirstName(firstName);
        String lastName = "";
        if (split.length > 1) {
            lastName = user.getFullName().substring(firstName.length());
            user.setLastName(lastName);
        }
        Date date = dateTimeService.now();
        user.setCreatedTs(common.dateFormat(date));
        user.setMobileNo(user.getCountryCode() + user.getMobileNo());
        repository.save(user);
        securityService.autologin(user.getEmail(), user.getPassword());
        RandomStringGenerator generator = new RandomStringGenerator();
        String token = generator.generate(16);
        EmailVerificationBean bean = new EmailVerificationBean();
        bean.setCreated_ts(user.getCreatedTs());
        bean.setUpdated_ts(user.getCreatedTs());
        bean.setEmail(user.getEmail());
        bean.setToken(token);
        bean.setRef_id(user.getRefId());
        repository.saveEmailVerification(bean);
        mailManager.sendConfirmationMailToEmail(user.getFirstName(), user.getEmail(), user.getRefId(), token);
        return "redirect:/loggedin";
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String redirect(ModelMap model) {
        return "redirect:/login";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(ModelMap model, HttpServletRequest request, @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {
        logger.debug("Request URL::" + request.getRequestURL().toString()
                + ":: Method Execute Start Time=" + System.currentTimeMillis());
        model.addAttribute("title", "Login " + projectName);
        if (error != null) {
            model.addAttribute("title", "Error " + projectName);
            model.addAttribute("message", "Invalid username and password!");
        }
        if (logout != null) {
            sessionManagementService.logout(repository);
            model.addAttribute("title", "Logout " + projectName);
            model.addAttribute("message", "You've been logged out successfully.");
        }
        if (getPrincipal() != null) {
            return "redirect:/welcome";
        }
        return "login";
    }

    @RequestMapping(value = "/user-confirm-email", method = RequestMethod.GET)
    public String confirmEmail(ModelMap model,
            @RequestParam(value = "token", required = false) String token,
            @RequestParam(value = "rId", required = false) String refId) {
        if (refId != null && token != null) {
            Date date = dateTimeService.now();
            EmailVerificationBean bean = new EmailVerificationBean();
            bean.setUpdated_ts(common.dateFormat(date));
            bean.setToken(token);
            bean.setRef_id(refId);
            repository.verifiedEmail(bean);
        }
        return "login";
    }

    //for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accesssDenied(ModelMap model) {
        return "403";
    }

    @RequestMapping(value = {"/forgetpwd"}, method = RequestMethod.GET)
    public String forgetPasswordPage(ModelMap modelMap) {
        modelMap.addAttribute("title", "Forget Password " + projectName);
        return "forgetPassword";
    }

    @RequestMapping(value = {"/forgetpwd"}, method = RequestMethod.POST)
    public String forgetPasswordPage(ModelMap modelMap, @RequestParam("email") String email) {
        UserInfo user = repository.findByUsername(email);
        if (user != null && user.getEmail() != null) {
            mailManager.sendResetToUser(user.getEmail(), user.getFullName());
            modelMap.addAttribute("title", "Sent Message " + projectName);
            modelMap.addAttribute("message", "We have sent a link on your email.");
            return "login";
        } else {
            modelMap.addAttribute("title", "Invalid Email " + projectName);
            modelMap.addAttribute("message", "That email has not found.");
            return "login";
        }
    }

    @RequestMapping(value = {"/resetpwd"}, method = RequestMethod.GET)
    public String resetPasswordPage(ModelMap modelMap, @RequestParam("token") String token,
            @RequestParam("csrf") String csrf) {
        ExpiryDate date = MailConstants.map.get(token);
        if (date != null) {
            UserInfo user = repository.findByUsername(date.getEmail());
            if (user != null && user.getEmail() != null) {
                modelMap.addAttribute("title", "Reset Password " + projectName);
                //            Date date = new Date();
                Date now = dateTimeService.now();
                if (date.getExpiryDate().before(now)) {
                    modelMap.addAttribute("title", "Token Expired " + projectName);
                    modelMap.addAttribute("message", "The token has expired.");
                    return "login";
                }
                ResetPasswordForm resetPwdForm = new ResetPasswordForm();
                resetPwdForm.setUserId(user.getUserId());
                resetPwdForm.setCsrf(csrf);
                resetPwdForm.setToken(token);
                modelMap.addAttribute("resetPwdForm", resetPwdForm);
                return "resetPassword";
            }
        }
        return "login";
    }

    @RequestMapping(value = {"/resetpwd"}, method = RequestMethod.POST)
    public String resetPasswordPage(ModelMap modelMap, @ModelAttribute("resetPwdForm") ResetPasswordForm resetPwdForm) {
        ExpiryDate date = MailConstants.map.get(resetPwdForm.getToken());
        if (date != null) {
            UserInfo user = repository.findByUsername(date.getEmail());
            //            Date date = new Date();
            Date now = dateTimeService.now();
            if (date.getExpiryDate().before(now)) {
                modelMap.addAttribute("title", "Token Expired " + projectName);
                modelMap.addAttribute("message", "The token has expired.");
                return "login";
            }
            if (user != null
                    && resetPwdForm.getUserId() != null
                    && !resetPwdForm.getNewPassword().isEmpty()
                    && !resetPwdForm.getRePassword().isEmpty()
                    && resetPwdForm.getNewPassword().equals(resetPwdForm.getRePassword())) {
                user.setPassword(resetPwdForm.getNewPassword());
                repository.updateUserPassword(user);
                modelMap.addAttribute("title", "Password Updated " + projectName);
                modelMap.addAttribute("message", "You have updated your password.");
                return "login";
            }
        }
        return "login";
    }

    @Autowired
    private CommonMethods common;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private MailManager mailManager;
    @Autowired
    private String adminEmailId;
    @Autowired
    private String projectName;
}
