<%-- 
    Document   : investment-fund-modal-lg
    Created on : Oct 15, 2017, 9:58:39 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade investment-fund-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <!--<h5 class="modal-title" id="investment-fund-modal-label"></h5>-->
                <h5 class="modal-title" id="investment-fund-modal-label"></h5>

                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <section id="summary" style="display:block;">
                    <div class="label investment-fund-modal-fundName" style="font-size: 1.5rem; color:forestgreen; text-align: center"></div>
                    <div class="label investment-fund-modal-fundDescription" style="font-size: 1rem; text-align: center"></div>
                    <div class="row" style="margin-top: 20px">
                        <div class="col-sm-12">
                            <div id="portfolio-linechart"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-8">
                            <div class="row">
                                <h5>
                                    Portfolio Performance (Calculated on 31/Aug/2018)
                                </h5>
<!--                                <div class="col-md-12 col-lg-6">
                                    <div class="element-box">
                                        <h5 class="modal-title" style="font-size:1rem">Hypothetical Performance</h5>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <h6>Invested</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-invested">$10000</p>
                                            </div>
                                        </div>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6  col-sm-6 col-6">
                                                <h6>Performance</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-performance">$144,539</p>
                                            </div>
                                        </div>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <h6>Gain</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-gain">545.4%</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-md-12 col-lg-12">
                                    <div class="element-box">
                                        <h5 class="modal-title" id="investment-fund-modal-label-span" style="font-size:1rem">Performance(%)</h5>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <h6>Weekly</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-months3">-48.9%</p>
                                            </div>
                                        </div>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <h6>1 Month</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-months6">54.4%</p>
                                            </div>
                                        </div>
                                        <div class="row margin-tb20">
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <h6>3 Months</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-6">
                                                <p id="investment-fund-modal-months12">545.4%</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <h6 class="element-header">Target Portfolio Allocation</h6>
                            <div id="portfolio-piechart"></div>
                        </div>
                    </div>
                    <!--</div>-->
                    <!--                    <div class="row investment-content" >
                                            <div class="col-sm-4">
                                                <div class="element-box el-tablo1">
                                                    <div class="color3">Total Invested</div>
                                                    <div class=" color3 value">$100</div>
                                                    <p>&nbsp;</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="element-box el-tablo1">
                                                    <div class="color3">Performance Since Inception</div>
                                                    <p class="color3">For Hypothetical</p>
                                                    <div class=" color3 value">$100</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="element-box el-tablo1">
                                                    <div class="color3">User Investment Performance</div>
                                                    <p class="color3">1st Day</p>
                                                    <div class=" color3 value">$100</div>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="modal-footer">
                        <a href="javascript:void(0)" class="btn btn-success" id="investment-fund-modal-pdflink" target="_blank">Download Portfolio Fact Sheet</a>
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary next" type="button">Make Investment</button>
                    </div>
                </section>
                <section id="investment" style="display:none;">
                    <div class="label" style="font-size: 1.5rem; color:forestgreen;">
                    </div>
                    <form method="post" action="./via-wallet-add-investment" id="via-wallet-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name='fundId' id="investment-fund-modal-fundId"/>
                                <input type="hidden" name='walletBalance' class='investment-fund-modal-walletBalance'/>
                                <input type="hidden" id="investment-fund-modal-fundIR" value="0"/>
                                <h3 class="color1">To make an investment, you must have sufficient cash available in your Invsta account (current balance shown below). To add money to your Invsta account, click on Add Money to Invsta Account</h3>
                                <!--<p style="font-size:12px; color: #666">(Minimum investment amount of $50. Either $50 now, or $50 as a regular payment)</p>-->
                                <div class="row" style="align-items: flex-end; margin-top: 20px">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="label" for="investment-fund-modal-walletBalance">Available Invsta Cash Balance(USD):</label>
                                            <input type="number" class="form-control investment-fund-modal-walletBalance" id="investment-fund-modal-walletBalance" value="0.00" step="0.01" readonly="readonly"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="label" for="investment-fund-modal-investmentAmount">How much would you like to invest in the <span class="investment-fund-modal-fundName"></span>(USD)?</label>
                                            <input type="number" class="form-control" id="investment-fund-modal-investmentAmount" name="investmentAmount" value="0.00" step="0.01"/>
                                            <input type="hidden" id="investment-fund-modal-localInvestmentAmount" name="localInvestmentAmount"/>
                                            <input type="hidden" id="investment-fund-modal-currency" name="currency"/>
                                            <input type="hidden" name="regularlyAmount" value="0"/>
                                            <input type="hidden" name="years" value="10"/>
                                            <input type="hidden" name="timeFrame" value="FIXED"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-warning" id="addbalances"  href="./paksdjfljdsfudskd#add-funds-modal-lg" style="margin:5px 0"> 
                                <!--data-toggle="modal" data-target="#add-funds-modal-lg">-->
                                Add money to Invsta Account 
                            </a>
                            <input class="btn btn-primary" type="button" id="investment-fund-modal-invest" value="Make Investment Now"/>
                        </div>
                    </form>
                </section>
                <section id="thanks" style="display: none">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="center-title">Thanks for Investing with Invsta<br>We will get your money working hard soon</h2>
                            <h1 class="center-title">Thanks <img src="resources/img/nervous/Slightly_Smiling_Face.png" style="width:5%"></h1>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<%--<jsp:include page="via_wallet.jsp"/>--%>
<jsp:include page="add-funds-modal-lg.jsp"/>