/* $(".submit").click(function(){	
 
 return false;
 }); */

/*-----------change background---*/
$(document).ready(function () {
 
    $("body");
    
    LoginModalController.initialize();

    ValidateForm();

});
/*-----------chacked botton----------*/



$("#yes-button").click(function () {
    $("#year1").show();

});
$("#no-button").click(function () {

    $("#year1").hide();
});
$("#kiwisaver").hide();
$("#radio03").click(function () {

    $("#kiwisaver").hide();
});
$("#radio04").click(function () {
    $("#kiwisaver").show();

});



$('#my_image').on({
    'click': function () {
        $('#my_image1').show();
        $('#my_image').hide()
    }
});

$('#my_image1').on({
    'click': function () {
        $('#my_image1').hide()
        $('#my_image').show()
    }
});

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating= false; //flag to prevent quick multi-click glitches

//function skip1stFS(){
//    
//}

function MoveNext(nextVal)
{
    if (animating)
        return false;
    animating = true;
    current_fs = $(nextVal).parent();
    next_fs = $(nextVal).parent().next();
   
//activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 1500,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

$(".previous").click(function () {
    if (animating)
        return false;
    animating = true;
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 1500,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});



var LoginModalController = {
    hidePasswordName: ".hide-password",
    hidePassword: null,
    findElements: function () {
        var base = this;

        base.tabsElement = $(base.tabsElementName);
        base.tabElement = $(base.tabElementName);
        base.inputElements = $(base.inputElementsName);
        base.hidePassword = $(base.hidePasswordName);

        return base;
    },
    setState: function (state) {
        var base = this,
                elem = null;

        if (!state) {
            state = 0;
        }

        if (base.tabsElement) {
            elem = $(base.tabsElement[state]);
            elem.addClass("current");
            $("." + elem.attr("data-tabtar")).addClass("show");
        }

        return base;
    },
    getActiveTab: function () {
        var base = this;

        base.tabsElement.each(function (i, el) {
            if ($(el).hasClass("current")) {
                base.activeTab = $(el);
            }
        });

        return base;
    },
    addClickEvents: function () {
        var base = this;

        base.hidePassword.on("click", function (e) {
            var $this = $(this),
                    $pwInput = $this.prev("input");

            if ($pwInput.attr("type") == "password") {
                $pwInput.attr("type", "text");


            } else {
                $pwInput.attr("type", "password");

            }
        });



        base.inputElements.find("label").on("click", function (e) {
            var $this = $(this),
                    $input = $this.next("input");

            $input.focus();
        });

        return base;
    },
    initialize: function () {
        var base = this;

        base.findElements().setState().getActiveTab().addClickEvents();
    }
};

function ValidateForm()
{

    jQuery.validator.addMethod("onespecial", function (value, element) {
        var pattern = /^(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;
        return (pattern.test(value));

    }, "Your password must contain 1 special character.");

    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("OneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");

    $("#msform").validate({
        rules: {
            Person_Email: {
                required: true,
                email: true,
                remote: {
                    url: "check-username.php",
                    type: "post"
                }
            },
                    Person_password: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        onespecial: true,
                        OneUppercaseLetter: true
                    },
                    newPassword :{
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        onespecial: true,
                        OneUppercaseLetter: true
                    },
            rePassword :{
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        onespecial: true,
                        OneUppercaseLetter: true
                    },
            fname: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            Person_Home_Phone: {
                required: true,
                number: true,
                minlength: 5,
                maxlength: 13
            },
            Person_DOB: {
                required: true,
        },
            gender: {
                required: true
            }
            ,
            Person_Home_Address: {
                required: true,
            },
            person_current_kivisaver_balance: {
                number: true,
                required: true
            },
            person_kivisaver_sal_before_tax: {
                number: true,
                required: true
            }
        },
        messages: {
             Person_Email: {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Email already in use!"
            },
            Person_password: {
                required: "Enter your password",
                Person_password: "Password should be minimum of 1 and maximum of 15 characters."
            },
             newPassword: {
                required: "Please enter your new password",
                Person_password: "Password should be minimum of 1 and maximum of 15 characters."
            },
            rePassword: {
                required: "Please enter your Re-password",
                Person_password: "Password should be minimum of 1 and maximum of 15 characters."
            },
            fname: {
                required: "Enter your full name",
            },
            Person_Home_Phone: {
                required: "Enter your Person_Home_Phone/mobile number",
                number: "Please enter numeric value with no spaces"
            },
            Person_DOB: {
                required: "Enter your date of birth",
            },
            gender: {
                required: "Please select gender",
            },
            Person_Home_Address: {
                required: "Please enter address",
            },
            person_current_kivisaver_balance: {
                required: "Please enter amount",
                number: "Please enter  numeric value"
            },
            person_kivisaver_sal_before_tax: {
                required: "Please enter  amount",
                number: "Please enter  numeric value"
            }

        }
    });

}




function addComma(income1) {
    income1.value = income1.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
