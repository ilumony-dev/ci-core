/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.filters;

/**
 *
 * @author palo12
 */
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class NoRespondToSameRequestFilter implements Filter {

    private FilterConfig filterConfig = null;
    private static final int REQUEST_TIME = 10000;
    private long lastRequestTime;
    private String lastRequestedUrl;

    {
        lastRequestTime = System.currentTimeMillis();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        long startTime = System.currentTimeMillis();
        String requestedUrl = request.getRequestURL().toString();
        System.out.println("RequestProcessingFilter.doFilter.startLine::" + requestedUrl);
        lastRequestTime = startTime;
        lastRequestedUrl = requestedUrl;
        System.out.println("RequestProcessingFilter.doFilter.nextLine::" + requestedUrl);
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
