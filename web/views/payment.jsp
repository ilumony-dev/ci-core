<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <link href="${home}/resources/css/stripe-button-el.css" rel="stylesheet"/>
        <link href="${home}/resources/css/checkbox.css" rel="stylesheet"/>
        <link href="${home}/resources/css/sweetalert2.css" rel="stylesheet"/>
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>CASH & TRANSACTIONS</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${endUser.fullName}</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Currency is US$</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">   
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">CASH & TRANSACTIONS (${endUser.fullName})<img src="./resources/img/info-icon.png" style="vertical-align:text-top; object-fit: contain"></h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-4 col-6">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Wallet Balance</div>
                                                        <div class="value">${common.formatMoney(actualWalletBalanceObject.balance,2, 'USD')}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-6">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Pending Investment Amount</div>
                                                        <div class="value">${common.formatMoney(actualWalletBalanceObject.amount,2, 'USD')}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-6">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Pending Withdrawal Amount</div>
                                                        <div class="value">${common.formatMoney(actualWalletBalanceObject.local, 2, 'USD')}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-6">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Available Wallet Balance</div>
                                                        <div class="value">${common.formatMoney(actualWalletBalanceObject.usd,2, 'USD')}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-6" id="lastdeposite">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Last Transaction</div>
                                                        <div class="value">${common.formatMoney(lastDeposite,2, 'USD')}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-6" id="accountstatus">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Account Status</div>
                                                        <div class="value">Active</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <a class="col-sm-4 col-4" id="addbalances" ${link} >
                                                    <div class="box" style="background-color: #638c25">
                                                        <div class="label">Add Funds</div>
                                                    </div>
                                                </a>
                                                <a class="col-sm-4 col-4" href="javascript:void(0)" id="widthdrawn">
                                                    <div class="box" style="background-color: #cc3d3d">
                                                        <div class="label">Withdraw Funds</div>
                                                    </div>
                                                </a>
                                                <a class="col-sm-4 col-4" id="mytransactions" href="javascript:void(0)">
                                                    <div class="box" style="background-color: #e8ac3e">
                                                        <div class="label">Bank Transactions</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div id="withdrawlbalance" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <div >
                                                                    <h5>Available Wallet Balance <span>${common.formatMoney(walletBalance, 2, 'USD')}</span></h5>
                                                                    <p>You can send this balance to any Bank account at a nominal charge</p>
                                                                    <form method="post" action="./transaction-request" onsubmit="return validatewithdraw();">
                                                                        <input type="hidden" name="maxAmount" value="${walletBalance}">
                                                                        <input type="hidden" name="userId" value="${endUser.userId}"/>
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="account">Account Number: </label>
                                                                                    <input type="text" class="form-control"  id="acc_number" placeholder="BB-bbbb-AAAAAAA-SSS" name="account">
                                                                                    <span class="error" id="error-acc_number"> </span> 
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="accountName">Account Holder Name: </label>
                                                                                    <input type="text" class="form-control" name="accountName" id="acc_name" value="${endUser.fullName}">
                                                                                    <span class="error" id="error-acc_name"> </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="bankName">Bank Name:</label>
                                                                                    <select class="form-control" name="bankName" id="bank-name">
                                                                                        <option value="null">-Select-</option>
                                                                                        <option>Bank of New Zealand</option>
                                                                                        <option>ANZ Bank New Zealand</option>
                                                                                        <option>ASB Bank</option>
                                                                                        <option>Westpac</option>
                                                                                        <option>Heartland Bank</option>
                                                                                        <option>Kiwibank</option>
                                                                                        <option>SBS Bank</option>
                                                                                        <option>TSB Bank</option>
                                                                                    </select>
                                                                                    <span class="error" id="error-bank-name"> </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-4">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="amount">Currency:</label>
                                                                                    <select class="form-control" name="currency" id="currency">
                                                                                        <option value="USD">United States Dollar (US$)</option>
                                                                                        <!--<option value="NZD">New Zealand Dollar (NZ$)</option>-->
                                                                                    </select>
                                                                                    <span class="error" id="error-currency"> </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="amount">Amount:</label>
                                                                                    <input type="number" class="form-control" id="amount_value"  name="localAmount" step="0.01" min="0" max="${walletBalance}">
                                                                                    <span class="error" id="error-amount_value"> </span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="refId">Customer Number:</label>
                                                                                    <input type="text" class="form-control"  name="refId" value="${endUser.refId}" readonly > 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label>Description</label>
                                                                                    <textarea class="form-control" name="description"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12 text-right">
                                                                                <button type="submit" class="btn btn-success">Proceed</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div style="display:none">
                                                                    <h5 class="element-header">Withdrawl Request</h5>
                                                                    <p>Hey ${endUser.fullName}, I understand you want to withdraw some money from your portfolios</p>
                                                                    <b>Note: This money will be credited to your Invsta Wallet. Please 3-5 working days for your balance to be credited. </b>
                                                                    <form method="post" action="./transaction-request">
                                                                        <div class="row">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="amount">Your Current Balance:</label>
                                                                                    <input type="number" class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label class="label" for="refId">I would like to withdraw:</label>
                                                                                    <input type="text" class="form-control" > 
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <Input type="checkbox" id="test2" style="margin-right: 10px">
                                                                                <label for="test2">Term and Conditions</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row" style="margin-top: 20px">
                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="btn btn-primary">Cancel</button>
                                                                                <button type="submit" class="btn btn-success pull-right">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="mytransaction" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <h5 class="form-header">Bank to Cash Wallet <img src="./resources/img/info-icon.png" style="vertical-align:text-top"></h5>
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Date
                                                                                </th>
                                                                                <th>
                                                                                    Particulars
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Inc
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Dec
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    US$ Balance
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${accountSummary}" var="trans">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${trans.created_ts}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${trans.particulars}
                                                                                    </td>
                                                                                    <td  class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                                            ${common.formatMoney(trans.amount,2, 'USD')}    
                                                                                        </c:if>

                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                                            ${common.formatMoney(trans.amount,2, 'USD')}    
                                                                                        </c:if>
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${common.formatMoney(trans.balance,2, 'USD')}
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="processedtransaction" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <h5 class="form-header">Processed Transactions <img src="./resources/img/info-icon.png" style="vertical-align:text-top"></h5>
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Date
                                                                                </th>
                                                                                <th>
                                                                                    Particulars
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Inc
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Dec
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    US$ Balance
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Transaction ID
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Status
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${accountSummary}" var="trans">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${trans.created_ts}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${trans.particulars}
                                                                                    </td>
                                                                                    <td  class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>

                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${trans.balance}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        123as45678dagf9
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        Processed
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="pendingtransaction" style="display: none">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">
                                                                Pending Money Investment In Portfolio Allocation <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                            </h6>
                                                            <div class="element-box">
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    DateTime
                                                                                </th>
                                                                                <th>
                                                                                    Customer Id /Name
                                                                                </th>
                                                                                <th>
                                                                                    Email
                                                                                </th>
                                                                                <th>
                                                                                    Portfolio
                                                                                </th>
                                                                                <th>
                                                                                    Shares
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Total Investment
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${pendingInvestments}" var="investment">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${investment.createdDate}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.refId} /
                                                                                        ${investment.customerName}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.email}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.fundName}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.shares}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        US$ ${investment.investmentAmount}
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                </div>
                                                                                                    <div class="row">-->
                                                    <div class="col-sm-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">
                                                                Pending Sell Transaction Requests <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                            </h6>
                                                            <div class="element-box">
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    DateTime
                                                                                </th>
                                                                                <th>
                                                                                    Customer Id /Name
                                                                                </th>
                                                                                <th>
                                                                                    Email
                                                                                </th>
                                                                                <th>
                                                                                    Investment Name
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Amount
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${pendingTransactions}" var="investment">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${investment.createdDate}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.refId} /
                                                                                        ${investment.customerName}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.email}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${investment.fundName}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        US$ ${investment.investmentAmount}
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                </div>
                                                                                                    <div class="row">-->
                                                    <div class="col-sm-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">
                                                                Pending Money In - Bank to Cash Wallet <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                            </h6>
                                                            <div class="element-box">
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    DateTime
                                                                                </th>
                                                                                <th>
                                                                                    Customer Id /Name
                                                                                </th>
                                                                                <th>
                                                                                    Email
                                                                                </th>
                                                                                <th>
                                                                                    A/c Details
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Amount
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Local Amount
                                                                                </th>
                                                                                <th>
                                                                                    Master
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${newWithdrawlRequests}" var="req">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${req.createdDate}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${req.refId} /
                                                                                        ${req.customerName}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${req.email}
                                                                                    </td>
                                                                                    <td>
                                                                                        <p>${req.bankName}</p>
                                                                                        <p>${req.accountName}</p>
                                                                                        <p>${req.account}</p>
                                                                                    </td>

                                                                                    <td class="text-right">
                                                                                        US$ ${req.amount}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${req.currency} ${req.localAmount}
                                                                                    </td>
                                                                                    <td>${req.master}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                </div>
                                                                                                    <div class="row">-->
                                                    <div class="col-sm-12">
                                                        <div class="element-wrapper">
                                                            <h6 class="element-header">
                                                                Pending Wallet's Transactions <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                            </h6>
                                                            <div class="element-box">
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    DateTime
                                                                                </th>
                                                                                <th>
                                                                                    Customer Id /Name
                                                                                </th>
                                                                                <th>
                                                                                    Email
                                                                                </th>
                                                                                <th>
                                                                                    Particulars 
                                                                                </th>
                                                                                <th>
                                                                                    Portfolio 
                                                                                </th>
                                                                                <th>
                                                                                    Payment Method 
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Amount
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${pendingWalletTxns}" var="txn">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${txn.created_ts}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${txn.ref_id} /
                                                                                        ${txn.full_name}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${txn.email_id}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${txn.particulars}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${txn.fund_name}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${txn.brand}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        US$ ${txn.amount}
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="element-wrapper">
                                                                                            <div class="element-box">
                                                                                                <h5 class="form-header">Pending Transactions <img src="./resources/img/info-icon.png" style="vertical-align:text-top"></h5>
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-lightborder">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>
                                                                                                                    Date
                                                                                                                </th>
                                                                                                                <th>
                                                                                                                    Particulars
                                                                                                                </th>
                                                                                                                <th class="text-right">
                                                                                                                    Inc
                                                                                                                </th>
                                                                                                                <th class="text-right">
                                                                                                                    Dec
                                                                                                                </th>
                                                                                                                <th class="text-right">
                                                                                                                    US$ Balance
                                                                                                                </th>
                                                                                                                <th class="text-right">
                                                                                                                    Transaction ID
                                                                                                                </th>
                                                                                                                <th class="text-right">
                                                                                                                    Status
                                                                                                                </th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                            <c:forEach items="${accountSummary}" var="trans">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        ${trans.created_ts}
                                                                                                                    </td>
                                                                                                                    <td>
                                                                                                                        ${trans.particulars}
                                                                                                                    </td>
                                                                                                                    <td  class="text-right">
                                                                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                                                                            ${trans.amount}    
                                                                                                                        </c:if>

                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${trans.balance}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        123as45678dagf9
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        Pending
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>--%>
                                            </div>

                                            <div id="cancelledtransaction" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <h5 class="form-header">Cancelled Transactions <img src="./resources/img/info-icon.png" style="vertical-align:text-top"></h5>
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Date
                                                                                </th>
                                                                                <th>
                                                                                    Particulars
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Inc
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Dec
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    US$ Balance
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Transaction ID
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Status
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${accountSummary}" var="trans">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${trans.created_ts}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${trans.particulars}
                                                                                    </td>
                                                                                    <td  class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>

                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${trans.balance}
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        123as45678dagf9
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        Cancelled
                                                                                    </td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="element-box">
                                                <h6 class="element-header">
                                                    Account Statements <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                                </h6>
                                                <div class="table-responsive">
                                                    <table class="table table-lightborder">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    Date
                                                                </th>
                                                                <th>
                                                                    Particulars
                                                                </th>
                                                                <th>
                                                                    Portfolio
                                                                </th>
                                                                <th class="text-right">
                                                                    US$ Inc
                                                                </th>
                                                                <th class="text-right">
                                                                    US$ Dec
                                                                </th>
                                                                <th class="text-right">
                                                                    US$ Balance
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${walletSummary}" var="trans">
                                                                <tr>
                                                                    <td>
                                                                        ${trans.created_ts}
                                                                    </td>
                                                                    <td>
                                                                        ${trans.particulars}
                                                                    </td>
                                                                    <td>
                                                                        ${trans.fund_name}
                                                                    </td>
                                                                    <td  class="text-right">
                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                            ${common.formatMoney(trans.amount,2, 'USD')}    
                                                                        </c:if>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                            ${common.formatMoney(trans.amount,2, 'USD')}    
                                                                        </c:if>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        ${common.formatMoney(trans.balance,2, 'USD')}
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <jsp:include page="modals/add-funds-modal-lg.jsp"/>
                        <jsp:include page="user-right-sidebar.jsp"/>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/sweetalert2.js"></script>
        <script src="${home}/resources/js/cryptolabs/fire-events.js"></script>
        <script>
                                                                        var info = null;
                                                                        var currency = '${localCurrency}';
                                                                        var currSymbol = '${currSymbol}';
        </script>
        <script src="${home}/resources/js/cryptolabs/user-main.js"></script>
        <script type="text/javascript">
                                                                        $(document).ready(function () {
                                                                        $.get("./rest/cryptolabs/modelmap/${key}", function (data, status) {
                                                                        var jO = JSON.parse(data);
                                                                        info = jO.info;
                                                                        endOfReady();
                                                                        });
                                                                        var val = "";
                                                                        $.ajax({
                                                                        type: "GET",
                                                                                dataType: "json",
                                                                                url: "${home}/rest/cryptolabs/api/marketcaps",
                                                                                success: function (data) {
                                                                                console.log("response:" + data);
                                                                                $.each(data, function (j, pdata) {
                                                                                val = val + '<tr><td>' + pdata.name + '</td><td>' + pdata.market_cap_usd + '</td><td>' + pdata.price_usd + '</td><td>' + pdata.total_supply + ' ' + pdata.symbol + '</td><td>' + pdata.volume_usd_24h + '</td><td>' + pdata.percent_change_24h + '</td></tr>';
                                                                                });
                                                                                $("#data").html(val);
                                                                                },
                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                console.log(' Error in processing! ' + textStatus);
                                                                                }
                                                                        });
                                                                        $("#add-fund-modal-currency").val('${currency}');
                                                                        $("#currency").val('${currency}');
                                                                        $("#addbalance").hide();
                                                                        $("#mytransaction").hide();
                                                                        $("#withdrawlbalance").hide();
                                                                        $("#addbalances").click(function () {
                                                                        $("#addbalance").show();
                                                                        $("#withdrawlbalance").hide();
                                                                        });
                                                                        $("#widthdrawn").click(function () {
                                                                        $("#withdrawlbalance").show();
                                                                        $("#mytransaction").hide();
                                                                        $("#addbalance").hide();
                                                                        $("#processedtransaction").hide();
                                                                        $("#pendingtransaction").hide();
                                                                        $("#cancelledtransaction").hide();
                                                                        });
                                                                        $("#mytransactions").click(function () {
                                                                        $("#mytransaction").show();
                                                                        $("#addbalance").hide();
                                                                        $("#withdrawlbalance").hide();
                                                                        $("#processedtransaction").hide();
                                                                        $("#pendingtransaction").hide();
                                                                        $("#cancelledtransaction").hide();
                                                                        });
                                                                        $("#process").click(function () {
                                                                        $("#processedtransaction").show();
                                                                        $("#pendingtransaction").hide();
                                                                        $("#cancelledtransaction").hide();
                                                                        $("#mytransaction").hide();
                                                                        $("#withdrawlbalance").hide();
                                                                        });
                                                                        $("#pending").click(function () {
                                                                        $("#pendingtransaction").show();
                                                                        $("#cancelledtransaction").hide();
                                                                        $("#processedtransaction").hide();
                                                                        $("#mytransaction").hide();
                                                                        $("#withdrawlbalance").hide();
                                                                        $("#admin-dash").show();
                                                                        });
                                                                        $("#cancelled").click(function () {
                                                                        $("#cancelledtransaction").show();
                                                                        $("#processedtransaction").hide();
                                                                        $("#pendingtransaction").hide();
                                                                        $("#mytransaction").hide();
                                                                        $("#withdrawlbalance").hide();
                                                                        });
                                                                        $("#acc_name").keyup(function () {
                                                                        var value = '';
                                                                        $("#error-acc_name").text(value);
                                                                        });
                                                                        $("#bank-name").change(function () {
                                                                        var value = '';
                                                                        $("#error-bank-name").text(value);
                                                                        });
                                                                        $("#amount_value").keyup(function () {
                                                                        var value = '';
                                                                        $("#error-amount_value").text(value);
                                                                        });
                                                                        $("#acc_number").keyup(function () {
                                                                        var value = '';
                                                                        $("#error-acc_number").text(value);
                                                                        var n = $(this).val().replace(/-/g, '');
                                                                        if (n.length > 1) {
                                                                        n = n.insert(2, "-");
                                                                        }
                                                                        if (n.length > 6) {
                                                                        n = n.insert(7, "-");
                                                                        }
                                                                        if (n.length > 14) {
                                                                        n = n.insert(15, "-");
                                                                        }
                                                                        $(this).val(n.toLocaleString());
                                                                        });
                                                                        $("#add-fund-modal-amount").keyup(function () {
                                                                        var value = this.value;
                                                                        $("#add-fund-modal-checkout-amount").val(value);
                                                                        });
                                                                        $("#add-fund-modal-description").keyup(function () {
                                                                        var value = this.value;
                                                                        $("#add-fund-modal-checkout-description").val(value);
                                                                        });
                                                                        $("#add-fund-modal-currency").change(function () {
                                                                        var value = this.value;
                                                                        $("#add-fund-modal-checkout-currency").val(value);
                                                                        });
                                                                        $("#add-fund-modal-label").text("Add money");
                                                                        $("#investment-fund-modal-continue").click(function () {
                                                                        $("#add-fund-modal-label").text("Select payment method  ");
                                                                        });
                                                                        $("#add-fund-modal-BANK_TRANSFER").click(function () {
                                                                        $("#user-add-funds-form").submit();
                                                                        });
                                                                        });
                                                                        function onSubmission() {
                                                                        $("#add-fund-modal-BANK_TRANSFER").unbind('click');
                                                                        }
                                                                        function validatewithdraw() {
                                                                        var acc_number = $('#acc_number').val().trim();
                                                                        var result = true;
                                                                        if (acc_number === '') {
                                                                        document.getElementById('error-acc_number').innerHTML = "Your account number is required.";
                                                                        result = result && false;
                                                                        }
//                                                                            if (acc_number.value.length <= 16 || acc_number.value.length >= 24){
//                                                                                document.getElementById('error-acc_number').innerHTML = "Account Number must be in length from 16 to 24 numeric values";
//                                                                                result = result && false;
//                                                                            }
                                                                        var acc_name = document.getElementById('acc_name');
                                                                        var regnameexp = /^[a-zA-Z\s]+$/;
                                                                        if (acc_name.value === '') {
                                                                        document.getElementById('error-acc_name').innerHTML = "Your account holder name is required.";
                                                                        result = result && false;
                                                                        }
                                                                        if (!regnameexp.test(acc_name.value)){
                                                                        document.getElementById('error-acc_name').innerHTML = "Enter the account name only in alphabetical characters";
                                                                        result = result && false;
                                                                        }
                                                                        var bank_name = document.getElementById('bank-name');
                                                                        if (bank_name.value === 'null') {
                                                                        document.getElementById('error-bank-name').innerHTML = "Please select the bank name ";
                                                                        result = result && false;
                                                                        }
                                                                        var amount_value = document.getElementById('amount_value');
                                                                        if (amount_value.value === '') {
                                                                        document.getElementById('error-amount_value').innerHTML = "Amount is required ";
                                                                        result = result && false;
                                                                        }
                                                                        var amt = parseFloat(amount_value.value);
                                                                        if (amt === 0) {
                                                                        document.getElementById('error-amount_value').innerHTML = "Amount can't be Zero ";
                                                                        result = result && false;
                                                                        }
                                                                        return result;
                                                                        }
        </script>
        <jsp:include page="footer.jsp"/>
    </body>
</html>
