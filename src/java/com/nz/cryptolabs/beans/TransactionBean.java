/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Administrator
 */
//Do not modify it, It use everytime.
public class TransactionBean {

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the fund_id
     */
    public String getFund_id() {
        return fund_id;
    }

    /**
     * @param fund_id the fund_id to set
     */
    public void setFund_id(String fund_id) {
        this.fund_id = fund_id;
    }

    /**
     * @return the details
     */
    public List<TransactionBean> getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(List<TransactionBean> details) {
        this.details = details;
    }

    /**
     * @return the coin_id
     */
    public String getCoin_id() {
        return coin_id;
    }

    /**
     * @param coin_id the coin_id to set
     */
    public void setCoin_id(String coin_id) {
        this.coin_id = coin_id;
    }

    /**
     * @return the created_date
     */
    public String getCreated_date() {
        return created_date;
    }

    /**
     * @param created_date the created_date to set
     */
    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    /**
     * @return the medium
     */
    public String getMedium() {
        return medium;
    }

    /**
     * @param medium the medium to set
     */
    public void setMedium(String medium) {
        this.medium = medium;
    }

    /**
     * @return the last_amount
     */
    public String getLast_amount() {
        return last_amount;
    }

    /**
     * @param last_amount the last_amount to set
     */
    public void setLast_amount(String last_amount) {
        this.last_amount = last_amount;
    }

    /**
     * @return the fund_name
     */
    public String getFund_name() {
        return fund_name;
    }

    /**
     * @param fund_name the fund_name to set
     */
    public void setFund_name(String fund_name) {
        this.fund_name = fund_name;
    }

    /**
     * @return the ref_inv_id
     */
    public String getRef_inv_id() {
        return ref_inv_id;
    }

    /**
     * @param ref_inv_id the ref_inv_id to set
     */
    public void setRef_inv_id(String ref_inv_id) {
        this.ref_inv_id = ref_inv_id;
    }

    /**
     * @return the txn_Req_id
     */
    public String getTxn_Req_id() {
        return txn_Req_id;
    }

    /**
     * @param txn_Req_id the txn_Req_id to set
     */
    public void setTxn_Req_id(String txn_Req_id) {
        this.txn_Req_id = txn_Req_id;
    }

    /**
     * @return the approved
     */
    public String getApproved() {
        return approved;
    }

    /**
     * @param approved the approved to set
     */
    public void setApproved(String approved) {
        this.approved = approved;
    }

    /**
     * @return the email_id
     */
    public String getEmail_id() {
        return email_id;
    }

    /**
     * @param email_id the email_id to set
     */
    public void setEmail_id(String email_id) {
        this.email_id = email_id;
    }

    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }

    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    /**
     * @return the full_name
     */
    public String getFull_name() {
        return full_name;
    }

    /**
     * @param full_name the full_name to set
     */
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    /**
     * @return the local_amount
     */
    public String getLocal_amount() {
        return local_amount;
    }

    /**
     * @param local_amount the local_amount to set
     */
    public void setLocal_amount(String local_amount) {
        this.local_amount = local_amount;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the usd
     */
    public String getUsd() {
        return usd;
    }

    /**
     * @param usd the usd to set
     */
    public void setUsd(String usd) {
        this.usd = usd;
    }

    /**
     * @return the btc
     */
    public String getBtc() {
        return btc;
    }

    /**
     * @param btc the btc to set
     */
    public void setBtc(String btc) {
        this.btc = btc;
    }

    /**
     * @return the minto
     */
    public String getMinto() {
        return minto;
    }

    /**
     * @param minto the minto to set
     */
    public void setMinto(String minto) {
        this.minto = minto;
    }

    /**
     * @return the units
     */
    public String getUnits() {
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(String units) {
        this.units = units;
    }

    /**
     * @return the txn_id
     */
    public String getTxn_id() {
        return txn_id;
    }

    /**
     * @param txn_id the txn_id to set
     */
    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the investment_id
     */
    public String getInvestment_id() {
        return investment_id;
    }

    /**
     * @param investment_id the investment_id to set
     */
    public void setInvestment_id(String investment_id) {
        this.investment_id = investment_id;
    }

    /**
     * @return the particulars
     */
    public String getParticulars() {
        return particulars;
    }

    /**
     * @param particulars the particulars to set
     */
    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    /**
     * @return the inc_dec
     */
    public String getInc_dec() {
        return inc_dec;
    }

    /**
     * @param inc_dec the inc_dec to set
     */
    public void setInc_dec(String inc_dec) {
        this.inc_dec = inc_dec;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * @param balance the balance to set
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public TransactionBean() {

    }

    public TransactionBean(StripeCharge charge) {
        this.created_ts = charge.getCreated_ts();
        this.user_id = charge.getUser_id();
        this.particulars = charge.getDescription();
        this.amount = charge.getAmount();
        this.txn_id = charge.getTxn_id();
    }

    private String id;
    private String created_ts;
    private String created_date;
    private String user_id;
    private String ref_id;
    private String email_id;
    private String full_name;
    private String fund_name;
    private String fund_id;
    private String coin_id;
    private String investment_id;
    private String ref_inv_id;
    private String particulars;
    private String inc_dec;
    private String amount;
    private String local_amount;
    private String active;
    private String approved;
    private String balance;
    private String txn_id, txn_Req_id, base;
    private String brand;
    private String medium;
    private String units;
    private String local, usd, last_amount, btc, minto;
    private List<TransactionBean> details;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TransactionBean.class);

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
