<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>${title}</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous"/>
        <link href="./resources/css/verify-style.css" rel="stylesheet" type="text/css"/>
        <link href="./resources/css/main.css" rel="stylesheet" type="text/css"/>
        <link href="./resources/css/verify-animate.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css"/>
        <!--<link rel="stylesheet" href="./resources/css/intl/intlTelInput.css">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
              rel="stylesheet" type="text/css" />
        <link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
              rel="stylesheet" type="text/css" />
        <link href="./resources/css/ion.calender.css" rel="stylesheet"/>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body class="verification_body">
        <div class="container">
            <div class="body_section">
                <div class="form_cont">
                    <form id="msform" action="./verification" method="post" autocomplete="off" enctype="multipart/form-data" >
                        <fieldset class="animated bounceInLeft showhide" id="first-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">Invsta Verification Process</h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container form_text">
                                <p>We are required to comply with Know Your Client (KYC)  and Anti-Money Laundering (AML) regulations. To comply with these regulations, all new Invsta users must complete the following verification process. We try to keep this process as quick and easy as possible. </p>
                                <p>If you are unsure or have questions about this, please email us at info@invsta.com or message us below. </p>
                            </div>
                            <div style="width:100%;display:flex;justify-content: center;align-items: center;">
                                <a class="prev action-button" href="./welcome" style="margin-right:15px; margin-bottom: 15px">Back</a>
                                <a id="anchor00" class=" action-button next" href="#" style="margin-bottom:15px">Start Verification</a>
                            </div>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide" id="second-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">Invsta Verification Process</h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container form_text">
                                <div class="form-style-6">
                                    <div class="Hi_Name">
                                        <!--<p>Hi,${user.fullName}</p>-->
                                        <p>Please check that the below details are correct, if not then please update these below. </p>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Full Name:
                                        </label>
                                        <input type="text" class="form-control second-fs-field" name="full_name" placeholder="Your Name" value="${info.fullName}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Date of Birth
                                        </label>
                                        <!--<input type="text" name="dob" id="dob" placeholder="D.O.B" data-lang="en" class="form-control" data-years="1934-1999" data-format="YYYY/MM/DD" data-lang="en"/>-->
                                        <input type="text" class="form-control second-fs-field" name="dob" placeholder="Date of Birth" value="${info.dob}" class="textbox-n" id="dob" data-years="1934-2000" data-format="YYYY/MM/DD" data-lang="en" required/>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Home Address
                                        </label>
                                        <textarea name="address" class="form-control second-fs-field" placeholder="Address" id="address" value="${ver.address}" style="padding-bottom:0; resize: none" required></textarea>
                                    </div>
                                    <p id="second-fs-field" style="display: block;"></p>
                                    <a id="prev back-fs" class=" action-button" href="#" style="margin-right: 15px; margin-bottom: 15px">Back</a>
                                    <a id="anchor01" class=" action-button next" href="#">Next</a>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide" id="third-fs">
                            <div class="header_form_s in_form_container">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside">
                                    <h2>Which country do you usually live in?</h2>
                                </div>
                            </div>
                            <div class="btn_section">
                                <input type="hidden" name="is_nz" id="is_nz" value="${ver.is_nz}"/> 
                                <a id="anchor02" onclick="is_nzd('Y');">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/Flag_of_New_Zealand.png">
                                        </div>
                                        <span class="btn_area1_span">New Zealand</span>
                                    </div>
                                </a>
                                <a id="anchor03" onclick="is_nzd('N');">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/2-2-globe-free-download-png.png">
                                        </div>
                                        <span class="btn_area1_span">Rest of the world</span>
                                    </div>
                                </a>
                            </div>
                            <a id="back-fs1" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide " id="fourth-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside">
                                    <h2>Which form of New Zealand issued identification you will use for verification?</h2>
                                </div>
                            </div>
                            <div class="btn_section">
                                <input type="hidden" name="nz_id" id="nz_id" value="${ver.nz_id}"/> 
                                <a id="anchor04" onclick="nzd_id('PASSPORT');">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/passport.png">
                                        </div>
                                        <span class="btn_area1_span">NZ Passport</span>
                                    </div>
                                </a>
                                <a id="anchor05" onclick="nzd_id('DRIVELIC');">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/linces.png">
                                        </div>
                                        <span class="btn_area1_span">NZ Driving licence</span>
                                    </div>
                                </a>
                                <a id="anchor06" onclick="is_nzd('N');">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/passport.png">
                                        </div>
                                        <span class="btn_area1_span">NO NZ ID? </span>
                                    </div>
                                </a>
                            </div>
                            <a id="back-fs2" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>                            
                        <!--                        <fieldset class="animated bounceInLeft showhide"  id="pp-ts-fs">
                                                    <div class="header_form_s in_form_container ">
                                                        <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                                        <img src="./resources/img/verify-images/header-dp.png">
                                                    </div>
                                                    <div class="in_form_container in_form2 form_text">
                                                        <div class="where_reside">
                                                            <h2>Please have your passport ready to provide your details</h2>
                                                        </div>
                                                    </div>
                                                    <div class="btn_section">
                                                                                    <a id="anchor07"  class="go6" href="#">
                                                                                        <div class="btn_area1">
                                                                                            <div class="btn_area1_img">
                                                                                                <img src="./resources/img/verify-images/photo-camera-7.png">
                                                                                            </div>
                                                                                            <span class="btn_area1_span">TAKE A SELFIE!</span>
                                                                                        </div>
                                                                                    </a>
                                                        <a id="anchor08" href="#">
                                                            <div class="btn_area1">
                                                                <div class="btn_area1_img">
                                                                    <img src="./resources/img/verify-images/passport.png">
                                                                </div>
                                                                <span class="btn_area1_span"> Passport </span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <a id="back-fs3" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                                                    <a id="" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Next</a>
                                                </fieldset>-->
                        <fieldset class="animated bounceInLeft showhide" id="passport-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">INVSTA SELF VERIFICATION PORTAL</h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container form_text">
                                <div class="where_reside">
                                    <h2>Please enter your passport details</h2>
                                </div>
                                <div class="form-style-3">
                                    <div class="form-style-3_form">
                                        <label for="pp_passport_number"><span>Passport number</span>
                                            <input type="text" class="input-field passport-fs-field" name="pp_passport_number" value="${ver.pp_passport_number}" required/>
                                        </label>
                                        <label for="pp_issue_country"><span>Country of Issue</span>
                                            <select name="pp_issue_country" class="select-field issue-country passport-fs-field" value="${ver.pp_issue_country}" required>
                                            </select>
                                        </label>
                                        <label for="pp_passport_expiry_date"><span>Expiry Date</span>
                                            <input type="text" class="input-field passport-fs-field" name="pp_passport_expiry_date" value="${ver.pp_passport_expiry_date}" id="dob1" data-years="2018-2099" data-format="YYYY/MM/DD" data-lang="en" placeholder="Expiry Date" required/>
                                        </label>
                                    </div>
                                    <p id="passport-fs-field" style="display: none;">Please red fields are required. </p>
                                    <div class="btn_section12">
                                        <a id="anchor11" class="next" href="#">
                                            <div class="btn_area1" style="margin:auto">
                                                <div class="btn_area1_span">Next</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a id="back-fs4" class="prev action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide" id="dl-ts-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside">
                                    <h2>OK great, we can use your drivers license for verification. Please click on the Driving License button below to continue. </h2>
                                </div>
                            </div>
                            <div class="btn_section">
                                <!--                            <a id="anchor09" href="#">
                                                                <div class="btn_area1">
                                                                    <div class="btn_area1_img">
                                                                        <img src="./resources/img/verify-images/photo-camera-7.png">
                                                                    </div>
                                                                    <span class="btn_area1_span">TAKE A SELFIE!</span>
                                                                </div>
                                                            </a>-->
                                <a id="anchor10" class="next" href="#">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/linces.png">
                                        </div>
                                        <span class="btn_area1_span">Driving License</span>
                                    </div>
                                </a>
                            </div>
                            <a id="back-fs5" class="prev action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide"  id="dl-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">INVSTA SELF VERIFICATION PORTAL</h2>
                                <img src="./resources/img/verify-images/header-dp.png"/>
                            </div>
                            <div class="in_form_container form_text">
                                <div class="where_reside">
                                    <h2>Please enter the details below:</h2>
                                </div>
                                <div class="form-style-3">
                                    <div class="form-style-3_form">
                                        <label for="dl_issue_country"><span>Country of issue: </span>
                                            <select name="dl_issue_country" class="select-field issue-country dl-fs-field" value="${ver.dl_issue_country}" required>
                                            </select>
                                        </label>
                                        <label for="dl_license_number"><span>License Number</span>
                                            <input type="text" class="input-field dl-fs-field" name="dl_license_number" value="${ver.dl_license_number}"  required/>
                                        </label>
                                        <label for="dl_license_version"><span>Version Number</span>
                                            <input type="text" class="input-field dl-fs-field" name="dl_license_version" value="${ver.dl_license_version}"  required/>
                                        </label>
                                        <label for="dl_expiry_date"><span>Expiry Date</span>
                                            <input type="text" class="input-field dl-fs-field" name="dl_expiry_date" id="dob2" value="${ver.dl_expiry_date}" data-years="2018-2099" data-format="YYYY/MM/DD" data-lang="en" placeholder="Expiry Date" required />
                                        </label>
                                    </div>
                                    <p id="dl-fs-field" style="display: none;">Please red fields are required. </p>
                                    <div class="btn_section12">
                                        <a id="anchor12" class="next" href="#">
                                            <div class="btn_area1">
                                                <div class="btn_area1_span">Next</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a id="back-fs6" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide" id="rest-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside">
                                    <h2>WHAT FROM OF ID WOULD YOU LIKE TO USE FOR IDENTIFICATION ?</h2>
                                </div>
                            </div>
                            <div class="btn_section">
                                <a id="anchor13" href="#">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/passport.png">
                                        </div>
                                        <span class="btn_area1_span">Passport</span>
                                    </div>
                                </a>
                                <a id="anchor14" href="#">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/linces.png">
                                        </div>
                                        <span class="btn_area1_span">Driving license</span>
                                    </div>
                                </a>
                                <a id="anchor15" href="#">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/passport.png">
                                        </div>
                                        <span class="btn_area1_span">NO  ID? </span>
                                    </div>
                                </a>
                            </div>
                            <a id="back-fs7" class=" action-button" href="#" style="margin-right: 15px; margin-top:20px">Back</a>
                        </fieldset>

                        <fieldset class="animated bounceInLeft showhide" id="selfie-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside">
                                    <h2>OK, I SEE YOU WANT TO USE YOUR PASSPORT! WHICH METHOD WOULD YOU LIKE TO USE?</h2>
                                </div>
                            </div>
                            <div class="btn_section take-selff">
                                <a id="anchor19" href="#">
                                    <div class="btn_area1">
                                        <div class="btn_area1_img">
                                            <img src="./resources/img/verify-images/photo-camera-7.png">
                                        </div>
                                    </div>
                                </a>
                                <a href="#" id="start-camera" class="btn_area1_span visible">Take a Selfie</a>
                                <div class="app-container">
                                    <div class="app">
                                        <video id="camera-stream"></video>
                                        <img id="snap" name="selfieImage"/>
                                        <input type="hidden" name="imageData" class="imageData"/>
                                        <!--<input type="file" name="file" class="imageData"/>-->
                                        <p id="error-message"></p>
                                        <div class="controls">
                                            <a href="#" id="delete-photo" title="Delete Photo" class="disabled"><i class="fas fa-trash-alt"></i></a>
                                            <a href="#" id="take-photo" title="Take Photo"><i class="fas fa-camera"></i></a>
                                            <a href="#" id="download-photo" download="selfie.png" title="Save Photo" class="disabled"><i class="fas fa-download"></i></a>
                                        </div>
                                        <canvas id="imageCanvas"></canvas>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide"  id="tax-fs">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside photoshot-re">
                                    <h2>Nearly finished! Just a few final details below, this is to comply with global taxation requirements.</h2>
                                </div>
                            </div>
                            <div class="form-style-3">
                                <div class="form-style-3_form">
                                    <label for="countries"><span>List all of the countries of which you are a resident or citizen:</span>
                                        <!--<input type="text" id="countryCode1" class="input-field" name="countries" value="${ver.countries}"/>-->
                                        <select  id="countryCode1"  name="countries" class="select-field" value="${ver.countries}" multiple required>
                                        </select>
                                    </label>
                                    <label for="tax_resi_country"><span>List all of the countries of which you are a tax resident of: </span>
                                        <!--<input type="text" id="countryCode2" class="input-field" name="tax_resi_country" value="${ver.tax_resi_country}" />-->
                                        <select  id="countryCode2"  name="tax_resi_country" class="select-field" value="${ver.tax_resi_country}" multiple required>
                                        </select>
                                    </label>
                                    <label for="tax_id"><span>Tax number (please list your tax numbers for each country)</span>
                                        <input type="text" class="input-field tax-fs-field" name="tax_id" value="${ver.tax_id}"  required/>
                                    </label>
                                    <label for="src_of_fund"><span>What is the source of money you are investing through Invsta?</span>
                                        <select id="src_of_fund"  name="src_of_fund" class="select-field" value="${ver.src_of_fund}"  required>
                                            <option value="Appointment">Salary</option>
                                            <option value="Interview">Savings</option>
                                            <option value="Regarding a post">Inheritance</option>
                                            <option value="Regarding a post">Sale of an asset</option>
                                            <option id="other" value="Other">Other</option>
                                        </select>
                                    </label>
                                    <label for="src_of_fund"><span>Over the next year, approximately how much do you intend to invest into cryptocurrencies?</span>
                                        <select id="src_of_fund"  name="src_of_fund" class="select-field" value=""  required>
                                            <option value="">Less than $1,000</option>
                                            <option value="">Between $1,000 - $5,000</option>
                                            <option value="">Between $5,000 $10,000</option>
                                            <option value="">Between $10,000 - $50,000</option>
                                            <option value="">More than $50,000</option>
                                        </select>
                                    </label>
                                    <div id="text" class="animated zoomIn">
                                        <label for="other"><span>Other </span><input type="text" class="input-field" name="other" value="${ver.other}"/></label>
                                    </div>
                                </div>
                                <p id="tax-fs-field" style="display: none;">Note:-**Please red fields are required.**</p>
                                <div class="btn_section12">
                                    <a id="submit" href="#" class="msform-submit">
                                        <div class="btn_area1" style="margin:auto">
                                            <div class="btn_area1_span">Submit </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="condition_terms">
                                    <p>I acknowledge that the information contained in this form may be provided to the IRD and that the IRD may exchange this information with tax authorities of another country/jurisdiction in which I may be tax resident. I certify that I am a Controlling Person (or am authorised to provide this information for the controlling person of the account to which this related. I declare that all statements made are, to the best of my knowledge, correct and complete. </p>
                                    <p>I undertake to advise within 90 days of any change in circumstances which affect my tax residency status or causes the information contained herein to become incorrect or incomplete, and to provide a suitably updated information and declaration of such change in circumstances. </p>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="animated bounceInLeft showhide"  id="thanks">
                            <div class="header_form_s in_form_container ">
                                <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                <img src="./resources/img/verify-images/header-dp.png">
                            </div>
                            <div class="in_form_container in_form2 form_text">
                                <div class="where_reside  ">
                                    <h2>Thanks for letting us know.Please contact us at info@invsta.coom to get verified manually </h2>
                                </div>
                            </div>
                            <div class="btn_section">
                                <a id="go15" href="#">
                                    <div class="btn_area1">
                                        <span class="btn_area1_span">CLOSE </span>
                                    </div>
                                </a>
                            </div>
                        </fieldset>
                        <!--                    <fieldset class="animated bounceInLeft showhide"  id="thanks-fs">
                <div class="header_form_s in_form_container ">
                    <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                    <img src="./resources/img/verify-images/header-dp.png">
                </div>
                <div class="in_form_container in_form2 form_text ">
                    <img class="right_image" src="./resources/img/verify-images/check-clipart-animated-gif-1.gif">
                        <div class="where_reside">
                            <h2>Thanks for giving us all the information 
                                we will be in touch with you soon.
                                <br/>
                                <br/>
                                Thanks
                            </h2>
                        </div>
                </div>
            </fieldset>-->
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="./resources/js/validation.js"></script>
    <script type="text/javascript" src="./resources/js/cryptolabs/countries.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <!--<script type="text/javascript" src="https://raw.githubusercontent.com/daneden/animate.css/master/animate.css"></script>-->
    <script src="./resources/bower_components/moment/moment.js"></script>
    <script src="./resources/js/ion.calendar.js"></script>
    <!--<script src="./resources/js/ion.calendar.min.js"></script>-->
    <script src="./resources/js/moment-with-locales.min.js"></script>
    <script>
                                    $(function () {
                                    valid = function(id) {
                                    var fields = document.getElementsByClassName(id);
                                    var full = true;
                                    for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i];
                                    if (field.value === '') {
                                    if (full === true) {
                                    full = false;
                                    }
                                    field.style.background = "#f16262";
                                    } else {
                                    field.style.background = "white";
                                    }
                                    }
                                    var p = document.getElementById(id);
//                                    alert(p);
                                    if (full === true) {
                                    p.style.display = 'none';
                                    p.style.color = 'black';
                                    p.innerHTML = "";
                                    } else {
                                    p.style.display = 'block';
                                    p.style.color = 'red';
                                    p.innerHTML = "Note:-**Please enter the required input fields.**";
                                    }
                                    return full;
                                    }
                                    $('#back-fs').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#first-fs').css("display", "block");
                                    });
                                    $('#back-fs1').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#second-fs').css("display", "block");
                                    });
                                    $('#back-fs2').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#third-fs').css("display", "block");
                                    });
                                    $('#back-fs3').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#fourth-fs').css("display", "block");
                                    });
                                    $('#back-fs4').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#fourth-fs').css("display", "block");
                                    });
                                    $('#back-fs5').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#rest-fs').css("display", "block");
                                    });
                                    $('#back-fs6').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#rest-fs').css("display", "block");
                                    });
                                    $('#back-fs7').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#third-fs').css("display", "block");
                                    });
                                    $('#anchor00').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#second-fs').css("display", "block");
                                    });
                                    $('#anchor01').click(function () {
                                    if (valid('second-fs-field') === true){
                                    $('.showhide').css("display", "none");
                                    $('#third-fs').css("display", "block");
                                    }
                                    });
                                    $('#anchor12').click(function () {
                                    if (valid('dl-fs-field') === true){
                                    $('.showhide').css("display", "none");
                                    $('#tax-fs').css("display", "block");
                                    }
                                    valid('tax-fs-field');
                                    });
                                    $('#anchor11').click(function () {
                                    if (valid('passport-fs-field') === true){
                                    $('.showhide').css("display", "none");
                                    $('#tax-fs').css("display", "block");
                                    }
                                    valid('tax-fs-field');
                                    });
                                    $('#anchor02').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#fourth-fs').css("display", "block");
                                    });
                                    $('#anchor03').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#rest-fs').css("display", "block");
                                    });
                                    $('#anchor04').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#passport-fs').css("display", "block");
                                    });
                                    $('#anchor07').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#selfie-fs').css("display", "block");
                                    });
                                    $('#anchor08').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#passport-fs').css("display", "block");
                                    });
                                    $('#anchor19').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#tax-fs').css("display", "block");
                                    });
                                    $('#anchor05').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#dl-fs').css("display", "block");
                                    });
                                    $('#anchor09').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#selfie-fs').css("display", "block");
                                    });
                                    $('#anchor10').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#dl-fs').css("display", "block");
                                    });
                                    $('#anchor13').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#passport-fs').css("display", "block");
                                    });
                                    $('#anchor14').click(function () {
                                    $('.showhide').css("display", "none");
                                    $('#dl-fs').css("display", "block");
                                    });
                                    $('#anchor15').click(function () {
                                    $('.showhide').css("display", "none");
//                                    $('#no-id-fs').css("display", "block");
                                    swal({
                                    title: "Please contact us on info@invsta.com to discuss verification options",
                                            showConfirmButton: true
                                    },
                                            function(){
                                            window.location.href = "./welcome";
                                            }
                                    );
                                    });
                                    $('#anchor06').click(function () {
                                    $('.showhide').css("display", "none");
//                                $('#no-id-fs').css("display", "block");
//                                    $('#rest-fs').css("display", "block");
                                    swal({
                                    title: "Please contact us on info@invsta.com to discuss verification options",
                                            showConfirmButton: true
                                    },
                                            function(){
                                            window.location.href = "./welcome";
                                            }
                                    );
                                    });
                                    var htmlCode = '';
                                    var cuntries = countries();
                                    for (var i = 0; i < cuntries.length; i++) {
                                    var country = cuntries[i];
                                    htmlCode += '<option>' + country.name + '</option>'; ;
                                    }
                                    $(".issue-country").html(htmlCode);
                                    $("#countryCode1").html(htmlCode);
                                    $("#countryCode2").html(htmlCode);
                                    $("#countryCode1").multiselect({includeSelectAllOption: true});
                                    $("#countryCode2").multiselect({includeSelectAllOption: true});
                                    $("#dob").ionDatePicker();
                                    $("#dob1").ionDatePicker();
                                    $("#dob2").ionDatePicker();
                                    });
                                    function submitform() {
                                    var form = document.getElementsByTagName('form')[0];
                                    form.submit();
                                    }

// References to all the element we will need.
//    var video = null, image = null, start_camera = null,
//            controls = null, take_photo_btn = null, delete_photo_btn = null,
//            download_photo_btn = null, error_message = null, imageCanvas = null;
                                    var video = document.querySelector('#camera-stream'),
                                            image = document.querySelector('#snap'),
                                            start_camera = document.querySelector('#start-camera'),
                                            controls = document.querySelector('.controls'),
                                            take_photo_btn = document.querySelector('#take-photo'),
                                            delete_photo_btn = document.querySelector('#delete-photo'),
                                            download_photo_btn = document.querySelector('#download-photo'),
                                            error_message = document.querySelector('#error-message'),
                                            imageCanvas = document.querySelector('#imageCanvas');
                                    $(document).ready(function () {
                                    $("#first-fs").show();
                                    $(".msform-submit").click(function () {
                                    submitform();
                                    });
                                    $("#text").hide();
                                    $("#field4").change(function () {
                                    var field4 = $("#field4").val();
                                    //alert( "Handler for .change() called." + field4);
                                    if (field4 === 'Other') {
                                    $("#text").show();
                                    }
                                    });
                                    $("#other").click(function () {
                                    $("#text").show();
                                    });
                                    // The getUserMedia interface is used for handling camera input.
                                    // Some browsers need a prefix so here we're covering all the options
                                    navigator.getMedia = (navigator.getUserMedia ||
                                            navigator.webkitGetUserMedia ||
                                            navigator.mozGetUserMedia ||
                                            navigator.msGetUserMedia);
                                    if (!navigator.getMedia) {
                                    displayErrorMessage("Your browser doesn't have support for the navigator.getUserMedia interface.");
                                    } else {
//Request the camera.
//            navigator.getMedia(
//                    {
//                        video: true
//                    },
//                    // Success Callback
//                            function (stream) {
//                                // Create an object URL for the video stream and
//                                // set it as src of our HTLM video element.
//                                video.src = window.URL.createObjectURL(stream);
//                                // Play the video element to start the stream.
//                                video.play();
//                                video.onplay = function () {
//                                    showVideo();
//                                };
//                            },
//                            // Error Callback
//                                    function (err) {
//                                        displayErrorMessage("There was an error with accessing the camera stream: " + err.name, err);
//                                    }
//                            );
                                    }
                                    // Mobile browsers cannot play video without user input,
                                    // so here we're using a button to start it manually.
                                    start_camera.addEventListener("click", function (e) {
                                    e.preventDefault();
                                    // Start video playback manually.
                                    video.play();
                                    showVideo();
                                    });
                                    take_photo_btn.addEventListener("click", function (e) {
                                    e.preventDefault();
                                    var snap = takeSnapshot(); // Show image.
                                    image.setAttribute('src', snap);
                                    image.classList.add("visible");
                                    // Enable delete and save buttons
                                    delete_photo_btn.classList.remove("disabled");
                                    download_photo_btn.classList.remove("disabled");
                                    // Set the href attribute of the download button to the snap url.
                                    download_photo_btn.href = snap;
                                    // Pause video playback of stream.
                                    video.pause();
                                    var arr = $('.imageData');
                                    $.each(arr, function (i, ele) {
                                    ele.value = snap;
                                    });
                                    alert(snap);
                                    });
                                    delete_photo_btn.addEventListener("click", function (e) {

                                    e.preventDefault();
                                    // Hide image.
                                    image.setAttribute('src', "");
                                    image.classList.remove("visible");
                                    // Disable delete and save buttons
                                    delete_photo_btn.classList.add("disabled");
                                    download_photo_btn.classList.add("disabled");
                                    // Resume playback of stream.
                                    video.play();
                                    });
                                    });
                                    showVideo = function () {
                                    // Display the video stream and the controls.
                                    hideUI();
                                    video.classList.add("visible");
                                    controls.classList.add("visible");
                                    };
                                    takeSnapshot = function () {
                                    // Here we're using a trick that involves a hidden canvas element.

                                    var hidden_canvas = document.querySelector('canvas'),
                                            context = hidden_canvas.getContext('2d');
                                    var width = video.videoWidth, height = video.videoHeight;
                                    if (width && height) {
                                    // Setup a canvas with the same dimensions as the video.
                                    hidden_canvas.width = width;
                                    hidden_canvas.height = height;
                                    // Make a copy of the current frame in the video on the canvas.
                                    context.drawImage(video, 0, 0, width, height);
                                    // Turn the canvas image into a dataURL that can be used as a src for our photo.
                                    var imgSrc = hidden_canvas.toDataURL("image/png");
                                    return imgSrc;
                                    }
                                    };
                                    displayErrorMessage = function (error_msg, error) {
                                    error = error || "";
                                    if (error) {
                                    console.log(error);
                                    }
                                    error_message.innerText = error_msg;
                                    hideUI();
                                    error_message.classList.add("visible");
                                    };
                                    hideUI = function () {
                                    controls.classList.remove("visible");
                                    start_camera.classList.remove("visible");
                                    video.classList.remove("visible");
                                    snap.classList.remove("visible");
                                    error_message.classList.remove("visible");
                                    };
                                    is_nzd = function (e) {
                                    $('#is_nz').val(e);
                                    };
                                    nzd_id = function (e) {
                                    $('#nz_id').val(e);
                                    };
    </script>
    <script>
        var placeSearch, autocomplete;
        var componentForm = {
        locality: 'long_name',
                sublocality_level_1: 'long_name'
        };
        function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('address')),
        {types: ['address'], componentRestrictions: {country: 'nz'}});
        autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
        var val = place.address_components[i][componentForm[addressType]];
        document.getElementById(addressType).value = val;
        }
        }
        }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
    </script>                                      
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKuUGMz8-xYyc6t1YWUhygrrkB4-WAFeY&libraries=places&callback=initAutocomplete"
    async defer></script>
</html>
