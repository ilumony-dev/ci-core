<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--<html>-->
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:th='http://www.thymeleaf.org'>
    <head>
        <title>${title}</title>
        <meta charset="utf-8"/>
        <meta content="ie=edge" http-equiv="x-ua-compatible"/>
        <meta content="template language" name="keywords"/>
        <meta content="Tamerlan Soziev" name="author"/>
        <meta content="Admin dashboard html template" name="description"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="${home}/resources/favicon.png" rel="shortcut icon"/>
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet"/>
        <link href="${home}/resources/css/style.css" rel="stylesheet"/>
        <link href="${home}/resources/css/stripe-button-el.css" rel="stylesheet"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
        <link href="${home}/resources/css/checkbox.css" rel="stylesheet"/>
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Investment Options</span>
                        </li>
                    </ul>
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Crypto Traded Portfolio (CTP) Investment Options <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom; object-fit: contain;">
                                        </h6>
                                        <h6 id="error-message">
                                            ${message}
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <c:forEach items="${portfolios}" var="fund">
                                                    <div class="col-md-3 col-sm-4">
                                                        <div class="ilumony-box text-center" onclick="setInfo('${fund.fundId}', '${fund.name}', '${fund.percentage}', '${fund.description}');" data-target=".investment-fund-modal-lg" data-toggle="modal"  style="cursor: pointer; background:url(./resources/img/${fund.master}) center center no-repeat; background-size: cover;" >
                                                            <h5 class="color3">${fund.name}</h5>
                                                        </div>
                                                    </div>
                                                </c:forEach>

                                                <c:forEach items="${funds}" var="fund">
                                                    <div class="col-sm-3">
                                                        <div class="ilumony-box ilumony-bg1 text-center" onclick="setInfo('${fund.fundId}', '${fund.name}', '${fund.percentage}', '${fund.description}');" data-target=".investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">
                                                            <h5 class="color3">${fund.name}</h5>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="modals/investment-fund-modal-lg.jsp"/>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!--<script src='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js'></script>-->
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <!--<script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>-->
        <script src="${home}/resources/js/cryptolabs/highcharts/highcharts.js"></script>
        <script src="${home}/resources/js/cryptolabs/highcharts/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script>
                                                            var info = null;
                                                            var isVerifiedEmail = ${isVerifiedEmail};
                                                            var currency = '${localCurrency}';
                                                            var currSymbol = '${currSymbol}';
                                                            var msg = "${message}";
                                                            var map = new Object(); // or var map = {};
        </script>
        <script src="${home}/resources/js/cryptolabs/investments.js"></script>
        <script>
    if (msg.length > 4) {
        swal({
            title: "Error in making investment.",
            text: msg
        });
    }
    donutSharesChart = function (db) {
        var shares = new Array();
        var amounts = new Array();
        $.each(db, function (i, share) {
            shares.push(share.shareName);
            amounts.push(share.shareAmount);
        });
    };
    setCheckoutAmount = function () {
        var ufeleVal = $("#investment-fund-modal-investmentAmount").val();
        var amount = eval(ufeleVal) * 100;
        var fundId = $("#investment-fund-modal-fundId").val();
        document.getElementsByClassName("checkout-amount")[0].value = amount;
        $(".checkout-fundId").val(fundId);
        $(".span-checkout-amount").text(amount);
    };
    setInfo = function (fundId, fundName, interestRate, description) {
        $("#investment-fund-modal-label").text('Portfolio Overview');
        $("#investment-fund-modal-label-span").text('Performance(%)');
        $(".investment-fund-modal-fundName").text(fundName);
        $(".investment-fund-modal-fundName").val(fundName);
        $("#investment-fund-modal-fundId").val(fundId);
        $(".investment-fund-modal-walletBalance").val('${walletBalance}');
        $("#investment-fund-modal-investmentAmount").attr('max', '${walletBalance}');
        $("#investment-type-fundId").val(fundId);
        $("#via-wallet-fundId").val(fundId);
        $("#investment-fund-modal-fundIR").val(interestRate);
        $("#investment-fund-modal-currency").val(currency);
        $("#portfolio-linechart").html('');
        $("#portfolio-piechart").html('');
        var obj = map[fundId];   
        $("#investment-fund-modal-invested").text(obj.invested);
        $("#investment-fund-modal-performance").text(obj.performance);
        $("#investment-fund-modal-gain").text(obj.gain);
        $("#investment-fund-modal-months3").text(obj.months3);
        $("#investment-fund-modal-months6").text(obj.months6);
        $("#investment-fund-modal-months12").text(obj.months12);
        $(".investment-fund-modal-fundDescription").html(description+'<br><b>(Portfolio fee is ' + obj.feeRate +' '+ 'pa' +  ')</b>');
    if(obj.pdflink != null){
            $("#investment-fund-modal-pdflink").show();
            $("#investment-fund-modal-pdflink").attr('href', obj.pdflink);
        }else{
            $("#investment-fund-modal-pdflink").hide();
        }
        if (obj.lineChart != null) {
            Highcharts.chart('portfolio-linechart', {
                chart: {
                    height: '200px'
                },
                title: null,
                xAxis: {
                    categories: ['1 Jan 2017', '1 Feb 2017', '1 Mar 2017', '1 Apr 2017', '1 May 2017', '1 Jun 2017', '1 Jul 2017', '1 Aug 2017', '1 Sep 2017', '1 Oct 2017', '1 Nov 2017', '1 Dec 2017', '1 Jan 2018', '1 Feb 2018', '1 Mar 2018'],
                    labels: {
                        style: {
                            color: ''
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: 'Performance'
                    },
                    labels: {
                        style: {
                            color: ''
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                                Highcharts.numberFormat(this.y, 2);
                    }
                },
                series: obj.lineChart,
                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }
            });
        }
        if (obj.pieChart != null) {
            Highcharts.chart('portfolio-piechart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false,
                    margin: [0, 0, 0, 0],
                    height: '180px'
                },
                title: {
                    text: '',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 30
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: '#000',
                            }
                        },
                        startAngle: -90,
                        endAngle: 270,
                        center: ['50%', '50%']
                    }
                },
                series: obj.pieChart
            });
        }
        activeSummary();
    }
    onShowSetModal = function () {
        $("#stepContent1").addClass("active");
        $("#trigger1").removeClass("complete");
        $("#trigger1").addClass("active");
        $("#stepContent2").removeClass("active");
        $("#trigger2").removeClass("active");
    };
    activeSummary = function () {
        var id = "summary";
        var sections = document.getElementsByTagName("section");
        for (var i = 0; i < sections.length; i++) {
            var section = sections[i];
            section.style.display = "none";
        }
        var cP = document.getElementById(id);
        cP.style.display = "block";
        onFundChange();
    };
    onFundChange = function () {
        var fundId = $("#investment-fund-modal-fundId").val();
        var iAmount = $("#investment-fund-modal-investmentAmount").val();
        var url = './rest/cryptolabs/api/aksadjfljdslfjds999?fId=' + fundId + '&iAmt=' + iAmount;
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (data) {
                donutSharesChart(data);
            }
        });
    };
    $(document).ready(function () {
        $.get("./rest/cryptolabs/modelmap/${key}", function (data, status) {
            var jO = JSON.parse(data);
            info = jO.info;
            if (isVerifiedEmail === false) {
                swal({
                    title: "Welcome to Invsta! It's great to have you join us.",
                    text: "We have sent you an email to confirm your email address, please click on the link in this email, you'll then be able to login and check out the rest of our platform.",
                    content: {
                        element: "textarea",
                        attributes: {
                            value: "Thanks, \n Invsta Team.",
                            disabled: true
                        },
                    }
                });
            }
            endOfReady();
        });

        $(".next").click(function () {
            var curr = $(this).parents("section");
            var next = curr.next();
            var fundName = document.getElementsByClassName("investment-fund-modal-fundName")[0].innerHTML;
            $("#investment-fund-modal-label").text("Invest into " + fundName);
            curr.hide();
            next.show();
        });
        $("#addbalances").click(function () {
            $("#addbalance").show();
            $("#withdrawlbalance").hide();
        });
        $("#investment-fund-modal-invest").bind('click', function () {
            var actionType = $("#add-or-sell-funds-to-portfolio-actionType").val();
            swal({
                title: "Success!",
                text: "Thanks for your investment.",
            });
            $("#via-wallet-form").submit();
            $("#investment-fund-modal-invest").unbind('click');
        });
    });
        </script>
        <script src="${home}/resources/js/cryptolabs/user-main.js"></script> 
        <jsp:include page="footer.jsp"/>
    </body>
</html>