<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--------------------
START - Mobile Menu
-------------------->

<div class="model-on model" id="model-id" style="display: none;">
    <div class="model_body" id="model-body" >
        <div class="element-wrapper">
            <h3 class="element-header">AML/KYC Verification</h3>
        </div>
        <p>
            Please click the link below to verify yourself.<br>Thanks 
        </p>
        <div id="forcely" class="popup-footer">
            <a class="btn btn-danger" href="#" id="close-modal" onclick="activeModal('none');" style="margin-right:5px">Close Popup</a>
            <a class="btn btn-primary" href="./verify-new">Start</a>
        </div>
        <div id="ucanlater" class="popup-footer">
            <a class="btn btn-primary" href="./verify-new">Verify Now</a>
            <a class="btn btn-danger" href="./verify-later" onclick="activeModal('none');" style="margin-right:5px">Verify Later</a>
        </div>
    </div>
</div>
<div id="loadingDiv"></div>
<div class="menu-mobile menu-activated-on-click">
    <div class="mm-logo-buttons-w">
        <a class="mm-logo" href="./welcome">
            <!--<img src="./resources/img/invsta-03(200px).png">-->
            <img class= "main-logo">
        </a>
        <a class="mm-logo1" href="./welcome" style="display:none">
            <!--<img src="./resources/img/invsta-white-logo.png">-->
            <img class= "main-logo">
        </a>
        <label class="switch">
            <input class="togglePre" type="checkbox">
            <span class="slider round switch-btn" style="margin-left:0"></span>
        </label>
        <div class="mm-buttons">
            <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles">Sidebar</div>
            </div>
            <div class="mobile-menu-trigger hamburger" id="hamburger-1" onclick="mobileMenu()">
                <!--<div class="os-icon os-icon-hamburger-menu-1"></div>-->
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>
        </div>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <!--            <div class="avatar-w">
                            <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
                        </div>-->
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    ${info.fullName}
                </div>
                <div class="logged-user-role">
                    <c:choose>
                        <c:when test="${info.admin}">
                            ADMINISTRATOR
                        </c:when>
                        <c:when test="${info.management}">
                            MANAGEMENT
                        </c:when>
                        <c:otherwise>
                            USER
                        </c:otherwise>
                    </c:choose>
                </div>

            </div>
            <div class="logged-user-menu">
                <ul>
                    <li>
                        <a href="./prsdldfjlsjrjewli"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                    </li>
<!--                    <li>
                        <a class="verify-new"><i class="os-icon os-icon-user-male-circle2"></i><span>User Verification</span></a>
                    </li>-->
                    <c:if test="${info.admin}">
                        <li>
                            <a href="./system-config"><i class="icon-settings"></i><span>System Settings</span></a>
                        </li>
                    </c:if>
                    <li>
                        <a href="./coasdflkerewrweasd"><i class="icon-settings"></i><span>Configuration</span></a>
                    </li>

                    <li>
                        <a href="./login?logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                    </li>
                </ul>
            </div>
        </div>

        <!--        ------------------
        START - Mobile Menu List
        -------------------->
        <ul class="main-menu">
            <sec:authorize access="hasRole('ROLE_USER') OR hasRole('ROLE_MANAGEMENT')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>User Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./inoetreoirpewirp">
                        <div class="icon-w">
                            <div class="os-icon os-icon-hierarchy-structure-2"></div>
                        </div>
                        <span>Investment Options</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./paksdjfljdsfudskd">
                        <div class="icon-w">
                            <div class="icon-wallet"></div>
                        </div>
                        <span>Cash & Transactions</span>
                    </a>                 
                </li>
                <li class="sub-menu">
                    <a href="./txnasdfjsdfjldss">
                        <div class="icon-w">
                            <div class="icon-credit-card"></div>
                        </div>
                        <span>Transaction Status</span>
                    </a>              
                </li>
<!--                <li class="sub-menu">
                    <a href="./b3ddfdsflohy88797">
                        <div class="icon-w">
                            <div class="icon-book-open"></div>
                        </div>
                        <span>Blog</span>
                    </a>
                </li>-->
                <li class="sub-menu">
                    <a href="./inksdjflsdjrjwel">
                        <div class="icon-w">
                            <div class="icon-eye"></div>
                        </div>
                        <span>Research</span>
                    </a>
                </li>
                <!--                <li class="sub-menu">
                                    <a href="./rehjkgjgtfdqwgfss">
                                        <div class="icon-w">
                                            <div class="icon-people"></div>
                                        </div>
                                        <span>Referral</span>
                                    </a>
                                </li>-->
                <!--                <script>
                                    var HW_config = {
                                        selector: ".headway-logo", // CSS selector where to inject the badge
                                        account: "7zwMgJ"
                                    }
                                </script>
                                <script async src="http://cdn.headwayapp.co/widget.js"></script>
                                <script>
                                    setInterval(function () {
                                        $("#HW_badge_cont").html('Invsta-Gram');
                                    }, 1000);
                                </script>-->
                <!--                                <li class="sub-menu">
                                                    <a href="" >
                                                        <div class="icon-w">
                                                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                                        </div>
                                                        <span class="headway-logo" style="margin-top: -20px; margin-left: -16px"></span>
                                                    </a>
                                                </li>-->
            </sec:authorize>               
            <sec:authorize access="hasRole('ROLE_MANAGEMENT')">
                <li class="sub-menu">
                    <a href="./mgmt-show-portfolios">
                        <div class="icon-w">
                            <div class="icon-diamond"></div>
                        </div>
                        <span>Buying of Portfolios</span>
                    </a>
                </li>
            </sec:authorize>               
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-invite-codes">
                        <div class="icon-w">
                            <div class="icon-envelope-letter"></div>
                        </div>
                        <span>Invite Codes</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-shares">
                        <div class="icon-w">
                            <div class="icon-share"></div>
                        </div>
                        <span>Show Shares</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-portfolios">
                        <div class="icon-w">
                            <div class="icon-diamond"></div>
                        </div>
                        <span>Show Portfolios</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-user-accounts">
                        <div class="icon-w">
                            <div class="icon-people"></div>
                        </div>
                        <span>Show User Accounts</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-new-users">
                        <div class="icon-w">
                            <div class="icon-user-follow"></div>
                        </div>
                        <span>Show New Users</span>
                    </a>
                </li>
                <li class="sub-menu drop1">
                    <a href="#">
                        <div class="icon-w">
                            <div class="icon-user"></div>
                        </div>
                        <span>Verification</span>
                    </a>    
                    <a href="./admin-verification-done" class="drop_show">
                        <div class="icon-w">
                            <div class="icon-check"></div>
                        </div>
                        <span>Done</span>
                    </a> 
                    <a href="./admin-verification-pending" class="drop_show">
                        <div class="icon-w">
                            <div class="icon-clock"></div>
                        </div>
                        <span>Pending</span>
                    </a> 
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-change-profiles">
                        <div class="icon-w">
                            <div class="icon-note"></div>
                        </div>
                        <span>Show Change Profile</span>
                    </a>
                </li>
            </sec:authorize>
        </ul>
    </div>
</div>
<!--------------------
END - Mobile Menu
-------------------->

<!--------------------
START - Menu side 
-------------------->
<div class="desktop-menu menu-side-w menu-activated-on-click">
    <div class="logo-w">
        <a class="logo" href="./welcome">
            <img class= "main-logo"/> 
        </a>
        <a class="logo1" href="./welcome" style="display:none">
            <img class= "main-logo"/>
        </a>
        <!--        <span id="checks">
                    <label class="switch">
                        <input type="checkbox" id="CB1" value="ON">
                        <span class="slider round switch-btn" style="margin-left:0"></span>
                    </label>
                </span>-->
        <label class="switch">
            <input class="togglePre" type="checkbox" /> 
            <span class="slider round"></span>
        </label>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="logged-user-i">
                <!--                <div class="avatar-w">
                                    <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
                                </div>-->
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        ${info.fullName}
                    </div>
                    <div class="logged-user-role">
                        <c:choose>
                            <c:when test="${info.admin}">
                                ADMINISTRATOR
                            </c:when>
                            <c:when test="${info.management}">
                                MANAGEMENT
                            </c:when>
                            <c:otherwise>
                                USER
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="logged-user-menu">
                    <div class="logged-user-avatar-info">
                        <!--                        <div class="avatar-w">
                                                    <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
                                                </div>-->
                        <div class="logged-user-info-w">
                            <div class="logged-user-name">
                                ${info.fullName}
                            </div>
                            <div class="logged-user-role">
                                <c:choose>
                                    <c:when test="${info.admin}">
                                        ADMINISTRATOR
                                    </c:when>
                                    <c:when test="${info.management}">
                                        MANAGEMENT
                                    </c:when>
                                    <c:otherwise>
                                        USER
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <div class="bg-icon">
                        <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="./prsdldfjlsjrjewli"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                        </li>
<!--                        <li>
                            <a class="verify-new"><i class="os-icon os-icon-user-male-circle2"></i><span>User Verification</span></a>
                        </li>-->
                        <c:if test="${info.admin}">
                            <li>
                                <a href="./system-config"><i class="icon-settings"></i><span>System Settings</span></a>
                            </li>
                        </c:if>
                        <li>
                            <a href="./coasdflkerewrweasd"><i class="icon-settings"></i><span>Configuration</span></a>
                        </li>
                        <!--                        <li>
                                                    <a href="./nav_cal"><i class="icon-settings"></i><span>NAV Calculation Engine</span></a>
                                                </li>-->
                        <li>
                            <a href="./login?logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="main-menu">
            <sec:authorize access="hasRole('ROLE_MANAGEMENT')">
                <li class="sub-menu">
                    <a href="./mgmt-show-portfolios">
                        <div class="icon-w">
                            <div class="icon-diamond"></div>
                        </div>
                        <span>Buying of Portfolios</span>
                    </a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER') OR hasRole('ROLE_MANAGEMENT')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>User Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./inoetreoirpewirp">
                        <div class="icon-w">
                            <div class="os-icon os-icon-hierarchy-structure-2"></div>
                        </div>
                        <span>Investment Options</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./paksdjfljdsfudskd">
                        <div class="icon-w">
                            <div class="icon-wallet"></div>
                        </div>
                        <span>Cash & Transactions</span>
                    </a>                 
                </li>
                <li class="sub-menu">
                    <a href="./txnasdfjsdfjldss">
                        <div class="icon-w">
                            <div class="icon-credit-card"></div>
                        </div>
                        <span>Transaction Status</span>
                    </a>              
                </li>

<!--                <li class="sub-menu">
                    <a href="./b3ddfdsflohy88797">
                        <div class="icon-w">
                            <div class="icon-book-open"></div>
                        </div>
                        <span>Blog</span>
                    </a>
-->                </li>
                <li class="sub-menu">
                    <a href="./inksdjflsdjrjwel">
                        <div class="icon-w">
                            <div class="icon-eye"></div>
                        </div>
                        <span>Research</span>
                    </a>
                </li>
                <!--                <li class="sub-menu">
                                    <a href="./rehjkgjgtfdqwgfss">
                                        <div class="icon-w">
                                            <div class="icon-people"></div>
                                        </div>
                                        <span>Referral</span>
                                    </a>
                                </li>-->

            </sec:authorize>               
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-invite-codes">
                        <div class="icon-w">
                            <div class="icon-envelope-letter"></div>
                        </div>
                        <span>Invite Codes</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-shares">
                        <div class="icon-w">
                            <div class="icon-share"></div>
                        </div>
                        <span>Show Shares</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-portfolios">
                        <div class="icon-w">
                            <div class="icon-diamond"></div>
                        </div>
                        <span>Show Portfolios</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-user-accounts">
                        <div class="icon-w">
                            <div class="icon-people"></div>
                        </div>
                        <span>Show User Accounts</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-new-users">
                        <div class="icon-w">
                            <div class="icon-user-follow"></div>
                        </div>
                        <span>Show New Users</span>
                    </a>
                </li>
                <li class="sub-menu drop1">
                    <a href="#">
                        <div class="icon-w">
                            <div class="icon-user"></div>
                        </div>
                        <span>Verification</span>
                    </a>    
                    <a href="./admin-verification-done" class="drop_show">
                        <div class="icon-w">
                            <div class="icon-check"></div>
                        </div>
                        <span>Done</span>
                    </a> 
                    <a href="./admin-verification-pending" class="drop_show">
                        <div class="icon-w">
                            <div class="icon-clock"></div>
                        </div>
                        <span>Pending</span>
                    </a> 
                </li>
                <li class="sub-menu">
                    <a href="./admin-show-change-profiles">
                        <div class="icon-w">
                            <div class="icon-note"></div>
                        </div>
                        <span>Show Change Profile</span>
                    </a>
                </li>
            </sec:authorize>
            <script>
                var HW_config = {
                    selector: ".headway-logo", // CSS selector where to inject the badge
                    account: "7zwMgJ"
                };
            </script>
            <script async src="https://cdn.headwayapp.co/widget.js"></script>
            <script>
                setInterval(function () {
                    $(".HW_visible").css("display", "inline").css("line-height", "4px").css("text-indent", "-3px");
                    window.dispatchEvent(new Event('resize'));
                }, 1000);
            </script>
            <li class="sub-menu">
                <a href="javascript:void(0)" >
                    <div class="icon-w">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                    </div>
                    <span class="headway-logo">Invsta Gram</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://use.fontawesome.com/ac51fe8084.js"></script>
<script>
                function activeModal(e) {
                    document.getElementById('model-id').style.display = e;
                }
                function hideLoading() {
                    document.getElementById('loadingDiv').style.display = 'none';
                }
                function showLoading() {
                    document.getElementById('loadingDiv').style.display = 'block';
                }
                endOfReady = function () {
                    hideLoading();
                    document.getElementById('model-id').className = 'expand';
                    document.getElementById('forcely').style.display = 'none';
                    document.getElementById('ucanlater').style.display = 'none';
                    if (info.loginCount > 0 && info.verifyCount === 0 && info.user === true && info.verifyLater === false) {
                        activeModal('block');
                        document.getElementById('close-modal').style.display = 'none';
                        document.getElementById('forcely').style.display = 'block';
                    }
                    if (info.loginCount === 0 && info.verifyCount === 0 && info.user === true && info.verifyLater === false) {
                        activeModal('block');
                        document.getElementById('ucanlater').style.display = 'block';
                    }
                };
                window.onload = function () {
                    var arr = document.getElementsByClassName('main-logo');
                    for (var i = 0; i < arr.length; i++) {
                        var ele = arr[i];
                        ele.src = "./resources/img/invsta-03(200px).png";
                    }
                    endOfReady();
                };</script>
<script>
    $(document).ready(function () {
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 50) {
                $(".menu-mobile").addClass("header-bg-color");
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $(".menu-mobile").removeClass("header-bg-color");
            }
        });
        $(".verify-new").click(function () {
            if (info.loginCount > 0 && info.verifyCount > 0 && info.user === true) {
                swal({
                    title: "Verified!",
                    text: "Thanks we have already received your KYC/AML Details, we will let you know should we need any other information."
                });
            } else {
                window.location.href = "./verify-new";
            }
        });
    });
</script>


<!--------------------
END - Menu side 
-------------------->