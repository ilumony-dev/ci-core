<%-- 
    Document   : addfund
    Created on : Oct 15, 2017, 9:50:50 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="user-account-summary" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Account Summary
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <!--<form action="./admin-user-account-summary" method="post">-->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input class="form-control" placeholder="Enter Username" type="text" name="username" id="username"/>
                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-top: 27px">
                        <div class="form-group" style="float: right">
                            <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                            <a class="btn btn-primary" href="#" onclick="userAccountSummary();">Submit</a>
                            <button id="btnExport" class="btn btn-success" onclick="javascript:xport.toCSV('user-Accounts-Summary-Table',$('#username').val());" 
                                    title="Export the table to CSV for all browsers.">Export to CSV</button>
                        </div>
                    </div>
                    <!--</form>-->
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-lightborder"  id="user-Accounts-Summary-Table" summary="" rules="groups">
                                <thead>
                                    <tr>
                                        <th>
                                            DateTime
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Inc
                                        </th>
                                        <th>
                                            Dec
                                        </th>
                                        <th class="text-center">
                                            Balance
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="UAS">
                                    <tr>
                                        <td class="nowrap">

                                        </td>
                                        <td class="nowrap">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-right">

                                        </td>
                                        <td class="text-right">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <!--                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Create New Fund</button>-->
            </div>
        </div>
    </div>
</div>