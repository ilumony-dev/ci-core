/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.CommonObject;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.EmailVerificationBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TempProfile;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.poloniex.PoloniexExchangeService;
import com.nz.cryptolabs.services.Tokenizer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 *
 * @author Administrator
 */
@Repository
public class CommonRepositoryImpl implements CommonRepository {

    @Autowired
    private DataSourceTransactionManager transactionManager;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private Tokenizer tokenizer;
    @Autowired
    private CommonMethods common;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CommonRepositoryImpl.class);

    private void saveUnitTransaction(TransactionBean tran) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUnitTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getInvestment_id());
            pstmnt.setString(4, tran.getLocal());
            pstmnt.setString(5, tran.getUsd());
            pstmnt.setString(6, tran.getBtc());
            pstmnt.setString(7, tran.getMinto());
            pstmnt.setString(8, tran.getParticulars());
            pstmnt.setString(9, tran.getRef_inv_id());
            return pstmnt;
        });
    }

    @Override
    public void saveEmailVerification(EmailVerificationBean bean) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertEmailVerification, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, bean.getCreated_ts());
            pstmnt.setString(2, bean.getCreated_ts());
            pstmnt.setString(3, bean.getEmail());
            pstmnt.setString(4, bean.getToken());
            pstmnt.setString(5, bean.getRef_id());
            return pstmnt;
        });
    }

    @Override
    public EmailVerificationBean getEmailVerification(String refId) {
        List<EmailVerificationBean> list = jdbcTemplate.query(SQLQueries.emailVerification, new Object[]{refId}, (ResultSet rs, int i) -> {
            EmailVerificationBean bean = new EmailVerificationBean();
            bean.setCreated_ts(rs.getString("created_ts"));
            bean.setUpdated_ts(rs.getString("updated_ts"));
            bean.setEmail(rs.getString("email"));
            bean.setToken(rs.getString("token"));
            bean.setRef_id(rs.getString("ref_id"));
            bean.setVerified(rs.getString("verified"));
            return bean;
        });
        EmailVerificationBean emailVerificationBean = !list.isEmpty() ? list.get(0) : new EmailVerificationBean();
        return emailVerificationBean;
    }

    @Override
    public void verifiedEmail(EmailVerificationBean bean) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.verifiedEmail, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, bean.getUpdated_ts());
            pstmnt.setString(2, bean.getToken());
            pstmnt.setString(3, bean.getRef_id());
            return pstmnt;
        });
    }

    @Override
    public void save(String userId, String activity) {
        String sql = SQLQueries.updateUserActivity + userId;
        jdbcTemplate.update(sql);
        jdbcTemplate.update(SQLQueries.insertUserActivity, new Object[]{userId, activity});
    }

    @Override
    public void save(InviteCode code) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInviteCode, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, code.getInviteCode());
            pstmnt.setString(2, code.getExpiryTs());
            pstmnt.setString(3, code.getCreatedBy());
            pstmnt.setString(4, code.getCreatedTs());
            pstmnt.setString(5, code.getPurpose());
            pstmnt.setString(6, code.getEmailId());
            pstmnt.setString(7, code.getName());
            return pstmnt;
        }, holder);
        String id = String.valueOf(holder.getKey().intValue());
        code.setId(id);
    }

    @Override
    public void allocateEmail(InviteCode code) {
        jdbcTemplate.update(SQLQueries.allocateEmail, new Object[]{code.getEmailId(), code.getId()});
    }

    @Override
    public void save(UserInfo user) {
        Object totalCustomers = null;
        List<Map<String, Object>> totals = totals();
        for (Map<String, Object> map : totals) {
            String tbl = (String) map.get("tbl");
            if (tbl.equals("user_master")) {
                totalCustomers = map.get("total");
                break;
            }
        }
        Object refId = "";
        try {
            int i = new BigInteger(totalCustomers.toString()).intValue() + 1;
            if (i > 0) {
                refId = tokenizer.refNo(i, user.getFullName());
            }
        } catch (Exception ex) {
            int i = 1;
            if (i > 0) {
                refId = tokenizer.refNo(i, user.getFullName());
            }
        }
        user.setRefId(refId.toString());
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUser, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getFullName());
            pstmnt.setString(2, user.getDob());
            pstmnt.setString(3, user.getEmail());
            String cryptedPassword = encoder.encode(user.getPassword());
            pstmnt.setString(4, cryptedPassword);
            pstmnt.setString(5, user.getInviteCode());
            pstmnt.setString(6, user.getMobileNo());
            pstmnt.setString(7, user.getCreatedTs());
            pstmnt.setString(8, user.getRefId());
            pstmnt.setString(9, user.getFirstName());
            pstmnt.setString(10, user.getLastName());
            return pstmnt;
        }, holder);
        final int user_id = holder.getKey().intValue();
        user.setUserId(String.valueOf(user_id));
        logger.debug("primaryKey user_id  ---- " + user_id);

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertRole, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getUserId());
            pstmnt.setString(2, user.getRole() != null ? user.getRole() : "ROLE_USER");
            return pstmnt;
        });

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.usedInviteCode, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getInviteCode());
            return pstmnt;
        });

        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(user.getCreatedTs());
        tran.setUser_id(user.getUserId());
        tran.setParticulars("Opening Account...");
        tran.setInc_dec("Inc");
        tran.setAmount("0.00");
        saveTransaction(tran);
    }

    @Override
    public List<UserInfo> users(Date... dateArr) {
        String sql = SQLQueries.users;
        Object[] arr = new Object[0];
        if (dateArr != null && dateArr.length == 2) {
            Date first = dateArr[0];
            Date last = dateArr[1];
            sql += " AND U.created_ts BETWEEN ? AND ? ";
            arr = new Object[]{first, last};
        }
        List<UserInfo> list = jdbcTemplate.query(sql, arr,
                (ResultSet rs, int rowNum) -> {
                    UserInfo user = new UserInfo();
                    user.setUserId(rs.getString("user_id"));
                    user.setFullName(rs.getString("full_name"));
                    user.setDob(rs.getString("dob"));
                    user.setEmail(rs.getString("email"));
//                    user.setPassword(rs.getString("password"));
                    user.setMobileNo(rs.getString("mobile_no"));
                    user.setInviteCode(rs.getString("invite_code"));
                    user.setRefId(rs.getString("ref_id"));
                    user.setRole(rs.getString("role"));
                    user.setCreatedTs(rs.getString("created_ts"));
                    user.setDateTime(rs.getString("date_time"));
                    user.setActive(rs.getString("active"));
                    return user;
                });
        return list;
    }

    @Override
    public UserInfo findByUsername(String username) {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.loginSQL, new Object[]{username}, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setCreatedTs(rs.getString("created_ts"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            user.setLoginCount(rs.getLong("login_count"));
            user.setVerifyCount(rs.getLong("verify_count"));
            user.setActive(rs.getString("active"));
            return user;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public UserInfo findManagementUser(String branchId) {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.managementUser, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setCreatedTs(rs.getString("created_ts"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setGender(rs.getString("gender"));
            user.setCountry(rs.getString("country"));
            user.setState(rs.getString("state"));
            user.setAddress(rs.getString("address"));
            user.setZipcode(rs.getString("zipcode"));
            return user;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public UserInfo findByUserId(String userId) {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.userById, new Object[]{userId}, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setCreatedTs(rs.getString("created_ts"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setGender(rs.getString("gender"));
            user.setCountry(rs.getString("country"));
            user.setState(rs.getString("state"));
            user.setAddress(rs.getString("address") != null && !rs.getString("address").trim().isEmpty() ? rs.getString("address") : rs.getString("v_address"));
            user.setZipcode(rs.getString("zipcode"));
            return user;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public UserInfo updateUserPassword(UserInfo user) {
        try {
            jdbcTemplate.update((Connection con) -> {
                PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updatePassword, 1);
                String cryptedPassword = encoder.encode(user.getPassword());
                pstmnt.setString(1, cryptedPassword);
                pstmnt.setString(2, user.getUserId());
                return pstmnt;
            });
            logger.debug("person update complete  ---- ");
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception ---- " + e);
        } finally {
            return user;
        }
    }

    @Override
    public UserInfo updateUserStatus(UserInfo user) {
        try {
            jdbcTemplate.update((Connection con) -> {
                PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updateActive, 1);
                pstmnt.setString(1, user.getActive());
                pstmnt.setString(2, user.getUserId());
                return pstmnt;
            });
            logger.debug("person update complete  ---- ");
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Exception ---- " + e);
        } finally {
            return user;
        }
    }

    @Override
    public void addShare(ShareFund share) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertShare, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, share.getName());
            pstmnt.setString(2, share.getDescription());
            pstmnt.setString(3, share.getCreatedBy());
            pstmnt.setString(4, share.getExchangeCode());
            pstmnt.setString(5, share.getCustodianId());
            return pstmnt;
        });
    }

    @Override
    public void addCurrency(ShareFund currency) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertCurrency, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, currency.getName());
            pstmnt.setString(2, currency.getCurrencySymbol());
            pstmnt.setString(3, currency.getDescription());
            pstmnt.setString(4, currency.getCreatedBy());
            pstmnt.setString(5, currency.getExchangeCode());
            pstmnt.setString(6, currency.getCustodianId());
            return pstmnt;
        });
    }

    @Override
    public ShareFund shareById(String shareId) {
        ShareFund shareById = jdbcTemplate.queryForObject(SQLQueries.shareById, new Object[]{shareId}, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            share.setPrice(rs.getString("price"));
            return share;
        });//id, name, desc, active, approved, created_by, exchange_code, custodian_id
        return shareById;
    }

    @Override
    public void updateShare(ShareFund share) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updateShare, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, share.getName());
            pstmnt.setString(2, share.getDescription());
            pstmnt.setString(3, share.getExchangeCode());
            pstmnt.setString(4, share.getCustodianId());
            pstmnt.setString(5, share.getShareId());
            return pstmnt;
        });
    }

    @Override
    public void updateSharePrice(ShareFund share) {
        jdbcTemplate.update(SQLQueries.updateOldSharePrice, new Object[]{share.getShareId()});
        jdbcTemplate.update(SQLQueries.insertSharePrice, new Object[]{share.getShareId(), share.getPrice(), share.getCreatedDate()});
    }

    @Override
    public List<ShareFund> shares() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.shares, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> currencies() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.currencies, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setCurrencySymbol(rs.getString("symbol"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            return share;
        });
        return list;
    }

    @Override
    public void addFund(ShareFund fund) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFund, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, fund.getName());
            pstmnt.setString(2, fund.getDescription());
            pstmnt.setString(3, fund.getCreatedBy());
            pstmnt.setString(4, fund.getCustodianId());
            pstmnt.setString(5, fund.getExchangeCode());
            pstmnt.setString(6, fund.getMaster());
            return pstmnt;
        }, holder);
        final String fundId = String.valueOf(holder.getKey().intValue());
        final String[] shareIds = fund.getShareId() != null ? fund.getShareId().split(",") : new String[]{};
        final String[] coinIds = fund.getCoinId() != null ? fund.getCoinId().split(",") : new String[]{};
        final String[] percentages = fund.getPercentage().split(",");
        final String[] quantities = fund.getQuantity().split(",");
        int length = shareIds.length > 0 ? shareIds.length : coinIds.length;

        jdbcTemplate.batchUpdate(SQLQueries.insertFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                boolean shbool = shareIds.length - 1 >= i;
                boolean cobool = coinIds.length - 1 >= i;
                ps.setString(1, fundId);
                ps.setString(2, shbool ? shareIds[i] : null);
                ps.setString(3, percentages[i]);
                ps.setString(4, quantities[i]);
                ps.setString(5, fund.getCreatedDate());
                ps.setString(6, cobool ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });
    }

    @Override
    public void updateFund(ShareFund fund) {
        final String fundId = fund.getFundId();
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updateFund, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, fund.getName());
            pstmnt.setString(2, fund.getDescription());
            pstmnt.setString(3, fund.getCustodianId());
            pstmnt.setString(4, fund.getExchangeCode());
            pstmnt.setString(5, fund.getFundId());
            return pstmnt;
        }, holder);
        jdbcTemplate.update(SQLQueries.inActiveFundDetail, new Object[]{fundId});

        final String[] shareIds = fund.getShareId() != null ? fund.getShareId().split(",") : new String[]{};
        final String[] coinIds = fund.getCoinId() != null ? fund.getCoinId().split(",") : new String[]{};
        final String[] percentages = fund.getPercentage().split(",");
        final String[] quantities = fund.getQuantity().split(",");
        int length = shareIds.length > 0 ? shareIds.length : coinIds.length;
        jdbcTemplate.batchUpdate(SQLQueries.insertFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                boolean shbool = shareIds.length - 1 >= i;
                boolean cobool = coinIds.length - 1 >= i;
                ps.setString(1, fundId);
                ps.setString(2, shbool ? shareIds[i] : null);
                ps.setString(3, percentages[i]);
                ps.setString(4, quantities[i]);
                ps.setString(5, fund.getCreatedDate());
                ps.setString(6, cobool ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });
    }

    RowMapper fundMapper = (ResultSet rs, int i) -> {
        ShareFund fund = new ShareFund();
        fund.setFundId(rs.getString("id"));
        fund.setName(rs.getString("name"));
        fund.setDescription(rs.getString("desc"));
        fund.setExchangeCode(rs.getString("exchange_code"));
        fund.setCustodianId(rs.getString("custodian_id"));
        fund.setPercentage(rs.getString("interest_rate"));
        fund.setShares(rs.getString("shares"));//count of registered shares with fund
        return fund;
    };

    @Override
    public ShareFund fundById(String fundId, String investmentId) {
        if (fundId != null) {
            List<ShareFund> list = jdbcTemplate.query(SQLQueries.fundById, fundMapper, new Object[]{fundId});
            return !list.isEmpty() ? list.get(0) : null;
        } else if (investmentId != null) {
            List<ShareFund> list = jdbcTemplate.query(SQLQueries.fundByInvestmentId, fundMapper, new Object[]{investmentId});
            return !list.isEmpty() ? list.get(0) : null;
        }
        return null;
    }

    @Override
    public List<ShareFund> funds() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.funds, (ResultSet rs, int i) -> {
            ShareFund fund = new ShareFund();
            fund.setFundId(rs.getString("id"));
            fund.setName(rs.getString("name"));
            fund.setCurrencySymbol(rs.getString("curr_sym"));
            fund.setDescription(rs.getString("desc"));
            fund.setExchangeCode(rs.getString("exchange_code"));
            fund.setCustodianId(rs.getString("custodian_id"));
            fund.setPercentage(rs.getString("interest_rate"));
            String image = rs.getString("img");
            fund.setMaster(image != null ? image : "crypto-coins1.png");
            fund.setShares(rs.getString("shares"));//count of registered shares with fund
            return fund;
        });
        return list;
    }

    @Override
    public void updateFundPrice(PortfolioBean fund
    ) {
        jdbcTemplate.update(SQLQueries.updateOldFundPrice, new Object[]{fund.getFund_id()});
        jdbcTemplate.update(SQLQueries.insertFundPrice, new Object[]{fund.getFund_id(), fund.getNav(), fund.getDate(), fund.getTotal_usd()});
    }

    @Override
    public List<UserFundsCommand> userFunds(String fundId, String userId,
            String investmentId
    ) {
        String sql = " SELECT IM.user_id, IM.id investment_id, IM.customer_name, IM.investment_amount, IM.fund_id, F.name AS investment_name, F.desc FROM `investment` IM \n"
                + " INNER JOIN fund F ON (F.active = 'Y' AND F.id = IM.fund_id) WHERE IM.active = 'Y' ";
        if (fundId != null) {
            sql += " AND IM.fund_id  = " + fundId;
        }
        if (userId != null) {
            sql += " AND IM.user_id  = " + userId;
        }
        if (investmentId != null) {
            sql += " AND IM.id  = " + investmentId;
        }
        List<UserFundsCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserFundsCommand fcm = new UserFundsCommand();
            fcm.setUser_id(rs.getString("user_id"));
            fcm.setName(rs.getString("investment_name"));
            fcm.setInvestment_id(rs.getString("investment_id"));
            fcm.setInvestment_amount(rs.getString("investment_amount"));
            fcm.setFund_id(rs.getString("fund_id"));
            return fcm;
        });
        return list;
    }

    @Override
    public List<ShareFund> sharesByFund(String fundId
    ) {
        String sql = " SELECT S.*,SP.price,FD.percentage,FD.quantity,FD.fund_id, F.name as fund_name FROM fund_detail FD\n "
                + " inner join share_price SP ON (FD.share_id = SP.share_id and SP.active = 'Y')\n "
                + " inner join share S ON (S.active= 'Y' AND SP.share_id = S.id)\n "
                + " inner join fund F ON (F.active = 'Y' AND FD.fund_id = F.id)\n "
                + " WHERE FD.active = 'Y' AND FD.fund_id = " + fundId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            share.setPrice(rs.getString("price"));
            share.setPercentage(rs.getString("percentage"));
            share.setQuantity(rs.getString("quantity"));
            share.setFundId(rs.getString("fund_id"));
            share.setShareName(rs.getString("name"));
            share.setFundName(rs.getString("fund_name"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> coinsByFund(String fundId
    ) {
        String sql = " SELECT FD.*, F.name as fund_name FROM fund_detail FD \n"
                + " inner join fund F ON (F.active = 'Y' AND FD.fund_id = F.id) \n"
                + " WHERE FD.active = 'Y' AND FD.coin_id IS NOT NULL AND FD.fund_id = " + fundId;
        //id, fund_id, share_id, percentage, cdate, active, quantity, coin_id
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setFundId(rs.getString("fund_id"));
            share.setPercentage(rs.getString("percentage"));
            share.setQuantity(rs.getString("quantity"));
            share.setCoinId(rs.getString("coin_id"));
            share.setShareName(rs.getString("coin_id"));
            share.setFundName(rs.getString("fund_name"));
            return share;
        });
        return list;
    }

    @Override
    public void saveInvestment(InvestmentBean bean) {
        String sql = "SELECT CONVERT(id, CHAR(11)) as investment_id FROM investment IM WHERE IM.user_id = " + bean.getUserId() + " and fund_id = " + bean.getFundId();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        String investmentId = null;
        if (!list.isEmpty()) {
            Map<String, Object> fR = list.get(0);
            Object obj = fR.get("investment_id");
            investmentId = (String) obj;
        }
        if (investmentId != null) {
            jdbcTemplate.update(SQLQueries.updateInvestmentAmount, new Object[]{bean.getInvestmentAmount(), bean.getLocalInvestmentAmount(), investmentId});
        } else {
            GeneratedKeyHolder holder = new GeneratedKeyHolder();
            jdbcTemplate.update((java.sql.Connection con) -> {
                PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInvestment, Statement.RETURN_GENERATED_KEYS);
                pstmnt.setString(1, bean.getUserId());
                pstmnt.setString(2, bean.getCustomerName());
                pstmnt.setString(3, bean.getInvestmentAmount());
                pstmnt.setString(4, bean.getBankAccount());
                pstmnt.setString(5, bean.getReferenceNo());
                pstmnt.setString(6, bean.getFundId());
                pstmnt.setString(7, bean.getCreatedDate());
                pstmnt.setString(8, bean.getTimeFrame());
                pstmnt.setString(9, bean.getBankName());
                pstmnt.setString(10, bean.getYears());
                pstmnt.setString(11, bean.getRegularlyAmount());
                pstmnt.setString(12, bean.getLocalInvestmentAmount());
                pstmnt.setString(13, bean.getCurrency());
                return pstmnt;
            }, holder);
            investmentId = String.valueOf(holder.getKey().intValue());
        }
        bean.setInvestmentId(investmentId);
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInvestmentDetails, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, bean.getUserId());
            pstmnt.setString(2, bean.getInvestmentId());
            pstmnt.setString(3, bean.getCreatedDate());
            pstmnt.setString(4, bean.getInvestmentAmount());
            pstmnt.setString(5, bean.getLocalInvestmentAmount());
            pstmnt.setString(6, bean.getCurrency());
            return pstmnt;
        }, holder);
        holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInvestmentRequest, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, bean.getUserId());
            pstmnt.setString(2, bean.getInvestmentId());
            pstmnt.setString(3, bean.getInvestmentAmount());
            pstmnt.setString(4, "DEPOSITE");
            pstmnt.setString(5, bean.getCreatedDate());
            pstmnt.setString(6, bean.getDescription());
            pstmnt.setString(7, "PENDING");
            pstmnt.setString(8, bean.getLocalInvestmentAmount());
            pstmnt.setString(9, bean.getCurrency());
            pstmnt.setString(10, bean.getPercentage());
            return pstmnt;
        }, holder);
        String investmentRequestId = String.valueOf(holder.getKey().intValue());
        List<ShareFund> fundActualShares = bean.getFundActualShares();
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentPendingShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, bean.getCreatedDate());
                ps.setString(2, bean.getUserId());
                ps.setString(3, bean.getInvestmentId());
                ps.setString(4, fundActualShares.get(i).getShareId());
                ps.setString(5, fundActualShares.get(i).getPrice());
                ps.setString(6, fundActualShares.get(i).getQuantity());
                ps.setString(7, "PENDING");
                ps.setString(8, fundActualShares.get(i).getCoinId());
                ps.setString(9, investmentRequestId);
            }

            @Override
            public int getBatchSize() {
                return fundActualShares.size();
            }
        });
    }

    @Override
    public List<InvestmentBean> userInvestmentRequests(String userId, String status, String id, String tokenNo) {
        if (id == null && tokenNo != null) {
            id = tokenizer.fetchRecordId(tokenNo, "IR");
        }
        String sql = " SELECT IR.created_ts, IR.id as req_id, IM.user_id, IM.id as investment_id, U.ref_id, U.full_name as customer_name, U.email, F.name as investment_name, IR.amount as investment_amount, IR.local_amount as local_investment_amount, IM.time_frame, IM.currency, IR.action\n"
                + " FROM investment IM \n"
                + " INNER JOIN user_master U ON(IM.user_id = U.user_id) \n"
                + " INNER JOIN investment_request IR ON(IM.id = IR.investment_id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id)\n"
                + " WHERE  IR.status = '" + status + "' ";
        if (id != null) {
            sql += " AND IR.id = " + id;
        }
        if (userId != null) {
            sql += " AND IR.user_id = " + userId;
        }
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setCreatedDate(rs.getString("created_ts"));
            investment.setReqId(rs.getString("req_id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setEmail(rs.getString("email"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setCurrency(rs.getString("currency"));
            investment.setAction(rs.getString("action"));
            investment.setRefId(rs.getString("ref_id"));
            investment.setTranId(tokenizer.tokenNo(investment.getRefId(), "IR", investment.getReqId()));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> pendingInvestmentRequests(String userId, String id, String tokenNo) {
        if (id == null && tokenNo != null) {
            id = tokenizer.fetchRecordId(tokenNo, "IR");
        }
        String sql = "  SELECT IR.created_ts, IR.id as req_id, IM.user_id, IM.id as investment_id, U.ref_id, U.full_name as customer_name, U.email, F.name as investment_name, IR.amount as investment_amount, IR.local_amount as local_investment_amount, IM.time_frame, IM.currency,A.shares, IR.action\n"
                + " FROM investment IM \n"
                + " INNER JOIN user_master U ON(IM.user_id = U.user_id) \n"
                + " INNER JOIN investment_request IR ON(IM.id = IR.investment_id)\n"
                + " INNER JOIN (SELECT UST2.investment_id, UST2.inv_req_id, group_concat(' ', UST2.a_share) as shares, UST2.action FROM (SELECT USPT.investment_id, USPT.inv_req_id, case when S.name is not null then S.name else USPT.coin_id end as a_share, USPT.action FROM user_shares_pending_transaction USPT LEFT JOIN share S ON(S.active= 'Y' AND S.id = USPT.share_id) WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y') UST2 GROUP BY investment_id, inv_req_id\n"
                + " ) A ON (A.investment_id = IM.id AND A.inv_req_id = IR.id) \n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id) \n"
                + " WHERE  IR.action = 'DEPOSITE' AND IR.status = 'PENDING' ";
        if (id != null) {
            sql += " AND IR.id = " + id;
        }
        if (userId != null) {
            sql += " AND IR.user_id = " + userId;
        }

        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setCreatedDate(rs.getString("created_ts"));
            investment.setReqId(rs.getString("req_id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setEmail(rs.getString("email"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setShares(rs.getString("shares"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setCurrency(rs.getString("currency"));
            investment.setAction(rs.getString("action"));
            investment.setRefId(rs.getString("ref_id"));
            investment.setTranId(tokenizer.tokenNo(investment.getRefId(), "IR", investment.getReqId()));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId,
            String fundId
    ) {
        String sql = "   SELECT IM.*, F.name as fund_name, S.name as share_name, UST.share_id, UST.quantity, UST.price, "
                + " UST.price * UST.quantity as value, UST.local_value FROM investment IM\n"
                + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                + " INNER JOIN share S ON(S.active= 'Y' AND UST.share_id = S.id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'FUND')\n"
                + " WHERE IM.active = 'Y' \n";
        if (fundId != null) {
            sql += " AND IM.fund_id = " + fundId + " \n";
        }
        if (userId != null) {
            sql += " AND IM.user_id = " + userId + " \n";
        }
        if (investmentId != null) {
            sql += " AND IM.id = " + investmentId + " \n";
        }
        sql += " ORDER BY id;  ";
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setInvestmentId(rs.getString("id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setRegularlyAmount(rs.getString("regularly_amount"));
            investment.setInterestRate(rs.getString("interest_rate"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setYears(rs.getString("years"));
            investment.setBankAccount(rs.getString("bank_acc"));
            investment.setBankName(rs.getString("bank_name"));
            investment.setReferenceNo(rs.getString("reference_no"));
            investment.setFundName(rs.getString("fund_name"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setShareName(rs.getString("share_name"));
            investment.setShareId(rs.getString("share_id"));
            investment.setQuantity(rs.getString("quantity"));
            investment.setPrice(rs.getString("price"));
            investment.setValue(rs.getString("value"));
            investment.setLocalValue(rs.getString("local_value"));
            investment.setCurrency(rs.getString("currency"));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId) {
        StringBuilder sql = new StringBuilder("SELECT U.ref_id, U.full_name, U.email, IM.*, F.name as fund_name, F.img as fund_image, UST.id as ust_id, UST.created_ts as created_date, UST.share_id, UST.coin_id, UST.share_id as as_share,\n"
                + " case when S.name is not null then S.name else UST.coin_id end as share_name, FD.percentage as target_percentage,   \n"
                + " sum(case when action = 'SOLD' then - UST.quantity else +UST.quantity end) as quantity, UST.price, \n"
                + " sum(case when action = 'SOLD' then - (UST.price * UST.quantity) else + (UST.price * UST.quantity) end) as value, \n"
                + " sum(case when action = 'SOLD' then - UST.local_value else +UST.local_value end) as local_value, \n"
                + " coalesce(FP.price, 0) as nav, coalesce(UT.units_qty, 0) as units_qty \n"
                + " FROM investment IM INNER JOIN user_master U ON(IM.user_id = U.user_id) \n"
                + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                + " LEFT JOIN share S on(UST.share_id = S.id) \n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'PORTFOLIO') \n"
                + " INNER JOIN fund_detail FD ON(FD.fund_id = F.id and FD.active = 'Y' and (FD.share_id = UST.share_id or FD.coin_id = UST.coin_id))\n"
                + " LEFT JOIN fund_price FP ON(FP.fund_id = F.id and FP.active = 'Y') \n"
                + " LEFT JOIN (SELECT user_id, investment_id, SUM(CASE WHEN action = 'SOLD' THEN -minto ELSE minto END) AS units_qty FROM user_inv_units_txn GROUP BY user_id, investment_id) UT ON(UT.user_id = IM.user_id and UT.investment_id = IM.id)\n"
                + " WHERE IM.active = 'Y' \n");
        if (userId != null) {
            sql.append(" AND IM.user_id = ").append(userId).append(" \n");
        }
        if (investmentId != null) {
            sql.append(" AND IM.id = ").append(investmentId).append(" \n");
        }
        sql.append(" GROUP BY IM.user_id, IM.id, share_name ORDER BY id; ");
        List<InvestmentBean> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setRefId(rs.getString("ref_id"));
            investment.setCustomerName(rs.getString("full_name"));
            investment.setEmail(rs.getString("email"));
            investment.setInvestmentId(rs.getString("id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setRegularlyAmount(rs.getString("regularly_amount"));
            investment.setInterestRate(rs.getString("interest_rate"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setYears(rs.getString("years"));
            investment.setBankAccount(rs.getString("bank_acc"));
            investment.setBankName(rs.getString("bank_name"));
            investment.setReferenceNo(rs.getString("reference_no"));
            investment.setFundName(rs.getString("fund_name"));
            String image = rs.getString("fund_image");
            image = image != null ? image : "crypto-coins1.png";
            investment.setFundImage(image);
            investment.setFundId(rs.getString("fund_id"));
            investment.setFundNav(rs.getString("nav"));
            investment.setUnits(rs.getString("units_qty"));
            investment.setCreatedDate(rs.getString("created_date"));
            investment.setAction(rs.getString("as_share"));
            investment.setShareName(rs.getString("share_name"));
            investment.setShareId(rs.getString("share_id"));
            investment.setCoinId(rs.getString("coin_id"));
            investment.setTargetPercentage(rs.getString("target_percentage"));
            investment.setQuantity(rs.getString("quantity"));
            investment.setPrice(rs.getString("price"));
            investment.setValue(rs.getString("value"));
            investment.setLocalValue(rs.getString("local_value"));
            investment.setCurrency(rs.getString("currency"));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> purchasedInvestmentsAndCash(String userId, String investmentId, String fundId) {
        StringBuilder sql = new StringBuilder(" SELECT IMAC.*, IMAC.units_qty * IMAC.nav AS investment_amount FROM (\n"
                + " SELECT U.ref_id, U.full_name, U.email, UT.user_id, UT.investment_id, IM.fund_id, UT.units_qty, FP.price as nav, IM.active, F.name as portfolio FROM investment IM \n"
                + " INNER JOIN user_master U ON(U.user_id = IM.user_id)\n"
                + " INNER JOIN (SELECT user_id, investment_id, SUM(CASE WHEN action = 'SOLD' THEN -minto ELSE minto END) AS units_qty FROM user_inv_units_txn WHERE active = 'Y' GROUP BY user_id, investment_id)  UT ON(UT.user_id = IM.user_id and UT.investment_id = IM.id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id) \n"
                + " INNER JOIN fund_price FP ON(FP.fund_id = F.id and FP.active = 'Y') \n"
                + " WHERE IM.active = 'Y' \n"
                + " UNION ALL \n"
                + " SELECT AWT.ref_id, AWT.full_name, AWT.email, AWT.user_id, 0 as investment_id , 0 as fund_id, AWT.actual_wallet_balance as units_qty , 1 as nav, AWT.active, 'US$ Cash' as portfolio FROM \n"
                + " (SELECT U.ref_id, U.full_name, U.email, U.user_id, U.active, coalesce(WT.wallet_balance, 0) AS wallet_balance, coalesce(UIP.pending_investment_amount, 0) AS pending_investment_amount,coalesce(WT.wallet_balance, 0) - coalesce(UIP.pending_investment_amount, 0) AS actual_wallet_balance, coalesce(WT.id, 0) AS last_txn_id FROM user_master U\n"
                + " LEFT JOIN (SELECT MAX(id) AS id, user_id, SUM(CASE WHEN WT.inc_dec = 'Dec' THEN -WT.amount ELSE WT.amount END) as wallet_balance FROM wallet_txns WT WHERE WT.active = 'Y' and WT.approved = 'Y' GROUP BY user_id) WT ON(WT.user_id = U.user_id)\n"
                + " LEFT JOIN (SELECT user_id, coalesce(SUM(amount),0) AS pending_investment_amount FROM (SELECT USPT.created_ts, USPT.investment_id, USPT.user_id, ROUND(SUM(USPT.price * USPT.quantity),2) as amount, USPT.action from user_shares_pending_transaction USPT WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' GROUP BY investment_id) UPA GROUP BY user_id) UIP ON(UIP.user_id = WT.user_id)\n"
                + " ) AWT \n"
                + " ORDER BY user_id, investment_id) IMAC WHERE IMAC.active = 'Y' ");
        if (userId != null) {
            sql.append(" AND IMAC.user_id = ").append(userId);
        }
        if (investmentId != null) {
            sql.append(" AND IMAC.investment_id = ").append(investmentId);
        }
        List<InvestmentBean> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setRefId(rs.getString("ref_id"));
            investment.setCustomerName(rs.getString("full_name"));
            investment.setEmail(rs.getString("email"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setFundNav(rs.getString("nav"));
            investment.setUnits(rs.getString("units_qty"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> findManagementInvestments(String branchId, String fundId,
            String investmentId, String userInvId) {
        String sql = " SELECT IM.* FROM investment IM\n"
                + "  INNER JOIN user_master U ON(U.user_id = IM.user_id)\n"
                + "  INNER JOIN role_master R ON(R.user_id = U.user_id AND R.role = 'ROLE_MANAGEMENT')\n"
                + "  WHERE IM.active ='Y'";
        if (fundId != null) {
            sql += " \nAND IM.fund_id = " + fundId;
        }
        if (investmentId != null) {
            sql += " \nAND IM.id = " + investmentId;
        }
        if (userInvId != null) {
            sql += "  \nAND IM.fund_id IN (SELECT IM.fund_id FROM investment IM WHERE IM.id = " + userInvId + "); ";
        }
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setInvestmentId(rs.getString("id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setInterestRate(rs.getString("interest_rate"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setYears(rs.getString("years"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setCurrency(rs.getString("currency"));
            return investment;
        });
        return list;
    }

    @Override
    public List<ShareFund> totalCoins(String userId, String investmentId
    ) {
        StringBuilder sql
                = new StringBuilder("SELECT C.coin_id, sum(C.quantity) as quantity FROM\n"
                        + " (SELECT UST.coin_id, UST.quantity\n"
                        + " FROM investment IM\n"
                        + " INNER JOIN user_master U ON(IM.user_id = U.user_id)\n"
                        + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                        + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'PORTFOLIO')\n"
                        + " WHERE IM.active = 'Y' AND UST.coin_id IS NOT NULL \n");
        if (userId != null) {
            sql.append(" AND IM.user_id = ").append(userId).append(" \n");
        }
        if (investmentId != null) {
            sql.append(" AND IM.id = ").append(investmentId).append(" \n");
        }
        sql.append(" ) C GROUP BY coin_id;  ");
        List<ShareFund> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            ShareFund coin = new ShareFund();
            coin.setCoinId(rs.getString("coin_id"));
            coin.setQuantity(rs.getString("quantity"));
            return coin;
        });
        return list;

    }

    @Override
    public List<ShareFund> pendingSharesByInvestment(String investmentId, String reqId) {
        String sql = " SELECT user_id, investment_id, share_id, coin_id, case when USPT.share_id is not null THEN S.name ELSE USPT.coin_id END as a_share, USPT.price, USPT.quantity, USPT.price * USPT.quantity AS amount\n"
                + " FROM `user_shares_pending_transaction` USPT\n"
                + " LEFT JOIN `share` S ON (S.active= 'Y' AND S.id = USPT.share_id)\n"
                + " WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' AND USPT.investment_id = " + investmentId;
        if (reqId != null) {
            sql += " AND USPT.inv_req_id = " + reqId;
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setCoinId(rs.getString("coin_id"));
            share.setName(rs.getString("a_share"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> pendingCoinsByInvestment(String investmentId
    ) {
        String sql = "  SELECT user_id, investment_id, coin_id, USPT.price, USPT.quantity, USPT.price * USPT.quantity AS amount\n"
                + " FROM `user_shares_pending_transaction` USPT\n"
                + " WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' AND USPT.`coin_id` IS NOT NULL AND USPT.investment_id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setCoinId(rs.getString("coin_id"));
            share.setName(rs.getString("coin_id"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            return share;
        });
        return list;
    }

    @Override
    public void updatePendingShares(ShareFund investmentShares) {
        String sql = "UPDATE `user_shares_pending_transaction` SET `active` = 'N'"
                + " WHERE `user_id` = ? and `investment_id` = ? and `inv_req_id` = ?"
                + " and `action` = 'PENDING'";
        jdbcTemplate.update(sql, new Object[]{investmentShares.getUserId(), investmentShares.getInvestmentId(), investmentShares.getReqId()});

        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentPendingShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "DONE");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
                ps.setString(9, investmentShares.getReqId());
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
    }

    @Override
    public void saveSellInvestmentRequest(AccountEventCommand event
    ) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInvestmentRequest, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, event.getUserId());
            pstmnt.setString(2, event.getInvestmentId());
            pstmnt.setString(3, event.getAmount());
            pstmnt.setString(4, event.getActionType());
            pstmnt.setString(5, event.getCreateDate());
            pstmnt.setString(6, event.getDescription());
            pstmnt.setString(7, "PENDING");
            pstmnt.setString(8, event.getLocal_amount());
            pstmnt.setString(9, event.getCurrency());
            pstmnt.setString(10, event.getPercentage());
            return pstmnt;
        }, holder);
    }

    @Override
    public TransactionBean purchaseShares(ShareFund investmentShares
    ) {
        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentUpdatedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "PURCHASED");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
                ps.setString(9, investmentShares.getRefInvId());
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < maxlength; i++) {
            total = total.add(new BigDecimal(prices[i]).multiply(new BigDecimal(quantities[i])));
        }
        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(investmentShares.getCreatedDate());
        tran.setUser_id(investmentShares.getUserId());
        tran.setInvestment_id(investmentShares.getInvestmentId());
        tran.setRef_inv_id(investmentShares.getRefInvId());
        tran.setTxn_Req_id(investmentShares.getReqId());
        tran.setBase("IR");
        tran.setParticulars(Constants.depositParticulars);
        tran.setInc_dec("Inc");
        tran.setAmount(total.toPlainString());
        tran.setLocal(investmentShares.getLocal() != null && !investmentShares.getLocal().isEmpty() ? investmentShares.getLocal() : "0.00");
        tran.setUsd(investmentShares.getUsd() != null && !investmentShares.getUsd().isEmpty() ? investmentShares.getUsd() : "0.00");
        tran.setBtc(investmentShares.getBtc() != null && !investmentShares.getBtc().isEmpty() ? investmentShares.getBtc() : "0.00");
        tran.setMinto(investmentShares.getMinto() != null && !investmentShares.getMinto().isEmpty() ? investmentShares.getMinto() : "0.00");
//        save(tran);
        if (tran.getMinto() != null) {
            tran.setParticulars("PURCHASED");
            saveUnitTransaction(tran);
        }
        tran.setParticulars("Purchased Shares/ Coins");
        tran.setInc_dec("Dec");
        saveWalletTransaction(tran);
        try {
            if (investmentShares.getReqId() != null && Integer.parseInt(investmentShares.getReqId()) > 0) {
                jdbcTemplate.update("UPDATE `investment_request` SET `status`='DONE' WHERE `id`=?;", new Object[]{investmentShares.getReqId()});
            }
        } catch (Exception ex) {

        }
        return tran;
    }

    @Override
    public void updatePurchasedShares(ShareFund updatedShares
    ) {
        String[] ids = updatedShares.getId().split(",");
        String[] prices = updatedShares.getPrice().split(",");
        String[] quantities = updatedShares.getQuantity().split(",");
        jdbcTemplate.batchUpdate(SQLQueries.updatePurchasedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, prices[i]);
                ps.setString(2, quantities[i]);
                ps.setString(3, ids[i]);
            }

            @Override
            public int getBatchSize() {
                return ids.length;
            }
        });
    }

    @Override
    public List<ShareFund> sharesByInvestment(String investmentId) {
        String sql = "  SELECT UST.created_ts, UST.user_id, UST.investment_id, UST.a_share, UST.share_id, UST.coin_id, UST.price,\n"
                + " sum(CASE WHEN UST.action = 'PURCHASED' THEN + UST.quantity ELSE - UST.quantity END) as quantity,\n"
                + " sum(CASE WHEN UST.action = 'PURCHASED' THEN + UST.value ELSE - UST.value END) AS value,\n"
                + " UST.local_value, UST.action FROM \n"
                + " (select UST.created_ts, UST.user_id, UST.investment_id, S.name as a_share, UST.share_id, UST.coin_id,  UST.price, UST.quantity, UST.price * UST.quantity as value, UST.local_value, UST.action from user_shares_transaction UST \n"
                + " inner join share S ON(S.active= 'Y' AND S.id = UST.share_id)\n"
                + " where UST.`active` = 'Y' \n"
                + " union all \n"
                + " select UST.created_ts, UST.user_id, UST.investment_id, UST.coin_id as a_share, UST.share_id, UST.coin_id, UST.price, UST.quantity, UST.price * UST.quantity as value, UST.local_value, UST.action from user_shares_transaction UST \n"
                + " where UST.`active` = 'Y' AND UST.`coin_id` is not null )\n"
                + " UST WHERE UST.investment_id = " + investmentId + " GROUP BY a_share ";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setName(rs.getString("a_share"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("value"));
            share.setCoinId(rs.getString("coin_id"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> detailsByInvestment(String userId, String investmentId,
            String action
    ) {
        String sql = " SELECT UST.id, UST.created_ts, UST.user_id, UST.investment_id, UST.share_id, UST.price, UST.quantity,UST.price * UST.quantity as amount, UST.active, UST.action, UST.coin_id, S.name as share_name FROM user_shares_transaction UST\n"
                + "left join share S ON (UST.share_id = S.id) \n"
                + "where UST.user_id = " + userId;
        if (investmentId != null) {
            sql += " and UST.investment_id = " + investmentId;
        }
        if (action != null) {
            sql += " and UST.action = '" + action + "'";
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setId(rs.getString("id"));
            share.setCreatedDate(rs.getString("created_ts"));
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            share.setMaster(rs.getString("action"));
            share.setCoinId(rs.getString("coin_id"));
            share.setName(rs.getString("share_name"));
            return share;
        });
        return list;
    }

    @Override
    public InvestmentBean sumOfpendingSellTransactions(String userId, String investmentId) {
        String sql = " SELECT min(IR.created_ts) as created_ts, count(IR.id) as req_id, IR.user_id, IR.investment_id, IM.fund_id, U.ref_id, U.full_name as customer_name, U.email, F.name as investment_name, sum(IR.amount) as amount, IR.action, sum(IR.local_amount) as local_amount, IR.currency, sum(IR.percentage) as percentage\n"
                + " FROM investment_request IR\n"
                + " INNER JOIN investment IM ON(IR.`investment_id` = IM.`id`)\n"
                + " INNER JOIN user_master U ON(U.user_id = IM.user_id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id)\n"
                + " WHERE IR.`active` = 'Y' AND IR.`action` = 'WITHDRAWL' AND IR.`status` = 'PENDING' \n"
                + " AND IR.user_id = " + userId + " AND IR.investment_id = " + investmentId;
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setCreatedDate(rs.getString("created_ts"));
            investment.setReqId(rs.getString("req_id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setRefId(rs.getString("ref_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setEmail(rs.getString("email"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setInvestmentAmount(rs.getString("amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_amount"));
            investment.setCurrency(rs.getString("currency"));
            investment.setPercentage(rs.getString("percentage"));
            investment.setAction(rs.getString("action"));
            investment.setTranId(tokenizer.tokenNo(investment.getRefId(), "IR", investment.getReqId()));
            return investment;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public List<InvestmentBean> pendingSellTransactions(String userId, String id, String tokenNo) {
        if (id == null && tokenNo != null) {
            id = tokenizer.fetchRecordId(tokenNo, "IR");
        }
        String sql = "  SELECT IR.created_ts, IR.id as req_id, IR.user_id, IR.investment_id, IM.fund_id, U.ref_id, U.full_name as customer_name, U.email, F.name as investment_name, IR.amount, IR.action, IR.local_amount, IR.currency, IR.percentage, A.shares\n"
                + " FROM investment_request IR\n"
                + " INNER JOIN investment IM ON(IR.`investment_id` = IM.`id`)\n"
                + " INNER JOIN (\n"
                + " select UST.investment_id, group_concat(' ', UST.a_share) as shares, UST.action from ( select UST.investment_id, case when UST.share_id is not null then S.name else UST.coin_id  end as a_share , UST.action from user_shares_transaction UST left join share S ON(S.active= 'Y' AND S.id = UST.share_id) where UST.`active` = 'Y' ) UST group by investment_id\n"
                + " )A ON (A.investment_id = IM.id)\n"
                + " INNER JOIN user_master U ON(U.user_id = IM.user_id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id)\n"
                + " WHERE IR.`active` = 'Y' AND IR.`action` = 'WITHDRAWL' AND IR.`status` = 'PENDING' ";
        if (id != null) {
            sql += " AND IR.id = " + id;
        }
        if (userId != null) {
            sql += " AND IR.user_id = " + userId;
        }
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setCreatedDate(rs.getString("created_ts"));
            investment.setReqId(rs.getString("req_id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setRefId(rs.getString("ref_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setEmail(rs.getString("email"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setInvestmentAmount(rs.getString("amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_amount"));
            investment.setCurrency(rs.getString("currency"));
            investment.setPercentage(rs.getString("percentage"));
            investment.setAction(rs.getString("action"));
            investment.setTranId(tokenizer.tokenNo(investment.getRefId(), "IR", investment.getReqId()));
            return investment;
        });
        return list;
    }

    @Override
    public void investmentRequestStatus(String id, String status) {
        if (id != null && Integer.parseInt(id) > 0) {
            jdbcTemplate.update("UPDATE `investment_request` SET `status`=? WHERE `id`=?;", new Object[]{status, id});
        }
    }

    @Override
    public void deactivePendingSharesStatus(String userId, String investmentId, String status) {
        String sql = "UPDATE `user_shares_pending_transaction` SET `active` = 'N'"
                + " WHERE `user_id` = ? and `investment_id` = ?"
                + " and `action` = ?";
        jdbcTemplate.update(sql, new Object[]{userId, investmentId, status});

    }

    @Override
    public void sellShares(ShareFund investmentShares) {
        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentUpdatedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "SOLD");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
                ps.setString(9, investmentShares.getRefInvId());
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < maxlength; i++) {
            total = total.add(new BigDecimal(prices[i]).multiply(new BigDecimal(quantities[i])));
        }
        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(investmentShares.getCreatedDate());
        tran.setUser_id(investmentShares.getUserId());
        tran.setInvestment_id(investmentShares.getInvestmentId());
        tran.setRef_inv_id(investmentShares.getRefInvId());
        tran.setTxn_Req_id(investmentShares.getReqId());
        tran.setBase("IR");
        tran.setParticulars(Constants.withdrawlParticulars);
        tran.setInc_dec("Inc");
        tran.setAmount(total.toPlainString());
        tran.setLocal(investmentShares.getLocal() != null && !investmentShares.getLocal().isEmpty() ? investmentShares.getLocal() : "0.00");
        tran.setUsd(investmentShares.getUsd() != null && !investmentShares.getUsd().isEmpty() ? investmentShares.getUsd() : "0.00");
        tran.setBtc(investmentShares.getBtc() != null && !investmentShares.getBtc().isEmpty() ? investmentShares.getBtc() : "0.00");
        tran.setMinto(investmentShares.getMinto() != null && !investmentShares.getMinto().isEmpty() ? investmentShares.getMinto() : "0.00");
//        save(tran);
        if (tran.getMinto() != null) {
            tran.setParticulars("SOLD");
            saveUnitTransaction(tran);
        }
        tran.setParticulars("Sold Shares/ Coins");
        tran.setInc_dec("Inc");
        saveWalletTransaction(tran);
        jdbcTemplate.update("UPDATE `investment` SET `investment_amount`=`investment_amount` - ? WHERE `id`=?;", new Object[]{total, investmentShares.getInvestmentId()});
        List<UserFundsCommand> userFunds = userFunds(null, investmentShares.getUserId(), investmentShares.getInvestmentId());
        for (UserFundsCommand inv : userFunds) {
            if (investmentShares.getInvestmentId().equalsIgnoreCase(inv.getInvestment_id())) {
                BigDecimal investmentAmount = new BigDecimal(inv.getInvestment_amount());
                if (investmentAmount.compareTo(BigDecimal.ZERO) == -1) {
                    jdbcTemplate.update("UPDATE `investment` SET `investment_amount`=? WHERE `id`=?;", new Object[]{BigDecimal.ZERO, investmentShares.getInvestmentId()});
                }
            }
        }
        try {
            investmentRequestStatus(investmentShares.getReqId(), "DONE");
        } catch (Exception ex) {

        }
    }

    @Override
    public List<ShareFund> actualSharePercentages(String investmentId
    ) {
        String sql = " select IM.user_id, IM.id as investment_id, S.*, SP.price, FD.percentage from investment IM \n"
                + "inner join fund_detail FD ON(IM.fund_id = FD.fund_id)\n"
                + "inner join share S ON(S.active= 'Y' AND FD.share_id = S.id)\n"
                + "inner join share_price SP ON (SP.share_id = S.id) \n"
                + "where IM.id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            share.setPrice(rs.getString("price"));
            share.setPercentage(rs.getString("percentage"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> actualCoinPercentages(String investmentId
    ) {
        String sql = " select IM.user_id, IM.id as investment_id, FD.fund_id, FD.coin_id, FD.percentage from investment IM \n"
                + "inner join fund_detail FD ON(IM.fund_id = FD.fund_id and  FD.active = 'Y')\n"
                + "inner join fund F ON(IM.fund_id = F.id and F.master = 'PORTFOLIO') where IM.id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
//                share.setShareId(rs.getString("id"));
//                share.setName(rs.getString("name"));
//                share.setDescription(rs.getString("desc"));
//                share.setExchangeCode(rs.getString("exchange_code"));
//                share.setCustodianId(rs.getString("custodian_id"));
//                share.setPrice(rs.getString("price"));
            share.setCoinId(rs.getString("coin_id"));
            share.setPercentage(rs.getString("percentage"));
            return share;
        });
        return list;
    }

    @Override
    public List<AccountEventCommand> latestInvestments(String userId
    ) {
        String sql = " select * FROM investment_request\n"
                + " where user_id = " + userId + " limit 5";

        List<AccountEventCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            AccountEventCommand trns = new AccountEventCommand();
            trns.setActionType(rs.getString("action"));
            trns.setStatus(rs.getString("status"));
            trns.setAmount(rs.getString("amount"));
            trns.setCreateDate(rs.getString("created_ts"));
            trns.setDescription(rs.getString("description"));
            return trns;
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> totals() {
        String sql = " SELECT FLOOR(count(*)) as total, 'user_master' AS tbl FROM user_master where active = 'Y'\n"
                + " UNION ALL\n"
                + " SELECT FLOOR(count(*)) as total, 'investment' as tbl FROM investment where active = 'Y'\n"
                + " UNION ALL\n"
                + " SELECT FLOOR(SUM(CASE WHEN action = 'PURCHASED' THEN + (price *quantity) ELSE - (price * quantity) END)) as total, 'user_shares_transaction' as tbl FROM user_shares_transaction WHERE active = 'Y'; ";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<ShareFund> latestBalanceData(String type, String userId,
            String investmentId
    ) {
        String sql = " SELECT id, created_ts, user_id, investment_id, share_id, price, quantity, active, action, coin_id FROM user_shares_transaction  "
                + " WHERE coin_id in (SELECT coin_id FROM user_shares_transaction WHERE active = 'Y' ";
        if (userId != null) {
            sql += " and user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and investment_id = " + investmentId;
        }
        sql += " group by coin_id) ";
        if (userId != null) {
            sql += " and user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and investment_id = " + investmentId;
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund coin = new ShareFund();
            coin.setCreatedDate(rs.getString("created_ts"));
            coin.setUserId(rs.getString("user_id"));
            coin.setInvestmentId(rs.getString("investment_id"));
            coin.setPrice(rs.getString("price"));
            coin.setQuantity(rs.getString("quantity"));
            coin.setCoinId(rs.getString("coin_id"));
            coin.setMaster(rs.getString("action"));
            return coin;
        });
        return list;
    }

    @Override
    public List<TransactionBean> userAccountSummary(String userId
    ) {
        String sql = " SELECT date(T1.created_ts) as date, T1.*, SUM(CASE WHEN T2.inc_dec = 'Inc' THEN T2.amount ELSE - T2.amount END) AS balance FROM transactions T1\n"
                + " INNER JOIN transactions T2 ON (T2.id <= T1.id and T1.user_id = T2.user_id) \n"
                + " WHERE T1.user_id = " + userId + " AND T1.approved = 'Y'  \n"
                + " GROUP BY T1.id; ";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("date"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setInvestment_id(rs.getString("investment_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setBalance(rs.getString("balance"));
            return tran;
        }//id, created_ts, user_id, investment_id, particulars, inc_dec, amount, active, balance
        );
        return list;
    }
//ref_id, user_id, investment_id, name, fund_id, minto, btc, created_ts, bitcoin

    @Override
    public List<ShareFund> userUnitSummary(String userId, String fromDate,
            String toDate
    ) {
        String sql = " SELECT UT.*, F.name, F.id as fund_id, FP.price as unit_price\n"
                + " FROM user_inv_units_txn UT \n"
                + " inner join investment IM on(UT.investment_id = IM.id) \n"
                + " inner join fund F on(F.id = IM.fund_id) \n"
                + " left join fund_price FP on(FP.fund_id = F.id and \n"
                + " date(FP.created_ts) = date(UT.created_ts))\n"
                + " inner join user_master U on(U.user_id = UT.user_id) \n"
                + " where UT.user_id = " + userId;
        if (fromDate != null && !fromDate.isEmpty()) {
            sql += " and UT.created_ts >= '" + fromDate + "' \n";
        }
        if (toDate != null && !toDate.isEmpty()) {
            sql += " and UT.created_ts <= '" + toDate + "' \n";
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund unit = new ShareFund();
            unit.setCreatedDate(rs.getString("created_ts"));
            unit.setUserId(rs.getString("user_id"));
            unit.setInvestmentId(rs.getString("investment_id"));
            unit.setFundName(rs.getString("name"));
            unit.setFundId(rs.getString("fund_id"));
            unit.setMinto(rs.getString("minto"));
            unit.setBtc(rs.getString("btc"));
            unit.setUsd(rs.getString("usd"));
            unit.setLocal(rs.getString("nzd"));
            unit.setPrice(rs.getString("unit_price"));
            unit.setMaster(rs.getString("action"));
            return unit;
        });
        return list;
    }

    private static final RowMapper<InviteCode> inviteCodeRowMapper = (ResultSet rs, int i) -> {
        InviteCode code = new InviteCode();
        code.setId(rs.getString("id"));
        code.setInviteCode(rs.getString("invite_code"));
        code.setExpiryTs(rs.getString("expiry_ts"));
        code.setCreatedBy(rs.getString("created_by"));
        code.setCreatedTs(rs.getString("created_ts"));
        code.setExpired(rs.getString("expired"));
        code.setUsed(rs.getString("used"));
        code.setActive(rs.getString("active"));
        code.setPurpose(rs.getString("purpose"));
        code.setEmailId(rs.getString("email"));
        code.setName(rs.getString("name"));
        return code;
    };

    @Override
    public List<InviteCode> inviteCodes(Boolean available, Boolean expired, Boolean used) {
        String sql = " SELECT * FROM invite_code where active = 'Y'; ";
        List<InviteCode> list = jdbcTemplate.query(sql, inviteCodeRowMapper);
        return list;
    }

    @Override
    public List<InviteCode> checkInviteCode(String code, String type) {
        String sql = " select * from invite_code where active = 'Y' and expired = 'N' and used = 'N' and expiry_ts > now()"
                + " and invite_code = '" + code + "' and purpose = '" + type + "' ; ";
        List<InviteCode> list = jdbcTemplate.query(sql, inviteCodeRowMapper);
        return list;
    }

    @Override
    public List<StateCity> countries(Boolean states, Boolean cities) {
        String sql = " select C.id, C.sortname code, C.name country, C.phonecode, S.id as state_id, S.name state from countries C\n"
                + "inner join states S on(C.id = S.country_id) order by id asc; ";
        List<StateCity> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            StateCity code = new StateCity();
            code.setId(rs.getString("id"));
            code.setCode(rs.getString("code"));
            code.setName(rs.getString("country"));
            code.setPhoneCode(rs.getString("phonecode"));
            code.setDetName(rs.getString("state"));
            code.setDetId(rs.getString("state_id"));
            return code;
        });
        return list;
    }

    @Override
    public Boolean deActiveShare(String shareId) {
        int id = jdbcTemplate.update(SQLQueries.deActiveShare, new Object[]{shareId});
        return id > 0;
    }

    @Override
    public Boolean deActiveFund(String fundId) {
        int id = jdbcTemplate.update(SQLQueries.deActiveFund, new Object[]{fundId});
        return id > 0;
    }

    @Override
    public void saveVerification(Verification ver) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertVerification, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, ver.getPp_passport_number());
            pstmnt.setString(2, ver.getPp_passport_expiry_date());
            pstmnt.setString(3, ver.getPp_filename());
            pstmnt.setString(4, ver.getPa_same_res_addr());
            pstmnt.setString(5, ver.getPa_is_your_pa_street_addr());
            pstmnt.setString(6, ver.getPa_postal_agency());
            pstmnt.setString(7, ver.getPa_po_box());
            pstmnt.setString(8, ver.getPa_private_bag());
            pstmnt.setString(9, ver.getPa_special_service());
            pstmnt.setString(10, ver.getPa_number());
            pstmnt.setString(11, ver.getPa_postal_office());
            pstmnt.setString(12, ver.getPa_postal_code());
            pstmnt.setString(13, ver.getPa_filename());
            pstmnt.setString(14, ver.getDl_first_name());
            pstmnt.setString(15, ver.getDl_middle_name());
            pstmnt.setString(16, ver.getDl_last_name());
            pstmnt.setString(17, ver.getDl_dob());
            pstmnt.setString(18, ver.getDl_street());
            pstmnt.setString(19, ver.getDl_street_name());
            pstmnt.setString(20, ver.getDl_street_number());
            pstmnt.setString(21, ver.getDl_license_number());
            pstmnt.setString(22, ver.getDl_license_version());
            pstmnt.setString(23, ver.getDl_filename());
            pstmnt.setString(24, ver.getAddress());
            pstmnt.setString(25, ver.getUser_id());
            pstmnt.setString(26, ver.getCreated_ts());
            pstmnt.setString(27, ver.getIs_nz());
            pstmnt.setString(28, ver.getNz_id());
            pstmnt.setString(29, ver.getFull_name());
            pstmnt.setString(30, ver.getDob());
            pstmnt.setString(31, ver.getCountries());
            pstmnt.setString(32, ver.getPp_issue_country());
            pstmnt.setString(33, ver.getDl_issue_country());
            pstmnt.setString(34, ver.getTax_resi_country());
            pstmnt.setString(35, ver.getTax_id());
            pstmnt.setString(36, ver.getSrc_of_fund());
            return pstmnt;
        });
    }

    @Override
    public List<Verification> findVerification(String userId, String status) {
        String sql = " SELECT V.*, U.invite_code FROM verification V\n"
                + " INNER JOIN user_master U on(V.user_id = U.user_id)\n"
                + " WHERE V.id > 0\n";
        if (status != null) {
            sql += " AND V.status = '" + status + "'";
        }
        if (userId != null) {
            sql += " AND U.user_id = " + userId;
        }
        List<Verification> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            Verification ver = new Verification();
            ver.setId(rs.getString("id"));
            ver.setPp_passport_number(rs.getString("pp_passport_number"));
            ver.setPp_passport_expiry_date(rs.getString("pp_passport_expiry_date"));
            ver.setPp_issue_country(rs.getString("pp_issue_country"));
            ver.setPp_filename(rs.getString("pp_filename"));
            ver.setPa_same_res_addr(rs.getString("pa_same_res_addr"));
            ver.setPa_is_your_pa_street_addr(rs.getString("pa_is_your_pa_street_addr"));
            ver.setPa_postal_agency(rs.getString("pa_postal_agency"));
            ver.setPa_po_box(rs.getString("pa_po_box"));
            ver.setPa_private_bag(rs.getString("pa_private_bag"));
            ver.setPa_special_service(rs.getString("pa_special_service"));
            ver.setPa_number(rs.getString("pa_number"));
            ver.setPa_postal_office(rs.getString("pa_postal_office"));
            ver.setPa_postal_code(rs.getString("pa_postal_code"));
            ver.setPa_filename(rs.getString("pa_filename"));
            ver.setDl_first_name(rs.getString("dl_first_name"));
            ver.setDl_middle_name(rs.getString("dl_middle_name"));
            ver.setDl_last_name(rs.getString("dl_last_name"));
            ver.setDl_dob(rs.getString("dl_dob"));
            ver.setDl_street(rs.getString("dl_street"));
            ver.setDl_street_name(rs.getString("dl_street_name"));
            ver.setDl_street_number(rs.getString("dl_street_number"));
            ver.setDl_license_number(rs.getString("dl_license_number"));
            ver.setDl_license_version(rs.getString("dl_license_version"));
            ver.setDl_expiry_date(rs.getString("dl_expiry_date"));
            ver.setDl_filename(rs.getString("dl_filename"));
            ver.setDl_issue_country(rs.getString("dl_issue_country"));
            ver.setAddress(rs.getString("address"));
            ver.setUser_id(rs.getString("user_id"));
            ver.setCreated_ts(rs.getString("created_ts"));
            ver.setInvite_code(rs.getString("invite_code"));
            ver.setIs_nz(rs.getString("is_nz"));
            ver.setNz_id(rs.getString("nz_id"));
            ver.setFull_name(rs.getString("full_name"));
            ver.setDob(rs.getString("dob"));
            ver.setCountries(rs.getString("countries"));
            ver.setTax_resi_country(rs.getString("tax_resi_country"));
            ver.setTax_id(rs.getString("tax_id"));
            ver.setSrc_of_fund(rs.getString("src_of_fund"));
            ver.setNotes(rs.getString("notes"));
            ver.setApproved_ts(rs.getString("approved_ts"));
            return ver;
        });
        return list;
    }

    @Override
    public Boolean setVerificationStatus(String id, String status, Date date) {
        int vid = jdbcTemplate.update(SQLQueries.verificationStatus, new Object[]{status, date, id});
        return vid > 0;
    }

    @Override
    public void saveProfile(UserInfo user) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(
                    " UPDATE `user_master` SET `first_name`=?, `last_name`=?, `full_name`= ?, `mobile_no`=?,"
                    //                            + " `dob`=?,"
                    + " `gender`=?, "
                    //                    + " `country`=?, `state`=?,"
                    + " `address`=?"
                    //                            + ", `zipcode`=?"
                    + " WHERE `user_id`= ?;");
            pstmnt.setString(1, user.getFirstName());
            pstmnt.setString(2, user.getLastName());
            pstmnt.setString(3, user.getFullName());
            pstmnt.setString(4, user.getMobileNo());
//            pstmnt.setString(5, user.getDob());
            pstmnt.setString(5, user.getGender());
//            pstmnt.setString(7, user.getCountry());
//            pstmnt.setString(8, user.getState());
            pstmnt.setString(6, user.getAddress());
//            pstmnt.setString(10, user.getZipcode());
            pstmnt.setString(7, user.getUserId());
            return pstmnt;
        });

    }

    @Override
    public void saveConfiguration(Configuration config) {
        jdbcTemplate.update(SQLQueries.updateOldConfig, new Object[]{config.getUserId()});
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertConfig, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, config.getLocalCurrency());
            pstmnt.setString(2, config.getShowCurrency());
            pstmnt.setString(3, config.getUserId());
            pstmnt.setString(4, config.getCreatedTs());
            return pstmnt;
        });
    }

    @Override
    public Configuration findConfiguration(String userId) {
        String sql = " SELECT C.* FROM config C WHERE C.active = 'Y' and C.user_id = " + userId;
        List<Configuration> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            Configuration config = new Configuration();
            config.setLocalCurrency(rs.getString("local_currency"));
            config.setShowCurrency(rs.getString("show_currency"));
            config.setUserId(rs.getString("user_id"));
            config.setCreatedTs(rs.getString("created_ts"));
            return config;
        });
        return (list != null && !list.isEmpty()) ? list.get(0) : null;
    }

    @Override
    public int saveStripeCharge(StripeCharge chg) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertStripeCharge, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, chg.getTxn_id());
            pstmnt.setString(2, chg.getUser_id());
            pstmnt.setString(3, chg.getBal_txn_id());
            pstmnt.setString(4, chg.getCreated_ts());
            pstmnt.setString(5, chg.getStripe_created_ts());
            pstmnt.setString(6, chg.getAmount());
            pstmnt.setString(7, chg.getAmount_refunded());
            pstmnt.setString(8, chg.getCurrency());
            pstmnt.setString(9, chg.getStatus());
            pstmnt.setString(10, chg.getFail_code());
            pstmnt.setString(11, chg.getFail_message());
            pstmnt.setString(12, chg.getDescription());
            pstmnt.setString(13, chg.getFund_id());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public int saveWalletTransaction(TransactionBean tran) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertWalletTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getInvestment_id());
            pstmnt.setString(4, tran.getTxn_id());
            pstmnt.setString(5, tran.getBrand());
            pstmnt.setString(6, tran.getParticulars());
            pstmnt.setString(7, tran.getInc_dec());
            pstmnt.setString(8, tran.getAmount());
            pstmnt.setString(9, tran.getTxn_Req_id());
            pstmnt.setString(10, tran.getRef_inv_id());
            pstmnt.setString(11, tran.getMedium());
            pstmnt.setString(12, tran.getBase());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public int saveTransactionRequest(RequestBean req) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertTransactionRequest, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, req.getCreatedDate());
            pstmnt.setString(2, req.getUserId());
            pstmnt.setString(3, req.getAccount());
            pstmnt.setString(4, req.getAccountName());
            pstmnt.setString(5, req.getBankName());
            pstmnt.setString(6, req.getAmount());
            pstmnt.setString(7, req.getLocalAmount());
            pstmnt.setString(8, req.getCurrency());
            pstmnt.setString(9, req.getMaster());
            pstmnt.setString(10, req.getAutoCheck());
            pstmnt.setString(11, req.getStatus());
            pstmnt.setString(12, req.getDescription());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public int saveTransaction(TransactionBean tran) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getInvestment_id());
            pstmnt.setString(4, tran.getParticulars());
            pstmnt.setString(5, tran.getInc_dec());
            pstmnt.setString(6, tran.getAmount());
            pstmnt.setString(7, tran.getTxn_Req_id());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public void updateTransactionRequest(RequestBean req) {
        jdbcTemplate.update(SQLQueries.updateTransactionRequest, new Object[]{req.getAmount(), req.getLocalAmount(), req.getCurrency(), req.getId()});
    }

    @Override
    public List<RequestBean> userTransactionRequests(String userId, String status, String id, String tokenNo) {
        if (id == null && tokenNo != null) {
            id = tokenizer.fetchRecordId(tokenNo, "WR");
        }
        String sql = " SELECT TR.*, U.ref_id, U.full_name as customer_name, U.email FROM transaction_request TR \n"
                + " INNER JOIN user_master U ON(U.user_id = TR.user_id)\n"
                + " WHERE status = '" + status + "' ";
        if (userId != null) {
            sql += " AND TR.user_id = " + userId;
        }
        if (id != null) {
            sql += " AND TR.id = " + id;
        }
        List<RequestBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            RequestBean req = new RequestBean();
            req.setId(rs.getString("id"));
            req.setCreatedDate(rs.getString("created_ts"));
            req.setUserId(rs.getString("user_id"));
            req.setAccount(rs.getString("acc_number"));
            req.setAccountName(rs.getString("acc_name"));
            req.setBankName(rs.getString("bank_name"));
            req.setAmount(rs.getString("amount"));
            req.setLocalAmount(rs.getString("local_amount"));
            req.setCurrency(rs.getString("currency"));
            req.setStatus(rs.getString("status"));
            req.setRefId(rs.getString("ref_id"));
            req.setCustomerName(rs.getString("customer_name"));
            req.setEmail(rs.getString("email"));
            req.setMaster(rs.getString("master"));
            req.setDescription(rs.getString("description"));
            req.setTxnId(tokenizer.tokenNo(req.getRefId(), "WR", req.getId()));
            return req;
        });
        return list;
    }

    @Override
    public Boolean transactionRequestStatus(String id, String status, String master) {
        int vid = jdbcTemplate.update(SQLQueries.transactionRequestStatus, new Object[]{status, id, master});
        return vid > 0;
    }

    @Override
    public List<TransactionBean> userWalletTransactions(String userId) {
        String sql = " SELECT UM.email, UM.ref_id, UM.full_name, date(T1.created_ts) as date, A.fund_name, T1.*, SUM(CASE WHEN T2.inc_dec = 'Inc' THEN T2.amount ELSE - T2.amount END) AS balance \n"
                + " FROM wallet_txns T1 \n"
                + " INNER JOIN wallet_txns T2 ON (T2.id <= T1.id and T1.approved = T2.approved and T1.user_id = T2.user_id) \n"
                + " INNER JOIN user_master UM ON(UM.user_id = T1.user_id)\n"
                + " LEFT JOIN (SELECT IM.id as investment_id, IM.user_id, F.name as fund_name FROM investment IM INNER JOIN fund F ON(F.id = IM.fund_id))A ON (A.investment_id = T1.investment_id) \n"
                + " WHERE T1.active = 'Y' and T1.approved = 'Y'";
        if (userId != null) {
            sql += " AND T1.user_id = " + userId;
        }
        sql += " GROUP BY T1.id; ";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setEmail_id(rs.getString("email"));
            tran.setRef_id(rs.getString("ref_id"));
            tran.setFull_name(rs.getString("full_name"));
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("date"));
            tran.setFund_name(rs.getString("fund_name"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setInvestment_id(rs.getString("investment_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setTxn_Req_id(rs.getString("txn_req_id"));
            tran.setBalance(rs.getString("balance"));
            tran.setBase(rs.getString("base"));
            if (tran.getTxn_Req_id() != null && tran.getBase() != null) {
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else if (tran.getTxn_Req_id() != null) {
                tran.setBase("WR");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else {
                tran.setBase("WT");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getId()));
            }
            return tran;
        });
        return list;
    }

    @Override
    public List<TransactionBean> pendingWalletTxns(String brand, String userId) {
        String sql = " SELECT WT.*, IM.fund_name, U.ref_id, U.full_name, U.email \n"
                + " FROM wallet_txns WT \n"
                + " INNER JOIN user_master U ON(WT.user_id = U.user_id)\n"
                + " LEFT JOIN (\n"
                + " SELECT IM.id, IM.fund_id,F.name as fund_name FROM investment IM\n"
                + " INNER JOIN fund F ON(IM.fund_id = F.id)\n"
                + " ) IM ON(IM.id = WT.investment_id)\n"
                + " WHERE WT.active = 'Y' AND (WT.approved = 'N' OR WT.approved is null)\n";
        if (userId != null) {
            sql += "  AND WT.user_id = " + userId;
        }
        sql += " GROUP BY WT.id;";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("created_ts"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setBrand(rs.getString("brand"));
            tran.setRef_id(rs.getString("ref_id"));
            tran.setFull_name(rs.getString("full_name"));
            tran.setEmail_id(rs.getString("email"));
            tran.setTxn_Req_id(rs.getString("txn_req_id"));
            tran.setBase(rs.getString("base"));
            tran.setFund_name(rs.getString("fund_name"));
            if (tran.getTxn_Req_id() != null && tran.getBase() != null) {
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else if (tran.getTxn_Req_id() != null) {
                tran.setBase("WR");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else {
                tran.setBase("WT");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getId()));
            }
            return tran;
        });
        return list;
    }

    @Override
    public List<TransactionBean> actualWalletBalances(String userId) {
        String sql = "  SELECT AWT.*, coalesce(WT.amount, 0) as last_txn_amount FROM (\n"
                + " \n"
                + " SELECT U.user_id, U.ref_id, U.full_name, U.email, coalesce(WT.wallet_balance, 0) AS wallet_balance, coalesce(UIP.pending_investment_amount, 0) AS pending_investment_amount, coalesce(WR.pending_withdrawl_amount, 0) AS pending_withdrawl_amount, coalesce(WT.wallet_balance, 0) - coalesce(UIP.pending_investment_amount, 0) - coalesce(WR.pending_withdrawl_amount, 0) AS actual_wallet_balance, coalesce(WT.id, 0) AS last_txn_id FROM user_master U\n"
                + " \n"
                + " LEFT JOIN (SELECT MAX(id) AS id, user_id, SUM(CASE WHEN WT.inc_dec = 'Dec' THEN -WT.amount ELSE WT.amount END) as wallet_balance FROM wallet_txns WT WHERE WT.active = 'Y' and WT.approved = 'Y' GROUP BY user_id) WT ON(WT.user_id = U.user_id)\n"
                + " \n"
                + " LEFT JOIN (SELECT user_id, SUM(WR.amount) as pending_withdrawl_amount FROM transaction_request WR WHERE WR.active = 'Y' and WR.status = 'PENDING' and WR.master = 'WITHDRAWL' GROUP BY user_id) WR ON(WR.user_id = U.user_id)\n"
                + " \n"
                + " LEFT JOIN (SELECT user_id, coalesce(SUM(amount),0) AS pending_investment_amount FROM (SELECT USPT.created_ts, USPT.investment_id, USPT.user_id, ROUND(SUM(USPT.price * USPT.quantity),2) as amount, USPT.action from user_shares_pending_transaction USPT WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' GROUP BY investment_id) UPA GROUP BY user_id) UIP ON(UIP.user_id = WT.user_id)\n"
                + " \n"
                + " ) AWT\n"
                + " \n"
                + " LEFT JOIN wallet_txns WT ON(WT.id = AWT.last_txn_id AND WT.user_id = AWT.user_id) ";
        if (userId != null) {
            sql += " WHERE AWT.user_id = " + userId;
        }
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
//user_id, ref_id, full_name, email, wallet_balance, pending_investment_amount, actual_wallet_balance, last_txn_id, last_txn_amount
            TransactionBean tran = new TransactionBean();
            tran.setUser_id(rs.getString("user_id"));
            tran.setRef_id(rs.getString("ref_id"));
            tran.setEmail_id(rs.getString("email"));
            tran.setFull_name(rs.getString("full_name"));
            tran.setBalance(rs.getString("wallet_balance"));
            tran.setAmount(rs.getString("pending_investment_amount"));
            tran.setUsd(rs.getString("actual_wallet_balance"));
            tran.setLocal(rs.getString("pending_withdrawl_amount"));
            tran.setId(rs.getString("last_txn_id"));
            tran.setLast_amount(rs.getString("last_txn_amount"));
            return tran;
        });
        return list;
    }

//    {
//        String sql = " SELECT AWT.*, coalesce(WT.amount, 0) as last_txn_amount FROM (SELECT U.user_id, U.ref_id, U.full_name, U.email, coalesce(WT.wallet_balance, 0) AS wallet_balance, coalesce(UIP.pending_investment_amount, 0) AS pending_investment_amount,coalesce(WT.wallet_balance, 0) - coalesce(UIP.pending_investment_amount, 0) AS actual_wallet_balance, coalesce(WT.id, 0) AS last_txn_id FROM user_master U\n"
//                + " LEFT JOIN (SELECT MAX(id) AS id, user_id, SUM(CASE WHEN WT.inc_dec = 'Dec' THEN -WT.amount ELSE WT.amount END) as wallet_balance FROM wallet_txns WT WHERE WT.active = 'Y' and WT.approved = 'Y' GROUP BY user_id) WT ON(WT.user_id = U.user_id)\n"
//                + " LEFT JOIN (SELECT user_id, coalesce(SUM(amount),0) AS pending_investment_amount FROM (SELECT USPT.created_ts, USPT.investment_id, USPT.user_id, ROUND(SUM(USPT.price * USPT.quantity),2) as amount, USPT.action from user_shares_pending_transaction USPT WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' GROUP BY investment_id) UPA GROUP BY user_id) UIP ON(UIP.user_id = WT.user_id)) AWT\n"
//                + " LEFT JOIN wallet_txns WT ON(WT.id = AWT.last_txn_id AND WT.user_id = AWT.user_id)\n";
//        if (userId != null) {
//            sql += " WHERE AWT.user_id = " + userId;
//        }
//        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
////user_id, ref_id, full_name, email, wallet_balance, pending_investment_amount, actual_wallet_balance, last_txn_id, last_txn_amount
//            TransactionBean tran = new TransactionBean();
//            tran.setUser_id(rs.getString("user_id"));
//            tran.setRef_id(rs.getString("ref_id"));
//            tran.setEmail_id(rs.getString("email"));
//            tran.setFull_name(rs.getString("full_name"));
//            tran.setBalance(rs.getString("wallet_balance"));
//            tran.setAmount(rs.getString("pending_investment_amount"));
//            tran.setUsd(rs.getString("actual_wallet_balance"));
//            tran.setId(rs.getString("last_txn_id"));
//            tran.setLast_amount(rs.getString("last_txn_amount"));
//            return tran;
//        });
//        return list;
//    }
    @Override
    public List<TransactionBean> walletTxns(String id, String brand, String userId, Boolean active, Boolean approved) {
        String sql = " SELECT date(WT.created_ts) as date, WT.*, U.ref_id, U.full_name, U.email \n"
                + "FROM wallet_txns WT \n"
                + "INNER JOIN user_master U ON(WT.user_id = U.user_id)\n"
                + "WHERE WT.id > 0"
                + (id != null ? " AND WT.id = " + id + " \n" : "")
                + (brand != null ? " AND WT.brand = '" + brand + "' \n" : "")
                + (userId != null ? " AND WT.user_id = '" + userId + "' \n" : "")
                + (active ? " AND WT.active = 'Y' \n" : "")
                + (approved ? " AND (WT.approved = 'Y') \n" : "")
                + "GROUP BY WT.id; ";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("date"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setBrand(rs.getString("brand"));
            tran.setRef_id(rs.getString("ref_id"));
            tran.setEmail_id(rs.getString("email"));
            tran.setFull_name(rs.getString("full_name"));
            tran.setTxn_id(rs.getString("txn_id"));
            tran.setActive(rs.getString("active"));
            tran.setApproved(rs.getString("approved"));
            tran.setTxn_Req_id(rs.getString("txn_req_id"));
            tran.setBase(rs.getString("base"));
            if (tran.getTxn_Req_id() != null && tran.getBase() != null) {
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else if (tran.getTxn_Req_id() != null) {
                tran.setBase("WR");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getTxn_Req_id()));
            } else {
                tran.setBase("WT");
                tran.setTxn_id(tokenizer.tokenNo(tran.getRef_id(), tran.getBase(), tran.getId()));
            }
            return tran;
        });
        return list;
    }

    @Override
    public Boolean approvedWalletTxn(String id) {
        int vid = jdbcTemplate.update(SQLQueries.approvedWalletTxn, new Object[]{id});
        return vid > 0;
    }

    @Override
    public Boolean approvedTransaction(String id, String txn_req_id) {
        if (id != null) {
            int vid = jdbcTemplate.update(SQLQueries.approvedTransaction, new Object[]{id});
            return vid > 0;
        } else if (txn_req_id != null) {
            int vid = jdbcTemplate.update("UPDATE `transactions` SET `approved`='Y' WHERE `active` = 'Y' AND `txn_req_id`=" + txn_req_id);
            return vid > 0;
        }
        return false;
    }

    @Override
    public void addReferenceNo(UserReference reference) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUserRefNo, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, reference.getReferenceNo());
            pstmnt.setString(2, reference.getStripeToken());
            pstmnt.setString(3, reference.getPoliToken());
            pstmnt.setString(4, reference.getUserId());
            pstmnt.setString(5, reference.getFundId());
            pstmnt.setString(6, reference.getPrice());
            pstmnt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            return pstmnt;
        });
    }

    @Override
    public UserReference findReferenceNo(String token, String ref) {
        String sql = "SELECT * FROM user_ref_no WHERE active ='Y'";
        if (ref != null) {
            sql += " and ref_no = '" + ref + "'";
        }
        if (token != null) {
            sql += " and poli_token = '" + token + "'";
        }
        List<UserReference> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserReference tran = new UserReference();
            tran.setReferenceNo(rs.getString("ref_no"));
            tran.setStripeToken(rs.getString("stripe_token"));
            tran.setPoliToken(rs.getString("poli_token"));
            tran.setUserId(rs.getString("user_id"));
            tran.setFundId(rs.getString("fund_id"));
            tran.setPrice(rs.getString("price_in_nzd"));
            tran.setAmounted(rs.getString("amounted"));
            return tran;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public void updatedAmounted(String ref) {
        String sql = "UPDATE `user_ref_no` SET `amounted`='Y' WHERE `ref_no`= ? ;";
        jdbcTemplate.update(sql, new Object[]{ref});
    }

    @Override
    public void addTrade(ShareFund trade, List<ShareFund> totalUnits) {
        String sql = "UPDATE `fund_trade` SET `active`='N' WHERE `id`>'0' and fund_id =" + trade.getFundId();
        jdbcTemplate.update(sql);

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFundTrade, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, trade.getCreatedDate());
            pstmnt.setString(2, trade.getFundId());
            pstmnt.setString(3, trade.getOpBal());
            pstmnt.setString(4, trade.getCloseBal());
            pstmnt.setString(5, trade.getPurTrades());
            pstmnt.setString(6, trade.getSoldTrades());
            pstmnt.setString(7, trade.getPurCoins());
            pstmnt.setString(8, trade.getSoldCoins());
            pstmnt.setString(9, trade.getPrice());
            return pstmnt;
        });

        sql = "UPDATE `user_curr_btc` SET `active`='N' WHERE `id`>'0' and fund_id =" + trade.getFundId();
        jdbcTemplate.update(sql);

        jdbcTemplate.batchUpdate(SQLQueries.insertUserCurrBtc, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ShareFund unit = totalUnits.get(i);
                ps.setString(1, trade.getCreatedDate());
                ps.setString(2, unit.getFundId());
                ps.setString(3, unit.getUserId());
                ps.setString(4, unit.getInvestmentId());
                ps.setString(5, unit.getBtc());
                ps.setString(6, unit.getMinto());
            }

            @Override
            public int getBatchSize() {
                return totalUnits.size();
            }
        });
    }

    @Override
    public void addBitcoin(ShareFund bitcoin) {
//        String sql = "UPDATE `fund_bitcoin` SET `active`='N' WHERE `id`>'0' and fund_id =" + bitcoin.getFundId();
//        jdbcTemplate.update(sql);
//
//        jdbcTemplate.update((java.sql.Connection con) -> {
//            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFundBitcoin, Statement.RETURN_GENERATED_KEYS);
//            pstmnt.setString(1, bitcoin.getCreatedDate());
//            pstmnt.setString(2, bitcoin.getFundId());
//            pstmnt.setString(3, bitcoin.getQuantity());
//            pstmnt.setString(4, bitcoin.getPrice());
//            pstmnt.setString(5, bitcoin.getShareAmount());
//            return pstmnt;
//        });
    }

    @Override
    public ShareFund tradeByFundId(String id) {
        String sql = " SELECT * FROM fund_trade WHERE active ='Y' and fund_id = " + id;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setFundId(rs.getString("fund_id"));
            trade.setOpBal(rs.getString("op_bal"));
            trade.setCloseBal(rs.getString("close_bal"));
            trade.setPurTrades(rs.getString("pur_trades"));
            trade.setSoldTrades(rs.getString("sold_trades"));
            trade.setPurCoins(rs.getString("pur_coins"));
            trade.setSoldCoins(rs.getString("sold_coins"));
            return trade;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<ShareFund> totalUnitsById(String fundId, String userId, String investmentId) {
        String sql = "  SELECT UT.user_id, UT.investment_id, F.name, F.id as fund_id, \n"
                + " sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as minto, \n"
                + " sum(case when action = 'SOLD' then -UT.btc else UT.btc end) as btc, \n"
                + " coalesce(sum(UCB.bitcoin), 0) as bitcoin, U.ref_id, U.full_name as customer_name, U.email, FP.price as unit_price \n"
                + " FROM user_inv_units_txn UT \n"
                + " inner join investment IM on(UT.investment_id = IM.id) \n"
                + " inner join fund F on(F.id = IM.fund_id) \n"
                + " INNER JOIN fund_price FP ON(FP.fund_id = F.id AND FP.active = 'Y') \n"
                + " inner join user_master U on(U.user_id = UT.user_id) \n"
                + " left join user_curr_btc UCB on(UCB.user_id = UT.user_id and UCB.investment_id = UT.investment_id and UCB.active = 'Y') \n"
                + " left join fund_trade FT on(FT.fund_id = F.id and FT.active = 'Y') \n"
                + " where UT.active = 'Y'\n";
        if (fundId != null) {
            sql += " and F.id = " + fundId;
        }
        if (userId != null) {
            sql += " and U.user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and IM.id = " + investmentId;
        }
        sql += " group by ref_id, user_id, investment_id, name, fund_id, unit_price;  ";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setReqId(rs.getString("ref_id"));
            trade.setCustomerName(rs.getString("customer_name"));
            trade.setEmail(rs.getString("email"));
            trade.setUserId(rs.getString("user_id"));
            trade.setInvestmentId(rs.getString("investment_id"));
            trade.setName(rs.getString("name"));
            trade.setFundId(rs.getString("fund_id"));
            trade.setMinto(rs.getString("minto"));
            trade.setBtc(rs.getString("btc"));
            trade.setQuantity(rs.getString("bitcoin"));
            trade.setPrice(rs.getString("unit_price"));
            return trade;
        });
        return list;
    }

    @Override
    public void saveUserTodayStatus(List<InvestmentBean> investments) {
        jdbcTemplate.batchUpdate(SQLQueries.insertUserTodayStatus, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                InvestmentBean userFund = investments.get(i);
                ps.setString(1, userFund.getCreatedDate());
                ps.setString(2, userFund.getUserId());
                ps.setString(3, userFund.getInvestmentId());
                ps.setString(4, userFund.getFundId());
                ps.setString(5, userFund.getInvestmentAmount());
            }

            @Override
            public int getBatchSize() {
                return investments.size();
            }
        });
    }

    private String innerSql(String currDate, String userId) {
        return " SELECT UT.id, '" + currDate + "' as curr_date, UT.user_id, UT.investment_id, FP.fund_id, \n"
                + " UT.active, F.name as fund_name, SUM(CASE WHEN action = 'SOLD' THEN - UT.minto ELSE + UT.minto END) minto, \n"
                + " FP.price FROM user_inv_units_txn UT \n"
                + " INNER JOIN investment IM ON(IM.id = UT.investment_id)\n"
                + " INNER JOIN fund F ON(F.id = IM.fund_id)\n"
                + " INNER JOIN (SELECT * FROM fund_price WHERE date(created_ts) = date('" + currDate + "') group by fund_id) FP ON(FP.fund_id = IM.fund_id)\n"
                + " WHERE UT.active = 'Y' AND date(UT.created_ts) <= date('" + currDate + "') AND UT.user_id = " + userId
                + " GROUP BY investment_id ";
    }

    @Override
    public List<UserFundsCommand> getUserTodayStatusNew(String userId, String investmentId, String fDate, String tDate, Boolean sum) {
        List<String> currDates = common.getAllDates(fDate, tDate);
        StringBuffer sql = new StringBuffer();
        sql.append("Select A.*,")
                .append(sum ? "SUM" : "")
                .append("(A.minto * A.price) as investment_amount")
                .append(" FROM (\n");
        for (int i = 0; i < currDates.size(); i++) {
            String currDate = currDates.get(i);
            String dateSql = innerSql(currDate, userId);
            sql.append(dateSql);
            if (i < currDates.size() - 1) {
                sql.append("\n")
                        .append("union all")
                        .append("\n");
            }
        }
        sql.append(" ) A").append(sum ? " GROUP BY curr_date; " : ";");
        List<UserFundsCommand> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            UserFundsCommand coin = new UserFundsCommand();
            coin.setCurrent_date(rs.getString("curr_date"));
            coin.setUser_id(rs.getString("user_id"));
            coin.setInvestment_id(rs.getString("investment_id"));
            coin.setFund_id(rs.getString("fund_id"));
            coin.setFund_name(rs.getString("fund_name"));
            coin.setInvestment_amount(rs.getString("investment_amount"));
            return coin;
        });
        return list;
    }

    @Override
    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate, Boolean sum) {
        String sql = " SELECT UTS.id, UTS.curr_date, UTS.user_id, UTS.investment_id, UTS.fund_id, \n"
                + (sum ? " sum(UTS.investment_amount) as investment_amount, \n" : " UTS.investment_amount, \n")
                + " UTS.active, CASE WHEN UTS.fund_id = 0 THEN 'Cash' ELSE F.name END as fund_name FROM user_today_status UTS "
                + " LEFT JOIN fund F ON(F.id = UTS.fund_id) WHERE UTS.active = 'Y' ";
        if (fundId != null) {
            sql += " AND UTS.fund_id = " + fundId;
        }
        if (userId != null) {
            sql += " AND UTS.user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " AND UTS.investment_id = " + investmentId;
        }
        sql += " AND UTS.curr_date BETWEEN '" + fDate + "' AND " + (tDate != null ? "'" + tDate + "'" : "now()");
        if (sum) {
            sql += "group by curr_date, user_id; ";
        }
        List<UserFundsCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserFundsCommand coin = new UserFundsCommand();
            coin.setCurrent_date(rs.getString("curr_date"));
            coin.setUser_id(rs.getString("user_id"));
            coin.setInvestment_id(rs.getString("investment_id"));
            coin.setFund_id(rs.getString("fund_id"));
            coin.setFund_name(rs.getString("fund_name"));
            coin.setInvestment_amount(rs.getString("investment_amount"));
            return coin;
        });
        return list;
    }

    @Override
    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals) {
        String sql = null;
        if (individuals) {
            String individualSql = " SELECT *, bitcoin / minto as unit_price FROM \n"
                    + " (SELECT F.name, UCB.created_ts, sum(UCB.bitcoin) as bitcoin, sum(UCB.minto) as minto \n"
                    + " FROM user_curr_btc UCB \n"
                    + " left join fund F on (UCB.fund_id = F.id) \n"
                    + " WHERE UCB.id > 0\n";
            if (fundId != null && !fundId.isEmpty()) {
                individualSql += " and UCB.fund_id = " + fundId;
            }
            if (userId != null && !userId.isEmpty()) {
                individualSql += " and UCB.user_id = " + userId;
            }
            if (investmentId != null && !investmentId.isEmpty()) {
                individualSql += " and UCB.investment_id = " + investmentId;
            }
            individualSql += " GROUP BY name, created_ts ORDER BY name, created_ts) UCB ";
            sql = individualSql;
        } else {
            String totalSql = " SELECT *, bitcoin / minto as unit_price FROM \n"
                    + " (SELECT UCB.created_ts, sum(UCB.bitcoin) as bitcoin, sum(UCB.minto) as minto FROM user_curr_btc UCB\n"
                    + " WHERE id > 0\n";
            if (fundId != null && !fundId.isEmpty()) {
                totalSql += " and fund_id = " + fundId;
            }
            if (userId != null && !userId.isEmpty()) {
                totalSql += " and user_id = " + userId;
            }
            if (investmentId != null && !investmentId.isEmpty()) {
                totalSql += " and investment_id = " + investmentId;
            }
            totalSql += " GROUP BY created_ts ORDER BY created_ts) UCB  ";
            sql = totalSql;
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            if (individuals) {
                trade.setName(rs.getString("name"));
            }
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setMinto(rs.getString("minto"));
            trade.setQuantity(rs.getString("bitcoin"));
            trade.setPrice(rs.getString("unit_price"));
            return trade;
        });
        return list;
    }

    private String dateSql(String currDate, String portfolioId) {
        return " SELECT A.*, total_units * nav as total_usd FROM (SELECT '" + currDate + "' as date, \n"
                + " F.id as fund_id, F.name as fund_name, F.img, coalesce(FP.price,0) as nav,\n"
                + " coalesce(count(distinct(UT.user_id)),0) as user_count, \n"
                + " coalesce(sum(case when action = 'SOLD' then -UT.minto else UT.minto end),0) as total_units,\n"
                + " coalesce(sum(case when action = 'SOLD' then -UT.usd else UT.usd end),0) as invested_usd\n"
                + " FROM user_inv_units_txn UT  \n"
                + " inner join investment IM on(UT.investment_id = IM.id) \n"
                + " inner join fund F on(F.id = IM.fund_id) \n"
                + " inner join (SELECT FP.* FROM fund_price FP where id = (SELECT max(FP.id) FROM fund_price FP \n"
                + " where date(created_ts) <= '" + currDate + "' and FP.fund_id = '" + portfolioId + "')) FP on (FP.fund_id = IM.fund_id)\n"
                + " where UT.active = 'Y' and IM.fund_id = '" + portfolioId + "' and date(UT.created_ts) <= '" + currDate + "') A ";
    }

    RowMapper<PortfolioBean> portfolioRowMapper = (ResultSet rs, int i) -> {
        PortfolioBean bean = new PortfolioBean();
        bean.setDate(rs.getString("date"));
        bean.setFund_id(rs.getString("fund_id"));
        bean.setFund_name(rs.getString("fund_name"));
        String image = rs.getString("img");
        bean.setFull_name(image != null ? image : "crypto-coins1.png");
        bean.setNav(rs.getString("nav"));
        bean.setUser_count(rs.getString("user_count"));
        bean.setTotal_units(rs.getString("total_units"));
        bean.setInvested_usd(rs.getString("invested_usd"));
        bean.setTotal_usd(rs.getString("total_usd"));
        return bean;
    };

    @Override
    public List<PortfolioBean> portfolioReportById(List<String> currDates, String portfolioId) {
        StringBuffer sql = new StringBuffer();
        for (int i = 0; i < currDates.size(); i++) {
            String currDate = currDates.get(i);
            String dateSql = dateSql(currDate, portfolioId);
            sql.append(dateSql);
            if (i < currDates.size() - 1) {
                sql.append("\n")
                        .append("union all")
                        .append("\n");
            }
        }
        List<PortfolioBean> list = jdbcTemplate.query(sql.toString(), portfolioRowMapper);
        return list;
    }

    @Override
    public List<PortfolioBean> portfoliosReport() {
        String sql = " SELECT A.*, total_units * nav as total_usd "
                + " FROM (SELECT date(now()) as date, F.id as fund_id, F.name as fund_name, F.img, FP.price as nav, count(distinct(IM.user_id)) as user_count, sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as total_units, sum(case when action = 'SOLD' then -UT.usd else UT.usd end) as invested_usd FROM fund F\n"
                + " INNER JOIN fund_price FP ON(FP.fund_id = F.id AND FP.active = 'Y')\n"
                + " INNER JOIN investment IM ON (F.id = IM.fund_id)\n"
                + " INNER JOIN role_master R ON (IM.user_id = R.user_id AND R.role = 'ROLE_USER')\n"
                + " INNER JOIN user_inv_units_txn UT ON (IM.id = UT.investment_id)\n"
                + "GROUP BY F.id) A; ";
        List<PortfolioBean> list = jdbcTemplate.query(sql.toString(), portfolioRowMapper);
        return list;
    }

    @Override
    public String fundPrice(String fundId, String investmentId) {
        if (fundId != null) {
            List<String> priceList = jdbcTemplate.query(SQLQueries.fundPrice, new Object[]{fundId}, (ResultSet rs, int i) -> {
                return rs.getString("price");
            });
            return !priceList.isEmpty() ? priceList.get(0) : null;
        } else if (investmentId != null) {
            List<String> priceList = jdbcTemplate.query(SQLQueries.fundPriceByInvestment, new Object[]{investmentId}, (ResultSet rs, int i) -> {
                return rs.getString("price");
            });
            return !priceList.isEmpty() ? priceList.get(0) : null;
        }
        return null;
    }

    @Override
    public List<ShareFund> fundPrices(String fundId, String investmentId) {
        //    id, fund_id, price, created_ts, active, total_value
        RowMapper rowMapper = (ResultSet rs, int i) -> {
            ShareFund price = new ShareFund();
            price.setId(rs.getString("id"));
            price.setFundId(rs.getString("fund_id"));
            price.setPrice(rs.getString("price"));
            price.setCreatedDate(rs.getString("created_ts"));
            price.setCloseBal(rs.getString("total_value"));
            return price;
        };
        if (fundId != null) {
            List<ShareFund> priceList = jdbcTemplate.query(" SELECT * FROM fund_price where fund_id = ? ", new Object[]{fundId}, rowMapper);
            return priceList;
        } else if (investmentId != null) {
            List<ShareFund> priceList = jdbcTemplate.query(" SELECT FP.* FROM fund_price FP\n"
                    + "INNER JOIN investment IM ON(FP.fund_id = IM.fund_id)\n"
                    + "where IM.id = ?  ", new Object[]{investmentId}, rowMapper);
            return priceList;
        }
        return null;
    }

    @Override
    public List<PortfolioBean> userPortfolioUnits(String portfolioId) {
        String sql = "SELECT IM.user_id, U.full_name, sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as total_units, sum(case when action = 'SOLD' then -UT.usd else UT.usd end) as total_usd\n"
                + " FROM user_inv_units_txn UT  \n"
                + " inner join investment IM on(UT.investment_id = IM.id)  \n"
                + " inner join user_master U on (IM.user_id = U.user_id)\n"
                + " where UT.active = 'Y' and IM.fund_id = " + portfolioId
                + " \n group by IM.user_id";
        List<PortfolioBean> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            PortfolioBean bean = new PortfolioBean();
            bean.setTotal_units(rs.getString("total_units"));
            bean.setTotal_usd(rs.getString("total_usd"));
            bean.setUser_id(rs.getString("user_id"));
            bean.setFull_name(rs.getString("full_name"));
            return bean;
        });
        return list;
    }

    @Override
    public List<PortfolioBean> userPortfolioShares(String portfolioId) {
        String sql = " SELECT UST.share_id, UST.price, UST.quantity, UST.price * UST.quantity AS value, UST.coin_id from user_shares_transaction UST \n"
                + " inner join investment IM on(UST.investment_id = IM.id)  \n"
                + " inner join user_master U on (IM.user_id = U.user_id)\n"
                + " left join (SELECT S.*, SP.price FROM share S INNER JOIN share_price SP ON(S.id = SP.share_id and SP.active = 'Y')) S on (S.id = UST.share_id)\n"
                + " where UST.active = 'Y' and IM.fund_id = " + portfolioId
                + " \ngroup by UST.coin_id";
        List<PortfolioBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            PortfolioBean bean = new PortfolioBean();
            bean.setShare_id(rs.getString("share_id") != null ? rs.getString("share_id") : rs.getString("coin_id"));
            bean.setTotal_usd(rs.getString("value"));
            return bean;
        });
        return list;
    }

    @Override
    public List<TransactionBean> portfolioReportType(String reportType, Date firstDate, Date lastDate, String fundId) {
        String sql = null;
        switch (reportType) {
            case "MR":
                sql = " SELECT UM.ref_id, UM.email, UM.full_name, IM.fund_id, F.name as portfolio_name, UT.*\n"
                        + " FROM user_inv_units_txn UT\n"
                        + " INNER JOIN investment IM ON(UT.investment_id = IM.id)\n"
                        + " INNER JOIN fund F ON(F.id = IM.fund_id)\n"
                        + " INNER JOIN user_master UM ON(UT.user_id = UM.user_id)\n"
                        + " WHERE UT.ref_inv_id is not null AND UT.action = 'SOLD' AND UT.created_ts BETWEEN ? AND ? AND IM.fund_id = ?"
                        + " ORDER BY id asc;";
                break;
            case "DR":
                sql = " SELECT UM.ref_id, UM.email, UM.full_name, IM.fund_id, F.name as portfolio_name, UT.*\n"
                        + " FROM user_inv_units_txn UT\n"
                        + " INNER JOIN investment IM ON(UT.investment_id = IM.id)\n"
                        + " INNER JOIN fund F ON(F.id = IM.fund_id)\n"
                        + " INNER JOIN user_master UM ON(UT.user_id = UM.user_id)\n"
                        + " WHERE UT.action = 'PURCHASED' AND UT.created_ts BETWEEN ? AND ? AND IM.fund_id = ?"
                        + " ORDER BY id asc;";
                break;
            case "WR":
                sql = " SELECT UM.ref_id, UM.email, UM.full_name, IM.fund_id, F.name as portfolio_name, UT.*\n"
                        + " FROM user_inv_units_txn UT\n"
                        + " INNER JOIN investment IM ON(UT.investment_id = IM.id)\n"
                        + " INNER JOIN fund F ON(F.id = IM.fund_id)\n"
                        + " INNER JOIN user_master UM ON(UT.user_id = UM.user_id)\n"
                        + " WHERE UT.action = 'SOLD' AND UT.created_ts BETWEEN ? AND ? AND IM.fund_id = ?"
                        + " ORDER BY id asc;";
                break;
        }
//        ref_id, email, full_name, fund_id, portfolio_name, id, created_ts, user_id, investment_id, nzd, usd, btc, minto, active, action, ref_inv_id
        List<TransactionBean> list = jdbcTemplate.query(sql, new Object[]{firstDate, lastDate, fundId}, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setRef_id(rs.getString("ref_id"));
            tran.setEmail_id(rs.getString("email"));
            tran.setFull_name(rs.getString("full_name"));
            tran.setId(rs.getString("fund_id"));
            tran.setFund_name(rs.getString("portfolio_name"));
            tran.setCreated_ts(rs.getString("created_ts"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setInvestment_id(rs.getString("investment_id"));
            tran.setUsd(rs.getString("usd"));
            tran.setMinto(rs.getString("minto"));
            tran.setRef_inv_id(rs.getString("ref_inv_id"));
            return tran;
        });
        return list;
    }

    @Override
    public void saveCurrencyConversionRate(String currencyPair, String date,
            String price
    ) {
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertConversionRate);
            pstmnt.setString(1, date);
            pstmnt.setString(2, "usd_nzd".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(3, "usd_gbp".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(4, "usd_inr".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(5, "usd_aud".equalsIgnoreCase(currencyPair) ? price : null);
            return pstmnt;
        });
        transactionManager.commit(txStatus);
    }

    @Override
    public ShareFund currencyConversionRate(String currencyPair, String date
    ) {
        String sql = " SELECT id, created_ts, " + currencyPair + " FROM conversion_rate WHERE created_ts = '" + date + "'";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setId(rs.getString("id"));
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setPrice(rs.getString(new String(currencyPair.toCharArray())));
            return trade;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public void updateCurrencyConversionRate(String currencyPair, String id,
            String price
    ) {
        String sql = "UPDATE `conversion_rate` SET `" + currencyPair + "`='" + price + "' WHERE `id` = " + id;
        jdbcTemplate.update(sql);
    }

    @Override
    public void addPortfolioBuying(TransactionBean txn) {
        if (txn.getTxn_Req_id() != null) {
            String sql = "UPDATE `mgmt_nav_master` SET `active`='N' WHERE `id`>'0' and `txn_req_id`= ?";
            jdbcTemplate.update(sql, new Object[]{txn.getTxn_Req_id()});
        }
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertNavMaster, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, txn.getCreated_ts());
            pstmnt.setString(2, txn.getCreated_date());
            pstmnt.setString(3, txn.getUser_id());
            pstmnt.setString(4, txn.getLocal());
            pstmnt.setString(5, txn.getUsd());
            pstmnt.setString(6, txn.getBtc());
            pstmnt.setString(7, txn.getMinto());
            pstmnt.setString(8, txn.getParticulars());
            pstmnt.setString(9, txn.getFund_id());
            pstmnt.setString(10, txn.getTxn_Req_id());
            return pstmnt;
        }, holder);
        int master_id = holder.getKey().intValue();
        if (txn.getTxn_Req_id() == null) {
            String sql = "UPDATE `mgmt_nav_master` SET `txn_req_id`= ? WHERE `id` = ? ;";
            jdbcTemplate.update(sql, new Object[]{master_id, master_id});
        }
        if (txn.getCoin_id() != null) {
            String[] coinIds = txn.getCoin_id().split(",");
            String[] amounts = txn.getAmount().split(",");
            List<CommonObject> list = new LinkedList<>();
            for (int i = 0; i < coinIds.length; i++) {
                Double amount = new Double(amounts[i]);
                if (amount > 0.00d) {
                    CommonObject obj = new CommonObject(coinIds[i], amounts[i]);
                    list.add(obj);
                }
            }
            jdbcTemplate.batchUpdate(SQLQueries.insertNavDetail, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ps.setString(1, list.get(i).getType());
                    ps.setString(2, list.get(i).getName());
                    ps.setInt(3, master_id);
                }

                @Override
                public int getBatchSize() {
                    return list.size();
                }
            });
        }
    }

//ref_id, customer_name, email, id, created_ts, created_date, user_id, nzd, usd, btc, minto, active, notes, a_share, amount, master_id
    @Override
    public List<TransactionBean> getPortfolioBuying(String portfolioId, Date fromDate, Date toDate) {
        String sql = " SELECT U.ref_id, U.full_name as customer_name, U.email,\n"
                + " MNM.*, case when S.name is not null then S.name ELSE MND.a_share eND a_share, MND.amount, MND.master_id FROM mgmt_nav_master MNM\n"
                + " LEFT JOIN mgmt_nav_detail MND ON(MNM.id = MND.master_id)\n"
                + " LEFT JOIN share S ON (S.id = MND.a_share)\n"
                + " LEFT JOIN user_master U  ON (MNM.user_id = U.user_id) "
                + " WHERE MNM.active = 'Y' AND MND.master_id IS NOT NULL AND MNM.fund_id = ? AND MNM.created_date between ? AND ?;";
        List<TransactionBean> list = jdbcTemplate.query(sql, new Object[]{portfolioId, fromDate, toDate}, (ResultSet rs, int i) -> {
            TransactionBean bean = new TransactionBean();
            bean.setRef_id(rs.getString("ref_id"));
            bean.setFull_name(rs.getString("customer_name"));
            bean.setEmail_id(rs.getString("email"));
            bean.setId(rs.getString("id"));
            bean.setCreated_ts(rs.getString("created_ts"));
            bean.setCreated_date(rs.getString("created_date"));
            bean.setUser_id(rs.getString("user_id"));
            bean.setLocal(rs.getString("nzd"));
            bean.setUsd(rs.getString("usd"));
            bean.setBtc(rs.getString("btc"));
            bean.setUnits(rs.getString("minto"));
            bean.setParticulars(rs.getString("notes"));
            bean.setCoin_id(rs.getString("a_share"));
            bean.setAmount(rs.getString("amount"));
            bean.setTxn_Req_id(rs.getString("txn_req_id"));
            return bean;
        });
        return list;
    }

//id, created_ts, created_date, user_id, nzd, usd, btc, minto, active, notes, fund_id
    @Override
    public List<TransactionBean> getPortfolioBuyingPending(String portfolioId) {
        String sql = " SELECT U.ref_id, U.full_name as customer_name, U.email,\n"
                + " MNM.*  FROM mgmt_nav_master MNM\n"
                + " LEFT JOIN mgmt_nav_detail MND ON (MNM.id = MND.master_id)\n"
                + " LEFT JOIN user_master U  ON (MNM.user_id = U.user_id)\n"
                + " WHERE MNM.active = 'Y' AND MND.master_id IS NULL AND fund_id = ? ";
        List<TransactionBean> list = jdbcTemplate.query(sql, new Object[]{portfolioId}, (ResultSet rs, int i) -> {
            TransactionBean bean = new TransactionBean();
            bean.setRef_id(rs.getString("ref_id"));
            bean.setFull_name(rs.getString("customer_name"));
            bean.setEmail_id(rs.getString("email"));
            bean.setId(rs.getString("id"));
            bean.setCreated_ts(rs.getString("created_ts"));
            bean.setCreated_date(rs.getString("created_date"));
            bean.setUser_id(rs.getString("user_id"));
            bean.setLocal(rs.getString("nzd"));
            bean.setUsd(rs.getString("usd"));
            bean.setBtc(rs.getString("btc"));
            bean.setUnits(rs.getString("minto"));
            bean.setParticulars(rs.getString("notes"));
            return bean;
        });
        return list;
    }

    @Override
    public List<TransactionBean> managementSharesBuyingPortfolio(String portfolioId, String recordId) {
        String sql = "  SELECT F.id as fund_id, F.name, V.share_name, V.share_id, MNM.ref_id, MNM.customer_name, MNM.email, MNM.id, MNM.created_ts, MNM.created_date, MNM.user_id, coalesce(MNM.nzd, 0.00)nzd, coalesce(MNM.usd,0.00)usd, coalesce(MNM.btc,0.00)btc, coalesce(MNM.minto,0.00)minto, MNM.active, MNM.notes, MNM.fund_id, MNM.txn_req_id, coalesce(MND.amount,0.00)amount FROM fund F \n"
                + " LEFT JOIN (SELECT CASE WHEN S.name IS NOT NULL THEN S.name ELSE coin_id end as share_name, CASE WHEN S.id IS NOT NULL THEN S.id ELSE coin_id end as share_id, FD.fund_id FROM fund_detail FD LEFT JOIN share S ON (S.active= 'Y' AND S.id = FD.share_id) WHERE FD.active = 'Y' ) V ON (F.id = V.fund_id)\n"
                + " LEFT JOIN (SELECT U.ref_id, U.full_name as customer_name, U.email, MNM.id, MNM.created_ts, MNM.created_date, MNM.user_id, coalesce(MNM.nzd,0.00)nzd, coalesce(MNM.usd,0.00)usd, coalesce(MNM.btc,0.00)btc, coalesce(MNM.minto,0.00)minto, MNM.active, MNM.notes, MNM.fund_id, MNM.txn_req_id FROM mgmt_nav_master MNM LEFT JOIN user_master U  ON (MNM.user_id = U.user_id) WHERE MNM.id = ?) MNM ON(MNM.fund_id = F.id)\n"
                + " LEFT JOIN mgmt_nav_detail MND ON(MND.master_id = MNM.id AND MND.a_share = V.share_id)\n"
                + " WHERE F.id = ? GROUP BY F.id, V.share_id ORDER BY MNM.id;  ";
        List<TransactionBean> list = jdbcTemplate.query(sql, new Object[]{recordId, portfolioId}, (ResultSet rs, int i) -> {
            TransactionBean bean = new TransactionBean();
            bean.setRef_id(rs.getString("ref_id"));
            bean.setFull_name(rs.getString("customer_name"));
            bean.setEmail_id(rs.getString("email"));
            bean.setCoin_id(rs.getString("share_id"));
            bean.setId(rs.getString("id"));
            bean.setCreated_ts(rs.getString("created_ts"));
            bean.setCreated_date(rs.getString("created_date"));
            bean.setUser_id(rs.getString("user_id"));
            bean.setLocal(rs.getString("nzd"));
            bean.setUsd(rs.getString("usd"));
            bean.setBtc(rs.getString("btc"));
            bean.setMinto(rs.getString("minto"));
            bean.setParticulars(rs.getString("notes"));
            bean.setTxn_Req_id(rs.getString("txn_req_id"));
            bean.setAmount(rs.getString("amount"));
            return bean;
        });
        return list;
    }
//ref_id, customer_name, email, id, created_ts, created_date, user_id, nzd, usd, btc, minto, active, notes, fund_id, txn_req_id, a_share, amount, master_id

    @Override
    public List<TransactionBean> getPortfolioBuyingHistory(String portfolioId, String txnReqId) {
        String sql = " SELECT U.ref_id, U.full_name as customer_name, U.email,\n"
                + " MNM.*, case when S.name is not null then S.name ELSE MND.a_share eND a_share, MND.amount, MND.master_id FROM mgmt_nav_master MNM\n"
                + " LEFT JOIN mgmt_nav_detail MND ON(MNM.id = MND.master_id)\n"
                + " LEFT JOIN share S ON (S.id = MND.a_share)\n"
                + " LEFT JOIN user_master U  ON (MNM.user_id = U.user_id)  \n"
                + " WHERE MNM.txn_req_id = ? AND MNM.fund_id = ? ORDER BY MNM.id ";
        List<TransactionBean> list = jdbcTemplate.query(sql, new Object[]{txnReqId, portfolioId}, (ResultSet rs, int i) -> {
            TransactionBean bean = new TransactionBean();
            bean.setRef_id(rs.getString("ref_id"));
            bean.setFull_name(rs.getString("customer_name"));
            bean.setEmail_id(rs.getString("email"));
            bean.setId(rs.getString("id"));
            bean.setCreated_ts(rs.getString("created_ts"));
            bean.setCreated_date(rs.getString("created_date"));
            bean.setUser_id(rs.getString("user_id"));
            bean.setLocal(rs.getString("nzd"));
            bean.setUsd(rs.getString("usd"));
            bean.setBtc(rs.getString("btc"));
            bean.setMinto(rs.getString("minto"));
            bean.setParticulars(rs.getString("notes"));
            bean.setTxn_Req_id(rs.getString("txn_req_id"));
            bean.setAmount(rs.getString("amount"));
            bean.setCoin_id(rs.getString("a_share"));
            return bean;
        });
        return list;
    }

    @Override
    public void saveTempProfile(UserInfo user) {
        jdbcTemplate.update("UPDATE `temp_profile` SET `active` = 'N' WHERE `user_id` = ?", new Object[]{user.getUserId()});

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertTempProfile, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getCreatedTs());
            pstmnt.setString(2, user.getUserId());
            pstmnt.setString(3, user.getFirstName());
            pstmnt.setString(4, user.getLastName());
            pstmnt.setString(5, user.getFullName());
            pstmnt.setString(6, user.getMobileNo());
            pstmnt.setString(7, user.getGender());
            pstmnt.setString(8, user.getAddress());
            return pstmnt;
        }, holder);
    }

    @Override
    public List<TempProfile> getChangingProfiles(String id) {
        String sql = " SELECT UM.email, UM.ref_id, UM.user_id, TP.id, TP.created_ts, \n"
                + " TP.first_name as new_first_name, TP.last_name as new_last_name, TP.full_name as new_full_name, TP.mobile_no as new_mobile_no, TP.gender as new_gender, TP.address as new_address, UM.first_name, UM.last_name, UM.full_name, UM.mobile_no, UM.gender, UM.address \n"
                + " FROM cryptolabsinternal.temp_profile TP \n"
                + " INNER JOIN user_master UM ON(UM.user_id = TP.user_id)\n"
                + " WHERE TP.active = 'Y' ";
        if (id != null) {
            sql += " AND TP.id = " + id;

        }
        List<TempProfile> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TempProfile.class
        ));
        return list;
    }

    @Override
    public void updateCancelChangeProfile(String id) {
        jdbcTemplate.update("UPDATE `temp_profile` SET `active` = 'N' WHERE `id` = ?", new Object[]{id});
    }

    @Override
    public void updateApprovedChangeProfile(String id) {
        List<TempProfile> list = getChangingProfiles(id);
        if (!list.isEmpty()) {
            TempProfile profile = list.get(0);
            UserInfo user = new UserInfo();
            user.setFirstName(profile.getNew_first_name());
            user.setLastName(profile.getNew_last_name());
            user.setFullName(profile.getNew_full_name());
            user.setMobileNo(profile.getNew_mobile_no());
//            user.setDob(profile.getNew_dob());
            user.setGender(profile.getNew_gender());
//            user.setCountry(profile.getNew_First_name());
//            user.setState(profile.getNew_First_name());
            user.setAddress(profile.getNew_address());
//            user.setZipcode(profile.getNew_First_name());
            user.setUserId(profile.getUser_id());
            saveProfile(user);
            jdbcTemplate.update("UPDATE `temp_profile` SET `active` = 'N' WHERE `id` = ?", new Object[]{id});
        }
    }

    @Override
    public void updateVerification(Verification v) {
        String sql = "UPDATE `verification` SET `pp_passport_number`=?, `pp_passport_expiry_date`=?, `dl_license_number`=?, `dl_license_version`=?, `dl_expiry_date`=?, `address`=?, `countries`=?, `tax_resi_country`=?, `tax_id`=?, `src_of_fund`=?, `full_name`=?, `dob`=?, `pp_issue_country`=?, `dl_issue_country`=?, `notes`=? WHERE `id`= ?;";
        jdbcTemplate.update(sql, new Object[]{v.getPp_passport_number(), v.getPp_passport_expiry_date(), v.getDl_license_number(), v.getDl_license_version(), v.getDl_expiry_date(), v.getAddress(), v.getCountries(), v.getTax_resi_country(), v.getTax_id(), v.getSrc_of_fund(), v.getFull_name(), v.getDob(), v.getPp_issue_country(), v.getDl_issue_country(), v.getNotes(), v.getId()});
    }

}
