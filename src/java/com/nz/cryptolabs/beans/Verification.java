/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import org.slf4j.LoggerFactory;

;

/**
 *
 * @author palo12
 */
public class Verification {

    /**
     * @return the approved_ts
     */
    public String getApproved_ts() {
        return approved_ts;
    }

    /**
     * @param approved_ts the approved_ts to set
     */
    public void setApproved_ts(String approved_ts) {
        this.approved_ts = approved_ts;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the imageData
     */
    public String getImageData() {
        return imageData;
    }

    /**
     * @param imageData the imageData to set
     */
    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    /**
     * @return the countries
     */
    public String getCountries() {
        return countries;
    }

    /**
     * @param countries the countries to set
     */
    public void setCountries(String countries) {
        this.countries = countries;
    }

    /**
     * @return the tax_resi_country
     */
    public String getTax_resi_country() {
        return tax_resi_country;
    }

    /**
     * @param tax_resi_country the tax_resi_country to set
     */
    public void setTax_resi_country(String tax_resi_country) {
        this.tax_resi_country = tax_resi_country;
    }

    /**
     * @return the tax_id
     */
    public String getTax_id() {
        return tax_id;
    }

    /**
     * @param tax_id the tax_id to set
     */
    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    /**
     * @return the src_of_fund
     */
    public String getSrc_of_fund() {
        return src_of_fund;
    }

    /**
     * @param src_of_fund the src_of_fund to set
     */
    public void setSrc_of_fund(String src_of_fund) {
        this.src_of_fund = src_of_fund;
    }

    /**
     * @return the other
     */
    public String getOther() {
        return other;
    }

    /**
     * @param other the other to set
     */
    public void setOther(String other) {
        this.other = other;
    }

    /**
     * @return the dl_expiry_date
     */
    public String getDl_expiry_date() {
        return dl_expiry_date;
    }

    /**
     * @param dl_expiry_date the dl_expiry_date to set
     */
    public void setDl_expiry_date(String dl_expiry_date) {
        this.dl_expiry_date = dl_expiry_date;
    }

    /**
     * @return the is_nz
     */
    public String getIs_nz() {
        return is_nz;
    }

    /**
     * @param is_nz the is_nz to set
     */
    public void setIs_nz(String is_nz) {
        this.is_nz = is_nz;
    }

    /**
     * @return the nz_id
     */
    public String getNz_id() {
        return nz_id;
    }

    /**
     * @param nz_id the nz_id to set
     */
    public void setNz_id(String nz_id) {
        this.nz_id = nz_id;
    }

    /**
     * @return the nz_dl_no
     */
    public String getNz_dl_no() {
        return nz_dl_no;
    }

    /**
     * @param nz_dl_no the nz_dl_no to set
     */
    public void setNz_dl_no(String nz_dl_no) {
        this.nz_dl_no = nz_dl_no;
    }

    /**
     * @return the nz_dl_version
     */
    public String getNz_dl_version() {
        return nz_dl_version;
    }

    /**
     * @param nz_dl_version the nz_dl_version to set
     */
    public void setNz_dl_version(String nz_dl_version) {
        this.nz_dl_version = nz_dl_version;
    }

    /**
     * @return the nz_dl_expiry_date
     */
    public String getNz_dl_expiry_date() {
        return nz_dl_expiry_date;
    }

    /**
     * @param nz_dl_expiry_date the nz_dl_expiry_date to set
     */
    public void setNz_dl_expiry_date(String nz_dl_expiry_date) {
        this.nz_dl_expiry_date = nz_dl_expiry_date;
    }

    /**
     * @return the nz_pp_no
     */
    public String getNz_pp_no() {
        return nz_pp_no;
    }

    /**
     * @param nz_pp_no the nz_pp_no to set
     */
    public void setNz_pp_no(String nz_pp_no) {
        this.nz_pp_no = nz_pp_no;
    }

    /**
     * @return the nz_pp_expiry
     */
    public String getNz_pp_expiry() {
        return nz_pp_expiry;
    }

    /**
     * @param nz_pp_expiry the nz_pp_expiry to set
     */
    public void setNz_pp_expiry(String nz_pp_expiry) {
        this.nz_pp_expiry = nz_pp_expiry;
    }

    /**
     * @return the full_name
     */
    public String getFull_name() {
        return full_name;
    }

    /**
     * @param full_name the full_name to set
     */
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the invite_code
     */
    public String getInvite_code() {
        return invite_code;
    }

    /**
     * @param invite_code the invite_code to set
     */
    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the pp_passport_number
     */
    public String getPp_passport_number() {
        return pp_passport_number;
    }

    /**
     * @param pp_passport_number the pp_passport_number to set
     */
    public void setPp_passport_number(String pp_passport_number) {
        this.pp_passport_number = pp_passport_number;
    }

    /**
     * @return the pp_passport_expiry_date
     */
    public String getPp_passport_expiry_date() {
        return pp_passport_expiry_date;
    }

    /**
     * @param pp_passport_expiry_date the pp_passport_expiry_date to set
     */
    public void setPp_passport_expiry_date(String pp_passport_expiry_date) {
        this.pp_passport_expiry_date = pp_passport_expiry_date;
    }

    /**
     * @return the pp_filename
     */
    public String getPp_filename() {
        return pp_filename;
    }

    /**
     * @param pp_filename the pp_filename to set
     */
    public void setPp_filename(String pp_filename) {
        this.pp_filename = pp_filename;
    }

    /**
     * @return the pa_same_res_addr
     */
    public String getPa_same_res_addr() {
        return pa_same_res_addr;
    }

    /**
     * @param pa_same_res_addr the pa_same_res_addr to set
     */
    public void setPa_same_res_addr(String pa_same_res_addr) {
        this.pa_same_res_addr = pa_same_res_addr;
    }

    /**
     * @return the pa_is_your_pa_street_addr
     */
    public String getPa_is_your_pa_street_addr() {
        return pa_is_your_pa_street_addr;
    }

    /**
     * @param pa_is_your_pa_street_addr the pa_is_your_pa_street_addr to set
     */
    public void setPa_is_your_pa_street_addr(String pa_is_your_pa_street_addr) {
        this.pa_is_your_pa_street_addr = pa_is_your_pa_street_addr;
    }

    /**
     * @return the pa_postal_agency
     */
    public String getPa_postal_agency() {
        return pa_postal_agency;
    }

    /**
     * @param pa_postal_agency the pa_postal_agency to set
     */
    public void setPa_postal_agency(String pa_postal_agency) {
        this.pa_postal_agency = pa_postal_agency;
    }

    /**
     * @return the pa_po_box
     */
    public String getPa_po_box() {
        return pa_po_box;
    }

    /**
     * @param pa_po_box the pa_po_box to set
     */
    public void setPa_po_box(String pa_po_box) {
        this.pa_po_box = pa_po_box;
    }

    /**
     * @return the pa_private_bag
     */
    public String getPa_private_bag() {
        return pa_private_bag;
    }

    /**
     * @param pa_private_bag the pa_private_bag to set
     */
    public void setPa_private_bag(String pa_private_bag) {
        this.pa_private_bag = pa_private_bag;
    }

    /**
     * @return the pa_special_service
     */
    public String getPa_special_service() {
        return pa_special_service;
    }

    /**
     * @param pa_special_service the pa_special_service to set
     */
    public void setPa_special_service(String pa_special_service) {
        this.pa_special_service = pa_special_service;
    }

    /**
     * @return the pa_number
     */
    public String getPa_number() {
        return pa_number;
    }

    /**
     * @param pa_number the pa_number to set
     */
    public void setPa_number(String pa_number) {
        this.pa_number = pa_number;
    }

    /**
     * @return the pa_postal_office
     */
    public String getPa_postal_office() {
        return pa_postal_office;
    }

    /**
     * @param pa_postal_office the pa_postal_office to set
     */
    public void setPa_postal_office(String pa_postal_office) {
        this.pa_postal_office = pa_postal_office;
    }

    /**
     * @return the pa_postal_code
     */
    public String getPa_postal_code() {
        return pa_postal_code;
    }

    /**
     * @param pa_postal_code the pa_postal_code to set
     */
    public void setPa_postal_code(String pa_postal_code) {
        this.pa_postal_code = pa_postal_code;
    }

    /**
     * @return the pa_filename
     */
    public String getPa_filename() {
        return pa_filename;
    }

    /**
     * @param pa_filename the pa_filename to set
     */
    public void setPa_filename(String pa_filename) {
        this.pa_filename = pa_filename;
    }

    /**
     * @return the dl_first_name
     */
    public String getDl_first_name() {
        return dl_first_name;
    }

    /**
     * @param dl_first_name the dl_first_name to set
     */
    public void setDl_first_name(String dl_first_name) {
        this.dl_first_name = dl_first_name;
    }

    /**
     * @return the dl_middle_name
     */
    public String getDl_middle_name() {
        return dl_middle_name;
    }

    /**
     * @param dl_middle_name the dl_middle_name to set
     */
    public void setDl_middle_name(String dl_middle_name) {
        this.dl_middle_name = dl_middle_name;
    }

    /**
     * @return the dl_last_name
     */
    public String getDl_last_name() {
        return dl_last_name;
    }

    /**
     * @param dl_last_name the dl_last_name to set
     */
    public void setDl_last_name(String dl_last_name) {
        this.dl_last_name = dl_last_name;
    }

    /**
     * @return the dl_dob
     */
    public String getDl_dob() {
        return dl_dob;
    }

    /**
     * @param dl_dob the dl_dob to set
     */
    public void setDl_dob(String dl_dob) {
        this.dl_dob = dl_dob;
    }

    /**
     * @return the dl_street
     */
    public String getDl_street() {
        return dl_street;
    }

    /**
     * @param dl_street the dl_street to set
     */
    public void setDl_street(String dl_street) {
        this.dl_street = dl_street;
    }

    /**
     * @return the dl_street_name
     */
    public String getDl_street_name() {
        return dl_street_name;
    }

    /**
     * @param dl_street_name the dl_street_name to set
     */
    public void setDl_street_name(String dl_street_name) {
        this.dl_street_name = dl_street_name;
    }

    /**
     * @return the dl_street_number
     */
    public String getDl_street_number() {
        return dl_street_number;
    }

    /**
     * @param dl_street_number the dl_street_number to set
     */
    public void setDl_street_number(String dl_street_number) {
        this.dl_street_number = dl_street_number;
    }

    /**
     * @return the dl_license_number
     */
    public String getDl_license_number() {
        return dl_license_number;
    }

    /**
     * @param dl_license_number the dl_license_number to set
     */
    public void setDl_license_number(String dl_license_number) {
        this.dl_license_number = dl_license_number;
    }

    /**
     * @return the dl_license_version
     */
    public String getDl_license_version() {
        return dl_license_version;
    }

    /**
     * @param dl_license_version the dl_license_version to set
     */
    public void setDl_license_version(String dl_license_version) {
        this.dl_license_version = dl_license_version;
    }

    /**
     * @return the dl_filename
     */
    public String getDl_filename() {
        return dl_filename;
    }

    /**
     * @param dl_filename the dl_filename to set
     */
    public void setDl_filename(String dl_filename) {
        this.dl_filename = dl_filename;
    }

    /**
     * @return the pp_issue_country
     */
    public String getPp_issue_country() {
        return pp_issue_country;
    }

    /**
     * @param pp_issue_country the pp_issue_country to set
     */
    public void setPp_issue_country(String pp_issue_country) {
        this.pp_issue_country = pp_issue_country;
    }

    /**
     * @return the dl_issue_country
     */
    public String getDl_issue_country() {
        return dl_issue_country;
    }

    /**
     * @param dl_issue_country the dl_issue_country to set
     */
    public void setDl_issue_country(String dl_issue_country) {
        this.dl_issue_country = dl_issue_country;
    }

    private String id;
    private String address;
    private String user_id;
    private String created_ts;
    private String approved_ts;
    private String pp_passport_number;
    private String pp_passport_expiry_date;
    private String pp_filename;
    private String pp_issue_country;
    private String pa_same_res_addr;
    private String pa_is_your_pa_street_addr;
    private String pa_postal_agency;
    private String pa_po_box;
    private String pa_private_bag;
    private String pa_special_service;
    private String pa_number;
    private String pa_postal_office;
    private String pa_postal_code;
    private String pa_filename;
    private String dl_first_name;
    private String dl_middle_name;
    private String dl_last_name;
    private String dl_dob;
    private String dl_street;
    private String dl_street_name;
    private String dl_street_number;
    private String dl_license_number;
    private String dl_license_version;
    private String dl_expiry_date;
    private String dl_issue_country;
    private String dl_filename;
    private String invite_code;
    private String is_nz;
    private String nz_id;
    private String nz_dl_no;
    private String nz_dl_version;
    private String nz_dl_expiry_date;
    private String nz_pp_no;
    private String nz_pp_expiry;
    private String full_name;
    private String dob;
    private String countries;
    private String tax_resi_country;
    private String tax_id;
    private String src_of_fund;
    private String other;
    private String imageData;
    private String notes;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Verification.class);

    @Override
    public String toString() {
        return toString(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

}
