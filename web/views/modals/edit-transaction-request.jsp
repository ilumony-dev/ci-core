<%-- 
    Document   : edit-investment-shares
    Created on : 18 Dec, 2017, 10:54:21 AM
    Author     : palo12
--%>
<%-- 
    Document   : updateinvestmentshares
    Created on : Oct 15, 2017, 9:47:52 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div class="modal fade" id="edit-transaction-request-modal" tabindex="-1" role="dialog" aria-labelledby="edit-transaction-request-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="edit-transaction-request-title">Edit Transaction Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="./admin-update-transacation-request">
                <div class="modal-body">
                    <input type="hidden" name="id" id="edit-transaction-request-id"/>
                    <input type="hidden" name="userId" id="edit-transaction-request-userId"/>
                    <div class="form-group">
                        <label>USD Amount:</label>
                        <input type="number" class="form-control" name="amount" placeholder="Enter amount" id="edit-transaction-request-amount" step="0.01"/>
                    </div>
                    <div class="form-group">
                        <label>Currency:</label>
                        <select class="form-control" name="currency" id="edit-transaction-request-currency">
                            <option value="USD">United States Dollar (US$)</option>
                            <option value="NZD">New Zealand Dollar (NZ$)</option>
                            <option value="SGD">Singapore Dollar ($)</option>
                            <option value="AUD">Australian Dollar (AU$)</option>
                            <option value="GBP">Great Britain Pound (GB�)</option>
                            <option value="HKD">Hong Kong Dollar (HK$)</option>
                            <option value="INR">Indian National Rupee (INR)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Local Amount:</label>
                        <input type="number" class="form-control" placeholder="Enter local amount" name="localAmount"  id="edit-transaction-request-localAmount" step="0.01"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Apply Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var data = new Array();

    function filterCoinsShares() {
        var createdDate = $("#datetimes").val();
        var filtereddata = new Array();
        $.each(data, function (index, obj) {
            if (createdDate === obj.createdDate) {
                filtereddata.push(obj);
            }
        });
        setSharesInTable(filtereddata, '');
    }

    function setSharesInTable(db, rId) {
        var pendingSharesHtmlCode = '';
        $("#purchasedSharesTable").html(pendingSharesHtmlCode);
        $.each(db, function (index, obj) {
            if (index === 0) {
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="reqId" value="' + rId + '">';
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="investmentId" value="' + obj.investmentId + '">';
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="userId" value="' + obj.userId + '">';
            }
            var htmlCode = '<tr>';
            htmlCode = htmlCode + '<input type="hidden" name="id" value="' + obj.id + '">';
            htmlCode = htmlCode + '<input type="hidden" name="shareId" value="' + obj.shareId + '">';
            htmlCode = htmlCode + '<input type="hidden" name="coinId" value="' + obj.coinId + '">';
            htmlCode = htmlCode + '<td>' + obj.createdDate + '</td>';
            htmlCode = htmlCode + '<td>' + obj.shareName + '</td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="shareAmount" id="shareAmount' + index + '" step="0.01" value="' + obj.shareAmount + '" onkeyup="sdpeq(' + index + ')"></td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="price" id="price' + index + '" step="0.0001" value="' + obj.price + '" onchange="sdpeq(' + index + ')"></td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="quantity" id="quantity' + index + '" step="0.0001" value="' + obj.quantity + '" onkeyup="qpes(' + index + ')"></td>';
            htmlCode = htmlCode + '</tr> ';
            pendingSharesHtmlCode = pendingSharesHtmlCode + htmlCode;
        });
        $("#purchasedSharesTable").html(pendingSharesHtmlCode);
    }

    function purchasedShares4Edit(mId, uId) {
        var url = './rest/cryptolabs/api/sdadfjlsdweroweiuxmcbncmxcmc?uId=' + uId + '&mId=' + mId;
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (db) {
                data = db;
                var datetimes = new Array();
                var createdDate = '';
                $.each(data, function (index, obj) {
                    if (createdDate !== obj.createdDate) {
                        datetimes.push(obj.createdDate);
                        createdDate = obj.createdDate;
                    }
                });
                var opts = '<option>Select Created Date</option>';
                $.each(datetimes, function (index, datetime) {
                    opts = opts + '<option>' + datetime + '</option>';
                });
                $("#datetimes").html(opts);
                $("#txnTitle").text('Update Coins/Shares');
                document.getElementById('updateCoinsSharesForm').action = "./admin-update-purchased-shares";
                filterCoinsShares();
            }
        });
    }
</script>
