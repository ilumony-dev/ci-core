/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Service
public class FileUtility {

    @Autowired
    private ServletContext servletContext;

    public String rootDirectoryPath() {
        String path = servletContext.getRealPath("/resources");
        return path;
    }

    public String imagesRootDirectoryPath() {
        return rootDirectoryPath() + File.separator + "img";
    }

    public String mailTemplatesDirectoryPath(String folderName) {
        if (folderName != null) {
            return rootDirectoryPath() + File.separator + "mail-templates" + File.separator + folderName;
        }
        return rootDirectoryPath() + File.separator + "mail-templates";
    }

    public File rootDirectory() {
        // Creating or fetch the directory to store file
        File root = new File(rootDirectoryPath());
        if (!root.exists()) {
            root.mkdirs();
        }
        return root;
    }

    public File imagesRootDirectory() {
        // Creating or fetch the directory to store file
        File root_images = new File(imagesRootDirectoryPath());
        if (!root_images.exists()) {
            root_images.mkdirs();
        }
        return root_images;
    }

    public File mailTemplatesDirectory() {
        // Creating or fetch the directory to store file
        File dir = new File(mailTemplatesDirectoryPath(null));
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public File storeImage(long createdBy, String imageSource, String imageName) throws IOException {
        byte[] imageByte = Base64.decode(imageSource);
        return storeImage(createdBy, imageByte, imageName);
    }

    public File storeImage(long createdBy, MultipartFile file, String imageName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(createdBy, imageByte, imageName);
        }
        return null;
    }

    public File storeImage(long createdBy, byte[] imageByte, String imageName) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();
        String dirPath = imagesRootDirectoryPath();
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + imageName);
        int lastIndexOf = imageName.lastIndexOf(".") + 1;
        String extension = imageName.substring(lastIndexOf);
        ImageIO.write(image, extension, outputfile);
        return outputfile;
    }

    public String getImage(long userid, String imageName) throws IOException {
        String dirPath = imagesRootDirectoryPath();
        File returnfile = new File(dirPath + File.separator + imageName);
        if (returnfile.exists()) {
            FileInputStream fis = new FileInputStream(returnfile);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] fileBytes = new byte[(int) returnfile.length()];
            inputStream.read(fileBytes);
            inputStream.close();
            String imageSource = Base64.encode(fileBytes);
            return imageSource;
        } else {
            return null;
        }
    }

    public File getFile(String fileDirectory, String filePath) {
        File file = new File(fileDirectory + File.separator + filePath);
        return file;
    }

    public String getFileExists(String fileDirectoryPath, String filePath, String noFile) {
        String fileExists = "";
        fileExists = filePath.replace('/', File.separatorChar);
        String path = fileDirectoryPath + File.separator + fileExists;
        File f = new File(path);
        if (f.exists()) {
            return fileExists;
        } else {
            return noFile;
        }
    }

    public Set<String> getFileNames(File folder) {
        File[] listOfFiles = folder.listFiles();
        Set<String> fileNames = new HashSet<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        return fileNames;
    }
}
