<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html xmlns:layout="http://www.thymeleaf.org" xmlns:th="http://www.thymeleaf.org">
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible"/>
        <meta content="template language" name="keywords"/>
        <meta content="Tamerlan Soziev" name="author"/>
        <meta content="User dashboard html template" name="description"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="${home}/resources/favicon.png" rel="shortcut icon"/>
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon"/>
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css"/>
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet"/>
        <link href="${home}/resources/css/style.css" rel="stylesheet"/>
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet"/>
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet"/>
        <link rel='stylesheet prefetch' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'/>
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script> 

    </head>
    <!--<body onload="balChart('today');">-->
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${endUser.fullName} Profile</span>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${currSymbol}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Crypto Net Worth Overview
                                        </h6>
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs smaller">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab_overview">Live Crypto Balance</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#tab_latest_week">Latest Week</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#tab_latest_15_days">Latest 15 Days</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#tab_latest_30_days">Latest 30 Days</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_overview">
                                                        <div class="label" style="display:inline-block">
                                                            INDICATIVE LIVE BALANCE<br>(excluding cash in wallet)  <img src="./resources/img/info-icon.png" data-toggle="tooltip" title="The Live Balance chart provides an indication of the real-time balance of your cryptocurrency portfolio, and is reliant on external data sources which may be different to what is reported on cryptocurrent exchanges. The actual daily closing value and withdrawal value of your portfolio will be different. This balance does not include any cash that is available in your Invsta cash account." data-placement="right" style="vertical-align:text-bottom">
                                                        </div>
                                                        <div class="label" style="float:right; text-align: right">
                                                            Current Balance<br>
                                                            (previous day close, incl cash in wallet)
                                                            <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom" data-toggle="tooltip" title="This represents the actual value of your cryptocurrency portfolio investments based on the previous day closing value, and includes any cash you may have available in your Invsta cash account.">
                                                        </div>
                                                        <div class="el-tablo">
                                                            <div class="value currentBalance">
                                                            </div>
                                                            <div class="value sumWalletBalance" style="float:right;">
                                                            </div>
                                                        </div>
                                                        <div class="el-chart-w"  id="linegraph">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="tab_latest_week">
                                                        <div class="label">
                                                            BALANCE (PREVIOUS DAY CLOSE)   <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                        </div>
                                                        <div class="el-tablo">
                                                            <div class="value prev-actual-balance">
                                                            </div>
                                                        </div>
                                                        <div class="el-chart-w"  id="lg-latestweek">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="tab_latest_15_days">
                                                        <div class="label">
                                                            BALANCE (PREVIOUS DAY CLOSE)   <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                        </div>
                                                        <div class="el-tablo">
                                                            <div class="value prev-actual-balance">
                                                            </div>
                                                        </div>
                                                        <div class="el-chart-w"  id="lg-latest15days">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="tab_latest_30_days">
                                                        <div class="label">
                                                            BALANCE (PREVIOUS DAY CLOSE)   <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                        </div>
                                                        <div class="el-tablo">
                                                            <div class="value prev-actual-balance">
                                                            </div>
                                                        </div>
                                                        <div class="el-chart-w"  id="lg-latest30days">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invsta Portfolio Breakdown
                                        </h6>
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs smaller">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab_overview-cons-invs-pieChart" aria-expanded="true">Live Crypto Balance
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#tab_latest_week-cons-invs-pieChart" aria-expanded="false">Previous Day Balance
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_overview-cons-invs-pieChart" aria-expanded="true" style="">
                                                        <div class=" el-tablo">
                                                            <div class="label">Live  INVESTMENT Balance  <img src="./resources/img/info-icon.png"  style="vertical-align:text-bottom"></div>
                                                            <div class="value currentBalance"></div>
                                                            <input type="hidden" id="actualInvested"/>
                                                            <div class="el-chart-w" id="cons-invs-pieChart"></div>
                                                            <div id="legend-1"></div>
                                                        </div>
                                                        <%--<div class="el-chart-w" id="linegraph" data-highcharts-chart="22"><div id="highcharts-evv9ta5-44" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 493px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="493" height="400" viewBox="0 0 493 400"><desc>Created with Highcharts 6.1.0</desc><defs><clipPath id="highcharts-evv9ta5-45"><rect x="0" y="0" width="398" height="351" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="493" height="400" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="85" y="10" width="398" height="351"></rect><g class="highcharts-pane-group"></g><g class="highcharts-plot-lines-0"><path fill="none" stroke="#808080" stroke-width="1" visibility="hidden"></path></g><g class="highcharts-grid highcharts-xaxis-grid "><path fill="none" opacity="1" class="highcharts-grid-line" d="M 152.1149583758552 10 L 152.1149583758552 361"></path><path fill="none" opacity="1" class="highcharts-grid-line" d="M 286.53033897519305 10 L 286.53033897519305 361"></path><path fill="none" opacity="1" class="highcharts-grid-line" d="M 421.0711342809771 10 L 421.0711342809771 361"></path></g><g class="highcharts-grid highcharts-yaxis-grid "><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 186.5 L 483 186.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 69.5 L 483 69.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 303.5 L 483 303.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 361.5 L 483 361.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 244.5 L 483 244.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 127.5 L 483 127.5"></path><path fill="none" stroke="#e6e6e6" stroke-width="1" opacity="1" class="highcharts-grid-line" d="M 85 9.5 L 483 9.5"></path></g><rect fill="none" class="highcharts-plot-border" x="85" y="10" width="398" height="351"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 152.0711342809771 361 L 152.0711342809771 371" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 286.53033897519305 361 L 286.53033897519305 371" opacity="1"></path><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 421.0711342809771 361 L 421.0711342809771 371" opacity="1"></path><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 85 361.5 L 483 361.5"></path></g><g class="highcharts-axis highcharts-yaxis "><text x="26.234375" text-anchor="middle" transform="translate(0,0) rotate(270 26.234375 185.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="185.5"><tspan>Value</tspan></text><path fill="none" class="highcharts-axis-line" d="M 85 10 L 85 361"></path></g><g class="highcharts-series-group"><g class="highcharts-series highcharts-series-0 highcharts-line-series highcharts-color-0 " transform="translate(85,10) scale(1 1)" clip-path="url(#highcharts-evv9ta5-45)"><path fill="none" d="M 3.901959933467147 215.0721247122439 L 11.226264891915676 313.56000000000745 L 24.684366593777685 176.66999999999894 L 38.14246829563946 98.28000000000372 L 51.600569997501786 127.53000000000372 L 65.04521359766159 313.56000000000745 L 78.50331529952392 126.36000000000479 L 91.97487510308777 126.36000000000479 L 105.43297680495009 153.27000000002022 L 118.89107850681008 118.16999999999894 L 132.3491802086723 177.84000000001117 L 145.79382380883166 218.79000000000053 L 159.25192551069387 238.68000000000904 L 172.723485314259 332.2799999999904 L 186.1815870161212 253.8899999999952 L 199.62623061628054 283.1399999999819 L 213.09779041984117 122.84999999999468 L 226.59626642681303 122.84999999999468 L 240.00053572186272 53.820000000004256 L 253.45863742373047 209.42999999999574 L 266.9167391255927 53.820000000004256 L 280.37484082745493 301.8599999999915 L 293.83294252931717 30.419999999998936 L 307.29104423117934 147.42000000001224 L 320.7491459330416 108.81000000000745 L 334.2072476349038 115.83000000001437 L 347.66534933676155 119.33999999999787 L 361.1234510386238 119.33999999999787 L 374.58155274048596 16.380000000011705 L 388.03965444234825 66.68999999999255 L 394.0980400665337 127.73945403019341" class="highcharts-graph" stroke="#7cb5ec" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path><path fill="none" d="M -6.0980392156863 313.56000000000745 L 3.9019607843137 313.56000000000745 L 17.356997971602 176.66999999999894 L 30.812035158891 98.28000000000372 L 44.26707234618 127.53000000000372 L 57.708654496281 313.56000000000745 L 71.16369168357 126.36000000000479 L 84.632183908046 126.36000000000479 L 98.087221095335 153.27000000002022 L 111.54225828262 118.16999999999894 L 124.99729546991 177.84000000001117 L 138.43887762001 218.79000000000053 L 151.8939148073 238.68000000000904 L 165.36240703178 332.2799999999904 L 178.81744421907 253.8899999999952 L 192.25902636917 283.1399999999819 L 205.72751859364 122.84999999999468 L 219.22292089249 122.84999999999468 L 232.62413793103 53.820000000004256 L 246.07917511832 209.42999999999574 L 259.53421230561 53.820000000004256 L 272.9892494929 301.8599999999915 L 286.44428668019 30.419999999998936 L 299.89932386748 147.42000000001224 L 313.35436105477 108.81000000000745 L 326.80939824206 115.83000000001437 L 340.26443542934 119.33999999999787 L 353.71947261663 119.33999999999787 L 367.17450980392 16.380000000011705 L 380.62954699121 66.68999999999255 L 394.09803921569 202.41000000000213 L 404.09803921569 202.41000000000213" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path></g><g class="highcharts-markers highcharts-series-0 highcharts-line-series highcharts-color-0  highcharts-tracker" transform="translate(85,10) scale(1 1)"><path fill="#7cb5ec" visibility="hidden" d="M 68 160.05600000000607 A 0 0 0 1 1 68 160.05600000000607 Z" class="highcharts-halo highcharts-color-0" fill-opacity="0.25"></path><path fill="#7cb5ec" d="M 74 160.05600000000607 A 6 6 0 1 1 73.99999700000025 160.05000000100605 Z" stroke="#ffffff" stroke-width="1" visibility="hidden"></path><path fill="#7cb5ec" d="M 14.702532003958504 313.56000000000745 A 4 4 0 1 1 14.702530003958671 313.5560000006741 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 28.152351146532897 176.66999999999894 A 4 4 0 1 1 28.152349146533062 176.6660000006656 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 41.74627807992682 98.28000000000372 A 4 4 0 1 1 41.746276079926986 98.2760000006704 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 55.19297250278919 127.53000000000372 A 4 4 0 1 1 55.19297050278936 127.5260000006704 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 68.74627807992681 313.56000000000745 A 4 4 0 1 1 68.74627607992697 313.5560000006741 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 82.19297250278919 126.36000000000479 A 4 4 0 1 1 82.19297050278935 126.35600000067146 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 95.19297250278919 126.36000000000479 A 4 4 0 1 1 95.19297050278935 126.35600000067146 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 109.19297250278919 153.27000000002022 A 4 4 0 1 1 109.19297050278935 153.2660000006869 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 122.23356650167763 118.16999999999894 A 4 4 0 1 1 122.23356450167779 118.1660000006656 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 135.78999469411437 177.84000000001117 A 4 4 0 1 1 135.78999269411455 177.83600000067784 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 149.23356650167764 218.79000000000053 A 4 4 0 1 1 149.23356450167782 218.7860000006672 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 162.78999469411437 238.68000000000904 A 4 4 0 1 1 162.78999269411455 238.6760000006757 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 176.1929725027892 332.2799999999904 A 4 4 0 1 1 176.19297050278936 332.27600000065706 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 189.7462780799268 253.8899999999952 A 4 4 0 1 1 189.746276079927 253.88600000066188 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 203.1929725027892 283.1399999999819 A 4 4 0 1 1 203.19297050278936 283.13600000064855 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 216.7462780799268 122.84999999999468 A 4 4 0 1 1 216.746276079927 122.84600000066135 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 230.1929725027892 122.84999999999468 A 4 4 0 1 1 230.19297050278936 122.84600000066135 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 243.78999469411437 53.820000000004256 A 4 4 0 1 1 243.78999269411455 53.81600000067092 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 257.2335665016776 209.42999999999574 A 4 4 0 1 1 257.2335645016778 209.4260000006624 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 270.2335665016776 53.820000000004256 A 4 4 0 1 1 270.2335645016778 53.81600000067092 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 283.8336801206641 301.8599999999915 A 4 4 0 1 1 283.8336781206643 301.8560000006581 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 297.2741315406167 30.419999999998936 A 4 4 0 1 1 297.27412954061685 30.4160000006656 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 310.7899946941144 147.42000000001224 A 4 4 0 1 1 310.7899926941146 147.4160000006789 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 324.2741315406167 108.81000000000745 A 4 4 0 1 1 324.27412954061685 108.80600000067412 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 337.8336801206641 115.83000000001437 A 4 4 0 1 1 337.8336781206643 115.82600000068103 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 351.314666018168 119.33999999999787 A 4 4 0 1 1 351.3146640181682 119.33600000066454 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 364.8773326349501 119.33999999999787 A 4 4 0 1 1 364.8773306349503 119.33600000066454 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 378.314666018168 16.380000000011705 A 4 4 0 1 1 378.3146640181682 16.37600000067837 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 391.8773326349501 66.68999999999255 A 4 4 0 1 1 391.8773306349503 66.68600000065922 Z" class="highcharts-point highcharts-color-0"></path><path fill="#7cb5ec" d="M 398 202 A 4 4 0 1 1 397.9999980000002 201.99600000066667 Z" class="highcharts-point highcharts-color-0"></path></g></g><text x="247" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text><text x="247" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text><g class="highcharts-axis-labels highcharts-xaxis-labels "><text x="152.58475280332357" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="380" opacity="1">10:41:40</text><text x="287.1652470454926" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="380" opacity="1">10:41:50</text><text x="421.7457412876562" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="380" opacity="1">10:42:00</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels "><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="188" opacity="1">670</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="71" opacity="1">671</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="305" opacity="1">669</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="364" opacity="1">668.5</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="247" opacity="1">669.5</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="130" opacity="1">670.5</text><text x="70" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="13" opacity="1">671.5</text></g><g class="highcharts-label highcharts-tooltip highcharts-color-0" style="cursor:default;pointer-events:none;white-space:nowrap;" transform="translate(68,-9999)" opacity="0" visibility="visible"><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 61.5 C 147.5 64.5 147.5 64.5 144.5 64.5 L 79.5 64.5 73.5 70.5 67.5 64.5 3.5 64.5 C 0.5 64.5 0.5 64.5 0.5 61.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 61.5 C 147.5 64.5 147.5 64.5 144.5 64.5 L 79.5 64.5 73.5 70.5 67.5 64.5 3.5 64.5 C 0.5 64.5 0.5 64.5 0.5 61.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path><path fill="none" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 61.5 C 147.5 64.5 147.5 64.5 144.5 64.5 L 79.5 64.5 73.5 70.5 67.5 64.5 3.5 64.5 C 0.5 64.5 0.5 64.5 0.5 61.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" isShadow="true" stroke="#000000" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path><path fill="rgba(247,247,247,0.85)" class="highcharts-label-box highcharts-tooltip-box" d="M 3.5 0.5 L 144.5 0.5 C 147.5 0.5 147.5 0.5 147.5 3.5 L 147.5 61.5 C 147.5 64.5 147.5 64.5 144.5 64.5 L 79.5 64.5 73.5 70.5 67.5 64.5 3.5 64.5 C 0.5 64.5 0.5 64.5 0.5 61.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path><text x="8" style="font-size:12px;color:#333333;fill:#333333;" y="20"><tspan style="font-weight:bold">Live data</tspan><tspan x="8" dy="15">2018-04-17 10:40:58</tspan><tspan x="8" dy="15">670.72</tspan></text></g></svg></div></div>--%>
                                                    </div>
                                                    <div class="tab-pane" id="tab_latest_week-cons-invs-pieChart" style="" aria-expanded="false">
                                                        <%--<div class="el-chart-w" id="lg-latestweek" data-highcharts-chart="23"><div id="highcharts-evv9ta5-48" dir="ltr" class="highcharts-container " style="position: relative; overflow: hidden; width: 493px; height: 400px; text-align: left; line-height: normal; z-index: 0; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg version="1.1" class="highcharts-root" style="font-family:&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="493" height="400" viewBox="0 0 493 400"><desc>Created with Highcharts 6.1.0</desc><defs><clipPath id="highcharts-evv9ta5-49"><rect x="0" y="0" width="391" height="351" fill="none"></rect></clipPath></defs><rect fill="#ffffff" class="highcharts-background" x="0" y="0" width="493" height="400" rx="0" ry="0"></rect><rect fill="none" class="highcharts-plot-background" x="92" y="10" width="391" height="351"></rect><g class="highcharts-pane-group"></g><g class="highcharts-plot-lines-0"><path fill="none" stroke="#808080" stroke-width="1" visibility="hidden"></path></g><g class="highcharts-grid highcharts-xaxis-grid "><path fill="none" class="highcharts-grid-line" d="M 287.5 10 L 287.5 361" opacity="1"></path></g><g class="highcharts-grid highcharts-yaxis-grid "><path fill="none" stroke="#e6e6e6" stroke-width="1" class="highcharts-grid-line" d="M 92 186.5 L 483 186.5" opacity="1"></path></g><rect fill="none" class="highcharts-plot-border" x="92" y="10" width="391" height="351"></rect><g class="highcharts-axis highcharts-xaxis "><path fill="none" class="highcharts-tick" stroke="#ccd6eb" stroke-width="1" d="M 287.5 361 L 287.5 371" opacity="1"></path><path fill="none" class="highcharts-axis-line" stroke="#ccd6eb" stroke-width="1" d="M 92 361.5 L 483 361.5"></path></g><g class="highcharts-axis highcharts-yaxis "><text x="26.28125" text-anchor="middle" transform="translate(0,0) rotate(270 26.28125 185.5)" class="highcharts-axis-title" style="color:#666666;fill:#666666;" y="185.5"><tspan>Value</tspan></text><path fill="none" class="highcharts-axis-line" d="M 92 10 L 92 361"></path></g><g class="highcharts-series-group"><g class="highcharts-series highcharts-series-0 highcharts-line-series " transform="translate(92,10) scale(1 1)" clip-path="url(#highcharts-evv9ta5-49)"><path fill="none" d="M 195.5 175.5" class="highcharts-graph" stroke="#FF0000" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"></path><path fill="none" d="M 185.5 175.5 L 195.5 175.5 L 205.5 175.5" stroke-linejoin="round" visibility="visible" stroke="rgba(192,192,192,0.0001)" stroke-width="22" class="highcharts-tracker"></path></g><g class="highcharts-markers highcharts-series-0 highcharts-line-series highcharts-tracker" transform="translate(92,10) scale(1 1)"><path fill="#FF0000" d="M 199 176 A 4 4 0 1 1 198.99999800000018 175.99600000066667 Z" class="highcharts-point"></path></g></g><text x="247" text-anchor="middle" class="highcharts-title" style="color:#333333;font-size:18px;fill:#333333;" y="24"></text><text x="247" text-anchor="middle" class="highcharts-subtitle" style="color:#666666;fill:#666666;" y="24"></text><g class="highcharts-axis-labels highcharts-xaxis-labels "><text x="287.5" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="middle" transform="translate(0,0)" y="380" opacity="1">10:30:18.274</text></g><g class="highcharts-axis-labels highcharts-yaxis-labels "><text x="77" style="color:#666666;cursor:default;font-size:11px;fill:#666666;" text-anchor="end" transform="translate(0,0)" y="188" opacity="1">666.57</text></g></svg></div></div>--%>
                                                        <div class=" el-tablo">
                                                            <div class="label">Current Balance<br>
                                                                (previous day close, incl cash in wallet)
                                                                <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom"></div>
                                                            <div class="value prev-actual-balance"></div>
                                                            <div class="el-chart-w" id="cons-invs-pieChart2"></div>
                                                            <div id="legend-2"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">Invsta Crypto Portfolio Snapshot</h6>
                                    </div>
                                </div>
                            </div>
                            <c:forEach items="${userInvestments}" var="inv">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <div id="elementboxcontent" class="element-box">
                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;">${inv.fundName}</h6><img src="./resources/img/info-icon.png" style="vertical-align:text-top"> in ${currSymbol}
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12">
                                                        <div class="fund-img" style="width:100%;height: 150px;cursor: pointer; background:url(./resources/img/${inv.fundImage}) center center no-repeat; background-size: cover;">
                                                            <span class="fund-name">${inv.fundName}</span>
                                                            <!--<img src="./resources/img/balance.png" >-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-4">
                                                        <div id="initialInvestment${inv.investmentId}" class="centered" style="height:150px"></div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-4">
                                                        <input type="hidden" class="hdn-currentAmount${inv.investmentId}"/>
                                                        <div id="currentAmount${inv.investmentId}" class="centered" style="height:150px"></div>
                                                    </div>
                                                    <div class="col-md-3  col-sm-4">
                                                        <div id="performance${inv.investmentId}" class="centered" style="height:150px"></div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:20px">
                                                    <div class="col-md-12 text-right">
                                                        <c:choose>
                                                            <c:when test="${info.admin}">
                                                                <a class="btn btn-primary" href="./suflsdjweafkjsdlkj?ok=${inv.investmentId}&un=${endUser.userId}">Show Me More</a>
                                                                <a class="btn btn-danger" href="#" onclick="purchasedShares4Edit('${inv.investmentId}', '${endUser.userId}');" data-target="#edit-investment-shares" data-toggle="modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                <a class="btn btn-primary" href="./suflsdjweafkjsdlkj?ok=${inv.investmentId}">Show Me More</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 10px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>                            
                        </div>
                        <jsp:include page = "modals/edit-investment-shares.jsp" />
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/moment/moment.js"></script>
        <script src="./resources/js/switch-color.js"></script>
        <script src="./resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="./resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="./resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="./resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="./resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="./resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="./resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="./resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="./resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="./resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="./resources/js/main.js?version=3.5.1"></script>
        <script src="./resources/js/cryptolabs/investmentJs/SliderJs.js"></script> 
        <script src="./resources/js/cryptolabs/investmentJs/MyPlanJs.js"></script> 
        <script src="./resources/js/cryptolabs/investmentJs/CalculateMyBalanceJs.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
        <script src="./resources/js/cryptolabs/user-main.js"></script>
        <!--<script src="./resources/js/cryptolabs/admin-main.js"></script>-->
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script th:inline="javascript">
                                                                    var currency = '${currency}', currSymbol = '${currSymbol}';
                                                                    var info = null, endUser = null;
        </script>
        <script src="${home}/resources/js/cryptolabs/user-charts.js"></script>
        <script src="${home}/resources/js/cryptolabs/fire-events.js"></script>
        <script>
                                                                    $(document).ready(function () {
                                                                    $.get("./rest/cryptolabs/modelmap/${key}", function (data, status) {
                                                                    var jO = JSON.parse(data);
                                                                    info = jO.info;
                                                                    endUser = jO.endUser;
                                                                    var userInvestments = jO.userInvestments;
                                                                    var previousDay = jO.previousDay;
                                                                    var latestWeek = jO.latestWeek;
                                                                    var latest15days = jO.latest15days;
                                                                    var latest30days = jO.latest30days;
                                                                    var sumWalletBalance = eval(jO.sumWalletBalance);
                                                                    initSetDataInCharts(userInvestments);
                                                                    var actualAmount = 0;
                                                                    for (var i = 0; i < userInvestments.length; i++) {
                                                                    var y = eval(userInvestments[i].unitsAmount);
                                                                    if (y > 0 || y < 0) {
                                                                    actualAmount += y;
                                                                    }
                                                                    }
                                                                    var values1 = new Array();
                                                                    for (var j = 0; j < latestWeek.length; j++) {
                                                                    var day = latestWeek[j];
                                                                    values1.push({
                                                                    x: new Date(day.current_date),
                                                                            y: eval(day.investment_amount)
                                                                    });
                                                                    }
                                                                    lglatestweek(values1, 'lg-latestweek');
                                                                    var values2 = new Array();
                                                                    for (var j = 0; j < latest15days.length; j++) {
                                                                    var day = latest15days[j];
                                                                    values2.push({
                                                                    x: new Date(day.current_date),
                                                                            y: eval(day.investment_amount)
                                                                    });
                                                                    }
                                                                    lglatestweek(values2, 'lg-latest15days');
                                                                    var values3 = new Array();
                                                                    for (var j = 0; j < latest30days.length; j++) {
                                                                    var day = latest30days[j];
                                                                    values3.push({
                                                                    x: new Date(day.current_date),
                                                                            y: eval(day.investment_amount)
                                                                    });
                                                                    }
                                                                    lglatestweek(values3, 'lg-latest30days');
                                                                    var isVerifiedEmail = ${isVerifiedEmail};
                                                                    if (isVerifiedEmail === false) {
                                                                    swal('Please verify your email.');
                                                                    }
                                                                    var prevInvs = new Array();
                                                                    var prevActual = 0;
                                                                    $.each(previousDay, function (idx, inv) {
                                                                    var actual = eval(inv.investment_amount);
                                                                    prevInvs.push({
                                                                    name: inv.fund_name,
                                                                            y: actual,
                                                                            invId: inv.investment_id
                                                                    });
                                                                    prevActual += actual;
                                                                    });
                                                                    var y1 = prevActual;
                                                                    $(".prev-actual-balance").text(currSymbol + y1.formatMoney(0, '.', ','));
                                                                    $(".sumWalletBalance").text(currSymbol + sumWalletBalance.formatMoney(0, '.', ','));
//                                                                    alert(prevInvs);
                                                                    investmentsActual('cons-invs-pieChart2', 'legend-2', prevInvs);
                                                                    endOfReady();
                                                                    });
                                                                    });
        </script>
        <script>
            $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <jsp:include page="footer.jsp"/>
    </body>
</html>