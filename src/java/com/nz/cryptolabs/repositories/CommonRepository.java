/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.EmailVerificationBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TempProfile;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.beans.Verification;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface CommonRepository {

    public void save(String userId, String activity);

    public void save(InviteCode code);

    public void allocateEmail(InviteCode code);

    public void save(UserInfo user);

    public UserInfo findByUsername(String username);

    public UserInfo findManagementUser(String branchId);

    public UserInfo findByUserId(String userId);

    public UserInfo updateUserPassword(UserInfo user);

    public UserInfo updateUserStatus(UserInfo user);

    public void addShare(ShareFund share);

    public ShareFund shareById(String shareId);

    public void updateShare(ShareFund share);

    public void updateSharePrice(ShareFund share);

    public List<ShareFund> shares();

    public void addFund(ShareFund fund);

    public void updateFund(ShareFund fund);

    public List<ShareFund> funds();

    public void updateFundPrice(PortfolioBean fund);

    public List<UserFundsCommand> userFunds(String fundId, String userId, String investmentId);

    public List<ShareFund> sharesByFund(String fundId);

    public List<ShareFund> coinsByFund(String fundId);

    public void saveInvestment(InvestmentBean bean);

    public void saveSellInvestmentRequest(AccountEventCommand event);

    public InvestmentBean sumOfpendingSellTransactions(String userId, String investmentId);

    public List<InvestmentBean> pendingInvestmentRequests(String userId, String id, String tokenNo);

    public List<InvestmentBean> userInvestmentRequests(String userId, String status, String id, String tokenNo);

    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId);

    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId);

    public List<InvestmentBean> purchasedInvestmentsAndCash(String userId, String investmentId, String fundId);

    public List<InvestmentBean> findManagementInvestments(String branchId, String fundId, String investmentId, String userInvId);

    public List<AccountEventCommand> latestInvestments(String userId);

    public List<ShareFund> pendingSharesByInvestment(String investmentId, String reqId);

    public List<ShareFund> pendingCoinsByInvestment(String investmentId);

    public void updatePendingShares(ShareFund investmentShares);

    public void deactivePendingSharesStatus(String userId, String investmentId, String status);

    public TransactionBean purchaseShares(ShareFund investmentShares);

    public void updatePurchasedShares(ShareFund updatedShares);

    public List<ShareFund> sharesByInvestment(String investmentId);

    public List<ShareFund> detailsByInvestment(String userId, String investmentId, String action);

    public List<InvestmentBean> pendingSellTransactions(String userId, String id, String tokenNo);

    public void investmentRequestStatus(String id, String status);

    public void sellShares(ShareFund investmentShares);

    public List<ShareFund> actualSharePercentages(String investmentId);

    public List<ShareFund> actualCoinPercentages(String investmentId);

    public List<Map<String, Object>> totals();

    public List<ShareFund> latestBalanceData(String type, String userId, String investmentId);

    public List<TransactionBean> userAccountSummary(String userId);

    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate);

    public List<InviteCode> inviteCodes(Boolean available, Boolean expired, Boolean used);

    public List<InviteCode> checkInviteCode(String code, String type);

    public List<StateCity> countries(Boolean states, Boolean cities);

    public Boolean deActiveShare(String shareId);

    public Boolean deActiveFund(String fundId);

    public void saveVerification(Verification verification);

    public List<Verification> findVerification(String userId, String status);

    public int saveStripeCharge(StripeCharge charge);

    public int saveTransactionRequest(RequestBean req);

    public void updateTransactionRequest(RequestBean req);

    public List<RequestBean> userTransactionRequests(String userId, String status, String id, String tokenNo);

    public Boolean transactionRequestStatus(String id, String status, String master);

    public int saveTransaction(TransactionBean tran);

    public int saveWalletTransaction(TransactionBean txn);

    public List<TransactionBean> userWalletTransactions(String userId);

    public List<TransactionBean> walletTxns(String id, String brand, String userId, Boolean active, Boolean approved);

    public List<TransactionBean> pendingWalletTxns(String brand, String userId);

    public List<TransactionBean> actualWalletBalances(String userId);

    public ShareFund fundById(String fundId, String investmentId);

    public void addReferenceNo(UserReference reference);

    public UserReference findReferenceNo(String token, String ref);

    public void updatedAmounted(String ref);

    public List<ShareFund> totalCoins(String userId, String investmentId);

    public List<UserInfo> users(Date... dateArr);

    public void addTrade(ShareFund trade, List<ShareFund> totalUnits);

    public ShareFund tradeByFundId(String id);

    public List<ShareFund> totalUnitsById(String fundId, String userId, String invesmentId);

    public void addBitcoin(ShareFund bitcoin);

    public List<ShareFund> currencies();

    public void addCurrency(ShareFund currency);

    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals);

    public void saveProfile(UserInfo userInfo);

    public void saveConfiguration(Configuration config);

    public Configuration findConfiguration(String userId);

    public List<PortfolioBean> portfolioReportById(List<String> currDates, String portfolioId);

    public List<PortfolioBean> portfoliosReport();

    public String fundPrice(String fundId, String investmentId);

    public List<ShareFund> fundPrices(String fundId, String investmentId);

    public List<PortfolioBean> userPortfolioUnits(String fundId);

    public List<PortfolioBean> userPortfolioShares(String fundId);

    public void saveCurrencyConversionRate(String currencyPair, String date, String price);

    public ShareFund currencyConversionRate(String currencyPair, String date);

    public void updateCurrencyConversionRate(String currencyPair, String date, String price);

    public void saveUserTodayStatus(List<InvestmentBean> investments);

    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate, Boolean sum);

    public List<UserFundsCommand> getUserTodayStatusNew(String userId, String investmentId, String fDate, String tDate, Boolean sum);

    public void saveEmailVerification(EmailVerificationBean bean);

    public EmailVerificationBean getEmailVerification(String refId);

    public void verifiedEmail(EmailVerificationBean bean);

    public Boolean approvedWalletTxn(String id);

    public Boolean approvedTransaction(String id, String txn_req_id);

    public Boolean setVerificationStatus(String id, String status, Date date);

    public List<TransactionBean> portfolioReportType(String reportType, Date firstDate, Date lastDate, String fundId);

    public void addPortfolioBuying(TransactionBean transactionBean);

    public List<TransactionBean> getPortfolioBuying(String portfolioId, Date fromDate, Date toDate);

    public List<TransactionBean> getPortfolioBuyingPending(String portfolioId);

    public List<TransactionBean> managementSharesBuyingPortfolio(String portfolioId, String recordId);

    public List<TransactionBean> getPortfolioBuyingHistory(String portfolioId, String txnReqId);

    public void saveTempProfile(UserInfo user);

    public List<TempProfile> getChangingProfiles(String id);

    public void updateCancelChangeProfile(String id);

    public void updateApprovedChangeProfile(String id);

    public void updateVerification(Verification verification);
}
