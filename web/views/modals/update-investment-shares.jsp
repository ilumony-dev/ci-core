<%-- 
    Document   : updateinvestmentshares
    Created on : Oct 15, 2017, 9:47:52 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="update-investment-shares" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="post" action="./" id="updateinvestmentshares-form">
            <div class="modal-content">
                <div class="modal-header ">
                    <div class="modal-heading">
                        <h5 class="modal-title txnTitle" id="txnTitle" style="display:inline-block">
                            Purchase Shares/ Coins 
                        </h5>
                        <h5 class="modal-title fundName" style="display:inline-block"></h5>
                    </div>
                    <input type="hidden" id="updateinvestmentshares-action"/>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Customer Id
                                                    </th>
                                                    <th>
                                                        Customer Name
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th class="options">
                                                        Options
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <select class="form-control" id="updateinvestmentshares-fundPrices"></select> 
                                                    </td>
                                                    <td class="refId">

                                                    </td>
                                                    <td class="customerName">

                                                    </td>
                                                    <td class="investmentAmount">

                                                    </td>
                                                    <td class="options">
                                                        <select class="form-control" name="sellOption">
                                                            <option value="SELL_TO_MARKET">Sell to Market</option>
                                                            <option value="SELL_TO_MANAGEMENT">Sell to Management</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Local Currency
                                                    </th>
                                                    <th>
                                                        N.A.V
                                                    </th>
                                                    <th>
                                                        US Dollars
                                                    </th>
                                                    <th>
                                                        Bitcoin Currency
                                                    </th>
                                                    <th>
                                                        Invsta Units
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control localInvestmentAmount" name="local" id="updateinvestmentshares-local"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="nav"  id="updateinvestmentshares-nav"/>
                                                        <input type="hidden" class="form-control" name="createdDate"  id="updateinvestmentshares-date"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control shareAmountTotal" name="usd"  id="updateinvestmentshares-usd"  onkeyup="usd2unit();"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="btc" id="updateinvestmentshares-btc"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="minto" id="updateinvestmentshares-minto"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Share/Coin
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th>
                                                        Price
                                                    </th>
                                                    <th>
                                                        Quantity 
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="pendingShareTable">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary" id="updateinvestmentshares-submit" type="button">Proceed</button>
                    </div>
                </div>
            </div>
        </form>    
    </div>
</div>