<%-- 
    Document   : checkout
    Created on : 28 Nov, 2017, 12:22:07 PM
    Author     : palo12
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-4.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:th='http://www.thymeleaf.org'>
    <title>Result</title>
</head>
<body>
    <form action='./charge' method='POST' id='checkout-form'>
        <input type='hidden' value='${amount}' name='amount' />
        <label>Price:<span>${amount/100}</span></label>
        <!-- NOTE: data-key/data-amount/data-currency will be rendered by Thymeleaf -->
        <script
            src='https://checkout.stripe.com/checkout.js'
            class='stripe-button'
            data-key=${stripePublicKey} 
            data-amount=${amount} 
            data-currency=${currency}
            data-name='Baeldung'
            data-description='${description}'
            data-image='http://www.baeldung.com/wp-content/themes/baeldung/favicon/android-chrome-192x192.png'
            data-locale='auto'
            data-zip-code='false'>
        </script>
    </form>
</body>
</html>
<!--th:attr='data-key=${stripePublicKey}, 
data-amount=${amount}, 
data-currency=${currency}'-->