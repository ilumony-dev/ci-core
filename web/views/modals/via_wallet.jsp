<%-- 
    Document   : via-wallet
    Created on : Feb 15, 2018, 10:01:04 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="via-wallet" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="via-wallet-title">
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <form action="./via-wallet-add-investment" method="post">
                    <!--<input type="hidden" id="via-wallet-reqId" name="reqId"/>-->
                    <input type="hidden" id="via-wallet-userId" name="userId"/>
                    <input type="hidden" id="via-wallet-fundId" name="fundId"/>
                    <div class="steps-w">
                        <div class="step-triggers"></div>
                        <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                                <div class="row">
                                    <div class="col-md-12" style="box-shadow: 0px 16px 10px -10px #011c53;margin-bottom: 20px;">
                                        <h6 class="element-header" style="color:#011c53">My Wallet Balance</h6>
                                        <small>Available balance in your wallet</small>
                                        <div class="col-md-12 wallet-bg"></div>
                                        <div class="row">
                                            <div class="col-md-12" style="border-bottom: 1px dotted #DEEAEE; margin-bottom: 10px;">
                                                <img src="./resources/img/wallet/wallet.png" style="width: 50px; height: 50px">
                                                <span class="value">US$${walletBalance}</span>
                                                <span class="text">Your Wallet Balance</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="investmentAmount">How much money you want to invest?In US$</label>
                                    <input class="form-control investmentAmount" id="via-wallet-investmentAmount" name="investmentAmount" type="number" step="0.01"/>
                                    <input class="form-control localInvestmentAmount" id="via-wallet-localInvestmentAmount" name="localInvestmentAmount" type="hidden"/>
                                    <input id="via-wallet-currency" type="hidden" name="currency" value="${paymentCurrency}"/>
                                    <input id="via-wallet-regularlyAmount" type="hidden" name="regularlyAmount" value="0"/>
                                    <input id="via-wallet-type-years" type="hidden" name="years" value="10"/>
                                    <input id="via-wallet-type-fixed" type="hidden" name="timeFrame" value="FIXED"/>
                                </div>
                            </div>
                            <div class="form-buttons-w text-right">
                                <button class="btn btn-primary" id="via-wallet-submit" type="submit">Done</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>