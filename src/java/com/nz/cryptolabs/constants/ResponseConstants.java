/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.constants;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class ResponseConstants {

    final private static HashMap<String, String> urlMap = new HashMap<>();

    static {
//        UserController
        urlMap.put("user-dashboard", "ud");
        urlMap.put("summary", "id");
        urlMap.put("investments", "is");
        urlMap.put("insights", "its");
        urlMap.put("profile", "pf");
        urlMap.put("payment", "pm");
        urlMap.put("transactions", "txns");
        urlMap.put("verify-new", "vn");
        urlMap.put("refferal", "rf");
        urlMap.put("config", "cf");
//        AdminController
        urlMap.put("admin-dashboard", "ah");
        urlMap.put("admin-invite-codes", "aic");
        urlMap.put("admin-show-user-accounts", "asua");
        urlMap.put("admin-show-new-users", "asnu");
        urlMap.put("admin-show-portfolios", "asp");
        urlMap.put("admin-show-shares", "assh");
        urlMap.put("admin-show-funds", "asf");
        urlMap.put("admin-show-portfolio", "asp");
        urlMap.put("admin-show-currencies", "asc");
        urlMap.put("admin-verification-done", "avd");
        urlMap.put("admin-verification-pending", "avp");
        urlMap.put("admin-portfolio-report", "apr");
        urlMap.put("admin-portfolio-report-type", "aprt");
    }

    public static HashMap<String, String> urlMap() {
        return urlMap;
    }
    final public static String[] colorCodes = new String[]{"#ed1c24",
        "#619a68",
        "#4d8a75",
        "#3e7f72",
        "#5d8778",
        "#4e7377",
        "#2abdbd",
        "#ba2121",
        "#4e7377",
        "#4d070b",
        "#e5d1e6",
        "#f6f0ee",
        "#ffffff",
        "#ffc0cb",
        "#131722",
        "#ff545e",
        "#57bb82",
        "#dbeeff",
        "#ba5796",
        "#bb349b",
        "#6e557c",
        "#5e40b2",
        "#8051cf",
        "#895adc",
        "#935da0",
        "#7a5e93",
        "#6c4475",
        "#403890",
        "#1b36eb",
        "#103d6a",
        "#45b9d3",
        "#0c6483",
        "#487995",
        "#428fb9",
        "#a183b3",
        "#210333",
        "#99ffcc",
        "#b2b2b2",
        "#c0fff4",
        "#c0fff4",
        "#97ffff"};

    final public static String LATER = "later";
    //resgister
    final public static String SUCCESSFUL_CREATED_ACCOUNT = "successful.created.account";
    final public static String SUCCESSFUL_UPDATED_ACCOUNT = "successful.updated.account";
    final public static String DUPLICATE_EMAIL = "duplicate.email";
    final public static String DUPLICATE_PHONE = "duplicate.phone";
    final public static String SUCCESSFULL_ASSIGNED_ROLE = "successful.assigned.role";
    final public static String SUCCESSFULL_ASSIGNED_COMMISSION = "successful.assigned.commission";
    final public static String SENT_RESET_PASSWORD_MAIL = "sent.reset.password.mail";
    //login
    final public static String CONGRATS_VERIFIED_LOGIN = "congrats.verified.login";
    final public static String SORRY_INVALID_LOGIN = "sorry.invalid.login";
    //chef
    final public static String SUCCESSFUL_CREATED_NEWDISH = "successful.created.newdish";
    final public static String SUCCESSFUL_UPDATED_OLDDISH = "successful.updated.olddish";
    final public static String SUCCESSFUL_UPDATED_ORDER_STATUS = "successful.updated.order_status";
    final public static String FAILED_UPDATE_ORDER_STATUS = "failed.update.order_status";
    final public static String SUCCESSFUL_MENTION_QUANTITY = "successful.mention.quantity";
    //client
    final public static String SUCCESSFUL_CREATED_NEWORDER = "successful.created.neworder";
    final public static String SUCCESSFUL_UPDATED_OLDORDER = "successful.updated.oldorder";

}
