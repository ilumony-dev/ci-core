<%-- 
    Document   : addfund
    Created on : Oct 15, 2017, 9:50:50 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>--%>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="user-change-status" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    User Change Status
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./admin-update-user-status" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select id="user-change-status-select-userId" name="userId" class="form-control">
                                    <c:forEach items="${totalUsers}" var="user">
                                        <option value="${user.userId}">${user.email}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12" style="margin-top: 10px">
                            <div class="form-group">
                                <select id="user-change-status-select-status" name="active" class="form-control">
                                    <option value="Y">Active</option>
                                    <option value="B">Blocked</option>
                                    <option value="P">Pending</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"> 
                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>