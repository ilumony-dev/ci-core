<%-- 
    Document   : unit-shifted-2-mgmt-acc
    Created on : Oct 15, 2017, 9:47:52 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="unit-shifted-2-mgmt-acc" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="post" action="./" id="unit-shifted-2-mgmt-acc-form">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title " id="unit-shifted-2-mgmt-acc-title">
                        Unit Shifted to Management Account
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Customer Id
                                                    </th>
                                                    <th>
                                                        Customer Name
                                                    </th>
                                                    <th>
                                                        Fund Name
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="refId">

                                                    </td>
                                                    <td class="customerName">

                                                    </td>
                                                    <td class="fundName">

                                                    </td>
                                                    <td class="investmentAmount">

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Local Currency
                                                    </th>
                                                    <th>
                                                        US Dollars
                                                    </th>
                                                    <th>
                                                        User Portfolio Units
                                                    </th>
                                                    <th>
                                                        Sold Units
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control localInvestmentAmount" name="local" id="unit-shifted-2-mgmt-acc-local"/>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" id="unit-shifted-2-mgmt-acc-nav"/>
                                                        <input type="text" class="form-control shareAmountTotal" name="usd"  id="unit-shifted-2-mgmt-acc-usd"  onkeyup="usd2unit();"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="btc" id="unit-shifted-2-mgmt-acc-btc"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="minto" id="unit-shifted-2-mgmt-acc-minto"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Credit $NBA to User Account
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control localInvestmentAmount" name="local" id="unit-shifted-2-mgmt-acc-local"/>
                                                    </td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button class="btn btn-warning" type="button">Delete</button>
                        <button class="btn btn-primary" type="submit">Approve</button>
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                    </div>
                </div>

            </div>
        </form>    
    </div>
</div>