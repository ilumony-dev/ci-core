/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.exceptions.CryptoException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * A utility class that encrypts or decrypts a file.
 *
 * @author Maninderjit
 *
 */
public class EncryptDecryptServiceImpl {

    private String ALGORITHM = "AES";
    private String TRANSFORMATION = "AES";
    private Key secretKey;
    private Cipher ENCRYPT_MODE;
    private Cipher DECRYPT_MODE;

    public EncryptDecryptServiceImpl() throws CryptoException {
        try {
            secretKey = new SecretKeySpec(Constants.cipherKey.getBytes(), ALGORITHM);
            ENCRYPT_MODE = Cipher.getInstance(TRANSFORMATION);
            ENCRYPT_MODE.init(Cipher.ENCRYPT_MODE, secretKey);
            DECRYPT_MODE = Cipher.getInstance(TRANSFORMATION);
            DECRYPT_MODE.init(Cipher.DECRYPT_MODE, secretKey);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }

    public String encrypt(String input)
            throws CryptoException {
        try {
            byte[] inputBytes = input.getBytes();
            byte[] outputBytes = ENCRYPT_MODE.doFinal(inputBytes);
            return new String(outputBytes);
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }

    public String decrypt(String input)
            throws CryptoException {
        try {
            byte[] inputBytes = input.getBytes();
            byte[] outputBytes = DECRYPT_MODE.doFinal(inputBytes);
            return new String(outputBytes);
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }

    public void encrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        try {
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
            byte[] outputBytes = ENCRYPT_MODE.doFinal(inputBytes);
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();
        } catch (FileNotFoundException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        } catch (IOException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }

    public void decrypt(String key, File inputFile, File outputFile)
            throws CryptoException {
        try {
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);
            byte[] outputBytes = DECRYPT_MODE.doFinal(inputBytes);
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);
            inputStream.close();
            outputStream.close();
        } catch (FileNotFoundException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        } catch (IOException ex) {
            throw new CryptoException("Error encrypting/decrypting file", ex);
        }
    }
}
