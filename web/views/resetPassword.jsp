<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Login - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="./resources/favicon.png" rel="shortcut icon">
        <link href="./resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css" rel="stylesheet">
        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
    </head>
    <body class="auth-wrapper">
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w">
                <div class="logo-w">
                    <a href="./login"><img alt="" src="./resources/img/invsta-03(200px).png" style="width:40%"></a>
                </div>
                <h4 class="auth-header">
                    Reset Password
                </h4>
                <form action="./resetpwd" method="post" class="form-login" id="msform">
                    <input type="hidden" name="userId" value="${resetPwdForm.userId}">
                    <input type="hidden" name="token" value="${resetPwdForm.token}">
                    <input type="hidden" name="csrf" value="${resetPwdForm.csrf}">
                        <div class="form-group">
                            <label for="newPassword">Please enter your new password</label>
                            <input type="password" class="form-control" id="newPassword" name="newPassword" data-error="Your username is invalid" required="required" placeholder="Enter your password" >
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                        </div>
                        
                        <div class="form-group">
                            <label for="rePassword">Please re-enter your new password</label>
                            <input type="password" class="form-control" id="rePassword" name="rePassword" data-error="Your re-password is invalid" required="required" placeholder="Enter your password" >
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                        </div>
                        
                        <div class="buttons-w">
                            <button class="btn btn-primary" onclick="return Validate()">Submit</button>
                        </div>
                        <div class="buttons-w">
                            <p> Don't have account <a  href='./register'>Sign Up</a></p>
                            <p> Already have account? <a  href='./login'> Login</a></p>
                        </div>
                    </form>
            </div>
        </div>
        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <!--<script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>-->
        <script src="./resources/js/validation.js"></script> 
        <script src="./resources/js/jquery.validate.min.js"></script>
        <script src="https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js"></script>
        <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("newPassword").value;
        var confirmPassword = document.getElementById("rePassword").value;
        if (password != confirmPassword) {
            swal("Passwords do not match.");
            return false;
        }
        return true;
    }
        </script>
    </body>
</html>
