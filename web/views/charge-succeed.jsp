<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
        <script>
            !function(){var analytics = window.analytics = window.analytics || []; if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else{analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "debug", "page", "once", "off", "on"]; analytics.factory = function(t){return function(){var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics}}; for (var t = 0; t < analytics.methods.length; t++){var e = analytics.methods[t]; analytics[e] = analytics.factory(e)}analytics.load = function(t, e){var n = document.createElement("script"); n.type = "text/javascript"; n.async = !0; n.src = ("https:" === document.location.protocol?"https://":"http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var o = document.getElementsByTagName("script")[0]; o.parentNode.insertBefore(n, o); analytics._loadOptions = e}; analytics.SNIPPET_VERSION = "4.1.0";
            analytics.load("yqEQxH2Ri5xkKD71qxtWFFkjEfox60zX");
            analytics.page();
            }}();
        </script>
        <!-- Hotjar Tracking Code for https://beta.invsta.com -->
        <script>
            (function(h, o, t, j, a, r){
            h.hj = h.hj || function(){(h.hj.q = h.hj.q || []).push(arguments)};
            h._hjSettings = {hjid:846291, hjsv:6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
        <script>
            window.intercomSettings = {
            app_id: "q28x66d9"
            };
        </script>

        <script>
            (function(){var w = window; varic = w.Intercom; if (typeof ic === "function"){ic('reattach_activator'); ic('update', intercomSettings); } else{var d = document; var i = function(){i.c(arguments)}; i.q = []; i.c = function(args){i.q.push(args)}; w.Intercom = i; function l(){var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/q28x66d9'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); }if (w.attachEvent){w.attachEvent('onload', l); } else{w.addEventListener('load', l, false); }}})()
        </script>
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <!--<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>-->
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>


    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Success</a>
                        </li>
                        <!--            <li class="breadcrumb-item">
                                      <span>Laptop with retina screen</span>
                                    </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-box text-center" id="VERIFICATION_DONE" style="display: none;">
                                        <h3 style="color:#011c53; font-size: 25px">Thanks</h3>
                                        <p>Thanks for Submitting your Verification Details.</p>
                                        <p>The Administrator will contact you should we need any other information.</p>
                                        <br>
                                        <!--<p>Please logout and login again.</p>-->

                                    </div>
                                    <div class="element-box text-center" id="POLI" style="display: none;">
                                        <h3 style="color:#011c53; font-size: 25px">Succeed</h3>
                                        <p>We have completed your transaction by POLI request.</p>

                                    </div>
                                    <div class="element-box text-center"  id="BANK_TRANSFER" style="display: none;">
                                        <h3 style="color:#011c53; font-size: 25px">Please arrange transfer</h3>
                                        <p>Please arrange to transfer your investment amount to the following bank account </p><p>(Note: we will also email you these bank account details):</p>
                                        <p><span>Amount:</span> <span>${currency} ${req.localAmount}</span></p>
                                        <p><span>Bank name:</span> <span>${currencyAcc.bankName}</span></p>
                                        <p><span>Bank Address:</span> <span>${currencyAcc.bankAddress}</span></p>
                                        <p><span>Bank Phone Number:</span> <span>${currencyAcc.bankPhone}</span></p>
                                        <p><span>Swift Code:</span> <span>${currencyAcc.swiftCode}</span></p>
                                        <p><span>Account Name:</span> <span>${currencyAcc.accName}</span></p>
                                        <p><span>Account Address:</span> <span>${currencyAcc.accAddress}</span></p>
                                        <p><span>Account Number: </span> <span>${currencyAcc.accNumber}</span></p>
                                        <p><span>Reference Number: </span> <span>${req.txnId}</span></p>
                                        <p><span>Payment Reference:</span> <span>${user.fullName}</span></p>
                                        <input type="hidden" name="ok" id="ok" value="${req.id}"/>
                                        <input type="hidden" name="un" id="un" value="${user.userId}"/>
                                        <p>We will email you to confirm once we have received your payment, you will then be able to choose your cryptocurrency investments.</p>
                                        <div class="btn-group" style="display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: space-evenly; -ms-flex-pack: space-evenly; justify-content: space-evenly; margin-top: 50px;">
                                            <a href="./welcome" class="btn btn-warning">I will do it later</a>
                                            <a href="#" class="btn btn-primary btn-ok">I have sent the money to Invsta</a>
                                            <a href="#" class="btn btn-danger btn-cancel">Cancel this Transfer</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <script>
//                $(document).on({
//                    "contextmenu": function (e) {
//                        e.preventDefault();
//                    }
//                });
            $(document).ready(function () {
            var ele = document.getElementById('${brand}');
            ele.style.display = "block";
            $(".btn-ok").click(function(){
            swal({
            title: "Proceed",
                    text: "Money Sent to Invsta",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: false
            }, function(){
            window.location.href = "./welcome";
            });
            var un = $("#un").val();
            var ok = $("#ok").val();
            $.post("./rest/cryptolabs/api/ok-transfer",
            {
            un: un,
                    ok: ok
            });
//            window.location.href = "./welcome";
            });
            $(".btn-cancel").click(function(){
            swal({
            title: "Cancelled",
                    text: "You Cancelled this Transfer",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: false
            },
                    function(){
                    window.location.href = "./welcome";
                    }
            );
            var un = $("#un").val();
            var ok = $("#ok").val();
            $.post("./rest/cryptolabs/api/cancel-transfer",
            {
            un: un,
                    ok: ok
            });
            });
            });
    </script>
    <script>
        function mobileMenu() {
        var x = document.getElementsByClassName("menu-and-user")[0];
        if (x.style.display === "none") {
        x.style.display = "block";
        } else {
        x.style.display = "none";
        }
        }
    </script>
</html>
