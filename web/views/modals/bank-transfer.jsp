<%-- 
    Document   : investment-type
    Created on : Oct 15, 2017, 10:01:04 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="bank-transfer" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="investment-type-title">
                    I want to invest regularly
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <form action="./bank-transfer-add-investment" method="post">
                    <input type="hidden" id="investment-type-fundId" name="fundId"/>
                    <div class="steps-w">
                        <div class="step-triggers">
                            <!--<a class="step-trigger active" id="trigger1" href="#stepContent1"><i  class="os-icon os-icon-arrow-right"></i></a>-->
                            <!--<a class="step-trigger" id="trigger2" href="#stepContent2"><i  class="os-icon os-icon-arrow-right"></i></a>-->
                        </div>
                        <div class="step-contents">
                            <div class="step-content active" id="stepContent2">
                                <input class="form-control upFrontValue" type="hidden" id="investment-type-investmentAmount" name="investmentAmount" placeholder="Enter investment amount" step="0.01"/>
                                <input id="investment-type-regularlyAmount" type="hidden" name="regularlyAmount" value="0"/>
                                <input id="investment-type-years" type="hidden"  name="years" value="10"/>
                                <input id="investment-type-fixed" type="hidden"  name="timeFrame" value="FIXED"/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="bankName"> Bank Name</label>
                                            <input class="form-control" id="investment-type-bankName" name="bankName" placeholder="Enter Bank Name" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="bankAccount"> Bank Account Number</label>
                                            <input class="form-control" id="investment-type-bankAccount" name="bankAccount" placeholder="Enter Bank Account No." type="number"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="referenceNo"> Reference Number</label>
                                            <input class="form-control" id="investment-type-referenceNo" name="referenceNo"  placeholder="Enter Reference No." type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="customerName">Customer Name</label>
                                            <input class="form-control" id="investment-type-customerName" name="customerName" placeholder="Enter Customer Name" type="text" value="${info.fullName}" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="customerNo"> Unique Customer Number</label>
                                            <input class="form-control" id="investment-type-customerNo" name="customerNo" placeholder="Enter Your Customer No" type="text" value="${info.refId}" readonly/>
                                        </div>            
                                    </div>
                                </div>
                                <div class="form-buttons-w text-right">
                                    <button class="btn btn-primary" type="submit">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
                                       