<%-- 
    Document   : add-fund-modal-lg
    Created on : Feb 15, 2018, 9:58:39 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="add-funds-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title " id="add-fund-modal-label">
                    Add money
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <section id="investments" style="display:block;">
                    <div class="label" class="add-fund-modal-fundName" style="font-size: 1.5rem; color:forestgreen;">
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="color1">How much would you like to add to your Invsta account now?</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Currency</label>
                                    <select class="form-control" name="currency" id="add-fund-modal-currency">
                                        <option value="USD">United States Dollar (US$)</option>
                                        <option value="NZD">New Zealand Dollar (NZ$)</option>
                                        <option value="SGD">Singapore Dollar ($)</option>
                                        <option value="AUD">Australian Dollar (AU$)</option>
                                        <option value="GBP">Great Britain Pound (GB�)</option>
                                        <option value="HKD">Hong Kong Dollar (HK$)</option>
                                        <option value="INR">Indian National Rupee (INR)</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label>Amount</label>
                                    <input type="number" class="form-control" id="add-fund-modal-amount" value="0.00" step="0.01"/>
                                </div>
                            </div>
                            <div class="row" style="margin-top:10px">
                                <div class="col-sm-12">
                                    <label>Description</label>
                                    <textarea class="form-control" id="add-fund-modal-description" value=""></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="investment-fund-modal-hideImg" style="display:none;">
                                    <center><img src="./resources/img/loading.gif" style="width:15%"></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary next" id="investment-fund-modal-continue" type="button" onclick="activePage('payGateways');">Pay Now</button>
                    </div>
                </section>
                <section id="payGateways" style="display:none;">
                    <div class="loader"><img src="resources/img/loading.gif"></div>
                    <!--                    <h3 class="color1">Please confirm your payment method.</h3>-->
                    <div class="row" id="form-pay-now" style="display:none;">
                        <div class="col-md-4" style="margin-top: 27px"><!--Deal in NZD-->
                            <a id="add-fund-modal-BANK_TRANSFER">
                                <img src="./resources/img/preferences/payment-4.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Wallet">
                            </a>
                        </div>
                        <div class="col-md-4" style="margin-top: 27px"><!--Deal in NZD-->
                            <!--<a id="add-fund-modal-NET_BANKING" href="">-->
                            <img src="./resources/img/preferences/paymentgateway.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Wallet">
                            <!--</a>-->
                        </div>
                        <div class="col-md-4" style="margin-top: 27px"><!--Deal in USD-->
                            <label for="payGateway4">
                                <img src="./resources/img/preferences/payment-5.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Wallet">
                            </label>
                        </div>
                    </div>
                    <form method="post" action="./user-add-funds" id="user-add-funds-form" onsubmit="onSubmission()">
                        <input type="hidden" name="userId"  id="add-fund-modal-checkout-userId" value="${endUser.userId}"/>
                        <input type="hidden" name="currency"  id="add-fund-modal-checkout-currency" value="${currency}"/>
                        <input type="hidden" class="checkout-amount" name="localAmount" id="add-fund-modal-checkout-amount"/>
                        <input type="hidden" name="description"  id="add-fund-modal-checkout-description"/>
                        <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>

    </div>
</div>
<jsp:include page="via_wallet.jsp"></jsp:include>
<script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
<script>
                        activePage = function (id) {
                            var sections = document.getElementsByTagName("section");
                            for (var i = 0; i < sections.length; i++) {
                                var section = sections[i];
                                section.style.display = "none";
                            }
                            if ('payGateways' === id) {
                                document.getElementById(id).style.display = "block";
                                var amt = $("#add-fund-modal-amount").val();
                                if (amt > 0) {
//                                    var url = './rest/cryptolabs/api/asd0lkjald0jfdsjsad0insight?amt=' + amt + '&curr=NZD';
//                                    $.ajax({
//                                        url: url,
//                                        type: 'GET',
//                                        async: true,
//                                        dataType: "json",
//                                        success: function (data) {
//                                            var txn = data.transaction;
//                                            if (txn.Success === true) {
//                                                $(".loader").hide();
//                                                var url = txn.NavigateURL;
//                                                document.getElementById("add-fund-modal-NET_BANKING").href = url;
////                                                    $(".localInvestmentAmount").val(data.localAmount);
////                                                    $(".investmentAmount").val(data.usdAmount);
//                                                var form = document.getElementById("form-pay-now");
//                                                form.style.display = "flex";
//                                            } else {
//                                                $(".loader").hide();
//                                                $(".error").show();
//                                            }
//                                        }
//                                    });
                                    $(".loader").hide();
                                    var form = document.getElementById("form-pay-now");
                                    form.style.display = "flex";
                                }
                            }
                        };
</script>