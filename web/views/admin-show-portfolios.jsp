<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Show Portfolios - Invsta</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Products</span>
                        </li>
                        
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Portfolio Reports <img src="./resources/img/info-icon.png" style="vertical-align:text-top">
                                        </h6>
                                        <div class="element-box-tp">
                                            <div class="controls-above-table">
                                                <div class="row" style="margin-bottom:25px;">
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-sm btn-secondary" href="#" onclick="setAddFundModal('Create New Portfolio');" data-target="#addfund" data-toggle="modal">Create New Portfolio</a>
                                                    </div>
                                                    <div class="col-sm-6">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <c:forEach items="${portfoliosReport}" var="portfolio">
                                                        <div class="col-sm-12">
                                                            <div class="element-wrapper">
                                                                <h3 class="element-header">${portfolio.fund_name}</h3>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-sm-3">
                                                                    <div class="ilumony-box text-center" style="cursor: pointer; background:url(./resources/img/${portfolio.full_name}) center center no-repeat; background-size: cover;position:relative;min-height: 206px;" >
                                                                        <h5 class="color3">${portfolio.fund_name}</h5>
                                                                        <div class="row-actions" style="    position: absolute;bottom: 5px;right: 5px;">
                                                                            <a href="./admin-portfolio-report-type?id=${portfolio.fund_id}" style="color:white"><i class="fa fa-file" style="font-size:20px; margin-right: 10px"></i></a>
                                                                            <a href="./admin-portfolio-report?id=${portfolio.fund_id}" style="color:white"><i class="fa fa-line-chart"  style="font-size:20px"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-9">
                                                                    <div id="elementboxcontent" class="element-box">
                                                                        <div class="row">                
                                                                            <div class="col-sm-4">
                                                                                <div id="piechart24hr${portfolio.fund_id}"></div>
                                                                                <div id="legend24hr${portfolio.fund_id}"></div>
                                                                                <center>1 Day</center>
                                                                                <center>
                                                                                    <div class="dropdown">
                                                                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">--Report Type--
                                                                                            <span class="caret"></span>
                                                                                        </button>
                                                                                        <ul class="dropdown-menu">
                                                                                            <!--<li><a href="" class="btn btn-primary">Burn Report</a></li>-->
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange24hr}&reportType=WR" class="">Withdraw Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange24hr}&reportType=DR" class="">Deposits Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange24hr}&reportType=MR" class="">Management Account Transfer Report</a></li>                                   
                                                                                        </ul>
                                                                                    </div>
                                                                                </center>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div id="piechart7d${portfolio.fund_id}"></div>
                                                                                <div id="legend7d${portfolio.fund_id}"></div>
                                                                                <center>1 Week</center>
                                                                                <center>
                                                                                    <div class="dropdown">
                                                                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">--Report Type--
                                                                                            <span class="caret"></span>
                                                                                        </button>
                                                                                        <ul class="dropdown-menu">
                                                                                            <!--<li><a href="" class="btn btn-primary">Burn Report</a></li>-->
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange7d}&reportType=WR" class="">Withdraw Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange7d}&reportType=DR" class="">Deposits Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange7d}&reportType=MR" class="">Management Account Transfer Report</a></li>                                   
                                                                                        </ul>
                                                                                    </div>
                                                                                </center>
                                                                            </div>
                                                                            <div class="col-sm-4">
                                                                                <div id="piechart30d${portfolio.fund_id}"></div>
                                                                                <div id="legend30d${portfolio.fund_id}"></div>
                                                                                <center>1 Month</center>
                                                                                <center>
                                                                                    <div class="dropdown">
                                                                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">--Report Type--
                                                                                            <span class="caret"></span>
                                                                                        </button>
                                                                                        <ul class="dropdown-menu">
                                                                                            <!--<li><a href="" class="btn btn-primary">Burn Report</a></li>-->
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange30d}&reportType=WR" class="">Withdraw Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange30d}&reportType=DR" class="">Deposits Report</a></li>
                                                                                            <li><a href="./admin-portfolio-report-type?id=${portfolio.fund_id}&daterange=${daterange30d}&reportType=MR" class="">Management Account Transfer Report</a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </center>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-4 col-sm-4">
                                                                    <div class="element-box el-tablo">
                                                                        <div class="label">Total Users</div>
                                                                        <div class="value">${portfolio.user_count}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4">
                                                                    <div class="element-box el-tablo">
                                                                        <div class="label">Total Units</div>
                                                                        <div class="value">${portfolio.total_units}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-4">
                                                                    <div class="element-box el-tablo">
                                                                        <div class="label">N.A.V</div>
                                                                        <div class="value">US$ ${portfolio.nav}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-6">
                                                                    <div class="element-box el-tablo">
                                                                        <div class="label">Total Invested Value</div>
                                                                        <div class="value">US$ ${portfolio.invested_usd}</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4 col-sm-6">
                                                                    <div class="element-box el-tablo">
                                                                        <div class="label">Total Current Value</div>
                                                                        <div class="value">US$ ${portfolio.total_usd}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <%--<jsp:include page = "modals/update-investment-shares.jsp" />--%>

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
                                                            var shares = [], coins = ${coins}, portfoliosReport = ${portfoliosReport};
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="${home}/resources/js/cryptolabs/user-charts.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script>
                                                            function piechart(amt, id, legendId) {
                                                                var chart = new Highcharts.chart(id, {
                                                                    chart: {
                                                                        plotBackgroundColor: null,
                                                                        plotBorderWidth: null,
                                                                        plotShadow: false,
                                                                        type: 'pie',
                                                                        height: '115px',
                                                                        margin: [0, 0, 0, 0],
                                                                        spacingTop: 0,
                                                                        spacingBottom: 0,
                                                                        spacingLeft: 0,
                                                                        spacingRight: 0
                                                                    },
                                                                    title: {
                                                                        text: null
                                                                    },
                                                                    subtitle: {
                                                                        text: null
                                                                    },
                                                                    credits: {
                                                                        enabled: false
                                                                    },
                                                                    exporting: {
                                                                        enabled: false
                                                                    },
                                                                    tooltip: {
//            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
                                                                        pointFormat: '<b>' + amt + '%</b>'
                                                                    },
                                                                    plotOptions: {
                                                                        pie: {
                                                                            allowPointSelect: true,
                                                                            cursor: 'pointer',
                                                                            dataLabels: {
                                                                                enabled: false,
                                                                                format: '<b>' + amt + '%</b>',
//                    format: '<b>{point.name}</b>: {point.y:.2f}',
                                                                                style: {
                                                                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                                                                }
                                                                            },
                                                                            showInLegend: false
                                                                        }
                                                                    },
                                                                    series: [{
                                                                            name: 'Particulars',
                                                                            innerSize: '75%',
                                                                            colorByPoint: true,
                                                                            data: [{name: 'Percentange', y: eval(100)}
//                    , {name: 'Other', y: eval(max - amt)}
                                                                            ]
                                                                        }]
                                                                });
                                                                centerText(chart, legendId, amt);
                                                            }
                                                            $.each(portfoliosReport, function (i, portfolio) {
                                                                piechart(portfolio.percent24hr, 'piechart24hr' + portfolio.fund_id, 'legend24hr' + portfolio.fund_id);
                                                                piechart(portfolio.percent7d, 'piechart7d' + portfolio.fund_id, 'legend7d' + portfolio.fund_id);
                                                                piechart(portfolio.percent30d, 'piechart30d' + portfolio.fund_id, 'legend30d' + portfolio.fund_id);
                                                            });
        </script>
    </body>
</html>
