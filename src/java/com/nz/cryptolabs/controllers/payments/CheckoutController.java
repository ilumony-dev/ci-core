/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author palo12
 */
@Controller
//@RequestMapping(value = {"/stripe"})
public class CheckoutController {

    final private String stripePublicKey = "pk_test_zrCXgtyeFkNkUj4HjXvEdnah";

    @RequestMapping("/checkout")
    public String checkout(Model model) {
        model.addAttribute("amount", 50); // in cents
        model.addAttribute("stripePublicKey", stripePublicKey);
        model.addAttribute("currency", ChargeRequest.Currency.USD);
        return "stripe/checkout";
    }
}
