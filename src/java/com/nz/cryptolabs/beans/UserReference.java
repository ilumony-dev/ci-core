/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import org.slf4j.LoggerFactory;

/**
 *
 * @author palo12
 */
public class UserReference {

    /**
     * @return the amounted
     */
    public String getAmounted() {
        return amounted;
    }

    /**
     * @param amounted the amounted to set
     */
    public void setAmounted(String amounted) {
        this.amounted = amounted;
    }

    /**
     * @return the stripeToken
     */
    public String getStripeToken() {
        return stripeToken;
    }

    /**
     * @param stripeToken the stripeToken to set
     */
    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    /**
     * @return the poliToken
     */
    public String getPoliToken() {
        return poliToken;
    }

    /**
     * @param poliToken the poliToken to set
     */
    public void setPoliToken(String poliToken) {
        this.poliToken = poliToken;
    }

    /**
     * @return the referenceNo
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * @param referenceNo the referenceNo to set
     */
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the fundId
     */
    public String getFundId() {
        return fundId;
    }

    /**
     * @param fundId the fundId to set
     */
    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }
    private String referenceNo;
    private String token;
    private String stripeToken;
    private String poliToken;
    private String userId;
    private String fundId;
    private String price;
    private String amounted;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserInfo.class);

}
