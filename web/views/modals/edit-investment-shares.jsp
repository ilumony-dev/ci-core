<%-- 
    Document   : edit-investment-shares
    Created on : 18 Dec, 2017, 10:54:21 AM
    Author     : palo12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="edit-investment-shares" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="post" action="./" id="updateCoinsSharesForm">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title " id="txnTitle">
                        Purchase Shares/ Coins
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-8" style="text-align: right;margin-top: 10px;">
                                            Created Date
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control" id="datetimes" onchange="filterCoinsShares()">
                                            </select>
                                        </div>                                        
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Created Date
                                                    </th>
                                                    <th>
                                                        Share/Coin
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th>
                                                        Price
                                                    </th>
                                                    <th>
                                                        Quantity 
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="purchasedSharesTable">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary" type="submit">Proceed</button>
                    </div>
                </div>

            </div>
        </form>    
    </div>
</div>
<script>
    var data = new Array();

    function filterCoinsShares() {
        var createdDate = $("#datetimes").val();
        var filtereddata = new Array();
        $.each(data, function (index, obj) {
            if (createdDate === obj.createdDate) {
                filtereddata.push(obj);
            }
        });
        setSharesInTable(filtereddata, '');
    }

    function setSharesInTable(db, rId) {
        var pendingSharesHtmlCode = '';
        $("#purchasedSharesTable").html(pendingSharesHtmlCode);
        $.each(db, function (index, obj) {
            if (index === 0) {
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="reqId" value="' + rId + '">';
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="investmentId" value="' + obj.investmentId + '">';
                pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="userId" value="' + obj.userId + '">';
            }
            var htmlCode = '<tr>';
            htmlCode = htmlCode + '<input type="hidden" name="id" value="' + obj.id + '">';
            htmlCode = htmlCode + '<input type="hidden" name="shareId" value="' + obj.shareId + '">';
            htmlCode = htmlCode + '<input type="hidden" name="coinId" value="' + obj.coinId + '">';
            htmlCode = htmlCode + '<td>' + obj.createdDate + '</td>';
            htmlCode = htmlCode + '<td>' + obj.shareName + '</td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="shareAmount" id="shareAmount' + index + '" step="0.01" value="' + obj.shareAmount + '" onkeyup="sdpeq(' + index + ')"></td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="price" id="price' + index + '" step="0.0001" value="' + obj.price + '" onchange="sdpeq(' + index + ')"></td>';
            htmlCode = htmlCode + '<td> <input class="form-control" type="text" name="quantity" id="quantity' + index + '" step="0.0001" value="' + obj.quantity + '" onkeyup="qpes(' + index + ')"></td>';
            htmlCode = htmlCode + '</tr> ';
            pendingSharesHtmlCode = pendingSharesHtmlCode + htmlCode;
        });
        $("#purchasedSharesTable").html(pendingSharesHtmlCode);
    }

    function purchasedShares4Edit(mId, uId) {
        var url = './rest/cryptolabs/api/sdadfjlsdweroweiuxmcbncmxcmc?uId=' + uId + '&mId=' + mId;
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (db) {
                data = db;
                var datetimes = new Array();
                var createdDate = '';
                $.each(data, function (index, obj) {
                    if (createdDate !== obj.createdDate) {
                        datetimes.push(obj.createdDate);
                        createdDate = obj.createdDate;
                    }
                });
                var opts = '<option>Select Created Date</option>';
                $.each(datetimes, function (index, datetime) {
                    opts = opts + '<option>' + datetime + '</option>';
                });
                $("#datetimes").html(opts);
                $("#txnTitle").text('Update Coins/Shares');
                document.getElementById('updateCoinsSharesForm').action = "./admin-update-purchased-shares";
                filterCoinsShares();
            }
        });
    }
</script>
