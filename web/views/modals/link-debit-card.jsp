<%-- 
    Document   : linkdebitcard
    Created on : Oct 15, 2017, 10:13:24 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="linkdebitcard" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Link your Debit Card
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="">Enter a Unique Number</label>
                            <input class="form-control" placeholder="Enter Unique No." type="number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                <button class="btn btn-primary" data-target="#validate1" data-toggle="modal" type="button" >Validate </button>
            </div>

        </div>
    </div>
</div>