/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.EmailVerificationBean;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.PortfolioBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.FileUtility;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.userModelMapEntry;
import static com.nz.cryptolabs.constants.ResponseConstants.urlMap;
import com.nz.cryptolabs.mail.MailManager;
import com.nz.cryptolabs.mail.ResetPasswordForm;
import com.nz.cryptolabs.services.DateTimeService;
import com.nz.cryptolabs.services.SecurityService;
import com.nz.cryptolabs.services.SessionManagementService;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Maninderjit
 */
@Controller
@Secured(value = {"ROLE_USER", "ROLE_ADMIN", "ROLE_MANAGEMENT"})
public class UserController {

    private void progressBar(ModelMap modelMap, String userId) {
        Integer progress = 0;
        List<Verification> verList = repository.findVerification(userId, "DONE");
        if (verList != null && !verList.isEmpty()) {
            Verification ver = verList.get(0);
            if (ver.getInvite_code() != null && !ver.getInvite_code().isEmpty()) {
                progress += 5;
            }
            if (ver.getAddress() != null && !ver.getAddress().isEmpty()) {
                progress += 5;
            }
            if (ver.getPp_passport_number() != null && !ver.getPp_passport_number().isEmpty()) {
                progress += 5;
            }
            if (ver.getPa_number() != null && !ver.getPa_number().isEmpty()) {
                progress += 5;
            }
            if (ver.getDl_license_number() != null && !ver.getDl_license_number().isEmpty()) {
                progress += 5;
            }
            modelMap.addAttribute("verification", ver);
        } else {
            modelMap.addAttribute("verification", new Verification());
        }
        List<UserFundsCommand> userFunds = repository.userFunds(null, userId, null);
        if (!userFunds.isEmpty()) {
            progress += 25;
        }
        String _currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : currency_;
        BigDecimal sumInv = BigDecimal.ZERO;
        List<InvestmentBean> userInvestments = databaseService.purchasedInvestments(userId, null, null, _currency);
        List<InvestmentBean> userInvestmentsAndCash = repository.purchasedInvestmentsAndCash(userId, null, null);
        if (!userInvestmentsAndCash.isEmpty()) {
            for (InvestmentBean inv : userInvestmentsAndCash) {
                BigDecimal invAmount = new BigDecimal(inv.getInvestmentAmount());
                if (!_currency.equalsIgnoreCase(currency_)) {
                    try {
                        BigDecimal localPrice = databaseService.currencyConversionRate(currency_ + "_" + _currency, null);
                        invAmount = invAmount.multiply(localPrice);
                        inv.setInvestmentAmount(invAmount.toPlainString());
                    } catch (IOException ex) {
                        Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        List<TransactionBean> actualWalletBalances = repository.actualWalletBalances(userId);
        if (!actualWalletBalances.isEmpty()) {
            TransactionBean actualWalletBalance = actualWalletBalances.get(0);
            String actual_wallet_balance = actualWalletBalance.getUsd();//actual_wallet_balance
            BigDecimal actWalBal = new BigDecimal(actual_wallet_balance);
            if (!_currency.equalsIgnoreCase(currency_)) {
                try {
                    BigDecimal localPrice = databaseService.currencyConversionRate(currency_ + "_" + _currency, null);
                    actWalBal = actWalBal.multiply(localPrice);
                } catch (IOException ex) {
                    Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            sumInv = sumInv.add(actWalBal);
        }
        List<ShareFund> totalCoins = databaseService.totalCoins(userId, null);
        if (!totalCoins.isEmpty()) {
            progress += 25;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(common.today());//today
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date date = cal.getTime();
        String tDate = common.dateFormat(date, CommonMethods.format3);
        List<UserFundsCommand> previousDay = databaseService.getUserTodayStatus(null, userId, null, tDate, tDate, false, _currency);
        if (!previousDay.isEmpty()) {
            progress += 25;
            for (UserFundsCommand inv : previousDay) {
                if (Integer.parseInt(inv.getFund_id()) != 0) {
                    sumInv = sumInv.add(new BigDecimal(inv.getInvestment_amount()));
                }
            }
        }
        modelMap.addAttribute("sumWalletBalance", sumInv.setScale(2, RoundingMode.HALF_UP).toPlainString());
        cal.add(Calendar.DAY_OF_MONTH, -6);
        date = cal.getTime();
        List<UserFundsCommand> latestWeek = databaseService.getUserTodayStatus(null, userId, null, common.dateFormat(date, CommonMethods.format3), null, true, _currency);
        cal.add(Calendar.DAY_OF_MONTH, -8);
        date = cal.getTime();
        List<UserFundsCommand> latest15days = databaseService.getUserTodayStatus(null, userId, null, common.dateFormat(date, CommonMethods.format3), null, true, _currency);
        cal.add(Calendar.DAY_OF_MONTH, -15);
        date = cal.getTime();
        List<UserFundsCommand> latest30days = databaseService.getUserTodayStatus(null, userId, null, common.dateFormat(date, CommonMethods.format3), null, true, _currency);
        modelMap.addAttribute("progress", progress);
        modelMap.addAttribute("userFunds", userFunds);
        modelMap.addAttribute("userInvestments", userInvestments);
        modelMap.addAttribute("userInvestmentsAndCash", userInvestmentsAndCash);
        modelMap.addAttribute("totalcoins", totalCoins);
        modelMap.addAttribute("currency", _currency);
        modelMap.addAttribute("currSymbol", common.currSymbol().get(_currency));
        modelMap.addAttribute("previousDay", previousDay);
        modelMap.addAttribute("latestWeek", latestWeek);
        modelMap.addAttribute("latest15days", latest15days);
        modelMap.addAttribute("latest30days", latest30days);
    }

    private void actualWalletBalance(ModelMap modelMap, String userId) {
        List<TransactionBean> actualWalletBalances = repository.actualWalletBalances(userId);
        if (!actualWalletBalances.isEmpty()) {
            TransactionBean actualWalletBalance = actualWalletBalances.get(0);
            String actual_wallet_balance = actualWalletBalance.getUsd();//actual_wallet_balance
            String last_txn_amount = actualWalletBalance.getLast_amount();//last_txn_amount
            modelMap.addAttribute("actualWalletBalanceObject", actualWalletBalance);
            modelMap.addAttribute("walletBalance", actual_wallet_balance);
            modelMap.addAttribute("lastDeposite", last_txn_amount);
        } else {
            modelMap.addAttribute("walletBalance", "0.00");
            modelMap.addAttribute("lastDeposite", "0.00");
        }
    }

    private void payment(ModelMap modelMap, String userId) {
        List<TransactionBean> accountSummary = repository.userAccountSummary(userId);
        modelMap.addAttribute("accountSummary", accountSummary);
        List<TransactionBean> walletSummary = repository.userWalletTransactions(userId);
        if (!walletSummary.isEmpty()) {
            modelMap.addAttribute("walletSummary", walletSummary);
        }
        actualWalletBalance(modelMap, userId);
        List<RequestBean> list = repository.userTransactionRequests(userId, "LATER", null, null);
        if (!list.isEmpty()) {
            modelMap.addAttribute("link", " href=\"./charge-succeed\"");
        } else {
            modelMap.addAttribute("link", "data-toggle=\"modal\" data-target=\"#add-funds-modal-lg\""); // in cents                
        }
        modelMap.addAttribute("amount", 0); // in cents
        modelMap.addAttribute("stripePublicKey", stripePublicKey);
        String _currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : currency_;
        modelMap.addAttribute("currency", _currency);
        modelMap.addAttribute("localCurrency", _currency);
        modelMap.addAttribute("common", common);
        modelMap.addAttribute("domain", domain);
        modelMap.addAttribute("currSymbol", common.currSymbol().get(_currency));

    }

    @RequestMapping(value = {"/user-dashboard"}, method = RequestMethod.GET)
    public String userDashboard(ModelMap modelMap, Locale loc,
            @RequestParam(value = "un", required = false) String userId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "User Dashboard " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));
            if (user.getAdmin() && userId != null) {
                UserInfo endUser = repository.findByUserId(userId);
                Date createdTs = common.parseDate(user.getCreatedTs(), CommonMethods.format2);
                endUser.setCreatedTime(createdTs.getTime());
                progressBar(modelMap, endUser.getUserId());
                modelMap.addAttribute("endUser", endUser);
            } else {
                userId = user.getUserId();
                progressBar(modelMap, user.getUserId());
                modelMap.addAttribute("endUser", user);
            }
            EmailVerificationBean bean = repository.getEmailVerification(user.getRefId());
            modelMap.addAttribute("isVerifiedEmail", "Y".equalsIgnoreCase(bean.getVerified()));
            userModelMapEntry(urlMap().get("user-dashboard") + "?id=" + userId, modelMap);
            return "user-dashboard";

        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"summary"}, method = RequestMethod.GET)
    public String investmentDashboard(ModelMap modelMap,
            @RequestParam("ok") String investmentId,
            @RequestParam(value = "un", required = false) String userId) {
        if (investmentId != null && investmentId.contains(",")) {
            investmentId = investmentId.split(",")[0];
        }
        if (userId != null && userId.contains(",")) {
            userId = userId.split(",")[0];
        }
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            if (user.getUser()) {
                userId = user.getUserId();
            }
            String _currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : currency_;
            if ((user.getUser() || user.getAdmin()) && userId != null) {
                InvestmentBean inv = databaseService.purchasedInvestments(userId, investmentId, null, _currency).get(0);
                List<ShareFund> fundActualShares = inv.getFundActualShares();
                if (fundActualShares != null && !fundActualShares.isEmpty()) {
                    Collections.sort(fundActualShares,
                            (ShareFund o1, ShareFund o2)
                            -> //                           new BigDecimal(o2.getShareAmount()).compareTo(new BigDecimal(o1.getShareAmount()))
                            new BigDecimal(o2.getTargetPercentage()).compareTo(new BigDecimal(o1.getTargetPercentage()))
                    );
                }
                List<ShareFund> coinsByFund = repository.coinsByFund(inv.getFundId());
                fundActualShares.forEach((share) -> {
                    share.sameNameUpdatePercentage(coinsByFund);
                });
                inv.setFundActualShares(fundActualShares);
                List<ShareFund> top5 = null;
                if (fundActualShares.size() > 4) {
                    top5 = fundActualShares.subList(0, 5);
                } else {
                    top5 = fundActualShares;
                }
                top5.forEach((share) -> {
                    share.setPercentage(new BigDecimal(share.getPercentage()).setScale(2, RoundingMode.HALF_UP).toPlainString());
                    share.setTargetPercentage(new BigDecimal(share.getTargetPercentage()).setScale(2, RoundingMode.HALF_UP).toPlainString());
                });
                Calendar cal = Calendar.getInstance();
                cal.setTime(common.today());//today
                cal.add(Calendar.DAY_OF_MONTH, -7);
                Date date = cal.getTime();
                List<UserFundsCommand> latestWeek = repository.getUserTodayStatus(null, userId, investmentId, common.dateFormat(date, CommonMethods.format3), null, false);
                modelMap.addAttribute("investment", inv);
                modelMap.addAttribute("portfolio", repository.fundById(inv.getFundId(), null));
                modelMap.addAttribute("top5Shares", top5);
                modelMap.addAttribute("coinsByFund", coinsByFund);
                modelMap.addAttribute("currency", _currency);
                modelMap.addAttribute("common", common);
                modelMap.addAttribute("currSymbol", common.currSymbol().get(_currency));
                modelMap.addAttribute("latestWeek", latestWeek);
                modelMap.addAttribute("title", "Investment Dashboard " + projectName);
                InvestmentBean sumOfpendingSellRequests = repository.sumOfpendingSellTransactions(userId, investmentId);
                if (sumOfpendingSellRequests != null && sumOfpendingSellRequests.getReqId() != null
                        && Integer.parseInt(sumOfpendingSellRequests.getReqId()) > 0) {
                    modelMap.addAttribute("sumOfSellingPercentage", new BigDecimal(sumOfpendingSellRequests.getPercentage()).setScale(2, RoundingMode.HALF_UP).toPlainString());
                    modelMap.addAttribute("countOfSellRequests", Integer.parseInt(sumOfpendingSellRequests.getReqId()));
                } else {
                    modelMap.addAttribute("sumOfSellingPercentage", "0.00");
                    modelMap.addAttribute("countOfSellRequests", 0);
                }
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                List<String> days = new ArrayList<>();
                Calendar now = Calendar.getInstance();
                now.setTime(common.today());//today
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -1);//yesterday
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -6);//before week
                days.add(format.format(now.getTime()));
                now.add(Calendar.DAY_OF_MONTH, -24);//before month
                days.add(format.format(now.getTime()));
                BigDecimal todayNav = null;
                List<PortfolioBean> portfolioReports = repository.portfolioReportById(days, inv.getFundId());
                for (int i = 0; i < portfolioReports.size(); i++) {
                    PortfolioBean bean = portfolioReports.get(i);
                    if (i == 0) {
                        todayNav = new BigDecimal(bean.getNav());
                    } else {
                        BigDecimal otherNav = new BigDecimal(bean.getNav());
                        BigDecimal difference = todayNav.subtract(otherNav);
                        BigDecimal performance = difference.divide(otherNav, Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP).multiply(new BigDecimal("100"));
                        bean.setPerformance(performance.toPlainString());
                    }
                }
                modelMap.addAttribute("portfolioReports", portfolioReports);
                List<TransactionBean> walletSummary = repository.userWalletTransactions(user.getUserId());
                if (!walletSummary.isEmpty()) {
                    modelMap.addAttribute("walletSummary", walletSummary);
                }
                String _currency2 = localCurrency != null && !localCurrency.isEmpty() ? localCurrency : currency_;
                modelMap.addAttribute("localCurrency", _currency2);
                actualWalletBalance(modelMap, user.getUserId());
            }
            userModelMapEntry(urlMap().get("summary") + "?id=" + userId + "&ok=" + investmentId, modelMap);
            return "investment-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/investments", method = RequestMethod.GET)
    public String investments(ModelMap modelMap,
            @RequestParam(value = "message", required = false) String message) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("message", message);
            modelMap.addAttribute("title", "Investment Options " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            List<ShareFund> portfolios = repository.funds();
            modelMap.addAttribute("portfolios", portfolios);
            modelMap.addAttribute("amount", 0); // in cents
            modelMap.addAttribute("stripePublicKey", stripePublicKey);
            String _currency = localCurrency != null && !localCurrency.isEmpty() ? localCurrency : currency_;
            modelMap.addAttribute("localCurrency", _currency);
            modelMap.addAttribute("currSymbol", common.currSymbol().get(_currency));
            modelMap.addAttribute("domain", domain);
            List<TransactionBean> walletSummary = repository.userWalletTransactions(user.getUserId());
            if (!walletSummary.isEmpty()) {
                modelMap.addAttribute("walletSummary", walletSummary);
            }
            actualWalletBalance(modelMap, user.getUserId());
            EmailVerificationBean bean = repository.getEmailVerification(user.getRefId());
            modelMap.addAttribute("isVerifiedEmail", "Y".equalsIgnoreCase(bean.getVerified()));
            userModelMapEntry(urlMap().get("investments") + "?id=" + user.getUserId(), modelMap);
            return "investments";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/insights"}, method = RequestMethod.GET)
    public String insights(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Research " + projectName);
            userModelMapEntry(urlMap().get("insights") + "?id=" + user.getUserId(), modelMap);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));
            return "insights";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/blog"}, method = RequestMethod.GET)
    public String blog(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Blog " + projectName);
            userModelMapEntry(urlMap().get("blog") + "?id=" + user.getUserId(), modelMap);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));
            return "blog";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
    public String profile(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));
            UserInfo userInfo = repository.findByUserId(user.getUserId());
            String fullName = userInfo.getFullName();
            if (userInfo.getFirstName() == null) {
                String[] arr = fullName.split(" ");
                userInfo.setFirstName(arr[0]);
                if (arr.length > 1) {
                    userInfo.setLastName(fullName.replaceFirst(arr[0], ""));
                }
            }
            modelMap.addAttribute("userInfo", userInfo);
            modelMap.addAttribute("title", "Profile " + projectName);
            userModelMapEntry(urlMap().get("profile") + "?id=" + user.getUserId(), modelMap);
            return "profile";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/payment"}, method = RequestMethod.GET)
    public String payment(ModelMap modelMap, Locale locale,
            @RequestParam(value = "un", required = false) String userId) throws IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Cash and Transaction " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            if (user.getAdmin() && userId != null) {
                UserInfo endUser = repository.findByUserId(userId);
                Date createdTs = common.parseDate(user.getCreatedTs(), CommonMethods.format2);
                endUser.setCreatedTime(createdTs.getTime());
                payment(modelMap, userId);
                modelMap.addAttribute("endUser", endUser);
            } else {
                userId = user.getUserId();
                payment(modelMap, user.getUserId());
                modelMap.addAttribute("endUser", user);
            }
            userModelMapEntry(urlMap().get("payment") + "?id=" + userId, modelMap);
            return "payment";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/transactions"}, method = RequestMethod.GET)
    public String transactions(ModelMap modelMap, Locale locale,
            @RequestParam(value = "un", required = false) String userId) throws IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Transactions " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            if (user.getAdmin() && userId != null) {
                UserInfo endUser = repository.findByUserId(userId);
                Date createdTs = common.parseDate(user.getCreatedTs(), CommonMethods.format2);
                endUser.setCreatedTime(createdTs.getTime());
                payment(modelMap, userId);
                modelMap.addAttribute("endUser", endUser);
            } else {
                userId = user.getUserId();
                payment(modelMap, user.getUserId());
                modelMap.addAttribute("endUser", user);
            }
            modelMap.addAttribute("newTransactionRequests", repository.userTransactionRequests(userId, "PENDING", null, null));
            modelMap.addAttribute("cancelInvestmentRequests", repository.userInvestmentRequests(userId, "CANCEL", null, null));
            modelMap.addAttribute("cancelTransactionRequests", repository.userTransactionRequests(userId, "CANCEL", null, null));
            modelMap.addAttribute("pendingWalletTxns", repository.pendingWalletTxns(null, userId));
            modelMap.addAttribute("pendingInvestments", databaseService.pendingInvestments(userId));
            modelMap.addAttribute("pendingSellTransactions", databaseService.pendingSellTransactions(userId));
            userModelMapEntry(urlMap().get("transactions") + "?id=" + userId, modelMap);
            return "transactions";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verification", "/verify-new"}, method = RequestMethod.GET)
    public String verifyNew(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    modelMap.addAttribute("info", user);
                    return "blocked";
                }
            }
            List<Verification> verList = repository.findVerification(user.getUserId(), "DONE");
            if (verList != null && !verList.isEmpty()) {
                return "redirect:/welcome";
            }
            modelMap.addAttribute("title", "Verification " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            List<Verification> vers = repository.findVerification(user.getUserId(), "PENDING");
            if (!vers.isEmpty()) {
                Verification ver = vers.get(vers.size() - 1);
                modelMap.addAttribute("ver", ver);
            }
            modelMap.addAttribute("info", user);
            Calendar cal = Calendar.getInstance();
            cal.setTime(common.today());//today
            cal.add(Calendar.YEAR, -18);
            Date adultDOB = cal.getTime();
            modelMap.addAttribute("adultDOB", common.dateFormat(adultDOB, common.format3));
            userModelMapEntry(urlMap().get("verify-new") + "?id=" + user.getUserId(), modelMap);
            return "verification2";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/refferal"}, method = RequestMethod.GET)
    public String refferal(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Referral " + projectName);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            modelMap.addAttribute("email", user.getUsername());
            modelMap.addAttribute("fullName", user.getFullName());
            modelMap.addAttribute("createdTime", user.getCreatedTime());

            userModelMapEntry(urlMap().get("refferal") + "?id=" + user.getUserId(), modelMap);
            return "referral";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/config"}, method = RequestMethod.GET)
    public String config(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "Configration " + projectName);
            userModelMapEntry(urlMap().get("config") + "?id=" + user.getUserId(), modelMap);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));
            return "config";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/nav_cal"}, method = RequestMethod.GET)
    public String nav_cal(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            modelMap.addAttribute("title", "NAV Calculation Engine " + projectName);
            userModelMapEntry(urlMap().get("nav_cal") + "?id=" + user.getUserId(), modelMap);
            modelMap.addAttribute("modelId", common.modelIds().get(user.getUserId()));

            return "nav_cal";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/charge-succeed"}, method = RequestMethod.GET)
    public String chargeSucceed(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            List<RequestBean> list = repository.userTransactionRequests(user.getUserId(), "LATER", null, null);
            if (!list.isEmpty()) {
                RequestBean req = list.get(0);
                modelMap.addAttribute("title", "Add Funds " + projectName);
                modelMap.addAttribute("user", user);
                modelMap.addAttribute("req", req);
                modelMap.addAttribute("currency", common.currSymbol().get(req.getCurrency()));
                HashMap<String, String> currencyAcc = common.accountDetails(req.getCurrency());
                modelMap.addAttribute("currencyAcc", currencyAcc);
                modelMap.addAttribute("brand", "BANK_TRANSFER");
                return "charge-succeed";
            }
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verify-later"}, method = RequestMethod.GET)
    public String verifyLater(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "blocked";
                }
            }
            user.setVerifyLater(Boolean.TRUE);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/transaction-request"}, method = RequestMethod.POST)
    public String requestForMoney(ModelMap modelMap, @ModelAttribute("request") RequestBean request) throws IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            Date now = dateTimeService.now();
            request.setCreatedDate(common.dateFormat(now));
            request.setMaster("WITHDRAWL");
            request.setStatus("PENDING");
            String _currency = request.getCurrency();
            BigDecimal maxamountbd = new BigDecimal(request.getMaxAmount());
            if (!currency_.equalsIgnoreCase(_currency)) {
                BigDecimal localPrice = databaseService.currencyConversionRate(_currency + "_" + currency_, null);
                BigDecimal amountbd = new BigDecimal(request.getLocalAmount());
                amountbd = amountbd.multiply(localPrice);
                amountbd = amountbd.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP);
                String usdAmount = amountbd.toPlainString();
                request.setAmount(usdAmount);
                maxamountbd = maxamountbd.divide(localPrice, Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP);
                String nzdmaxamount = maxamountbd.toPlainString();
                request.setMaxAmount(request.getMaxAmount() + " Approx. " + common.currSymbol().get(_currency) + " " + nzdmaxamount);
            } else {
                request.setAmount(request.getLocalAmount());
                maxamountbd = maxamountbd.setScale(Constants.DISPLAY_DECIMAL_PLACES, RoundingMode.HALF_UP);
                request.setMaxAmount(maxamountbd.toPlainString());
            }
            if (user.getAdmin() && request.getUserId() != null) {
                request.setUserId(request.getUserId());
            } else {
                request.setUserId(user.getUserId());
            }
            int reqId = repository.saveTransactionRequest(request);
            if (reqId > 0) {
                if (user.getUser()) {
                    mailManager.sendWithdrawlNotificationToUser(user.getEmail(), user.getFullName(), request);
                }
                mailManager.sendWithdrawlNotificationToAdmin(adminEmailId, request);
            }
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/add-investment", "/bank-transfer-add-investment"}, method = RequestMethod.POST)
    public String bankTransferAddInvestment(ModelMap modelMap, @ModelAttribute("investment") InvestmentBean invBean) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            invBean.setUserId(user.getUserId());
            Date now = dateTimeService.now();
            invBean.setCreatedDate(common.dateFormat(now));
            if (invBean.getTimeFrame() == null) {
                invBean.setTimeFrame("FIXED");
            }
            List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
            invBean.setFundActualShares(fundActualShares);
            repository.saveInvestment(invBean);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/via-wallet-add-investment"}, method = RequestMethod.POST)
    public String viaWalletAddInvestment(ModelMap modelMap, @ModelAttribute("investment") InvestmentBean invBean) throws IOException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            if (user.getAuthorities().contains(Constants.superAdminRole) || user.getAuthorities().contains(Constants.adminRole)) {
                invBean.setUserId(invBean.getUserId());
            } else {
                invBean.setUserId(user.getUserId());
            }
            BigDecimal invAmt = new BigDecimal(invBean.getInvestmentAmount());
            if (invAmt.compareTo(BigDecimal.ZERO) == -1 || invAmt.compareTo(BigDecimal.ZERO) == 0) {
                return "redirect:/investments?message=Please! You make investment greater than zero amount. "
                        + "Right Now, You tried to make investment of " + invAmt.toPlainString();
            }
            String actualWalletBalance = "0.00";
            List<TransactionBean> actualWalletBalances = repository.actualWalletBalances(invBean.getUserId());
            if (!actualWalletBalances.isEmpty()) {
                TransactionBean actualWalletBalanceObject = actualWalletBalances.get(0);
                actualWalletBalance = actualWalletBalanceObject.getUsd();
            }
            BigDecimal walBal = new BigDecimal(actualWalletBalance);
            if (walBal.compareTo(invAmt) == -1) {
                return "redirect:/investments?message=You don't have enough balance in your wallet to make investment.";
            }
            if (walBal.compareTo(invAmt) == 1 || walBal.compareTo(invAmt) == 0) {
                Date now = dateTimeService.now();
                invBean.setCreatedDate(common.dateFormat(now));
                if (invBean.getTimeFrame() == null) {
                    invBean.setTimeFrame("FIXED");
                }
                String _currency = invBean.getCurrency();
                if (!currency_.equalsIgnoreCase(_currency)) {
                    BigDecimal localPrice = databaseService.currencyConversionRate(currency_ + "_" + _currency, null);
                    BigDecimal amountbd = invAmt.multiply(localPrice);
                    amountbd = amountbd.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                    String localInvestmentAmount = amountbd.toPlainString();
                    invBean.setLocalInvestmentAmount(localInvestmentAmount);
                }
                List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
                invBean.setFundActualShares(fundActualShares);
                repository.saveInvestment(invBean);
                if (user.getUser()) {
                    mailManager.sendAddFundsNotificationToUser(user.getEmail(), user.getFullName(), invBean);
                }
                mailManager.sendAddFundsNotificationToAdmin(adminEmailId, invBean);
            }
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/add-investment-request", method = RequestMethod.POST)
    public String addInvestmentRequest(ModelMap model, @ModelAttribute("request") AccountEventCommand investmentReq) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            investmentReq.setUserId(user.getUserId());
            long creationTime = dateTimeService.timeDate();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(creationTime);
            investmentReq.setCreateDate(timestamp.toString());
//            databaseService.saveInvestmentRequest(investmentReq);
            databaseService.saveSellInvestmentRequest(investmentReq);
            if ("WITHDRAWL".equalsIgnoreCase(investmentReq.getActionType())) {
                if (user.getUser()) {
                    mailManager.sendSellFundsNotificationToUser(user.getEmail(), user.getFullName(), investmentReq);
                }
                mailManager.sendSellFundsNotificationToAdmin(adminEmailId, investmentReq);
            }
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verification"}, method = RequestMethod.POST)
    public String verification(ModelMap modelMap,
            final @ModelAttribute("verificaiton") @Valid Verification verification) throws IOException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            verification.setUser_id(user.getUserId());
            Date now = dateTimeService.now();
            verification.setCreated_ts(common.dateFormat(now));
            final String imgSrc = verification.getImageData();
            if (imgSrc != null && !imgSrc.trim().isEmpty()) {
                String image = imgSrc.replaceAll("data:image/png;base64,", "");
                image = image.replaceAll(" ", "+");
                File file = fileUtility.storeImage(15l, image, user.getFullName() + user.getUserId() + ".png");
                Map<String, File> files = new HashMap<>();
                files.put("data", file);
                Map<String, String> headers = new HashMap<>();
                headers.put("x-api-key", "RZHp1UuhsB52ZWfLJmxjq2PeJEPmoInB1GaalRwU");
//                String response = requestManager.sendHTTPPostRequest("https://api.trueface.ai/v1/facedetect", files, headers);
                verification.setImageData(file.getName());
//            } else {
//                model.addAttribute("ver", verification);
//                return "verification2";
            }
            repository.saveVerification(verification);
            user = sessionManagementService.getCurrentUser(Boolean.TRUE);
//            UserInfo info = repository.findByUsername(user.getUsername());
//            sessionManagementService.logout(repository);
//            sessionManagementService.logout(user);
//            securityService.autologin(user.getUsername(), info.getPassword());
            common.modelIds().remove(user.getUserId());
            modelMap.addAttribute("brand", "VERIFICATION_DONE");
            return "charge-succeed";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/profile"}, method = RequestMethod.POST)
    public String profile(ModelMap model, @ModelAttribute("profile") UserInfo profile) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            Date now = dateTimeService.now();
            profile.setCreatedTs(common.dateFormat(now));
            profile.setUserId(user.getUserId());
            profile.setFullName(profile.getFirstName() + " " + profile.getLastName());
            repository.saveTempProfile(profile);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/config"}, method = RequestMethod.POST)
    public String config(ModelMap model, @ModelAttribute("config") Configuration config) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            config.setUserId(user.getUserId());
            Date now = dateTimeService.now();
            config.setCreatedTs(common.dateFormat(now));
            repository.saveConfiguration(config);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/currency"}, method = RequestMethod.POST)
    public String config(ModelMap model, @ModelAttribute("currency") String currency) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() == null
                        || user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                }
            }
            common.userCurrency().put(user.getUserId(), currency);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/resetpass"}, method = RequestMethod.POST)
    public String resetPasswordPage(ModelMap modelMap, @ModelAttribute("resetPwdForm") ResetPasswordForm resetPwdForm) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null && user.getUserId().equals(resetPwdForm.getUserId())) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            UserInfo userInfo = repository.findByUserId(resetPwdForm.getUserId());
            if (encoder.matches(resetPwdForm.getOldPassword(), userInfo.getPassword())
                    && !resetPwdForm.getNewPassword().isEmpty()
                    && !resetPwdForm.getRePassword().isEmpty()
                    && resetPwdForm.getNewPassword().equals(resetPwdForm.getRePassword())) {
                userInfo.setPassword(resetPwdForm.getNewPassword());
                repository.updateUserPassword(userInfo);
                modelMap.addAttribute("title", "Password Updated " + projectName);
                modelMap.addAttribute("message", "You have updated your password.");
                modelMap.addAttribute("info", user);
                modelMap.addAttribute("modelId", common.modelIds().get(userInfo.getUserId()));
                return "config";
            } else {
                modelMap.addAttribute("title", "Old password was not same " + projectName);
                modelMap.addAttribute("message", "You have provided old password. Old password was not same.");
                modelMap.addAttribute("info", user);
                modelMap.addAttribute("modelId", common.modelIds().get(userInfo.getUserId()));
                return "config";
            }
        }
        return "login";
    }

    @RequestMapping(value = {"/user-add-funds"}, method = RequestMethod.POST)
    public String userAddFunds(ModelMap model, @ModelAttribute("request") RequestBean request) throws IOException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getUser()) {
                if (user.getActive() != null && "B".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() != null && "P".equalsIgnoreCase(user.getActive())) {
                    return "redirect:/blocked";
                } else if (user.getActive() == null) {
                    return "redirect:/blocked";
                }
            }
            Date now = dateTimeService.now();
            request.setCreatedDate(common.dateFormat(now));
            request.setMaster("ADDFUNDS");
            String _currency = request.getCurrency();
            request.setAmount(request.getLocalAmount());
            if (!currency_.equalsIgnoreCase(_currency)) {
                BigDecimal amountbd = new BigDecimal(request.getLocalAmount());
                BigDecimal localPrice = databaseService.currencyConversionRate(_currency + "_" + currency_, null);
                amountbd = amountbd.multiply(localPrice);
                amountbd = amountbd.setScale(Constants.MAXIMUM_DECIMAL_PLACES, RoundingMode.HALF_UP);
                String usdAmount = amountbd.toPlainString();
                request.setAmount(usdAmount);
            }
            if (user.getAdmin() && request.getUserId() != null) {
                request.setStatus("PENDING");
                request.setUserId(request.getUserId());
            } else {
                request.setStatus("LATER");
                request.setUserId(user.getUserId());
            }
            repository.saveTransactionRequest(request);
            return "redirect:/charge-succeed";
        } else {
            return "redirect:/login";
        }
    }

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private MailManager mailManager;
    @Autowired
    private FileUtility fileUtility;
    @Autowired
    private String adminEmailId;
    @Autowired
    private String domain;
    @Autowired
    private String currency_;
    @Autowired
    private String localCurrency;
    @Autowired
    private String projectName;
    final private String stripePublicKey = "pk_test_zrCXgtyeFkNkUj4HjXvEdnah";

}
