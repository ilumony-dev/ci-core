<!--                <section id="investments" style="display:none;">
                                    <div class="label" class="investment-fund-modal-fundName" style="font-size: 1.5rem; color:forestgreen;">
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="hidden" name='fundId' id="investment-fund-modal-fundId" value="0"/>
                                            <input type="hidden" name='fundName' class='investment-fund-modal-fundName' value="null"/>
                                            <input type="hidden" id="investment-fund-modal-fundIR" value="0"/>
                                            <h3 class="color1">How much do you want to invest now?</h3>
                                            <p style="font-size:12px; color: #666">(Minimum investment amount of $50. Either $50 now, or $50 as a regular payment)</p>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div id="slider1"></div>
                                                    <input type="range" class="form-control" id="slider1" value="0" min="0" max="100000" onchange="setCheckoutAmount();"/>
                                                </div><span style="margin-top: 10px">${currSymbol}</span>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="investment-fund-modal-upFrontValue" name="upFrontAmount" value="0.00" step="0.01" onkeyup="setCheckoutAmount();" onchange="setCheckoutAmount();"/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button id="btnload">reset</button>
                                                    <button id="submit" class="btn btn-warning" onclick="displayContinue(this);">Submit Amount</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12" id="investment-fund-modal-hideImg" style="display:none;">
                                                    <center><img src="./resources/img/loading.gif" style="width:15%"></center>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                                        <button class="btn btn-primary next" id="investment-fund-modal-continue" type="button" onclick="activePage('payGateways');">Pay Now</button>
                                    </div>
                                </section>-->
<!--                <section id="payGateways" style="display:none;">
                    <h3 class="color1">How would you like to pay?</h3>
                    <div class="row">
                        <div class="col-md-4" style="margin-top: 28px">Deal in NZD
                            <a id="NET_BANKING" href="">
                                <img src="./resources/img/preferences/payment-1.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px;" alt="Internet Banking">
                            </a>
                        </div>  
                        <div class="col-md-4" style="margin-top: 28px">Deal in NZD
                            <form action='./user-stripe-charge-add-investment' method='POST' id='checkout-form'>
                                <input type='hidden' class="checkout-amount localInvestmentAmount" name='localInvestmentAmount' value="0.00" step="0.01"/>
                                <input type='hidden' class="checkout-amount investmentAmount" name='investmentAmount' value="0.00" step="0.01"/>
                                <input type='hidden' class="checkout-fundId" name='fundId'/>
                                <script
                                    src='https://checkout.stripe.com/checkout.js'
                                    class='stripe-button'
                                    data-key='${stripePublicKey}' 
                                    data-amount='${amount}' 
                                    data-currency='${paymentCurrency}'
                                    data-name='Minto'
                                    data-description='${description}'
                                    data-image='${domain}/resources/img/invsta-03(200px).png'
                                    data-locale='auto'
                                    data-zip-code='false'>
                                </script>
                            </form>
                        </div> 
                        <div class="col-md-4">Deal in USD
                            <input type="button" name="payGateway" value="WALLET" id="payGateway4" class="input-hidden getway radio-btn" data-target="#via-wallet" data-toggle="modal"/>
                            <label for="payGateway4">
                                <img src="./resources/img/preferences/mywallet.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Wallet">
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn btn-warning previous" type="button" onclick="activePage('investments');">Back</button>
                    </div>
                </section>-->