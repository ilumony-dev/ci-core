<%-- 
    Document   : addfund
    Created on : Oct 15, 2017, 9:50:50 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="addfund" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addfund-title">
                    Creation Form
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./admin-add-fund" method="post" id="addFundForm">
                <div class="modal-body"> 
                    <div class="row" id="addfund-first-row-div">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="master">Purpose</label>
                                <select class="form-control" name="master" id="master" onchange="onAddFundChangeMaster(this)">
                                    <option value=""> -Select- </option>
                                    <option value="FUND">Fund</option>
                                    <option value="PORTFOLIO">Portfolio</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="currencySymbol">Currency Symbol</label>
                                <select class="form-control" name="currencySymbol" id="currencySymbol">
                                    <option value=""> -Select- </option>
                                    <c:forEach items="${currencies}" var="currency">
                                        <option value="${currency.currencySymbol}">${currency.currencySymbol}</option>
                                    </c:forEach> 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="fundId" id="addfund-fundId"/>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control" placeholder="Enter Name" type="text" name="name" id="addfund-name"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input class="form-control" placeholder="Enter Description" type="text" name="description" id="addfund-description"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exchangeCode">Exchange Code</label>
                                <input class="form-control" placeholder="Enter Exchange Code" type="text" name="exchangeCode" id="addfund-exchangeCode"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="custodianId">Custodian Id</label>
                                <input class="form-control" placeholder="Enter Custodian Id" type="text" name="custodianId" id="addfund-custodianId"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <h6 class="element-header">
                                    Details
                                </h6>
                                <div class="element-box">
                                    <div class="table-responsive">
                                        <table class="table table-lightborder" id="addfund-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Name
                                                    </th>
                                                    <th class="text-center">
                                                        Percentage
                                                    </th>
                                                    <th class="text-center">
                                                        Quantity
                                                    </th>
                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="addfund-details">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                    <button class="btn btn-primary" type="button" ondblclick="addFundSubmit();">Proceed</button>
                </div>

            </form>
        </div>
    </div>
</div>