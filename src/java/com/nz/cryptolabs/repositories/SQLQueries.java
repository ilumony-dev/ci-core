/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

/**
 *
 * @author Administrator
 */
public interface SQLQueries {

    String insertUserActivity = "INSERT INTO `user_activity` (`user_id`, `activity`, `date_time`, `active`)\n"
            + "VALUES (?, ?, now(), 'Y');";

    String updateUserActivity = "UPDATE `user_activity` SET `active` = 'N' WHERE `id` > 0 and `user_id` = ";

    String insertTransaction = "INSERT INTO `transactions`"
            + " (`created_ts`, `user_id`, `investment_id`, `particulars`, `inc_dec`, `amount`, `active`, `approved`, `txn_req_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', 'N', ?);";

    String insertUnitTransaction = " INSERT INTO `user_inv_units_txn`"
            + " (`created_ts`, `user_id`, `investment_id`, `nzd`, `usd`, `btc`, `minto`, `active`, `action`, `ref_inv_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y', ?, ?); ";

    String insertInviteCode = " INSERT INTO `invite_code`"
            + " (`invite_code`, `expiry_ts`, `created_by`, `created_ts`, `expired`, `used`, `active`, `purpose`, `email`,`name`)"
            + " VALUES (?, ?, ?, ?, 'N', 'N', 'Y', ?, ?, ?); ";

    String inviteCode = " SELECT * FROM invite_code WHERE used = 'N' AND invite_code = ?; ";

    String usedInviteCode = " UPDATE `invite_code` SET `used` = 'Y' WHERE `used` = 'N' AND `invite_code` = ?; ";

    String allocateEmail = " UPDATE `invite_code` SET `email`=? WHERE `id`=?; ";

    String insertUser = "INSERT INTO `user_master`"
            + "(`full_name`, `dob`, `email`, `password`, `invite_code`, `mobile_no`, `created_ts`, `active`, `ref_id`, `first_name`, `last_name`)"
            + "VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?);";

    String insertRole = "INSERT INTO `role_master` (`user_id`, `role`) VALUES (?, ?);";

    String updatePassword = "update user_master U set U.password=? where U.user_id = ?;";
    
    String updateActive = "update user_master U set U.active=? where U.user_id = ?;";

    String loginSQL = " SELECT U.*, R.role, coalesce(UA.count,0) as login_count, coalesce(V.count,0) as verify_count "
            //            + ", C.currency"
            + " FROM user_master U "
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) "
            //            + " LEFT JOIN config C ON (U.user_id = C.user_id and C.active = 'Y') "
            + " LEFT JOIN (SELECT count(*) as count, user_id FROM user_activity UA GROUP BY user_id) UA ON (U.user_id = UA.user_id) "
            + " LEFT JOIN (SELECT count(*) as count, user_id FROM verification V GROUP BY user_id)V ON (U.user_id = V.user_id) "
            + " WHERE U.email = ?;";

    String userById = " SELECT U.*, R.role, V.address as v_address FROM user_master U\n"
            + " INNER JOIN role_master R ON (U.user_id = R.user_id)\n"
            + " LEFT JOIN verification V ON (U.user_id = V.user_id)\n"
            + " WHERE U.user_id = ?;";

    String managementUser = "SELECT U.*, R.role FROM user_master U"
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) WHERE R.role = 'ROLE_MANAGEMENT';";//AND U.branch_id = ?

    String insertShare = " INSERT INTO `share` "
            + " (`name`, `desc`, `active`, `approved`, `created_by`, `exchange_code`, `custodian_id`) "
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?)";

    String insertCurrency = " INSERT INTO `currency` "
            + " (`name`, `symbol`, `active`, `approved`, `desc`, `created_by`, `exchange_code`, `custodian_id`) "
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?, ?);";

    String shareById = "SELECT S.*, SP.price FROM `share` S\n"
            + " INNER JOIN `share_price` SP ON(S.id = SP.share_id)\n"
            + " WHERE SP.active= 'Y' AND S.id = ?";

    String updateShare = " UPDATE `share` SET `name`=?, `desc`=?, `exchange_code`=?, `custodian_id`=? WHERE `id`=?;";

    String deActiveShare = " UPDATE `share` SET `active`='N' WHERE `id`=?;";

    String insertFund = " INSERT INTO `fund`"
            + " (`name`, `desc`, `active`, `approved`, `created_by`, `custodian_id`, `exchange_code`, `master`)"
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?, ?); ";

    String updateFund = " UPDATE `fund` SET `name`=?, `desc`=?, `exchange_code`=?, `custodian_id`=? WHERE `id`=?;";

    String deActiveFund = " UPDATE `fund` SET `active`='N' WHERE `id`=?;";

    String inActiveFundDetail = " UPDATE `fund_detail` SET `active`='N' WHERE `fund_id`=?; ";

    String insertFundDetail = " INSERT INTO `fund_detail`"
            + " (`fund_id`, `share_id`, `percentage`, `quantity`, `cdate`, `active`, `coin_id`)"
            + " VALUES (?, ?, ?, ?, ?, 'Y', ?);";

    String insertInvestment = "INSERT INTO `investment`"
            + " (`user_id`, `customer_name`, `investment_amount`, `bank_acc`, `reference_no`, `fund_id`, `active`, `created_ts`, `time_frame`, `bank_name`, `years`, `regularly_amount`, `local_investment_amount`, `currency`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?, ?, ?, ?, ?);";

    String updateInvestmentAmount = " UPDATE `investment` SET `investment_amount`= `investment_amount` + ?, `local_investment_amount`= `local_investment_amount` + ? WHERE `id` = ?;";

    String insertInvestmentDetails = " INSERT INTO `investment_details`"
            + " (`user_id`, `investment_id`, `created_ts`, `investment_amount`, `local_investment_amount`, `currency`, `active`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y'); ";

    String insertInvestmentPendingShares = "INSERT INTO `user_shares_pending_transaction`"
            + " (`created_ts`, `user_id`, `investment_id`, `share_id`, `price`, `quantity`, `active`, `action`, `coin_id`, `inv_req_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?);";

    String insertInvestmentRequest = "INSERT INTO `investment_request`"
            + " (`user_id`, `investment_id`, `amount`, `action`, `created_ts`, `description`, `status`, `active`, `local_amount`, `currency`, `percentage`)"
            + " VALUES (?, ?,?, ?, ?, ?, ?, 'Y',?, ?, ?);";

    String insertInvestmentUpdatedShares = "INSERT INTO `user_shares_transaction`"
            + " (`created_ts`, `user_id`, `investment_id`, `share_id`, `price`, `quantity`, `active`, `action`, `coin_id`, `ref_inv_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?);";

    String updatePurchasedShares = "UPDATE `user_shares_transaction` "
            + "SET `price`= ?, `quantity` = ? WHERE `id`= ?;";

    String updateOldSharePrice = "UPDATE `share_price` SET `active` = 'N' WHERE `share_id` = ?";

    String insertSharePrice = "INSERT INTO `share_price`"
            + " (`share_id`, `price`, `created_ts`, `active`)"
            + " VALUES (?, ?, ?, 'Y');";

    String updateOldFundPrice = "UPDATE `fund_price` SET `active` = 'N' WHERE `fund_id` = ?";

    String insertFundPrice = "INSERT INTO `fund_price`"
            + " (`fund_id`, `price`, `created_ts`, `active`, `total_value`)"
            + " VALUES (?, ?, ?, 'Y', ?);";

    String fundPrice = " SELECT * FROM fund_price where active = 'Y' and fund_id = ? ";

    String fundPriceByInvestment = " SELECT FP.* FROM fund_price FP\n"
            + "INNER JOIN investment IM ON(FP.fund_id = IM.fund_id)\n"
            + "where FP.active = 'Y' and IM.id = ?  ";

    String insertUserRefNo = "INSERT INTO `user_ref_no` "
            + " (`ref_no`, `stripe_token`, `poli_token`, `user_id`, `fund_id`, `price_in_nzd`, `created_ts`, `active`,`amounted`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y', 'N');";

    String insertVerification = "INSERT INTO `verification` "
            + " (`pp_passport_number`, `pp_passport_expiry_date`, `pp_filename`, `pa_same_res_addr`, `pa_is_your_pa_street_addr`, "
            + " `pa_postal_agency`, `pa_po_box`, `pa_private_bag`, `pa_special_service`, `pa_number`, `pa_postal_office`, `pa_postal_code`, "
            + " `pa_filename`, `dl_first_name`, `dl_middle_name`, `dl_last_name`, `dl_dob`, `dl_street`, `dl_street_name`, `dl_street_number`, "
            + " `dl_license_number`, `dl_license_version`, `dl_filename`, `address`, `user_id`, `created_ts`, `is_nz`, `nz_id`, `full_name`, "
            + " `dob`, `status`, `countries`, `pp_issue_country`, `dl_issue_country`, `tax_resi_country`, `tax_id`, `src_of_fund`)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'PENDING', ?, ?, ?, ?, ?, ?);";

    String verificationStatus = " UPDATE `verification` SET `status` = ?, `approved_ts` = ? WHERE `id` = ?;";

    String updateOldConfig = "UPDATE `config` SET `active` = 'N' WHERE `user_id` = ?";

    String insertConfig = "INSERT INTO `config` "
            + " (`local_currency`, `show_currency`, `user_id`, `created_ts`, `active`) "
            + " VALUES (?, ?, ?, 'Y');";

    String insertStripeCharge = "INSERT INTO `stripe_charge` "
            + " (`stripe_charge_id`, `user_id`, `bal_txn_id`, `created_ts`, `stripe_created_ts`, `amount`, `amount_refunded`, `currency`, "
            + " `status`, `fail_code`, `fail_message`, `description`, `fund_id`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    String insertWalletTransaction = " INSERT INTO `wallet_txns` "
            + " (`created_ts`, `user_id`, `investment_id`, `txn_id`, `brand`, `particulars`, `inc_dec`, `amount`, `active`, `approved`, `txn_req_id`, `ref_inv_id`, `medium`, `base`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'Y', 'N', ?, ?, ?, ?);";

    String approvedWalletTxn = " UPDATE `wallet_txns` SET `approved`='Y' WHERE `active` = 'Y' AND `id`=?;";

    String approvedTransaction = " UPDATE `transactions` SET `approved`='Y' WHERE `active` = 'Y' AND `id`=?;";

    String transactionRequestStatus = " UPDATE `transaction_request` SET `status` = ? WHERE `active` = 'Y' AND `id` = ? AND `master` = ?;";

    String insertTransactionRequest = " INSERT INTO `transaction_request`"
            + " (`created_ts`, `user_id`, `acc_number`, `acc_name`, `bank_name`, `amount`, `active`, `local_amount`, `currency`,`master`,`autoCheck`,`status`, `description`) "
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?, ?, ?, ?); ";

    String updateTransactionRequest = " UPDATE `transaction_request` SET `amount` = ?, `local_amount` = ?, `currency` = ? WHERE `active` = 'Y' AND `id` = ?;";

    String funds = " SELECT F.*, group_concat(V.a_share) as shares FROM fund F \n"
            + " LEFT JOIN \n"
            + " (SELECT FD.*, coin_id as a_share FROM fund_detail FD \n"
            + " WHERE FD.active = 'Y'\n"
            + " UNION ALL\n"
            + " SELECT FD.*, S.id as a_share FROM fund_detail FD \n"
            + " INNER JOIN share S ON (S.active = 'Y' AND S.id = FD.share_id) \n"
            + " WHERE FD.active = 'Y'\n"
            + " ) V ON (F.id = V.fund_id) \n"
            + " WHERE F.active = 'Y' AND F.approved = 'Y'\n"
            + " GROUP BY F.id; ";

    String fundById = " SELECT F.*, group_concat(V.name) as shares FROM fund F "
            + " LEFT JOIN ("
            + " SELECT FD.*, case when S.name is not null then S.name else coin_id end as name FROM fund_detail FD \n"
            + " LEFT JOIN share S ON (S.active= 'Y' AND S.id = FD.share_id) WHERE FD.active = 'Y'"
            + ") V ON (F.id = V.fund_id) WHERE F.active = 'Y' AND F.id = ? "
            + " GROUP BY F.id; ";

    String fundByInvestmentId = "  SELECT F.*, group_concat(V.name) as shares FROM fund F \n"
            + " INNER JOIN investment IM ON(F.id = IM.fund_id)\n"
            + " LEFT JOIN (\n"
            + " SELECT FD.*, case when S.name is not null then S.name else coin_id end as name FROM fund_detail FD \n"
            + " LEFT JOIN share S ON (S.active= 'Y' AND S.id = FD.share_id) WHERE FD.active = 'Y'\n"
            + " ) V ON (F.id = V.fund_id)\n"
            + " WHERE IM.id = ?\n"
            + " GROUP BY F.id; ";

    String shares = " SELECT * FROM `share` WHERE `active`= 'Y' ";

    String currencies = " SELECT * FROM `currency` WHERE `active`= 'Y' ";

    String users = " SELECT U.*, R.role, UA.date_time FROM user_master U \n"
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) \n"
            + " LEFT JOIN user_activity UA ON (U.user_id = UA.user_id AND UA.active = 'Y') \n"
            + " WHERE U.user_id > 0 ";

    String insertFundTrade = " INSERT INTO `fund_trade` "
            + " (`created_ts`, `fund_id`, `op_bal`, `close_bal`, `pur_trades`, `sold_trades`, `pur_coins`, `sold_coins`, `active`, `unit_price`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'Y', ?); ";

    String insertConversionRate = " INSERT INTO `conversion_rate` "
            + " (`created_ts`, `usd_nzd`, `usd_gbp`, `usd_inr`, `usd_aud`) "
            + " VALUES (?, ?, ?, ?, ?); ";

    String insertUserCurrBtc = " INSERT INTO `user_curr_btc` "
            + " (`created_ts`, `fund_id`, `user_id`, `investment_id`, `bitcoin`, `minto`, `active`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y') ";

    String insertUserTodayStatus = " INSERT INTO `user_today_status` "
            + " (`curr_date`, `user_id`, `investment_id`, `fund_id`, `investment_amount`, `active`) "
            + " VALUES (?, ?, ?, ?, ?, 'Y') ";

    String insertEmailVerification = " INSERT INTO `email_verification` "
            + " (`created_ts`, `updated_ts`, `email`, `token`, `ref_id`, `verified`) "
            + " VALUES (?, ?, ?, ?, ?, 'N');";

    String verifiedEmail = " UPDATE `email_verification` SET `updated_ts`= ?, `verified`='Y' "
            + " WHERE `id` > 0  AND `token` = ? AND `ref_id` = ?; ";

    String emailVerification = " SELECT * FROM `email_verification` WHERE `ref_id` = ? ";

    String insertNavMaster = " INSERT INTO `mgmt_nav_master`\n"
            + "(`created_ts`, `created_date`, `user_id`, `nzd`, `usd`, `btc`, `minto`, `active`, `notes`, `fund_id`, `txn_req_id`)\n"
            + "VALUES(?, ?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?); ";

    String insertNavDetail = " INSERT INTO `mgmt_nav_detail` (`a_share`, `amount`, `active`, `master_id`)\n"
            + " VALUES (?,?,'Y',?); ";

    String insertTempProfile = " INSERT INTO `temp_profile`\n"
            + "(`created_ts`,\n"
            + "`user_id`,\n"
            + "`first_name`,\n"
            + "`last_name`,\n"
            + "`full_name`,\n"
            + "`mobile_no`,\n"
            + "`gender`,\n"
            + "`address`,\n"
            + "`active`)\n"
            + "VALUES\n"
            + "(?, ?, ?, ?, ?, ?, ?, ?, 'Y'); ";

}
