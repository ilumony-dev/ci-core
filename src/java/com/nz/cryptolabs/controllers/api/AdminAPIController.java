/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.api;

import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.RequestBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.components.CommonMethods;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import com.nz.cryptolabs.mail.AdminMailManager;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.DateTimeService;
import com.nz.cryptolabs.services.SessionManagementService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping(value = "/rest/cryptolabs/api")
public class AdminAPIController {

    @Autowired
    private AdminMailManager mailManager;

    @RequestMapping(value = {"/onlineUsers", "/oluuytrerty"}, method = RequestMethod.GET)
    @ResponseBody
    public String getOnlineUsers(@RequestParam(name = "api_key", required = false) String apiKey) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        List<SecuredUser> principals = (List) sessionManagementService.getAllOnlineUsers();
        return toString(principals);
    }

    @RequestMapping(value = {"/deActiveShare", "/met98765678w356"}, method = RequestMethod.GET)
    @ResponseBody
    public String deActiveShare(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "sId") String shareId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        Boolean result = repository.deActiveShare(shareId);
        return result.toString();
    }

    @RequestMapping(value = {"/deActiveFund", "/met5456876w789"}, method = RequestMethod.GET)
    @ResponseBody
    public String deActiveFund(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "fId") String fundId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        Boolean result = repository.deActiveFund(fundId);
        return result.toString();
    }

    @RequestMapping(value = {"/wallet-app", "/met13u4i23lkjh"}, method = RequestMethod.GET)
    @ResponseBody
    public String approvedWalletTxn(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "id") String id) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        Boolean result = repository.approvedWalletTxn(id);
        if (result) {
            List<TransactionBean> walletTxns = repository.walletTxns(id, null, null, false, false);
            if (!walletTxns.isEmpty()) {
                TransactionBean txn = walletTxns.get(0);
                if ("WR".equalsIgnoreCase(txn.getBase()) && txn.getTxn_Req_id() != null && !txn.getTxn_Req_id().isEmpty()) {
                    result = repository.approvedTransaction(null, txn.getTxn_Req_id());
                }
                if ("POLI".equalsIgnoreCase(txn.getBrand())) {
                    repository.saveTransaction(txn);
                }
                mailManager.approvedWalletTxn(txn);
            }
        }
        return result.toString();
    }

    @RequestMapping(value = {"/withdrawl-req-done", "/met089234845034"}, method = RequestMethod.POST)
    public void withdrawlRequestDone(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "ok") String id,
            @RequestParam(name = "un") String userId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null && id != null && userId != null) {
            RequestBean txn = repository.userTransactionRequests(userId, "PENDING", id, null).get(0);
            if (txn != null) {
                TransactionBean bean = new TransactionBean();
                Date date = dateTimeService.now();
                bean.setCreated_ts(common.dateFormat(date));
                bean.setUser_id(txn.getUserId());
                if ("ADDFUNDS".equals(txn.getMaster())) {
                    bean.setParticulars(txn.getDescription() != null ? txn.getDescription() : "We have completed your requested add funds.");
                    bean.setInc_dec("Inc");
                } else if ("WITHDRAWL".equals(txn.getMaster())) {
                    bean.setParticulars(txn.getDescription() != null ? txn.getDescription() : "We have desposited US$" + txn.getAmount() + " to your bank account.");
                    bean.setInc_dec("Dec");
                }
                bean.setAmount(txn.getAmount());
                bean.setTxn_Req_id(txn.getId());
                repository.saveWalletTransaction(bean);
//        if ("WITHDRAWL".equals(txn.getMaster())) {
                repository.saveTransaction(bean);
//        }
                String status = "DONE";
                repository.transactionRequestStatus(id, status, txn.getMaster());
            }
        }
    }

    @RequestMapping(value = {"/withdrawl-req-cancel", "/met091234850423"}, method = RequestMethod.POST)
    public void withdrawlRequestCancel(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "ok") String id,
            @RequestParam(name = "un") String userId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null && id != null && userId != null) {
            RequestBean txn = repository.userTransactionRequests(userId, "PENDING", id, null).get(0);
            String status = "CANCEL";
            repository.transactionRequestStatus(id, status, txn.getMaster());
        }
    }

    @RequestMapping(value = {"/admin-tran-req-cancel", "/met97863ww43r5897458sdrfser3wr"}, method = RequestMethod.POST)
    public void adminTranReqCancel(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "ok", required = false) String tokenNo,
            @RequestParam(name = "bs", required = false) String base,
            @RequestParam(name = "act", required = false) String action
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null && tokenNo != null) {
            String status = "CANCEL";
            switch (base) {
                case "NWR":
                    RequestBean txn = repository.userTransactionRequests(null, "PENDING", null, tokenNo).get(0);
                    repository.transactionRequestStatus(txn.getId(), status, txn.getMaster());
                    break;
                case "NIR":
                    if ("WITHDRAWL".equalsIgnoreCase(action)) {
                        InvestmentBean sellTxn = repository.pendingSellTransactions(null, null, tokenNo).get(0);
                        repository.investmentRequestStatus(sellTxn.getReqId(), status);
                    } else if ("DEPOSITE".equalsIgnoreCase(action)) {
                        InvestmentBean invTxn = repository.pendingInvestmentRequests(null, null, tokenNo).get(0);
                        repository.investmentRequestStatus(invTxn.getReqId(), status);
                        repository.deactivePendingSharesStatus(invTxn.getUserId(), invTxn.getInvestmentId(), "PENDING");
                    }
                    break;
            }

        }
    }

    @RequestMapping(value = {"/verify-done", "/met029lksdjfjdsljf"}, method = RequestMethod.GET)
    @ResponseBody
    public String verifyDone(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "id") String id) {
        id = id.replaceAll("vd_", "");
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        String status = "DONE";
        Boolean result = repository.setVerificationStatus(id, status, dateTimeService.now());
        return result.toString();
    }

    @RequestMapping(value = {"/verify-cancel", "/met48950oiriirtu"}, method = RequestMethod.GET)
    @ResponseBody
    public String verifyCancel(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "id") String id) {
        id = id.replaceAll("vc_", "");
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        String status = "CANCEL";
        Boolean result = repository.setVerificationStatus(id, status, dateTimeService.now());
        return result.toString();
    }

    @RequestMapping(value = {"/balChart"}, method = RequestMethod.GET)
    @ResponseBody
    public String balChart(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("type") String type) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        Map<Date, BigDecimal> map = databaseService.latestBalanceData(type, null, null);
        String values = new String();
        String labels = new String();
        String totalBalance = new String();
        Set<Map.Entry<Date, BigDecimal>> set = map.entrySet();
        for (Iterator<Map.Entry<Date, BigDecimal>> iterator = set.iterator(); iterator.hasNext();) {
            int i = 0;
            Map.Entry<Date, BigDecimal> next = iterator.next();
            values = values + ((i > 0 && i != map.size()) ? ", " : "") + next.getValue();
            labels = labels + ((i > 0 && i != map.size()) ? ", " : "") + ("\"" + next.getKey() + "\"");
            i++;
        }
        return "{"
                + "\"totalBalance\":" + totalBalance + " ,\n"
                + "\"labels\":[" + labels + "] ,\n"
                + "\"values\":[" + values + "] \n"
                + "}";
    }

    @RequestMapping(value = {"/userAccountSummary"}, method = RequestMethod.GET)
    @ResponseBody
    public String userAccountSummary(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<TransactionBean> trans = repository.userAccountSummary(userInfo.getUserId());
            return toString(trans);
        }
        return null;
    }

    @RequestMapping(value = {"/userUnitSummary"}, method = RequestMethod.GET)
    @ResponseBody
    public String userUnitSummary(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username,
            @RequestParam(value = "fdt", required = false) String fromDate,
            @RequestParam(value = "tdt", required = false) String toDate
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<ShareFund> units = databaseService.userUnitSummary(userInfo.getUserId(), fromDate, toDate);
            fromDate = (fromDate != null && !fromDate.isEmpty()) ? fromDate : "First Day";
            toDate = (toDate != null && !toDate.isEmpty()) ? toDate : "Present Day";
            return "{"
                    + "\"units\":" + toString(units) + " ,\n"
                    + "\"refId\":" + checkNull(userInfo.getRefId()) + " ,\n"
                    + "\"fromDate\":" + checkNull(fromDate) + " ,\n"
                    + "\"toDate\":" + checkNull(toDate) + " "
                    + "}";

        }
        return null;
    }

    @RequestMapping(value = {"/userDailyUpdates"}, method = RequestMethod.GET)
    @ResponseBody
    public String userDailyUpdates(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<ShareFund> dailyUpdates = databaseService.dailyUpdates(null, userInfo.getUserId(), null, true);
            return toString(dailyUpdates);
        }
        return null;
    }

    @RequestMapping(value = {"/coinsByAllInvestments", "/qwergnuyhalltinyhkgyi"}, method = RequestMethod.GET)
    @ResponseBody
    public String coinsByInvestments(@RequestParam(name = "api_key", required = false) String apiKey) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        List<InvestmentBean> invs = databaseService.coinsByInvestments(null, null, false, null);
        Set<String> fundNameSet = new LinkedHashSet<>();
        Set<String> coinNameSet = new LinkedHashSet<>();
        Set<String> refIdSet = new LinkedHashSet<>();
        for (Iterator<InvestmentBean> it = invs.iterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            if (inv.getRefId() != null) {
                refIdSet.add("\"" + inv.getRefId() + "\"");
            }
            if (!"Total".equalsIgnoreCase(inv.getFundName())) {
                fundNameSet.add("\"" + inv.getFundName() + "\"");
                if (inv.getFundActualShares() != null) {
                    for (Iterator<ShareFund> it1 = inv.getFundActualShares().iterator(); it1.hasNext();) {
                        ShareFund share = it1.next();
                        coinNameSet.add("\"" + share.getCoinId() + "\"");
                    }
                }
            }
        }
        return "{"
                + "\"investments\":" + toString(invs) + " ,\n"
                + "\"refIds\":" + toString(new ArrayList(refIdSet)) + " ,\n"
                + "\"funds\":" + toString(new ArrayList(fundNameSet)) + " ,\n"
                + "\"coins\":" + toString(new ArrayList(coinNameSet)) + "\n"
                + "}";
    }

    @RequestMapping(value = {"/new-users", "/nueeiuytewrtsdrqwetsers"}, method = RequestMethod.POST)
    @ResponseBody
    public String newUsers(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "daterange", required = false) String daterange) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        String[] arr = daterange.split(" - ");
        if (arr != null && arr.length == 2) {
            Date first = common.parseDate(arr[0], CommonMethods.format3);
            Date last = common.parseDate(arr[1], CommonMethods.format3);
            List<UserInfo> users = repository.users(first, last);
            return toString(users);
        }
        return "";
    }

    public String toString(List list) {
        if (list == null) {
            return null;
        }
        String objects = "";
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);
            objects = objects + ((i > 0 && i != list.size()) ? "," : "") + obj.toString();
        }
        return "[" + objects + "]";

    }

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DateTimeService dateTimeService;

}
