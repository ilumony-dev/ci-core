/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Maninderjit
 */
public class POLiTransaction {

    /**
     * @return the CountryName
     */
    public String getCountryName() {
        return CountryName;
    }

    /**
     * @param CountryName the CountryName to set
     */
    public void setCountryName(String CountryName) {
        this.CountryName = CountryName;
    }

    /**
     * @return the FinancialInstitutionCountryCode
     */
    public String getFinancialInstitutionCountryCode() {
        return FinancialInstitutionCountryCode;
    }

    /**
     * @param FinancialInstitutionCountryCode the
     * FinancialInstitutionCountryCode to set
     */
    public void setFinancialInstitutionCountryCode(String FinancialInstitutionCountryCode) {
        this.FinancialInstitutionCountryCode = FinancialInstitutionCountryCode;
    }

    /**
     * @return the TransactionID
     */
    public String getTransactionID() {
        return TransactionID;
    }

    /**
     * @param TransactionID the TransactionID to set
     */
    public void setTransactionID(String TransactionID) {
        this.TransactionID = TransactionID;
    }

    /**
     * @return the MerchantEstablishedDateTime
     */
    public String getMerchantEstablishedDateTime() {
        return MerchantEstablishedDateTime;
    }

    /**
     * @param MerchantEstablishedDateTime the MerchantEstablishedDateTime to set
     */
    public void setMerchantEstablishedDateTime(String MerchantEstablishedDateTime) {
        this.MerchantEstablishedDateTime = MerchantEstablishedDateTime;
    }

    /**
     * @return the PayerAccountNumber
     */
    public String getPayerAccountNumber() {
        return PayerAccountNumber;
    }

    /**
     * @param PayerAccountNumber the PayerAccountNumber to set
     */
    public void setPayerAccountNumber(String PayerAccountNumber) {
        this.PayerAccountNumber = PayerAccountNumber;
    }

    /**
     * @return the PayerAccountSortCode
     */
    public String getPayerAccountSortCode() {
        return PayerAccountSortCode;
    }

    /**
     * @param PayerAccountSortCode the PayerAccountSortCode to set
     */
    public void setPayerAccountSortCode(String PayerAccountSortCode) {
        this.PayerAccountSortCode = PayerAccountSortCode;
    }

    /**
     * @return the MerchantAccountSortCode
     */
    public String getMerchantAccountSortCode() {
        return MerchantAccountSortCode;
    }

    /**
     * @param MerchantAccountSortCode the MerchantAccountSortCode to set
     */
    public void setMerchantAccountSortCode(String MerchantAccountSortCode) {
        this.MerchantAccountSortCode = MerchantAccountSortCode;
    }

    /**
     * @return the MerchantAccountName
     */
    public String getMerchantAccountName() {
        return MerchantAccountName;
    }

    /**
     * @param MerchantAccountName the MerchantAccountName to set
     */
    public void setMerchantAccountName(String MerchantAccountName) {
        this.MerchantAccountName = MerchantAccountName;
    }

    /**
     * @return the MerchantData
     */
    public String getMerchantData() {
        return MerchantData;
    }

    /**
     * @param MerchantData the MerchantData to set
     */
    public void setMerchantData(String MerchantData) {
        this.MerchantData = MerchantData;
    }

    /**
     * @return the CurrencyName
     */
    public String getCurrencyName() {
        return CurrencyName;
    }

    /**
     * @param CurrencyName the CurrencyName to set
     */
    public void setCurrencyName(String CurrencyName) {
        this.CurrencyName = CurrencyName;
    }

    /**
     * @return the TransactionStatus
     */
    public String getTransactionStatus() {
        return TransactionStatus;
    }

    /**
     * @param TransactionStatus the TransactionStatus to set
     */
    public void setTransactionStatus(String TransactionStatus) {
        this.TransactionStatus = TransactionStatus;
    }

    /**
     * @return the IsExpired
     */
    public Boolean getIsExpired() {
        return IsExpired;
    }

    /**
     * @param IsExpired the IsExpired to set
     */
    public void setIsExpired(Boolean IsExpired) {
        this.IsExpired = IsExpired;
    }

    /**
     * @return the MerchantEntityID
     */
    public String getMerchantEntityID() {
        return MerchantEntityID;
    }

    /**
     * @param MerchantEntityID the MerchantEntityID to set
     */
    public void setMerchantEntityID(String MerchantEntityID) {
        this.MerchantEntityID = MerchantEntityID;
    }

    /**
     * @return the UserIPAddress
     */
    public String getUserIPAddress() {
        return UserIPAddress;
    }

    /**
     * @param UserIPAddress the UserIPAddress to set
     */
    public void setUserIPAddress(String UserIPAddress) {
        this.UserIPAddress = UserIPAddress;
    }

    /**
     * @return the POLiVersionCode
     */
    public String getPOLiVersionCode() {
        return POLiVersionCode;
    }

    /**
     * @param POLiVersionCode the POLiVersionCode to set
     */
    public void setPOLiVersionCode(String POLiVersionCode) {
        this.POLiVersionCode = POLiVersionCode;
    }

    /**
     * @return the MerchantName
     */
    public String getMerchantName() {
        return MerchantName;
    }

    /**
     * @param MerchantName the MerchantName to set
     */
    public void setMerchantName(String MerchantName) {
        this.MerchantName = MerchantName;
    }

    /**
     * @return the TransactionRefNo
     */
    public String getTransactionRefNo() {
        return TransactionRefNo;
    }

    /**
     * @param TransactionRefNo the TransactionRefNo to set
     */
    public void setTransactionRefNo(String TransactionRefNo) {
        this.TransactionRefNo = TransactionRefNo;
    }

    /**
     * @return the CurrencyCode
     */
    public String getCurrencyCode() {
        return CurrencyCode;
    }

    /**
     * @param CurrencyCode the CurrencyCode to set
     */
    public void setCurrencyCode(String CurrencyCode) {
        this.CurrencyCode = CurrencyCode;
    }

    /**
     * @return the CountryCode
     */
    public String getCountryCode() {
        return CountryCode;
    }

    /**
     * @param CountryCode the CountryCode to set
     */
    public void setCountryCode(String CountryCode) {
        this.CountryCode = CountryCode;
    }

    /**
     * @return the PaymentAmount
     */
    public Double getPaymentAmount() {
        return PaymentAmount;
    }

    /**
     * @param PaymentAmount the PaymentAmount to set
     */
    public void setPaymentAmount(Double PaymentAmount) {
        this.PaymentAmount = PaymentAmount;
    }

    /**
     * @return the AmountPaid
     */
    public Double getAmountPaid() {
        return AmountPaid;
    }

    /**
     * @param AmountPaid the AmountPaid to set
     */
    public void setAmountPaid(Double AmountPaid) {
        this.AmountPaid = AmountPaid;
    }

    /**
     * @return the EstablishedDateTime
     */
    public String getEstablishedDateTime() {
        return EstablishedDateTime;
    }

    /**
     * @param EstablishedDateTime the EstablishedDateTime to set
     */
    public void setEstablishedDateTime(String EstablishedDateTime) {
        this.EstablishedDateTime = EstablishedDateTime;
    }

    /**
     * @return the StartDateTime
     */
    public String getStartDateTime() {
        return StartDateTime;
    }

    /**
     * @param StartDateTime the StartDateTime to set
     */
    public void setStartDateTime(String StartDateTime) {
        this.StartDateTime = StartDateTime;
    }

    /**
     * @return the EndDateTime
     */
    public String getEndDateTime() {
        return EndDateTime;
    }

    /**
     * @param EndDateTime the EndDateTime to set
     */
    public void setEndDateTime(String EndDateTime) {
        this.EndDateTime = EndDateTime;
    }

    /**
     * @return the BankReceipt
     */
    public String getBankReceipt() {
        return BankReceipt;
    }

    /**
     * @param BankReceipt the BankReceipt to set
     */
    public void setBankReceipt(String BankReceipt) {
        this.BankReceipt = BankReceipt;
    }

    /**
     * @return the BankReceiptDateTime
     */
    public String getBankReceiptDateTime() {
        return BankReceiptDateTime;
    }

    /**
     * @param BankReceiptDateTime the BankReceiptDateTime to set
     */
    public void setBankReceiptDateTime(String BankReceiptDateTime) {
        this.BankReceiptDateTime = BankReceiptDateTime;
    }

    /**
     * @return the TransactionStatusCode
     */
    public String getTransactionStatusCode() {
        return TransactionStatusCode;
    }

    /**
     * @param TransactionStatusCode the TransactionStatusCode to set
     */
    public void setTransactionStatusCode(String TransactionStatusCode) {
        this.TransactionStatusCode = TransactionStatusCode;
    }

    /**
     * @return the ErrorCode
     */
    public Integer getErrorCode() {
        return ErrorCode;
    }

    /**
     * @param ErrorCode the ErrorCode to set
     */
    public void setErrorCode(Integer ErrorCode) {
        this.ErrorCode = ErrorCode;
    }

    /**
     * @return the ErrorMessage
     */
    public String getErrorMessage() {
        return ErrorMessage;
    }

    /**
     * @param ErrorMessage the ErrorMessage to set
     */
    public void setErrorMessage(String ErrorMessage) {
        this.ErrorMessage = ErrorMessage;
    }

    /**
     * @return the FinancialInstitutionCode
     */
    public String getFinancialInstitutionCode() {
        return FinancialInstitutionCode;
    }

    /**
     * @param FinancialInstitutionCode the FinancialInstitutionCode to set
     */
    public void setFinancialInstitutionCode(String FinancialInstitutionCode) {
        this.FinancialInstitutionCode = FinancialInstitutionCode;
    }

    /**
     * @return the FinancialInstitutionName
     */
    public String getFinancialInstitutionName() {
        return FinancialInstitutionName;
    }

    /**
     * @param FinancialInstitutionName the FinancialInstitutionName to set
     */
    public void setFinancialInstitutionName(String FinancialInstitutionName) {
        this.FinancialInstitutionName = FinancialInstitutionName;
    }

    /**
     * @return the MerchantReference
     */
    public String getMerchantReference() {
        return MerchantReference;
    }

    /**
     * @param MerchantReference the MerchantReference to set
     */
    public void setMerchantReference(String MerchantReference) {
        this.MerchantReference = MerchantReference;
    }

    /**
     * @return the MerchantAccountSuffix
     */
    public String getMerchantAccountSuffix() {
        return MerchantAccountSuffix;
    }

    /**
     * @param MerchantAccountSuffix the MerchantAccountSuffix to set
     */
    public void setMerchantAccountSuffix(String MerchantAccountSuffix) {
        this.MerchantAccountSuffix = MerchantAccountSuffix;
    }

    /**
     * @return the MerchantAccountNumber
     */
    public String getMerchantAccountNumber() {
        return MerchantAccountNumber;
    }

    /**
     * @param MerchantAccountNumber the MerchantAccountNumber to set
     */
    public void setMerchantAccountNumber(String MerchantAccountNumber) {
        this.MerchantAccountNumber = MerchantAccountNumber;
    }

    /**
     * @return the PayerFirstName
     */
    public String getPayerFirstName() {
        return PayerFirstName;
    }

    /**
     * @param PayerFirstName the PayerFirstName to set
     */
    public void setPayerFirstName(String PayerFirstName) {
        this.PayerFirstName = PayerFirstName;
    }

    /**
     * @return the PayerFamilyName
     */
    public String getPayerFamilyName() {
        return PayerFamilyName;
    }

    /**
     * @param PayerFamilyName the PayerFamilyName to set
     */
    public void setPayerFamilyName(String PayerFamilyName) {
        this.PayerFamilyName = PayerFamilyName;
    }

    /**
     * @return the PayerAccountSuffix
     */
    public String getPayerAccountSuffix() {
        return PayerAccountSuffix;
    }

    /**
     * @param PayerAccountSuffix the PayerAccountSuffix to set
     */
    public void setPayerAccountSuffix(String PayerAccountSuffix) {
        this.PayerAccountSuffix = PayerAccountSuffix;
    }

    POLiTransaction(JSONObject object) throws JSONException {
        CountryName = (String) object.get("CountryName");
        FinancialInstitutionCountryCode = (String) object.get("FinancialInstitutionCountryCode");
        TransactionID = (String) object.get("TransactionID");
        MerchantEstablishedDateTime = (String) object.get("MerchantEstablishedDateTime");
        PayerAccountNumber = (String) object.get("PayerAccountNumber");
        PayerAccountSortCode = (String) object.get("PayerAccountSortCode");
        MerchantAccountSortCode = (String) object.get("MerchantAccountSortCode");
        MerchantAccountName = (String) object.get("MerchantAccountName");
        MerchantData = (String) object.get("MerchantData");
        CurrencyName = (String) object.get("CurrencyName");
        TransactionStatus = (String) object.get("TransactionStatus");
        IsExpired = (Boolean) object.get("IsExpired");
        MerchantEntityID = (String) object.get("MerchantEntityID");
        UserIPAddress = (String) object.get("UserIPAddress");
        POLiVersionCode = (String) object.get("POLiVersionCode");
        MerchantName = (String) object.get("MerchantName");
        TransactionRefNo = (String) object.get("TransactionRefNo");
        CurrencyCode = (String) object.get("CurrencyCode");
        CountryCode = (String) object.get("CountryCode");
        PaymentAmount = (Double) object.get("PaymentAmount");
        AmountPaid = (Double) object.get("AmountPaid");
        EstablishedDateTime = (String) object.get("EstablishedDateTime");
        StartDateTime = (String) object.get("StartDateTime");
        EndDateTime = (String) object.get("EndDateTime");
        BankReceipt = (String) object.get("BankReceipt");
        BankReceiptDateTime = (String) object.get("BankReceiptDateTime");
        TransactionStatusCode = (String) object.get("TransactionStatusCode");
        ErrorCode = object.isNull("ErrorCode") ? null : (Integer) object.get("ErrorCode");
        ErrorMessage = object.isNull("ErrorMessage") ? null : (String) object.get("ErrorMessage");
        FinancialInstitutionCode = (String) object.get("FinancialInstitutionCode");
        FinancialInstitutionName = (String) object.get("FinancialInstitutionName");
        MerchantReference = (String) object.get("MerchantReference");
        MerchantAccountSuffix = (String) object.get("MerchantAccountSuffix");
        MerchantAccountNumber = (String) object.get("MerchantAccountNumber");
        PayerFirstName = (String) object.get("PayerFirstName");
        PayerFamilyName = (String) object.get("PayerFamilyName");
        PayerAccountSuffix = (String) object.get("PayerAccountSuffix");
    }

    @Override
    public String toString() {
        return "{"
                + "\"CountryName\":" + checkNull(CountryName) + ", "
                + "\"FinancialInstitutionCountryCode\":" + checkNull(FinancialInstitutionCountryCode) + ", "
                + "\"TransactionID\":" + checkNull(TransactionID) + ", "
                + "\"MerchantEstablishedDateTime\":" + checkNull(MerchantEstablishedDateTime) + ", "
                + "\"PayerAccountNumber\":" + checkNull(PayerAccountNumber) + ", "
                + "\"PayerAccountSortCode\":" + checkNull(PayerAccountSortCode) + ", "
                + "\"MerchantAccountSortCode\":" + checkNull(MerchantAccountSortCode) + ", "
                + "\"MerchantAccountName\":" + checkNull(MerchantAccountName) + ", "
                + "\"MerchantData\":" + checkNull(MerchantData) + ", "
                + "\"CurrencyName\":" + checkNull(CurrencyName) + ", "
                + "\"TransactionStatus\":" + checkNull(TransactionStatus) + ", "
                + "\"IsExpired\":" + checkNull(IsExpired) + ", "
                + "\"MerchantEntityID\":" + checkNull(MerchantEntityID) + ", "
                + "\"UserIPAddress\":" + checkNull(UserIPAddress) + ", "
                + "\"POLiVersionCode\":" + checkNull(POLiVersionCode) + ", "
                + "\"MerchantName\":" + checkNull(MerchantName) + ", "
                + "\"TransactionRefNo\":" + checkNull(TransactionRefNo) + ", "
                + "\"CurrencyCode\":" + checkNull(CurrencyCode) + ", "
                + "\"CountryCode\":" + checkNull(CountryCode) + ", "
                + "\"PaymentAmount\":" + checkNull(PaymentAmount) + ", "
                + "\"AmountPaid\":" + checkNull(AmountPaid) + ", "
                + "\"EstablishedDateTime\":" + checkNull(EstablishedDateTime) + ", "
                + "\"StartDateTime\":" + checkNull(StartDateTime) + ", "
                + "\"EndDateTime\":" + checkNull(EndDateTime) + ", "
                + "\"BankReceipt\":" + checkNull(BankReceipt) + ", "
                + "\"BankReceiptDateTime\":" + checkNull(BankReceiptDateTime) + ", "
                + "\"TransactionStatusCode\":" + checkNull(TransactionStatusCode) + ", "
                + "\"ErrorCode\":" + checkNull(ErrorCode) + ", "
                + "\"ErrorMessage\":" + checkNull(ErrorMessage) + ", "
                + "\"FinancialInstitutionCode\":" + checkNull(FinancialInstitutionCode) + ", "
                + "\"FinancialInstitutionName\":" + checkNull(FinancialInstitutionName) + ", "
                + "\"MerchantReference\":" + checkNull(MerchantReference) + ", "
                + "\"MerchantAccountSuffix\":" + checkNull(MerchantAccountSuffix) + ", "
                + "\"MerchantAccountNumber\":" + checkNull(MerchantAccountNumber) + ", "
                + "\"PayerFirstName\":" + checkNull(PayerFirstName) + ", "
                + "\"PayerFamilyName\":" + checkNull(PayerFamilyName) + ", "
                + "\"PayerAccountSuffix\":" + checkNull(PayerAccountSuffix) + " "
                + "}";
    }
    private String CountryName;
    private String FinancialInstitutionCountryCode;
    private String TransactionID;
    private String MerchantEstablishedDateTime;
    private String PayerAccountNumber;
    private String PayerAccountSortCode;
    private String MerchantAccountSortCode;
    private String MerchantAccountName;
    private String MerchantData;
    private String CurrencyName;
    private String TransactionStatus;
    private Boolean IsExpired;
    private String MerchantEntityID;
    private String UserIPAddress;
    private String POLiVersionCode;
    private String MerchantName;
    private String TransactionRefNo;
    private String CurrencyCode;
    private String CountryCode;
    private Double PaymentAmount;
    private Double AmountPaid;
    private String EstablishedDateTime;
    private String StartDateTime;
    private String EndDateTime;
    private String BankReceipt;
    private String BankReceiptDateTime;
    private String TransactionStatusCode;
    private Integer ErrorCode;
    private String ErrorMessage;
    private String FinancialInstitutionCode;
    private String FinancialInstitutionName;
    private String MerchantReference;
    private String MerchantAccountSuffix;
    private String MerchantAccountNumber;
    private String PayerFirstName;
    private String PayerFamilyName;
    private String PayerAccountSuffix;

}
