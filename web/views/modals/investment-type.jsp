<%-- 
    Document   : investment-type
    Created on : Oct 15, 2017, 10:01:04 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="investment-type" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="investment-type-title">
                    I want to invest regularly
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <form action="./add-investment" method="post">
                    <input type="hidden" id="investment-type-fundId" name="fundId"/>
                    <div class="steps-w">
                        <div class="step-triggers">
                            <a class="step-trigger active" id="trigger1" href="#stepContent1"><i  class="os-icon os-icon-arrow-right"></i></a>
                            <a class="step-trigger" id="trigger2" href="#stepContent2"><i  class="os-icon os-icon-arrow-right"></i></a>
                        </div>
                        <div class="step-contents">
                            <div class="step-content active" id="stepContent1">
                                <div class="row">
                                    <div class="col-md-12" style="box-shadow: 0px 16px 10px -10px #011c53;margin-bottom: 20px;">
                                        <h6 class="element-header" style="color:#011c53">My Wallet Balance</h6>
                                        <small>Available balance in your wallet</small>
                                        <div class="col-md-12 wallet-bg"></div>
                                        <div class="row">
                                            <div class="col-md-12" style="border-bottom: 1px dotted #DEEAEE; margin-bottom: 10px;">
                                                <img src="./resources/img/wallet/wallet.png" style="width: 50px; height: 50px">
                                                <span class="value">$ 1</span>
                                                <span class="text">Your Wallet Balance</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="investmentAmount">How much money you want to invest?(Initial Amount)</label>
                                    <input class="form-control upFrontValue" id="investment-type-investmentAmount" name="investmentAmount" placeholder="Enter investment amount" type="number" step="0.01"/>
                                </div>
                                <div class="form-group">
                                    <label for="regularlyAmount">How much money you want to invest?(Regular Basis Amount)</label>
                                    <input class="form-control monthlyValue" id="investment-type-regularlyAmount" name="regularlyAmount" placeholder="Enter regularly amount" type="number" />
                                </div>
                                <div class="form-group">
                                    <label for="years">Please enter your years?</label>
                                    <input class="form-control years" id="investment-type-years" name="years" placeholder="Enter your years" type="number" />
                                </div>
                                <div class="form-group" id="regularly">
                                    <label for="">How you want to invest?</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="timeFrame" type="radio" value="FIXED" id="investment-type-fixed">
                                                    One Time
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="timeFrame" type="radio" value="MONTHLY" id="investment-type-monthly">
                                                    Monthly
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="timeFrame" type="radio" value="WEEKLY" id="investment-type-weekly">
                                                    Weekly
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-buttons-w text-right">
                                    <a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>
                                </div>
                            </div>
                            <div class="step-content" id="stepContent2">
                                <!--                                <input class="form-control upFrontValue" type="hidden" id="investment-type-investmentAmount" name="investmentAmount" placeholder="Enter investment amount" step="0.01"/>
                                                                <input id="investment-type-regularlyAmount" type="hidden" name="regularlyAmount" value="0"/>
                                                                <input id="investment-type-years" type="hidden"  name="years" value="10"/>
                                                                <input id="investment-type-fixed" type="hidden"  name="timeFrame" value="FIXED" >-->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="bankName"> Bank Name</label>
                                            <input class="form-control" id="investment-type-bankName" name="bankName" placeholder="Enter Bank Name" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="bankAccount"> Bank Account Number</label>
                                            <input class="form-control" id="investment-type-bankAccount" name="bankAccount" placeholder="Enter Bank Account No." type="number"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="referenceNo"> Reference Number</label>
                                            <input class="form-control" id="investment-type-referenceNo" name="referenceNo"  placeholder="Enter Reference No." type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="customerName">Customer Name</label>
                                            <input class="form-control" id="investment-type-customerName" name="customerName" placeholder="Enter Customer Name" type="text" value="${info.fullName}" readonly/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="customerNo"> Unique Customer Number</label>
                                            <input class="form-control" id="investment-type-customerNo" name="customerNo" placeholder="Enter Your Customer No" type="text" value="${info.refId}" readonly/>
                                        </div>            
                                    </div>
                                </div>
                                <div class="form-buttons-w text-right">
                                    <button class="btn btn-primary" type="submit">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>