/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var investments = new Array();
var invMap = new Map();
var perf = 0;
//setInterval(function () {
//    var url = './rest/cryptolabs/api/sdkjfhkdsjlfsder?uId=' + endUser.userId;
//    $.ajax({
//        url: url,
//        type: 'GET',
//        async: true,
//        dataType: "json",
//        success: function (data) {
//            serviceSetDataInCharts(data);
//        }
//    });
//}, 10000);

function initialInvestment(id, currentAmount, idx) {
    var idc = 'initialInvestment' + id;
    var gaugeOptions = {
        chart: {
            type: 'solidgauge',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingRight: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            animation: Highcharts.svg, // don't animate in old IE
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                                var y = series.data[0].y;
                                $(".total-" + idc).text(y.formatMoney(0, '.', ','));
                            }, 1000);
                }
            }
        },
        title: null,
        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -90,
            endAngle: 270,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '80%',
                outerRadius: '100%',
                shape: 'circle'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        // the value axis
        yAxis: {
            stops: [
                [0.1, '#95ceff'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            startOnTick: true,
            title: {
                y: -15
            },
            labels: {
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                innerRadius: '80%',
                dataLabels: {
                    y: -40,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
    var chartSpeed = Highcharts.chart(idc, Highcharts.merge(gaugeOptions, {
        yAxis: {
            showFirstLabel: false,
            showLastLabel: false,
            min: 0,
            max: currentAmount * 2,
            title: null
        },
        exporting: {enabled: false},
        series: [{
                index: idx,
                name: 'Investment Amount',
                data: [currentAmount],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:20px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '" class="total-' + idc + '">{y:.0f}</span><br/>' +
                            '<span style="font-size:10px;color:#011c53">Investment</span></div>'
                },
                tooltip: {
                    valueSuffix: ''
                }
            }]

    }));
    return currentAmount;
}
// The RPM gauge
function currentAmount(id, currentAmount, idx) {
    var idc = 'currentAmount' + id;
    var gaugeOptions = {
        chart: {
            type: 'solidgauge',
            margin: [0, 0, 0, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            animation: Highcharts.svg, // don't animate in old IE
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var index = idx;
                    setInterval(
                            function () {
                                var y = investments[index].y + Math.random();
                                if (y > 0 || y < 0) {
                                    series.data[0].y = y;
                                    var rnd = y.toFixed(2);
                                    $(".hdn-" + idc).val(rnd);
                                    $(".total-" + idc).text(y.formatMoney(2, '.', ','));
                                }
                            }
                    , 1000);
                }
            }
        },
        title: null,
        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -90,
            endAngle: 270,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '80%',
                outerRadius: '100%',
                shape: 'circle'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        // the value axis
        yAxis: {
            stops: [
                [0.1, '#95ceff'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            startOnTick: true,
            title: {
                y: -15
            },
            labels: {
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                innerRadius: '80%',
                dataLabels: {
                    y: -40,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
    var chart = Highcharts.chart(idc, Highcharts.merge(gaugeOptions, {
        yAxis: {
            showFirstLabel: false,
            showLastLabel: false,
            min: 0,
            max: parseFloat(currentAmount) * 2,
            title: null
        },
        exporting: {enabled: false},
        series: [{
                index: idx,
                name: 'Current Amount',
                data: [parseFloat(currentAmount)],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:20px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '" class="total-' + idc + '">{y:.2f}</span><br/>' +
                            '<span style="font-size:10px;color:#011c53">Current<br>Balance</span></div>'
                },
                tooltip: {
                    valueSuffix: ''
                }
            }]

    }));
    return currentAmount;
}

function performance(id, percentage, idx) {
// The percentage gauge
    var background = eval(percentage) > 0 ? '#DDDF0D' : '#DF5353';
    var idc = 'performance' + id;
    var gaugeOptions = {
        chart: {
            type: 'solidgauge',
            margin: [0, 0, 0, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            animation: Highcharts.svg
        },
        title: null,
        pane: {
            center: ['50%', '50%'],
            size: '100%',
            startAngle: -90,
            endAngle: 270,
            background: {
                backgroundColor: background,
                innerRadius: '80%',
                outerRadius: '100%',
                shape: 'circle'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        // the value axis
        yAxis: {
//            stops: [
//                [-0.1, '#DF5353'], // red
//                [0.5, '#DDDF0D'], // yellow
//                [0.9, '#011c53'] // blue
//            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            startOnTick: true,
            title: {
                y: -15
            },
            labels: {
                y: 16
            }
        },
        plotOptions: {
            solidgauge: {
                innerRadius: '80%',
                dataLabels: {
                    y: -40,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
    var chartPercentage = Highcharts.chart(idc, Highcharts.merge(gaugeOptions, {
        yAxis: {
            showFirstLabel: false,
            showLastLabel: false,
            min: 0,
            max: 100,
            labels: {step: 2},
            title: null
        },
        exporting: {enabled: false},
        series: [{
                name: 'Performance',
                data: [percentage],
//                background: '#95ceff',
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:20px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y:.2f}%</span><br/>' +
                            '<span style="font-size:10px;color:#011c53">Performance</span></div>'
                },
                tooltip: {
                    valueSuffix: ' '
                }
            }]
    }));
}

function centerText(chart, textId, number) { // on complete
    var legendId = '#' + textId;
    var spanId = 'span' + textId;
    var span = '<span id="' + spanId + '">';
    if (number > 0) {
        span += '<span class="trending trending-up" style="margin: 0;position: absolute;left: 50%;top: 40%;-webkit-transform: translate(-50%,-50%);">' + number.toFixed(2);
        span += '%<i class="os-icon os-icon-arrow-up2"></i></span>';
    } else {
        span += '<span class="trending trending-down" style="margin: 0;position: absolute;left: 50%;top: 40%;-webkit-transform: translate(-50%,-50%);">' + number.toFixed(2);
        span += '%<i class="os-icon os-icon-arrow-down"></i></span>';
    }
    span += '</span>';
    $(legendId).append(span);
    span = $('#' + spanId);
}

function investmentsActual(idc, legend, investments, perf) {
    var chart = new Highcharts.chart(idc, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            animation: Highcharts.svg, // don't animate in old IE
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    var data = series.data;
                    setInterval(
                            function () {
                                var actualAmount = 0;
                                for (var i = 0; i < data.length; i++) {
                                    var y = eval($('.' + data[i].hdnClass).val());
                                    if (y > 0 || y < 0) {
                                        data[i].y = y;
                                        actualAmount += y;
                                        invMap.set("id" + data[i].invId, y);
                                    }
                                }
                                if (actualAmount > 0 || actualAmount < 0) {
                                    $('#actualInvested').val(actualAmount);
                                    $(".currentBalance").text(currSymbol + actualAmount.formatMoney(2, '.', ','));
                                }
                            }
                    , 1000);
                }
            }
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {enabled: false},
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
//        legend: {
//            align: 'center',
//            layout: 'horizontal',
//            verticalAlign: 'bottom',
//            x: 0,
//            y: 0
//        },
//        colors: ['#FDEC6D', '#F15C80', '#8085E9', '#F7A35C', '#A9FF96', '#5C5C61', '#7CB5EC'],
        series: [{
                name: 'Investments',
                innerSize: '55%',
                colorByPoint: true,
                data: investments
            }]
    });
//    alert(perf);
    if (perf !== 'undefined') {
        centerText(chart, legend, perf);
    }
}

function getInvMap(id) {
    return invMap.get(id);
}

//function serviceSetDataInCharts(data) {
//    $.each(data, function (i, inv) {
//        investments[i].y = eval(inv.unitsAmount);
//        invMap.set("id" + inv.investmentId, inv.unitsAmount);
//    });
//    investmentsActual('cons-invs-pieChart', 'legend-1', investments);
//}

function initSetDataInCharts(data) {
    var actualAmount = 0;
    $.each(data, function (idx, inv) {
        var investmentAmount = eval(inv.investmentAmount);
        var val = initialInvestment(inv.investmentId, investmentAmount, idx);
        var curr_amount = eval(inv.unitsAmount);
        currentAmount(inv.investmentId, curr_amount, idx);
        var inc = curr_amount - val;
        var per = inc * 100 / val;
        performance(inv.investmentId, per, idx);
        perf = perf + per;
        investments.push({
            name: inv.fundName,
            y: curr_amount,
            hdnClass: 'hdn-currentAmount' + inv.investmentId,
            invId: inv.investmentId
        });
        invMap.set("id" + inv.investmentId, curr_amount);
        actualAmount += curr_amount;
    });
    perf = perf / data.length;
    investmentsActual('cons-invs-pieChart', 'legend-1', investments, perf);
    linegraph(actualAmount);
}

function linegraph(value, exist) {
    var values = [];
    var time = (new Date()).getTime();
    var i = 0;
    for (i = -29; i <= 0; i += 1) {
        values.push({
            x: time + i * 1000,
            y: null //y: value + Math.random()
        });
    }
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    Highcharts.chart('linegraph', {
        chart: {
            type: 'line',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(
                            function () {
                                var x = (new Date()).getTime();
                                var y = eval($("#actualInvested").val());
                                series.addPoint([x, y], true, true);
                            }
                    , 1000);
                }
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        plotOptions: {
            series: {
                color: '#ff0000'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                        '<b>Time: </b>' + Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        '<b>Value: </b>' + Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
                name: 'Live data',
                data: values
            }]
    });
}

function lglatestweek(values, idc) {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    Highcharts.chart(idc, {
        chart: {
            type: 'line',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    setInterval(
                            function () {
                                var y = eval($("#actualInvested").val());
                                this.series[0].data[2].y = y;
                            }
                    , 1000);
                }
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        plotOptions: {
            series: {
                color: '#FF0000'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
                name: 'Latest Week',
                data: values
            }]
    });
}

function lineChart(id, values, labels) {

    Highcharts.chart(id, {
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        exporting: {enabled: false},
        credits: {
            enabled: false
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        xAxis: {
            categories: labels
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                }
                //                            ,pointStart: yearsArr[0]
            }
        },
        series: [{
                name: 'Minto Value',
                data: values
            }],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
        }
    });
}

function linegraphlive(value, exist) {
    var values = [];
    var time = (new Date()).getTime();
    var i = 0;
    for (i = -29; i <= 0; i += 1) {
        values.push({
            x: time + i * 1000,
            y: value + Math.random()
        });
    }
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    Highcharts.chart('linegraph-live', {
        chart: {
            type: 'line',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(
                            function () {
                                var x = (new Date()).getTime();
                                var y = eval($("#actualInvested").val());
                                series.addPoint([x, y], true, true);
                            }
                    , 1000);
                }
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
                name: 'Live data',
                data: values
            }]
    });
}

function firstdayChart(db) {
    var shares = new Array();
    $.each(db, function (i, share) {
        shares.push({name: share.shareName.toUpperCase(),
            y: eval(share.targetPercentage)});
    });
    var idc = 'firstday-chart';
    sharesChart(shares, idc);
}

function todayChart(db) {
    var shares = new Array();
    $.each(db, function (i, share) {
        shares.push({name: share.shareName.toUpperCase(),
            y: eval(share.shareAmount)});
    });
    var idc = 'today-chart';
    sharesChart(shares, idc);
}

function centerTextOnPieChart(chart, textId, text) { // don't call it. Call centerText instead
    var legendId = '#' + textId;
    var spanId = 'span' + textId;
    var textX = chart.plotLeft + (chart.plotWidth * 0.5);
    var textY = chart.plotTop + (chart.plotHeight * 0.5);
    var span = '<span id="' + spanId + '" style="position:absolute; text-align:center;">';
    span += '<span style="font-size: 14px">' + text.toFixed(2) + '%</span>';
    span += '</span>';
    $(legendId).append(span);
    span = $('#' + spanId);
    span.css('left', textX + (span.width() * -0.5));
    span.css('top', textY + (span.height() * -0.5));
}

function hr30dChart(amt, max) {//hr247Chart;
    var chart = new Highcharts.chart('hr1Chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: '115px',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        tooltip: {
//            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
            pointFormat: '<b>' + amt + '%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>' + amt + '%</b>',
//                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: false
            }
        },
        series: [{
                name: 'Particulars',
                innerSize: '75%',
                colorByPoint: true,
                data: [{name: 'Percentange', y: eval(100)}
//                    , {name: 'Other', y: eval(max - amt)}
                ]
            }]
    });
    centerText(chart, 'legend-2', amt);

}

function hr24hChart(amt, max) {
    var chart = new Highcharts.chart('hr24Chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: '115px',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
//            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
            pointFormat: '<b>' + amt + '%</b>'
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>' + amt + '%</b>',
//                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: false
            }
        },
        colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
        series: [{
                name: 'Particulars',
                innerSize: '75%',
                colorByPoint: true,
                data: [{name: 'Percentage', y: eval(100)}
//                    , {name: 'Other', y: eval(max - amt)}
                ]
            }]
    });
    centerText(chart, 'legend-3', amt);
}

function hr7dChart(amt, max) {
    var chart = new Highcharts.chart('hr247Chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            height: '115px',
            margin: [0, 0, 0, 0],
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        tooltip: {
//            pointFormat: '{series.name}: <b>{point.y:.2f}</b>'
            pointFormat: '<b>' + amt + '%</b>'
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    legend: 'Particulars',
                    enabled: false,
                    format: '<b>' + amt + '%</b>',
//                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: false
            }
        },
        series: [{
                name: 'Particulars',
                innerSize: '75%',
                colorByPoint: true,
                data: [{name: 'Percentage', y: eval(100)}
//                    , {name: 'Other', y: eval(max - amt)}
                ]
            }]
    });
    centerText(chart, 'legend-4', amt);
}

function sharesChart(shares, idc) {
// donut chart data
    Highcharts.chart(idc, {
        chart: {
//            margin: [0, 0, 0, 0],
//            borderWidth: null,
//            plotBorderWidth: null,
            spacingTop: 0,
            spacingBottom: 0,
            spacingLeft: 0,
            spacingRight: 0,
            plotShadow: false,
            type: 'pie'
        },
        legend: {
            align: 'left',
            layout: 'horizontal',
            verticalAlign: 'bottom',
            x: 0,
            y: 0
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.2f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.y:.2f}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
                x: 0,
                y: 0,
                name: 'Shares/Coins',
                innerSize: '75%',
                colorByPoint: true,
                data: shares
            }]
    });
}

// format the number with comma seprated and round up of last value
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// Bring life to the dials
setInterval(function () {
// Speed
    var point,
            newVal,
            inc;
    if (chartSpeed) {
        point = chartSpeed.series[0].points[0];
        inc = Math.round((Math.random() - 0.5) * 100);
        newVal = point.y + inc;
        if (newVal < 0 || newVal > 200) {
            newVal = point.y - inc;
        }

        point.update(newVal);
    }

// RPM
    if (chartRpm) {
        point = chartRpm.series[0].points[0];
        inc = Math.random() - 0.5;
        newVal = point.y + inc;
        if (newVal < 0 || newVal > 5) {
            newVal = point.y - inc;
        }

        point.update(newVal);
    }
}, 2000);
