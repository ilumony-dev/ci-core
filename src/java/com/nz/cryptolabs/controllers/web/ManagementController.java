/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.services.SessionManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import com.nz.cryptolabs.constants.ResponseConstants;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.DateTimeService;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Administrator
 */
@Controller
public class ManagementController implements Constants {

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private String projectName;

    @RequestMapping(value = {"/mgmt-show-portfolios"}, method = RequestMethod.GET)
    public String showBuyingPortfolios(ModelMap modelMap) throws IOException {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            modelMap.addAttribute("title", "Management Buying Portfolio " + projectName);
            Date date = dateTimeService.now();
            modelMap.addAttribute("today", common.dateFormat(date, "dd/MM/yyyy"));
            modelMap.addAttribute("portfolios", repository.funds());
            List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
            List<ShareFund> shares = repository.shares();
            for (ShareFund share : shares) {
                CryptoCoin coin = new CryptoCoin();
                coin.setName(share.getName());
                coin.setUniqueId(share.getShareId());
                coinList.add(coin);
            }
            modelMap.addAttribute("coins", coinList);
            modelMap.addAttribute("totalUsers", repository.users());
            return "mgmt-show-portfolios";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/mgmt-show-buying-portfolio"}, method = RequestMethod.GET)
    public String showBuyingPortfolio(ModelMap modelMap,
            @RequestParam(name = "id", required = false) String portfolioId,
            @RequestParam(name = "daterange", required = false) String daterange) throws IOException {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            modelMap.addAttribute("title", "Management Buying Portfolio " + projectName);
            ShareFund portfolio = repository.fundById(portfolioId, null);
            Date firstDate = null;
            Date lastDate = null;
            String[] arr = null;
            if (daterange != null) {
                arr = daterange.split(" - ");
            }
            if (arr != null && arr.length == 2) {
                firstDate = common.parseDate(arr[0], CommonMethods.format3);
                lastDate = common.parseDate(arr[1], CommonMethods.format3);
            } else {
                lastDate = dateTimeService.now();
                Calendar cal = Calendar.getInstance();
                cal.setTime(lastDate);
                cal.add(Calendar.DAY_OF_MONTH, -7);
                firstDate = cal.getTime();
            }
            daterange = common.dateFormat(firstDate, CommonMethods.format3) + " - " + common.dateFormat(lastDate, CommonMethods.format3);
            modelMap.addAttribute("daterange", daterange);
            List<TransactionBean> portfolioBuying = repository.getPortfolioBuying(portfolioId, firstDate, lastDate);
            HashMap<String, List<TransactionBean>> map = new HashMap<>();
            for (TransactionBean tran : portfolioBuying) {
                List<TransactionBean> details = map.get(tran.getId());
                if (details == null) {
                    details = new LinkedList<>();
                }
                details.add(tran);
                map.put(tran.getId(), details);
            }
            List<TransactionBean> report = new LinkedList<>();
            for (Map.Entry<String, List<TransactionBean>> entry : map.entrySet()) {
                List<TransactionBean> list = entry.getValue();
                TransactionBean tran = list.get(0);
                tran.setDetails(list);
                report.add(tran);
            }
            modelMap.addAttribute("report", report);
            modelMap.addAttribute("portfolios", repository.funds());
            modelMap.addAttribute("name", portfolio.getName());
            modelMap.addAttribute("id", portfolio.getFundId());
            return "mgmt-show-buying-portfolio";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/mgmt-buying-portfolio-add"}, method = RequestMethod.GET)
    public String addBuyingPortfolio(ModelMap modelMap,
            @RequestParam(name = "id", required = false) String portfolioId,
            @RequestParam(name = "ok", required = false) String recordId) throws IOException {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            modelMap.addAttribute("title", "Management Buying Portfolio " + projectName);
            Date date = dateTimeService.now();
            ShareFund portfolio = repository.fundById(portfolioId, null);
            modelMap.addAttribute("portfolio", portfolio);
            List<TransactionBean> managementSharesBuyingPortfolio = repository.managementSharesBuyingPortfolio(portfolioId, recordId);
            modelMap.addAttribute("managementSharesByPortfolio", managementSharesBuyingPortfolio);
            if (managementSharesBuyingPortfolio != null && !managementSharesBuyingPortfolio.isEmpty()) {
                modelMap.addAttribute("buyingPortfolio", managementSharesBuyingPortfolio.get(0));
            }
            modelMap.addAttribute("today", common.dateFormat(date, "dd/MM/yyyy"));
            List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
            List<ShareFund> shares = repository.shares();
            for (ShareFund share : shares) {
                CryptoCoin coin = new CryptoCoin();
                coin.setName(share.getName());
                coin.setUniqueId(share.getShareId());
                coinList.add(coin);
            }
            modelMap.addAttribute("coins", coinList);
            modelMap.addAttribute("totalUsers", repository.users());
            return "mgmt-buying-portfolio-add";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/mgmt-show-buying-portfolio-pending"}, method = RequestMethod.GET)
    public String showBuyingPortfolioPending(ModelMap modelMap,
            @RequestParam(name = "id", required = false) String portfolioId) throws IOException {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            modelMap.addAttribute("title", "Management Buying Portfolio " + projectName);
            ShareFund portfolio = repository.fundById(portfolioId, null);
            List<TransactionBean> report = repository.getPortfolioBuyingPending(portfolioId);
            modelMap.addAttribute("report", report);
            modelMap.addAttribute("portfolios", repository.funds());
            modelMap.addAttribute("portfolio", portfolio);
            return "mgmt-show-buying-portfolio-pending";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/mgmt-show-buying-portfolio-history"}, method = RequestMethod.GET)
    public String showBuyingPortfolioHistory(ModelMap modelMap,
            @RequestParam(name = "id", required = false) String portfolioId,
            @RequestParam(name = "ok", required = false) String txnReqId
    ) throws IOException {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            modelMap.addAttribute("title", "Management Buying Portfolio " + projectName);
            ShareFund portfolio = repository.fundById(portfolioId, null);
            String oldColorCode = null;
            String oldId = null;
            int idx = 0, index = 0;
            boolean last = false;
            List<TransactionBean> report = repository.getPortfolioBuyingHistory(portfolioId, txnReqId);
            for (TransactionBean tran : report) {
                if (oldColorCode == null || !tran.getId().equalsIgnoreCase(oldId)) {
                    idx++;
                    if (last) {
                        index = idx;
                        last = false;
                    } else {
                        index = ResponseConstants.colorCodes.length - idx;
                        last = true;
                    }
                    oldColorCode = ResponseConstants.colorCodes[index];
                    oldId = tran.getId();
                }
                tran.setBrand(oldColorCode);
            }
            modelMap.addAttribute("report", report);
            modelMap.addAttribute("portfolio", portfolio);
            return "mgmt-show-buying-portfolio-history";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/mgmt-add-portfolio-buying"}, method = RequestMethod.POST)
    public String addPortfolioBuying(ModelMap modelMap,
            @ModelAttribute TransactionBean transactionBean
    ) {
        SecuredUser management = sessionManagementService.getCurrentUser(managementRole);
        if (management != null) {
            Date date = dateTimeService.now();
            transactionBean.setCreated_ts(common.dateFormat(date));
            repository.addPortfolioBuying(transactionBean);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

}
