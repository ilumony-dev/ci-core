/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.payments;

import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.DateTimeService;
import com.nz.cryptolabs.services.SessionManagementService;
import com.stripe.exception.StripeException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author palo12
 */
@Controller
public class ChargeController {

    @RequestMapping(value = {"/user-stripe-charge-add-investment"}, method = {RequestMethod.POST})
    public String stripeChargeAddInvestment(ChargeRequest chargeRequest, Model model)
            throws StripeException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            chargeRequest.setDescription("This is a chargable request");
            String currency = common.userCurrency().get(user.getUserId()) != null ? common.userCurrency().get(user.getUserId()) : "USD";
            chargeRequest.setCurrency(common.payCurrency().get(currency));
            Map<String, Object> map = databaseService.saveStripeCharge(chargeRequest, user.getUserId());
            TransactionBean walletTxn = (TransactionBean) map.get("walletTxn");
            InvestmentBean invBean = new InvestmentBean();
            invBean.setFundName(user.getFullName());
            invBean.setRefId(user.getRefId());
            invBean.setFundId(chargeRequest.getFundId());
            invBean.setUserId(user.getUserId());
            invBean.setRegularlyAmount("0");
            invBean.setYears("10");
            invBean.setTimeFrame("FIXED");
            invBean.setInvestmentAmount(walletTxn.getAmount());
            invBean.setLocalInvestmentAmount(walletTxn.getLocal_amount());
            invBean.setCreatedDate(walletTxn.getCreated_ts());
            List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
            invBean.setFundActualShares(fundActualShares);
            repository.saveInvestment(invBean);
            return "redirect:/welcome";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/user-stripe-charge"}, method = {RequestMethod.POST})
    public String stripeCharge(ChargeRequest chargeRequest, Model model)
            throws StripeException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            chargeRequest.setDescription("This is a chargable request");
            chargeRequest.setCurrency(ChargeRequest.Currency.USD);
            Map<String, Object> map = databaseService.saveStripeCharge(chargeRequest, user.getUserId());
            return "redirect:/welcome";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/user-poli-charge-success"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String poliChargeSuccess(ModelMap model,
            @RequestParam(value = "token", required = false) String token,
            @RequestParam(value = "ref", required = false) String refNo)
            throws StripeException, IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            UserReference ref = repository.findReferenceNo(token, refNo);
            if (ref != null && "N".equalsIgnoreCase(ref.getAmounted())) {
                StripeCharge stripeCharge = new StripeCharge();
                stripeCharge.setTxn_id(ref.getReferenceNo());
                stripeCharge.setBal_txn_id(ref.getPoliToken());
                stripeCharge.setAmount(ref.getPrice());
                stripeCharge.setAmount_refunded("0");
                stripeCharge.setCurrency("nzd");
                stripeCharge.setStatus("succeeded");
                stripeCharge.setDescription("This is a chargable request");
                Date now = dateTimeService.now();
                stripeCharge.setStripe_created_ts(common.dateFormat(now));
                stripeCharge.setCreated_ts(common.dateFormat(now));
                stripeCharge.setUser_id(ref.getUserId());
                stripeCharge.setFund_id(ref.getFundId());
                int id = repository.saveStripeCharge(stripeCharge);
                stripeCharge.setId(String.valueOf(id));
                TransactionBean walletTxn = new TransactionBean(stripeCharge);
                walletTxn.setCreated_ts(common.dateFormat(now));
                walletTxn.setAmount(stripeCharge.getAmount());
                walletTxn.setActive("Y");
                walletTxn.setInc_dec("Inc");
                walletTxn.setBrand("POLI");
                int tid = repository.saveWalletTransaction(walletTxn);
                walletTxn.setId(String.valueOf(tid));
                repository.updatedAmounted(ref.getReferenceNo());
                model.addAttribute("brand", "POLI");
                return "charge-succeed";
            }
            return "redirect:/welcome";
        }
        return "redirect:/login";
    }

    void saveInvestment(Map<String, Object> map, SecuredUser user) {
        TransactionBean walletTxn = (TransactionBean) map.get("walletTxn");
        StripeCharge stripeCharge = (StripeCharge) map.get("stripeCharge");
        InvestmentBean invBean = new InvestmentBean();
        invBean.setFundName(user.getFullName());
        invBean.setRefId(user.getRefId());
        invBean.setFundId(stripeCharge.getFund_id());
        invBean.setUserId(user.getUserId());
        invBean.setRegularlyAmount("0");
        invBean.setYears("10");
        invBean.setTimeFrame("FIXED");
        invBean.setInvestmentAmount(walletTxn.getAmount());
        invBean.setLocalInvestmentAmount(walletTxn.getAmount());
        invBean.setCreatedDate(walletTxn.getCreated_ts());
        List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
        invBean.setFundActualShares(fundActualShares);
        repository.saveInvestment(invBean);
    }

    @RequestMapping(value = {"/user-poli-charge-failure"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String poliChargeFailure(ModelMap model,
            @RequestParam(value = "token", required = false) String token)
            throws StripeException, IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            POLiTransaction transaction = pOLiManager.getTransaction(token);
            model.addAttribute("transaction", transaction);
            return "emptyfile";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/user-poli-charge-cancelled"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String poliChargeCancelled(ModelMap model,
            @RequestParam(value = "token", required = false) String token)
            throws StripeException, IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            POLiTransaction transaction = pOLiManager.getTransaction(token);
            model.addAttribute("transaction", transaction);
            return "emptyfile";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = {"/user-poli-charge-nudge"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String poliChargeNudge(ModelMap model,
            @RequestParam(value = "token", required = false) String token)
            throws StripeException, IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            POLiTransaction transaction = pOLiManager.getTransaction(token);
            model.addAttribute("transaction", transaction);
            return "emptyfile";
        }
        return "redirect:/login";
    }

    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        model.addAttribute("error", ex.getMessage());
        return "stripe/result";
    }
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private POLiManager pOLiManager;
    @Autowired
    private CommonMethods common;
}
