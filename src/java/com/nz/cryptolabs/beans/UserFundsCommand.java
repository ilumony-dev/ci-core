/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.constants.Constants;
import java.lang.reflect.Field;
import java.util.logging.Level;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Shivi
 */
public class UserFundsCommand implements ToObjectConverter {

    /**
     * @return the fund_name
     */
    public String getFund_name() {
        return fund_name;
    }

    /**
     * @param fund_name the fund_name to set
     */
    public void setFund_name(String fund_name) {
        this.fund_name = fund_name;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the customer_name
     */
    public String getCustomer_name() {
        return customer_name;
    }

    /**
     * @param customer_name the customer_name to set
     */
    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    /**
     * @return the investment_amount
     */
    public String getInvestment_amount() {
        return investment_amount;
    }

    /**
     * @param investment_amount the investment_amount to set
     */
    public void setInvestment_amount(String investment_amount) {
        this.investment_amount = investment_amount;
    }

    /**
     * @return the investment_id
     */
    public String getInvestment_id() {
        return investment_id;
    }

    /**
     * @param investment_id the investment_id to set
     */
    public void setInvestment_id(String investment_id) {
        this.investment_id = investment_id;
    }

    /**
     * @return the bank_acc
     */
    public String getBank_acc() {
        return bank_acc;
    }

    /**
     * @param bank_acc the bank_acc to set
     */
    public void setBank_acc(String bank_acc) {
        this.bank_acc = bank_acc;
    }

    /**
     * @return the reference_no
     */
    public String getReference_no() {
        return reference_no;
    }

    /**
     * @param reference_no the reference_no to set
     */
    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    /**
     * @return the fund_id
     */
    public String getFund_id() {
        return fund_id;
    }

    /**
     * @param fund_id the fund_id to set
     */
    public void setFund_id(String fund_id) {
        this.fund_id = fund_id;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the time_frame
     */
    public String getTime_frame() {
        return time_frame;
    }

    /**
     * @param time_frame the time_frame to set
     */
    public void setTime_frame(String time_frame) {
        this.time_frame = time_frame;
    }

    /**
     * @return the current_date
     */
    public String getCurrent_date() {
        return current_date;
    }

    /**
     * @param current_date the current_date to set
     */
    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }

    private String id;
    private String user_id;
    private String name;
    private String customer_name;
    private String investment_amount;
    private String investment_id;
    private String bank_acc;
    private String reference_no;
    private String fund_id;
    private String fund_name;
    private String active;
    private String created_ts;
    private String current_date;
    private String time_frame;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserFundsCommand.class);

    @Override
    public String toString() {
        return toString(this);
    }

    @Override
    public String toJSONObject() {
        return toJSONObject(this);
    }

    public String toString(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append("\"").append(fieldName).append("\"");
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(",").append(Constants.CODE_NEW_LINE);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }

    public String toJSONObject(Object THIS) {
        Field[] declaredFields = THIS.getClass().getDeclaredFields();
        StringBuffer buffer = new StringBuffer();
        buffer.append("{");
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            try {
                String fieldName = field.getName();
                Object fieldValue = field.get(THIS);
                buffer.append(fieldName);
                buffer.append(":");
                buffer.append(checkNull(fieldValue));
                if (i != declaredFields.length - 1) {
                    buffer.append(", ");
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                logger.error("", ex);
            }
        }
        buffer.append("}");
        return buffer.toString();
    }
}
