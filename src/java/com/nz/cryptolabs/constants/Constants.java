/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.constants;

import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.SystemConfig;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.ui.ModelMap;

/**
 *
 * @author Administrator
 */
public interface Constants {

    String depositParticulars = "Deposit-(Purchased Investments)";
    String withdrawlParticulars = "Withdrawal-(Sold Investments)";

    String kickboxApiKey = "live_48e510a673dd08002c45f039145c3e895e2598122ac132de1217424b284a1bc6"; //fill in
    String kickboxAppCode = "AM-gCN-viQrBx3YcqFGv";

    String bittrexApiKey = "e191dbf4e17b475ebaead51e1cdb7b2f";
    String bittrexSecret = "94c41d791d6947dcac98904a41b0b600";
    String poliniexApiKey = "";
    String poliniexSecret = "";
    String cipherKey = "AM-gCN-viQrBx3YcqFGv";
    BigDecimal HUNDRED = new BigDecimal(100);
    Integer PERCENTANGE_DECIMAL_PLACES = new Integer(8);
    Integer MAXIMUM_DECIMAL_PLACES = new Integer(16);
    Integer DISPLAY_DECIMAL_PLACES = new Integer(2);
    String ENV_VAL = "DEV";
    String ENV_DEV = "DEV";
    String ENV_PROD = "PROD";
    String CODE_NEW_LINE = " ";
    GrantedAuthority superAdminRole = new SimpleGrantedAuthority("ROLE_SUPER_ADMIN");
    GrantedAuthority adminRole = new SimpleGrantedAuthority("ROLE_ADMIN");
    GrantedAuthority userRole = new SimpleGrantedAuthority("ROLE_USER");
    GrantedAuthority managementRole = new SimpleGrantedAuthority("ROLE_MANAGEMENT");

    HashMap<String, CryptoCoin> coinPriceMap = new HashMap<>();
    LinkedHashMap<String, ModelMap> userModelMap = new LinkedHashMap<>();
    LinkedHashMap<String, ModelMap> adminModelMap = new LinkedHashMap<>();
//    backgroundImage, logo, textColor, primaryButtonColor, menuColor, profileColor, backgroundColor;
    SystemConfig systemConfig = new SystemConfig("url(\"../img/invstabanner.jpg\")", "url(\"../img/invsta-03(200px).png\")", "#000000", "#383d4d", "#ffffff", "#0061da", "#ffffff");
    public static void CoinPriceMapClear() {
        coinPriceMap.clear();
    }

    public static GrantedAuthority superAdminRole() {
        return superAdminRole;
    }

    public static GrantedAuthority adminRole() {
        return adminRole;
    }

    public static void userModelMapEntry(String key, ModelMap modelMap) {
        modelMap.addAttribute("key", key);
        userModelMap.put(key, modelMap);
    }

    public static ModelMap userModelMap(String key) {
        return userModelMap.get(key);
    }

    public static HashMap<String, Object> userMap(String key) {
        ModelMap modelMap = userModelMap.get(key);
        if (modelMap != null && !modelMap.isEmpty()) {
            HashMap<String, Object> userMap = new HashMap<>();
            Set<Map.Entry<String, Object>> entrySet = modelMap.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Object value = entry.getValue();
                if (value != null && !org.springframework.validation.BeanPropertyBindingResult.class.isInstance(value)) {
                    userMap.put(entry.getKey(), value);
                }
            }
            return userMap;
        }
        return null;
    }

    public static void adminModelMapEntry(String key, ModelMap modelMap) {
        adminModelMap.put(key, modelMap);
    }

    public static ModelMap adminModelMap(String key) {
        return adminModelMap.get(key);
    }

    public static HashMap<String, Object> adminMap(String key) {
        ModelMap modelMap = adminModelMap.get(key);
        if (modelMap != null && !modelMap.isEmpty()) {
            HashMap<String, Object> adminMap = new HashMap<>();
            Set<Map.Entry<String, Object>> entrySet = modelMap.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Object value = entry.getValue();
                if (value != null && !org.springframework.validation.BeanPropertyBindingResult.class.isInstance(value)) {
                    adminMap.put(entry.getKey(), value);
                }
            }
            return adminMap;
        }
        return null;
    }

}
