/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import java.util.Date;

/**
 *
 * @author palo12
 */
public class TimeRangeBean {

    /**
     * @return the uniqueId
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * @param uniqueId the uniqueId to set
     */
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    private String uniqueId;
    private Date date;

    public TimeRangeBean(String uniqueId, Date date) {
        this.uniqueId = uniqueId;
        this.date = date;
    }

    @Override
    public String toString() {
        return "{"
                + "\"uniqueId\":" + checkNull(uniqueId) + ",\n"
                + "\"date\":" + checkNull(date) + "\n"
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        TimeRangeBean timeRangeBean = (TimeRangeBean) o;
        return timeRangeBean.uniqueId.equals(this.uniqueId)
                && timeRangeBean.date.equals(this.date);
    }

    @Override
    public int hashCode() {
        return uniqueId.hashCode() + date.hashCode();
    }

}
