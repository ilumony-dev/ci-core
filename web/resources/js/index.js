 

/*-----------chacked botton----------*/

$("#goal-slot-1").click(function () {
    $("#other").hide();

});
$("#goal-slot-2").click(function () {
    $("#other").hide();

});

$("#goal-slot-3").click(function () {
    $("#other").hide();

});

$("#goal-slot-4").click(function () {
    $("#other").hide();

});


$("#goal-slot-5").click(function () {
    $("#other").show();

});

$("#show-me").click(function () {
    $("#showme").show();

});
$("#radio07").click(function () {
    $("#showme").hide();

});

$("#yes-button").click(function () {
    $("#year1").show();

});
$("#no-button").click(function () {

    $("#year1").hide();
});
$("#kiwisaver").hide();
$("#radio03").click(function () {

    $("#kiwisaver").hide();
});
$("#radio04").click(function () {
    $("#kiwisaver").show();

});



$('#my_image').on({
    'click': function () {
        $('#my_image1').show();
        $('#my_image').hide()
    }
});

$('#my_image1').on({
    'click': function () {
        $('#my_image1').hide()
        $('#my_image').show()
    }
});


function hideGnP() {
    document.getElementById("goalsDiv").style.display = 'none';
    document.getElementById("portofoliosDiv").style.display = 'none';
}

function checkedAdvisedFund() {
//    $("#3").prop("checked", true);
    var pid = "3";
    document.getElementById(pid).click();
    for (var i = 1; i < 6; i++) {
        document.getElementById("radiobottom" + i).style.display = 'none';
    }
    document.getElementById("radiobottom"+pid).style.display = 'block';
    changePlan();
}

function checkedShareTheme() {
    $("#6").prop("checked", true);
    changePlan();
}

function showGnP() {
    document.getElementById("goalsDiv").style.display = 'block';
    document.getElementById("portofoliosDiv").style.display = 'block';
}

//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches



$(".next").click(function () {
    
    if ($("#msform").valid()) {
        MoveNext(this);
    }
});


function MoveNext(nextVal)
{
    if (animating){
        return false;
    }
    
    animating = true;
    current_fs = $(nextVal).parent();
    next_fs = $(nextVal).parent().next();
    
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, max) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50) + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale(' + scale + ')',
                'position': 'absolute'
            });
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 1500,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

$(".previous").click(function () {
    if (animating)
        return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    if (current_fs.attr("id") === "step5b") {
        var id = $('.fund:checked').val();
        if (id === "advisedFund") {
//                 4  -> skip step5a -> step5b;
            previous_fs = previous_fs.prev();
            showGnP();
        } else if (id === "shareTheme") {
//                  4  -> step5a -> step5b
            previous_fs = current_fs.prev();
            hideGnP();
        }
    }
    if (current_fs.attr("id") === "step11") {
        var id = $('.getway:checked').val();
        if (id === "DEBIT_CARD") {
//                 4  -> skip step5a -> step5b;
            previous_fs = current_fs.prev();
        } else if (id === "NET_BANKING") {
//                  4  -> step5a -> step5b
            previous_fs = previous_fs.prev();
        }
        console.log("Hi");
    }

    if (current_fs.attr("id") === "step10") {
        var id = $('.getway:checked').val();
        if (id === "DEBIT_CARD") {
//                 4  -> skip step5a -> step5b;
            previous_fs = previous_fs.prev();
        } else if (id === "NET_BANKING") {
//                  4  -> step5a -> step5b
            previous_fs = current_fs.prev();
        }
    }

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1 - now) * 50) + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
        },
        duration: 1500,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});



var LoginModalController = {
    hidePasswordName: ".hide-password",
    hidePassword: null,
    findElements: function () {
        var base = this;

        base.tabsElement = $(base.tabsElementName);
        base.tabElement = $(base.tabElementName);
        base.inputElements = $(base.inputElementsName);
        base.hidePassword = $(base.hidePasswordName);

        return base;
    },
    setState: function (state) {
        var base = this,
                elem = null;

        if (!state) {
            state = 0;
        }

        if (base.tabsElement) {
            elem = $(base.tabsElement[state]);
            elem.addClass("current");
            $("." + elem.attr("data-tabtar")).addClass("show");
        }

        return base;
    },
    getActiveTab: function () {
        var base = this;

        base.tabsElement.each(function (i, el) {
            if ($(el).hasClass("current")) {
                base.activeTab = $(el);
            }
        });

        return base;
    },
    addClickEvents: function () {
        var base = this;

        base.hidePassword.on("click", function (e) {
            var $this = $(this),
                    $pwInput = $this.prev("input");

            if ($pwInput.attr("type") == "password") {
                $pwInput.attr("type", "text");


            } else {
                $pwInput.attr("type", "password");

            }
        });



        base.inputElements.find("label").on("click", function (e) {
            var $this = $(this),
                    $input = $this.next("input");

            $input.focus();
        });

        return base;
    },
    initialize: function () {
        var base = this;

        base.findElements().setState().getActiveTab().addClickEvents();
    }
};
 
function ValidateForm()
{
   
    jQuery.validator.addMethod("onespecial", function (value, element) {
        var pattern = /^(?=.*[0-9])|(?=.*[!@#$%^&*()-+=]).*$/;
        return (pattern.test(value));

    }, "Your password must contain 1 special character.");

    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("OneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");
 

    $("#msform").validate({ 
        rules: {
            email: "required",
            email: true,
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        onespecial: true,
                        OneUppercaseLetter: true
                    },
            fullName: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            phone: {
                required: true,
                number: true,
                minlength: 5,
                maxlength: 20
            },
            dob: {
                required: true,
            },
            gender: {
                required: true
            }
            ,
            addres: {
                required: true,
            },
            income1: {
                required: true,
            },
            income2: {
                required: true,
            },
            bankName: {
                required: true,
            },
             accNumber: {
                required: true,
				 number: false,
				  maxlength: 18
            },
//            emotion:{
//              required: true,   
//            }
             
             tinRadio:{
                   required: true,
             },
             ird:{
                   required: true,
             },
        },
        messages: {
            email: {
                required: "Enter your Email",
                email: "Please enter a valid email address.",
            },
            password: {
                required: "Enter your password",
                pass: "Password should be minimum of 8 and maximum of 15 characters."
            },
            fullName: {
                required: "Enter your full name",
            },
            phone: {
                required: "Enter your phone/mobile number",
                number: "Please enter numeric value with no spaces"
            },
            dob: {
                required: "Enter your date of birth",
            },
            gender: {
                required: "Please select gender",
            },
            addres: {
                required: "Please enter address",
            },
            income1: {
                required: "Please enter amount",
            },
            income2: {
                required: "Please enter  amount",
            },
 bankName: {
                required: "Please enter  Bank Name",
            },
             accNumber: {
                 required: "Please enter  Account Number.",
				 number: "Please enter numeric value with no spaces.",
				 maxlength:" maximum of 20 characters."
            },
//             emotion:{
//              required: "Please select atleast one option",   
//            }
        }
    });

}


function addComma(income1) {
    income1.value = income1.value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



