/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.mail;

import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.components.FileUtility;
import com.nz.cryptolabs.repositories.CommonRepository;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Palo Dev
 */
@Service
public class AdminMailManager {

    @Autowired
    private MailManager mailManager;
    @Autowired
    private FileUtility fileUtility;
    @Autowired
    private String adminEmailId;
    @Autowired
    private String domain;
    @Autowired
    private String mailFolder;
    @Autowired
    private CommonRepository repository;

    public void allocateEmail(SecuredUser admin, String name, String email, String inviteCode) {
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "allocateEmail.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var customerName", name);
        htmlMessage = htmlMessage.replace("var customerEmail", email);
        htmlMessage = htmlMessage.replace("var inviteCode", inviteCode);
        htmlMessage = htmlMessage.replace("var domain", domain);
        mailManager.sendMail(email, adminEmailId, "Welcome to Invsta", htmlMessage);
    }

    public void sendSoldCompletedEmail(SecuredUser admin, String name, String email, String total) {
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "sendSoldCompletedEmail.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var customerName", name);
        mailManager.sendMail(email, adminEmailId, "Sale completed", htmlMessage);
    }

    public void sendPurchasedSharesEmail(SecuredUser admin, String name, String email, TransactionBean tran) {
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "sendPurchasedSharesEmail.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var customerName", name);
        mailManager.sendMail(email, adminEmailId, "Purchase completed", htmlMessage);
    }

    public void approvedWalletTxn(TransactionBean bean) {
        UserInfo user = repository.findByUserId(bean.getUser_id());
        StringBuilder html = new StringBuilder();
        File file = fileUtility.getFile(fileUtility.mailTemplatesDirectoryPath(mailFolder), "approvedWalletTranNotification.html");
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                html.append(sc.nextLine());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SendGridMailManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String htmlMessage = html.toString();
        htmlMessage = htmlMessage.replace("var fullName", bean.getFull_name());
        htmlMessage = htmlMessage.replace("var customerName", user.getFullName());
        htmlMessage = htmlMessage.replace("var customerEmail", user.getEmail());
        htmlMessage = htmlMessage.replace("var customerId", user.getRefId());
        htmlMessage = htmlMessage.replace("var txnId", bean.getTxn_id());
        mailManager.sendMail(bean.getEmail_id(), adminEmailId, "Confirming your deposit", htmlMessage);
    }
}
